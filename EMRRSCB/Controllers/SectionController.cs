﻿using iHos.MVC.Property;
using Newtonsoft.Json;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class SectionController : Controller
    {
        // GET: Section
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public string GetData(string id)
        {
            try
            {
                var m = new LoginDataViewModel();
                using (var s = new SIM_Entities())
                {
                    var sec = s.SIMmSection.FirstOrDefault(x => x.SectionID == id);
                    var tipepelayanan = s.Pelayanan_TipePelayanan_Get_New.FirstOrDefault(x => x.TipePelayanan == sec.TipePelayanan);
                    var tipelogin = s.Pelayanan_Login().FirstOrDefault(x => x.SectionID == id);
                    m.LoginTipe = tipelogin.Tipe;
                    m.TipePelayanan = tipepelayanan.TipePelayanan;
                    m.TipeNamaPelayanan = tipepelayanan.NamaPelayanan;
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = m
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }   
    }
}