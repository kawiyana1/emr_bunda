﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace EMRRSCB.Models
{
    public class EMRHasilPembacaanElektrokardiografiViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Irama { get; set; }
        public string Frekuensi { get; set; }
        public string Gelombang { get; set; }
        public string IntervalPR { get; set; }
        public string Axis { get; set; }
        public string Durasi { get; set; }
        public string Amplitudo { get; set; }
        public string Morfologi { get; set; }
        public string MorfologiLainnya { get; set; }
        public string SegmenST { get; set; }
        public string GelombangT { get; set; }
        public string IntervalQT { get; set; }
        public string GelembungU { get; set; }
        public string GelembungUKet { get; set; }
        public string DokterPemeriksa { get; set; }
        public string DokterPemeriksaNama { get; set; }
        public string TTDDokterPemeriksa { get; set; }
        public Nullable<bool> Validasi { get; set; }
        public string Frekuensi_Ket { get; set; }
        public string Gelombang_Ket { get; set; }
        public string IntervalPR_Ket { get; set; }
        public string Durasi_Ket { get; set; }
        public string Morfologi_Ket { get; set; }
        public string Segmen_Ket { get; set; }
        public string GelombangT_Ket { get; set; }
        public string IntervalQT_Ket { get; set; }



        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string NamaSaya { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }

        public int MODEVIEW { get; set; }


    }
}