﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel
    {
        public ListDetail<EMRPenyakitDalam_KeperawatanDetailViewModel> Keperawatan_List { get; set; }
        public ListDetail<EMRPenyakitDalam_RencanaKerjaDetailViewModel> RencanaKerja_List { get; set; }

        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDatang { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPengkajian { get; set; }
        public string Rujukan { get; set; }
        public bool RujukanRS { get; set; }
        public string RujukanRSKet { get; set; }
        public bool RujukanPuskesmas { get; set; }
        public string RujukanPuskesmasKet { get; set; }
        public bool RujukanDR { get; set; }
        public string RujukanDRKet { get; set; }
        public bool RujukanLainnya { get; set; }
        public string RujukanLainnyaKet { get; set; }
        public string RujukanDX { get; set; }
        public bool RujukanDatangSendiri { get; set; }
        public bool RujukanDatangDiantar { get; set; }
        public string RujukanDatangDiantarKet { get; set; }
        public string SumberData { get; set; }
        public string SumberDataLainnya { get; set; }
        public string AlasanKunjungan { get; set; }
        public string PemeriksaanFisikTekananDarah { get; set; }
        public string PemeriksaanFisikRR { get; set; }
        public string PemeriksaanFisikNadi { get; set; }
        public string PemeriksaanFisikSuhu { get; set; }
        public string PemeriksaanFisikBB { get; set; }
        public string PemeriksaanFisikTB { get; set; }
        public string SkriningGiziApakahAsupanMengalami { get; set; }
        public string SkriningGiziApakahAsupanMengalamiKet { get; set; }
        public string SkriningGiziApakahAsupanMakan { get; set; }
        public string SkriningGizi1 { get; set; }
        public string SkriningGizi2 { get; set; }
        public string SkriningGiziTotalSkor { get; set; }
        public string SkriningGiziKriteriaGizi { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriPenilaianNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeriSkor { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPenyebab { get; set; }
        public string SkriningNyeriPenyebabKet { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public string AsesmenFungsional1Skor { get; set; }
        public string AsesmenFungsional1Keterangan { get; set; }
        public string AsesmenFungsional1NilaiSkor { get; set; }
        public string AsesmenFungsional2Skor { get; set; }
        public string AsesmenFungsional2Keterangan { get; set; }
        public string AsesmenFungsional2NilaiSkor { get; set; }
        public string AsesmenFungsional3Skor { get; set; }
        public string AsesmenFungsional3Keterangan { get; set; }
        public string AsesmenFungsional3NilaiSkor { get; set; }
        public string AsesmenFungsional4Skor { get; set; }
        public string AsesmenFungsional4Keterangan { get; set; }
        public string AsesmenFungsional4NilaiSkor { get; set; }
        public string AsesmenFungsional5Skor { get; set; }
        public string AsesmenFungsional5Keterangan { get; set; }
        public string AsesmenFungsional5NilaiSkor { get; set; }
        public string AsesmenFungsional6Skor { get; set; }
        public string AsesmenFungsional6Keterangan { get; set; }
        public string AsesmenFungsional6NilaiSkor { get; set; }
        public string AsesmenFungsional7Skor { get; set; }
        public string AsesmenFungsional7Keterangan { get; set; }
        public string AsesmenFungsional7NilaiSkor { get; set; }
        public string AsesmenFungsional8Skor { get; set; }
        public string AsesmenFungsional8Keterangan { get; set; }
        public string AsesmenFungsional8NilaiSkor { get; set; }
        public string AsesmenFungsional9Skor { get; set; }
        public string AsesmenFungsional9Keterangan { get; set; }
        public string AsesmenFungsional9NilaiSkor { get; set; }
        public string AsesmenFungsional10Skor { get; set; }
        public string AsesmenFungsional10Keterangan { get; set; }
        public string AsesmenFungsional10NilaiSkor { get; set; }
        public string AsesmenFungsionalTotalSkor { get; set; }
        public string AsesmenFungsionalKreteriaStatus { get; set; }
        public string AsesmenResikoRiwayatJatuh { get; set; }
        public string AsesmenResikoRiwayatJatuhSkala { get; set; }
        public string AsesmenResikoRiwayatJatuhSkor { get; set; }
        public string AsesmenResikoDiagnosisSekunder { get; set; }
        public string AsesmenResikoDiagnosisSekunderSkala { get; set; }
        public string AsesmenResikoDiagnosisSekunderSkor { get; set; }
        public string AsesmenResikoAlatBantu { get; set; }
        public string AsesmenResikoAlatBantuSkala { get; set; }
        public string AsesmenResikoAlatBantuSkor { get; set; }
        public string AsesmenResikoTerpasangInfus { get; set; }
        public string AsesmenResikoTerpasangInfusSkala { get; set; }
        public string AsesmenResikoTerpasangInfusSkor { get; set; }
        public string AsesmenResikoGayaBerjalan { get; set; }
        public string AsesmenResikoGayaBerjalanSkala { get; set; }
        public string AsesmenResikoGayaBerjalanSkor { get; set; }
        public string AsesmenResikoSiklusMental { get; set; }
        public string AsesmenResikoSiklusMentalSkala { get; set; }
        public string AsesmenResikoSiklusMentalSkor { get; set; }
        public string AsesmenResikoTotalSkor { get; set; }
        public string AsesmenResikoKategori { get; set; }
        public string BioPsikososialStatusPernikahan { get; set; }
        public string BioPsikososialAnak { get; set; }
        public string BioPsikososialAnakJumlahAnak { get; set; }
        public string BioPsikososialPekerjaan { get; set; }
        public string BioPsikososialTinggalBersama { get; set; }
        public string BioPsikososialTinggalBersamaLainnyaNama { get; set; }
        public string BioPsikososialTinggalBersamaLainnyaTelp { get; set; }
        public string BioPsikososialWarganegara { get; set; }
        public string BioPsikososialWarganegaraSuku { get; set; }
        public string BioPsikososialAgama { get; set; }
        public string BioPsikososialPercayaKeyakinan { get; set; }
        public string BioPsikososialKebiasaan { get; set; }
        public string BioPsikososialKebiasaanLainnya { get; set; }
        public string BioPsikososialResikoMencederai { get; set; }
        public string KebutuhanKomunikasiBicara { get; set; }
        public string KebutuhanKomunikasiBicaraKapan { get; set; }
        public bool KebutuhanKomunikasiBahasaIndonesia { get; set; }
        public string KebutuhanKomunikasiBahasaIndonesiaKet { get; set; }
        public bool KebutuhanKomunikasiBahasaInggris { get; set; }
        public string KebutuhanKomunikasiBahasaInggrisKet { get; set; }
        public bool KebutuhanKomunikasiBahasaDaerah { get; set; }
        public string KebutuhanKomunikasiBahasaDaerahKet { get; set; }
        public bool KebutuhanKomunikasiBahasaLainnya { get; set; }
        public string KebutuhanKomunikasiBahasaLainnyaKet { get; set; }
        public string KebutuhanKomunikasiPenerjemah { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasa { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasaIsyarat { get; set; }
        public bool KebutuhanKomunikasiHambatanBahasa { get; set; }
        public bool KebutuhanKomunikasiHambatanCemas { get; set; }
        public bool KebutuhanKomunikasiHambatanKognitif { get; set; }
        public bool KebutuhanKomunikasiHambatanEmosi { get; set; }
        public bool KebutuhanKomunikasiHambatanMenulis { get; set; }
        public bool KebutuhanKomunikasiHambatanPendengaran { get; set; }
        public bool KebutuhanKomunikasiHambatanKesulitanBicara { get; set; }
        public bool KebutuhanKomunikasiHambatanAudioVisual { get; set; }
        public bool KebutuhanKomunikasiHambatanHilangMemori { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakAdaPartisipasi { get; set; }
        public bool KebutuhanKomunikasiHambatanDiskusi { get; set; }
        public bool KebutuhanKomunikasiHambatanMotivasiBuruk { get; set; }
        public bool KebutuhanKomunikasiHambatanSecaraFisiologi { get; set; }
        public bool KebutuhanKomunikasiHambatanMembaca { get; set; }
        public bool KebutuhanKomunikasiHambatanMasalahPenglihatan { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakDitemukanHambatan { get; set; }
        public bool KebutuhanKomunikasiHambatanMendengar { get; set; }
        public bool KebutuhanKomunikasiHambatanDemontrasi { get; set; }
        public string KebutuhanKomunikasiTingkatkanPendidikan { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhan { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhanLainnya { get; set; }
        public bool KebutuhanPrivasiIdentitas { get; set; }
        public bool KebutuhanPrivasiRekamMedis { get; set; }
        public bool KebutuhanPrivasiSaatPemeriksaan { get; set; }
        public bool KebutuhanPrivasiSaatTindakanMedis { get; set; }
        public bool KebutuhanPrivasiTransportasi { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string AnamnesaPenyakitKeluarga { get; set; }
        public string AnamnesaPengobatan { get; set; }
        public string AnamnesaRiwayatOperasi { get; set; }
        public string AnamnesaRiwayatTranfusi { get; set; }
        public string TandaVitalKeadaanUmum { get; set; }
        public string TandaVitalGizi { get; set; }
        public string TandaVitalGCS_E { get; set; }
        public string TandaVitalGCS_V { get; set; }
        public string TandaVitalGCS_M { get; set; }
        public string TandaVitalTindakanResusitasi { get; set; }
        public string TandaVitalTindakanResusitasi_Ket { get; set; }
        public string TandaVitalTensi { get; set; }
        public string TandaVitalRR { get; set; }
        public string TandaVitalNadi { get; set; }
        public string TandaVitalSuhuAxilla { get; set; }
        public string TandaVitalSuhuRectal { get; set; }
        public string TandaVitalBB { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string PemeriksaanKepala { get; set; }
        public string PemeriksaanKepala_Ket { get; set; }
        public string PemeriksaanMata { get; set; }
        public string PemeriksaanMata_Ket { get; set; }
        public string PemeriksaanTHT { get; set; }
        public string PemeriksaanTHT_Ket { get; set; }
        public string PemeriksaanLeher { get; set; }
        public string PemeriksaanLeher_Ket { get; set; }
        public string PemeriksaanDada { get; set; }
        public string PemeriksaanDada_Ket { get; set; }
        public string PemeriksaanJantung { get; set; }
        public string PemeriksaanJantung_Ket { get; set; }
        public string PemeriksaanParu { get; set; }
        public string PemeriksaanParu_Ket { get; set; }
        public string PemeriksaanPerut { get; set; }
        public string PemeriksaanPerut_Ket { get; set; }
        public string PemeriksaanHeper { get; set; }
        public string PemeriksaanHeper_Ket { get; set; }
        public string PemeriksaanLien { get; set; }
        public string PemeriksaanLien_Ket { get; set; }
        public string PemeriksaanPunggung { get; set; }
        public string PemeriksaanPunggung_Ket { get; set; }
        public string PemeriksaanGenital { get; set; }
        public string PemeriksaanGenital_Ket { get; set; }
        public string PemeriksaanEktrimitas { get; set; }
        public string PemeriksaanEktrimitas_Ket { get; set; }
        public string PemeriksaanRectalToucher { get; set; }
        public string PemeriksaanRectalToucher_Ket { get; set; }
        public string PengkajianKebutuhanKhusus { get; set; }
        public string Laboratorium { get; set; }
        public string Xray { get; set; }
        public string EKG { get; set; }
        public string DiagnosaKerja { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string ICDX { get; set; }
        public string PengkajianKeperawatan { get; set; }
        public string Terapi { get; set; }
        public string DokterKonsul { get; set; }
        public string DokterKonsulNama { get; set; }
        public string Catatan { get; set; }
        public string Kontrol { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Kontrol_Tanggal { get; set; }
        public string Rujuk { get; set; }
        public string Rujuk_Ket { get; set; }
        public string RujukBalik { get; set; }
        public string RujukBalik_Ket { get; set; }
        public string Dirawat { get; set; }
        public string Dirawat_Section { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string JawabanKonsultasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JawabanKonsultasi_Jam { get; set; }
        public string JawabanKonsultasi_Kepada { get; set; }
        public string JawabanKonsultasi_KepadaNama { get; set; }
        public string JawabanKonsultasi_Keterangan { get; set; }
        public string JawabanKonsultasi_HormatKami { get; set; }
        public string JawabanKonsultasi_HormatKamiNama { get; set; }
        public string Konsultasi { get; set; }
        public string TTDDPJP { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Konsultasi_Jam { get; set; }
        public string Konsultasi_Kepada { get; set; }
        public string Konsultasi_KepadaNama { get; set; }
        public string Konsultasi_Keterangan { get; set; }
        public string Konsultasi_HormatKami { get; set; }
        public string Konsultasi_HormatKamiNama { get; set; }
        public bool VerifikasiDPJP { get; set; }
        public bool VerifikasiPerawat { get; set; }

        public int MODEVIEW { get; set; }
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitDalam> ListTemplate { get; set; }

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }

    }

    public class SelectItemListPenyakitDalam
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}