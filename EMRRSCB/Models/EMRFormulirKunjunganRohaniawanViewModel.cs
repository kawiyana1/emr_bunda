﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormulirKunjunganRohaniawanViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string HariTanggal { get; set; }
        public string Jam { get; set; }
        public string NamaRohaniawan { get; set; }
        public string JenisPelayanan { get; set; }
        public string TandaTangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}