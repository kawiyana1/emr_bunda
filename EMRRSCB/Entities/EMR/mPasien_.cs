//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class mPasien_
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public string TempatLahir { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string Pekerjaan { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public int JenisKerjasamaID { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public string NoKartu { get; set; }
        public bool PasienKTP { get; set; }
        public Nullable<int> AgamaID { get; set; }
        public bool PenanggungIsPasien { get; set; }
        public string PenanggungNRM { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungAlamat { get; set; }
        public string PenanggungPhone { get; set; }
        public string PenanggungHubungan { get; set; }
        public bool Aktif { get; set; }
    }
}
