﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRProseduralPasienRencanaOperasiKirimanDokterViewModel
    {
        public string NoBukti { get; set; }
        public string DokterPengirim { get; set; }
        public string RencanaJasaOperator { get; set; }
        public string RencanaJasaVisite { get; set; }
        public string RencanaAnastesi { get; set; }
        public string BiayaObat { get; set; }
        public string BiayaRumahSakitOk { get; set; }
        public string BiayaRumahSakiitAdministrasi { get; set; }
        public string BiayaRumahSakitInstrumen { get; set; }
        public string BiayaRumahSakitKamar { get; set; }
        public string BiayaRumahSakitDll { get; set; }
        public string Total { get; set; }
        public string PersetujuanKeluargaPasien { get; set; }
        public string PetugasPenerima { get; set; }
        public string TtdPetugas { get; set; }
        public string TtdKeluarga { get; set; }

        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public int? Umur { get; set; }
        public string Diagnosa { get; set; }

        public string JenisTindakan { get; set; }

        public int MODEVIEW { get; set; }




    }
}