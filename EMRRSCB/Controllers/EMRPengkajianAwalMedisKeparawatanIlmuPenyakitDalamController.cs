﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianAwalMedisKeparawatanIlmuPenyakitDalam.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel>(dokumen);
                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                        var konsuldokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterKonsul);
                        if (konsuldokter != null) model.DokterKonsulNama = konsuldokter.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_Kepada);
                        if (jawabankonsultasikpd != null) model.JawabanKonsultasi_KepadaNama = jawabankonsultasikpd.NamaDOkter;

                        var jawabankonsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_HormatKami);
                        if (jawabankonsultasihormatkami != null) model.JawabanKonsultasi_HormatKamiNama = jawabankonsultasihormatkami.NamaDOkter;

                        var konsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_Kepada);
                        if (konsultasikpd != null) model.Konsultasi_KepadaNama = konsultasikpd.NamaDOkter;

                        var konsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_HormatKami);
                        if (konsultasihormatkami != null) model.Konsultasi_HormatKamiNama = konsultasihormatkami.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                        model.Keperawatan_List = new ListDetail<EMRPenyakitDalam_KeperawatanDetailViewModel>();
                        var detail_1 = s.Keperawatan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPenyakitDalam_KeperawatanDetailViewModel>(x);
                            var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Pengkajian);
                            if (dokter_detail != null) y.PengkajianNama = dokter_detail.NamaDOkter;
                            model.Keperawatan_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPenyakitDalam_RencanaKerjaDetailViewModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPenyakitDalam_RencanaKerjaDetailViewModel>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListPenyakitDalam>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitDalam()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianAwalMedisKeparawatanIlmuPenyakitDalam.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel>(dokumen);
                        model.NoBukti = id;

                        var konsuldokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterKonsul);
                        if (konsuldokter != null) model.DokterKonsulNama = konsuldokter.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_Kepada);
                        if (jawabankonsultasikpd != null) model.JawabanKonsultasi_KepadaNama = jawabankonsultasikpd.NamaDOkter;

                        var jawabankonsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_HormatKami);
                        if (jawabankonsultasihormatkami != null) model.JawabanKonsultasi_HormatKamiNama = jawabankonsultasihormatkami.NamaDOkter;

                        var konsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_Kepada);
                        if (konsultasikpd != null) model.Konsultasi_KepadaNama = konsultasikpd.NamaDOkter;

                        var konsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_HormatKami);
                        if (konsultasihormatkami != null) model.Konsultasi_HormatKamiNama = konsultasihormatkami.NamaDOkter;


                        model.Keperawatan_List = new ListDetail<EMRPenyakitDalam_KeperawatanDetailViewModel>();
                        var detail_1 = s.Keperawatan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPenyakitDalam_KeperawatanDetailViewModel>(x);
                            var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Pengkajian);
                            if (dokter_detail != null) y.PengkajianNama = dokter_detail.NamaDOkter;
                            model.Keperawatan_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPenyakitDalam_RencanaKerjaDetailViewModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPenyakitDalam_RencanaKerjaDetailViewModel>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.VerifikasiDPJP = false;
                    model.VerifikasiPerawat = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListPenyakitDalam>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitDalam()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                var item = new EMRPengkajianAwalMedisKeparawatanIlmuPenyakitDalamViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.PengkajianAwalMedisKeparawatanIlmuPenyakitDalam.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";


                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianAwalMedisKeparawatanIlmuPenyakitDalam>(item);
                        s.PengkajianAwalMedisKeparawatanIlmuPenyakitDalam.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Pengkajian Penyakit Dalam";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianAwalMedisKeparawatanIlmuPenyakitDalam>(item);
                        s.PengkajianAwalMedisKeparawatanIlmuPenyakitDalam.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Pengkajian Penyakit Dalam";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJP;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    if (item.Keperawatan_List == null) item.Keperawatan_List = new ListDetail<EMRPenyakitDalam_KeperawatanDetailViewModel>();
                    item.Keperawatan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Keperawatan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Keperawatan_List;
                    var real_list = s.Keperawatan_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Keperawatan_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.Keperawatan_Detail.Add(IConverter.Cast<Keperawatan_Detail>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                        }
                    }

                    if (item.RencanaKerja_List == null) item.RencanaKerja_List = new ListDetail<EMRPenyakitDalam_RencanaKerjaDetailViewModel>();
                    item.RencanaKerja_List.RemoveAll(x => x.Remove);
                    foreach (var x2 in item.RencanaKerja_List)
                    {
                        x2.Model.NoBukti = item.NoBukti;
                    }
                    var new_list2 = item.RencanaKerja_List;
                    var real_list2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x2 in real_list2)
                    {
                        var m2 = new_list.FirstOrDefault(y => y.Model.No == x2.No);
                        if (m2 == null) s.RencanaKerja_Detail.Remove(x2);
                    }

                    foreach (var x2 in new_list2)
                    {
                        var _m2 = real_list.FirstOrDefault(y => y.No == x2.Model.No);
                        if (_m2 == null) s.RencanaKerja_Detail.Add(IConverter.Cast<RencanaKerja_Detail>(x2.Model));
                        else
                        {
                            _m2.NoBukti = x2.Model.NoBukti;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}