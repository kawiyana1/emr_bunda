﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPemberianInformasiHasilPelayananPengobatanViewModel
    {

        public string NoBukti { get; set; }
        public string NRM { get; set; }
        public string Dokter { get; set; }
        public string PemberianInformasi { get; set; }
        public string PenerimaInformasi { get; set; }
        public string No1 { get; set; }
        public bool No1Tanda { get; set; }
        public string No2 { get; set; }
        public bool No2Tanda { get; set; }
        public string No3 { get; set; }
        public bool No3Tanda { get; set; }
        public string No4 { get; set; }
        public bool No4Tanda { get; set; }
        public string No5 { get; set; }
        public bool No5Tanda { get; set; }
        public string No6 { get; set; }
        public bool No6Tanda { get; set; }
        public string No7 { get; set; }
        public bool No7Tanda { get; set; }
        public string No8 { get; set; }
        public bool No8Tanda { get; set; }
        public string TTD1 { get; set; }
        public string TTD2 { get; set; }

        public int  MODEVIEW { get; set; }
    }
}