//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_Registrasi____
    {
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NamaDokter { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Phone { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string StatusBayar { get; set; }
    }
}
