﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRKunjunganPoliklinikViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Poliklinik { get; set; }
        public string SOAP { get; set; }
        public string Intruksi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string TandaTangan { get; set; }
        public int MODEVIEW { get; set; }
    }
}