﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMEWSViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string KesadaranHasil { get; set; }
        public string KesadaranSkor { get; set; }
        public string PernapasanHasil { get; set; }
        public string PernapasanSkor { get; set; }
        public string SaturasiOksigenHasil_1 { get; set; }
        public string SaturasiOksigenHasil_2 { get; set; }
        public string SaturasiOksigenSkor { get; set; }
        public string TekananDarahHasil { get; set; }
        public string TekananDarahSkor { get; set; }
        public string HeartRateHasil { get; set; }
        public string HeartRateSkor { get; set; }
        public string SuhuAxillaHasil { get; set; }
        public string SuhuAxillaSkor { get; set; }
        public string TotalSkor { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public bool Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}