﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRAsuhanKeperawatanDewasaIGDController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new AsuhanKeperawatanDewasaIGDViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.AsuhanKeperawatanDewasaIGD.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<AsuhanKeperawatanDewasaIGDViewModel>(dokumen);
                        var pengkaji = sim.mDokter.FirstOrDefault(x => x.DokterID == model.PerawatPengkaji);
                        var perawat = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Perawat);
                        model.PerawatPengkaji_Nama = pengkaji == null ? "" : pengkaji.NamaDOkter;
                        model.Perawat_Nama = perawat == null ? "" : perawat.NamaDOkter;
                        model.MDOEVEIW = view;
                        model.Tanggal_View = dokumen.Tanggal;
                        model.Jam_View = dokumen.Tanggal;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal_View = DateTime.Now;
                        model.Jam_View = DateTime.Now;

                        var noreg = s.trDokumenAssesmenPasien.FirstOrDefault(e => e.NoBukti == id);
                        if (noreg != null)
                        {
                            var domucumen_igd = s.trDokumenAssesmenPasien.FirstOrDefault(e => e.NoReg == noreg.NoReg && e.DokumenID == "PengkajianMedisIGD" && e.Simpan == true);
                            if (domucumen_igd != null)
                            {
                                var medis_igd = s.PengkajianMedisGawatDarurat.FirstOrDefault(e => e.NoBukti == domucumen_igd.NoBukti);

                                model.KeluhanUtama = medis_igd.Anamnesis_KeluhanUtama;
                                model.RiwayatPenyakitSekarang = medis_igd.Anamnesis_RiwayatPenyakitSekarang;
                                model.RiwayatPenyakitDahulu = medis_igd.Anamnesis_RiwayatPenyakitDahulu;
                                model.RiwayatAlergi = medis_igd.Anamnesis_RiwayatAlergi;
                                model.Airway_Bebas = medis_igd.Airway_Bebas;
                                model.Airway_Gargling = medis_igd.Airway_Gargling;
                                model.Airway_Stridor = medis_igd.Airway_Stridor;
                                model.Airway_Wheezing = medis_igd.Airway_Wheezing;
                                model.Airway_Ronchi = medis_igd.Airway_Ronchi;
                                model.Airway_Terintubasi = medis_igd.Airway_Terintubasi;
                                model.Breathing_Spontan = medis_igd.Breathing_Spontan;
                                model.Breathing_Tachipneu = medis_igd.Breathing_Tachipneu;
                                model.Breathing_Dispneu = medis_igd.Breathing_Dispneu;
                                model.Breathing_Apneu = medis_igd.Breathing_Apneu;
                                model.Breathing_VentilasiMekanik = medis_igd.Breathing_VentilasiMekanik;
                                model.Breathing_MemakaiVentilator = medis_igd.Breathing_MemakaiVentilator;
                                model.Circulation_Nadi = medis_igd.Circulation_Nadi;
                                model.Circulation_CRT = medis_igd.Circulation_CRT;
                                model.Circulation_WarnaKulit = medis_igd.Circulation_WarnaKulit;
                                model.Circulation_Pendarahan = medis_igd.Circulation_Pendarahan;
                                model.Circulation_Kulit = medis_igd.Circulation_Kulit;
                                model.DisabilityNeurogicalRespon_Alert = medis_igd.DisabilityNeurogicalRespon_Alert;
                                model.DisabilityNeurogicalRespon_Pain = medis_igd.DisabilityNeurogicalRespon_Pain;
                                model.DisabilityNeurogicalRespon_Verbal = medis_igd.DisabilityNeurogicalRespon_Verbal;
                                model.DisabilityNeurogicalRespon_Unrespon = medis_igd.DisabilityNeurogicalRespon_Unrespon;
                                model.DisabilityNeurogicalPupil_Isokor = medis_igd.DisabilityNeurogicalPupil_Isokor;
                                model.DisabilityNeurogicalPupil_Anisokor = medis_igd.DisabilityNeurogicalPupil_Anisokor;
                                model.DisabilityNeurogicalPupil_Pinpoint = medis_igd.DisabilityNeurogicalPupil_Pinpoint;
                                model.DisabilityNeurogicalPupil_Midriasis = medis_igd.DisabilityNeurogicalPupil_Midriasis;
                                model.DisabilityNeurogicalReflek = medis_igd.DisabilityNeurogicalReflek;
                                model.GCS_E = medis_igd.StatusPresent_LevelKesadaranCGS_E;
                                model.GCS_V = medis_igd.StatusPresent_LevelKesadaranCGS_V;
                                model.GCS_M = medis_igd.StatusPresent_LevelKesadaranCGS_M;
                                model.StatusPresent_KeadaanUmum = medis_igd.KondisiKeluar_KeadaanUmum;
                                model.StatusPresent_LevelKesadaranCGS_E = medis_igd.StatusPresent_LevelKesadaranCGS_E;
                                model.StatusPresent_LevelKesadaranCGS_V = medis_igd.StatusPresent_LevelKesadaranCGS_V;
                                model.StatusPresent_LevelKesadaranCGS_M = medis_igd.StatusPresent_LevelKesadaranCGS_M;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_TD = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_TD;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_N = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_N;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_R = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_R;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_S = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_S;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_BB = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_BB;
                                model.StatusPresent_TandaTandaVitalDanKeadaanUmum_SpO2 = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_SpO2;
                                model.SkriningNyeri = medis_igd.SkriningNyeri;
                                model.PengkajianNyeri = medis_igd.PengkajianNyeri;
                                model.IntensitasNyeri = medis_igd.IntensitasNyeri;
                                model.OnsetNyeri = medis_igd.OnsetNyeri;
                                model.OnsetNyeriKronisSejakKapan = medis_igd.OnsetNyeriKronisSejakKapan;
                                model.Pencetus = medis_igd.Pencetus;
                                model.PencetusLainnya = medis_igd.PencetusLainnya;
                                model.KualitasNyeri_SepertiDitusuk = medis_igd.KualitasNyeri_SepertiDitusuk;
                                model.KualitasNyeri_SakitBerdenyut = medis_igd.KualitasNyeri_SakitBerdenyut;
                                model.KualitasNyeri_Lainnya = medis_igd.KualitasNyeri_Lainnya;
                                model.KualitasNyeri_LainnyaKeterangan = medis_igd.KualitasNyeri_LainnyaKeterangan;
                                model.RegioNyeriLokasi = medis_igd.RegioNyeriLokasi;
                                model.RegioNyeriMenjalar = medis_igd.RegioNyeriMenjalar;
                                model.RegioNyeriMenjalarTidakKeterangan = medis_igd.RegioNyeriMenjalarTidakKeterangan;
                                model.SkalaNyeri = medis_igd.SkalaNyeri;
                                model.NyeriDatangSaatIstirahat = medis_igd.NyeriDatangSaatIstirahat;
                                model.NyeriDatangSaatBeraktifitas = medis_igd.NyeriDatangSaatBeraktifitas;
                                model.NyeriDatangSaatLainnya = medis_igd.NyeriDatangSaatLainnya;
                                model.NyeriDatangSaatLainnyaKeterangan = medis_igd.NyeriDatangSaatLainnyaKeterangan;
                                model.NyeriMembaikBilaIstirahat = medis_igd.NyeriDatangSaatIstirahat;
                                model.NyeriMembaikBilaBeraktifitas = medis_igd.NyeriDatangSaatBeraktifitas;
                                model.NyeriMembaikBilaMendengarkanMusik = medis_igd.NyeriMembaikBilaMendengarkanMusik;
                                model.NyeriMembaikBilaMinumObat = medis_igd.NyeriMembaikBilaMinumObat;
                                model.NyeriMembaikBilaMinumKeterangan = "";
                                model.NyeriMembaikBilaBerubahPosisiAtauTidur = medis_igd.NyeriMembaikBilaBerubahPosisiAtauTidur;
                                model.NyeriMembaikBilaLainnya = medis_igd.NyeriMembaikBilaLainnya;
                                model.NyeriMembaikBilaLainnyaKeterangan = medis_igd.NyeriMembaikBilaLainnyaKeterangan;
                                model.FrekuensiNyeri = medis_igd.FrekuensiNyeri;
                                model.PenilaianNyeri = medis_igd.PenilaianNyeriSkor_Keterangan;
                                model.SkriningNutrisi_TotalSkor = medis_igd.PenilaianNyeriSkor_Keterangan;
                                model.IntensitasNyeri = medis_igd.IntensitasNyeri;
                            }
                        }
                    }

                    var ListEvaluasi = s.AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan.Where(x => x.NoBukti == id).ToList();
                    if (ListEvaluasi != null)
                    {
                        model.Evaluasi_List = new ListDetail<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembanganViewModel>();
                        foreach (var x in ListEvaluasi)
                        {
                            var y = IConverter.Cast<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembanganViewModel>(x);
                            var perawat = sim.mDokter.FirstOrDefault(d => d.DokterID == x.Dokter);
                            y.Dokter_Nama = perawat == null ? "" : perawat.NamaDOkter;
                            model.Evaluasi_List.Add(false, y);
                        }
                    }

                    var ListObservasi = s.AsuhanKeperawatanDewasaIGD_ObservasiCairan.Where(x => x.NoBukti == id).ToList();
                    if (ListObservasi != null)
                    {
                        model.Observasi_List = new ListDetail<AsuhanKeperawatanDewasaIGD_ObservasiCairanViewModel>();
                        foreach (var x in ListObservasi)
                        {
                            var y = IConverter.Cast<AsuhanKeperawatanDewasaIGD_ObservasiCairanViewModel>(x);
                            model.Observasi_List.Add(false, y);
                        }
                    }

                    var ListTindakan = s.AsuhanKeperawatanDewasaIGD_TindakanPerawatan.Where(x => x.NoBukti == id).ToList();
                    if (ListTindakan != null)
                    {
                        model.TindakanKeperawatan_List = new ListDetail<AsuhanKeperawatanDewasaIGD_TindakanPerawatanViewModel>();
                        foreach (var x in ListTindakan)
                        {
                            var y = IConverter.Cast<AsuhanKeperawatanDewasaIGD_TindakanPerawatanViewModel>(x);
                            var perawat = sim.mDokter.FirstOrDefault(d => d.DokterID == x.Petugas);
                            y.Petugas_Nama = perawat == null ? "" : perawat.NamaDOkter;
                            model.TindakanKeperawatan_List.Add(false, y);
                        }
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new AsuhanKeperawatanDewasaIGDViewModel();
                TryUpdateModel(item);
                ResultSS result;
                item.NyeriMembaikBilaMinumKeterangan = (item.NyeriMembaikBilaMinumKeterangan == null) ? "-" : item.NyeriMembaikBilaMinumKeterangan;
                using (var s = new EMREntities())
                {
                    item.Tanggal = item.Tanggal_View.Date + item.Jam_View.TimeOfDay;
                    var model = s.AsuhanKeperawatanDewasaIGD.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AsuhanKeperawatanDewasaIGD>(item);
                        s.AsuhanKeperawatanDewasaIGD.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Assesmen AsuhanKeperawatanDewasaIGD";
                    }
                    else
                    {
                        model = IConverter.Cast<AsuhanKeperawatanDewasaIGD>(item);
                        s.AsuhanKeperawatanDewasaIGD.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Assesmen AsuhanKeperawatanDewasaIGD";
                    }

                    #region ==== DETAIL 1

                    if (item.Evaluasi_List == null) item.Evaluasi_List = new ListDetail<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembanganViewModel>();
                    item.Evaluasi_List.RemoveAll(x => x.Remove);


                    foreach (var x in item.Evaluasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_ListEvaluasi = item.Evaluasi_List;
                    var real_ListEvaluasi = s.AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    // delete | delete where (real_list not_in new_list)
                    foreach (var x in real_ListEvaluasi)
                    {
                        var m = new_ListEvaluasi.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null)
                        {
                            s.AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan.Remove(x);
                        }

                    }

                    foreach (var x in new_ListEvaluasi)
                    {

                        var _m = real_ListEvaluasi.FirstOrDefault(y => y.No == x.Model.No);
                        new DataColumn("RiwayatKehamilan_TglPartus", typeof(DateTime));
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {

                            _m = new AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan();
                            s.AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan.Add(IConverter.Cast<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan>(x.Model);
                            s.AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan.AddOrUpdate(_m);
                        }
                    }

                    #endregion

                    #region ==== DETAIL 2

                    if (item.Observasi_List == null) item.Observasi_List = new ListDetail<AsuhanKeperawatanDewasaIGD_ObservasiCairanViewModel>();
                    item.Observasi_List.RemoveAll(x => x.Remove);


                    foreach (var x in item.Observasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_ListObservasi = item.Observasi_List;
                    var real_ListObservasi = s.AsuhanKeperawatanDewasaIGD_ObservasiCairan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    // delete | delete where (real_list not_in new_list)
                    foreach (var x in real_ListObservasi)
                    {
                        var m = new_ListObservasi.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null)
                        {
                            s.AsuhanKeperawatanDewasaIGD_ObservasiCairan.Remove(x);
                        }

                    }

                    foreach (var x in new_ListObservasi)
                    {

                        var _m = real_ListObservasi.FirstOrDefault(y => y.No == x.Model.No);
                        new DataColumn("RiwayatKehamilan_TglPartus", typeof(DateTime));
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {

                            _m = new AsuhanKeperawatanDewasaIGD_ObservasiCairan();
                            s.AsuhanKeperawatanDewasaIGD_ObservasiCairan.Add(IConverter.Cast<AsuhanKeperawatanDewasaIGD_ObservasiCairan>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<AsuhanKeperawatanDewasaIGD_ObservasiCairan>(x.Model);
                            s.AsuhanKeperawatanDewasaIGD_ObservasiCairan.AddOrUpdate(_m);
                        }
                    }

                    #endregion

                    #region ==== DETAIL 3

                    if (item.TindakanKeperawatan_List == null) item.TindakanKeperawatan_List = new ListDetail<AsuhanKeperawatanDewasaIGD_TindakanPerawatanViewModel>();
                    item.TindakanKeperawatan_List.RemoveAll(x => x.Remove);


                    foreach (var x in item.TindakanKeperawatan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_ListTindakan = item.TindakanKeperawatan_List;
                    var real_ListTindakan = s.AsuhanKeperawatanDewasaIGD_TindakanPerawatan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    // delete | delete where (real_list not_in new_list)
                    foreach (var x in real_ListTindakan)
                    {
                        var m = new_ListTindakan.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null)
                        {
                            s.AsuhanKeperawatanDewasaIGD_TindakanPerawatan.Remove(x);
                        }

                    }

                    foreach (var x in new_ListTindakan)
                    {

                        var _m = real_ListTindakan.FirstOrDefault(y => y.No == x.Model.No);
                        new DataColumn("RiwayatKehamilan_TglPartus", typeof(DateTime));
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {

                            _m = new AsuhanKeperawatanDewasaIGD_TindakanPerawatan();
                            s.AsuhanKeperawatanDewasaIGD_TindakanPerawatan.Add(IConverter.Cast<AsuhanKeperawatanDewasaIGD_TindakanPerawatan>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<AsuhanKeperawatanDewasaIGD_TindakanPerawatan>(x.Model);
                            s.AsuhanKeperawatanDewasaIGD_TindakanPerawatan.AddOrUpdate(_m);
                        }
                    }

                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}