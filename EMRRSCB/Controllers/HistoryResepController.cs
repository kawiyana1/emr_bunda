﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class HistoryResepController : Controller
    {
        // GET: HistoryResep
        #region ===== T A B L E  H I S T O R Y

        [HttpPost]
        public string ListHistory(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var nrm = filter[1];
                        IQueryable<EMR_FN_GetHistoryResep_Result> p = sim.EMR_FN_GetHistoryResep().Where(x => x.NRM == nrm && x.JenisResep == "ResepNonRacik");
                        p = p.Where($"NoRegistrasi.Contains(@0)", filter[0]);
                        p = p.Where($"NRM.Contains(@0)", filter[1]);
                        p = p.Where($"NoResep.Contains(@0)", filter[4]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<HistoryResepViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<HistoryResepViewModel>(x);
                            m.Tanggal_View = x.Tanggal.ToString("yyyy/MM/dd");
                          
                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E  H I S T O R Y  N O N  R E S E P

        [HttpPost]
        public string ListHistoryNonResep(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var nrm = filter[1];
                        IQueryable<EMR_FN_GetHistoryResep_Result> p = sim.EMR_FN_GetHistoryResep().Where(x => x.NRM == nrm && x.JenisResep == "ResepRacik");
                        p = p.Where($"NoRegistrasi.Contains(@0)", filter[0]);
                        p = p.Where($"NRM.Contains(@0)", filter[1]);
                        p = p.Where($"NoResep.Contains(@0)", filter[4]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<HistoryResepViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<HistoryResepViewModel>(x);
                            m.Tanggal_View = x.Tanggal.ToString("yyyy/MM/dd");

                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}