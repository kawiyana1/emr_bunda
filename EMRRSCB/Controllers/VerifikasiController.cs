﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class VerifikasiController : Controller
    {
        // GET: Verifikasi
        public string CekVerifikasiUser(string dokter)
        {
            try
            {
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var dt = sim.mSupplier.FirstOrDefault(x => x.Kode_Supplier == dokter);
                        var userlogin = User.Identity.GetUserId();
                        if (dt.TTD == null) {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Tanda tangan masih kosong"

                            });
                        }
                        if (dt.UserIdWeb == userlogin)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = true,
                                Message = ""

                            });
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = false,
                    Data = "",
                    Message = "User login tidak sesuai"

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}