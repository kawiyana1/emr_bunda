﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRAsuhanKeperawatanAnakGawatDaruratController : Controller
    {
        // GET: EMRAsuhanKeperawatanAnakGawatDarurat
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRAsuhanKeperawatanAnakGawatDaruratViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.AsuhanKeperawatanAnakGawatDarurat.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAsuhanKeperawatanAnakGawatDaruratViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (dokter != null) model.PerawatNama = dokter.NamaDOkter;

                        var dokter2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PerawatPengkaji);
                        if (dokter2 != null) model.PerawatPengkajiNama = dokter2.NamaDOkter;

                        #region === Detail Observasi
                        model.Observasi_List = new ListDetail<EMRAsuhanKeperawatanAnak_ObservasiModelDetail>();
                        var detail_1 = s.AsuhanKeperawatanAnakGawatDarurat_ObservasiCairan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRAsuhanKeperawatanAnak_ObservasiModelDetail>(x);
                            model.Observasi_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Evaluasi
                        model.Evaluasi_List = new ListDetail<EMRAsuhanKeperawatanAnak_EvaluasiPerkembanganPasienModelDetail>();
                        var detail_2 = s.AsuhanKeperawatanAnakGawatDarurat_EvaluasiPerkembangan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRAsuhanKeperawatanAnak_EvaluasiPerkembanganPasienModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Evaluasi_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Tindakan

                        model.Tindakan_List = new ListDetail<EMRAsuhanKeperawatanAnakGawatDarurat_TindakanPerawatanModelDetail>();
                        var detail_3 = s.AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRAsuhanKeperawatanAnakGawatDarurat_TindakanPerawatanModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Petugas);
                            if (dokterdetail != null)
                            {
                                y.PetugasNama = dokterdetail.NamaDOkter;
                            }
                            model.Tindakan_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.PemindahanRuanganMeninggalJam = DateTime.Now;
                        model.PemindahanRuanganMinggatKet = DateTime.Now;

                        // auto fill
                        // get in pengkajian medis gawat darurat
                        var noreg = s.trDokumenAssesmenPasien.FirstOrDefault(e => e.NoBukti == id);
                        if (noreg != null)
                        {
                            var domucumen_igd = s.trDokumenAssesmenPasien.FirstOrDefault(e => e.NoReg == noreg.NoReg && e.DokumenID == "PengkajianMedisIGD" && e.Simpan == true);
                            if (domucumen_igd != null)
                            {
                                var medis_igd = s.PengkajianMedisGawatDarurat.FirstOrDefault(e => e.NoBukti == domucumen_igd.NoBukti);
                                model.Tanggal = medis_igd.TangalKedatangan;
                                model.KeluhanUtama = medis_igd.Anamnesis_KeluhanUtama;
                                model.RiwayatPenyakitSekarang = medis_igd.Anamnesis_RiwayatPenyakitSekarang;
                                model.RiwayatPenyakitDahulu = medis_igd.Anamnesis_RiwayatPenyakitDahulu;
                                model.RiwayatAlergi = medis_igd.Anamnesis_RiwayatAlergi;
                                model.AirwayBebas = medis_igd.Airway_Bebas;
                                model.AirwayGargling = medis_igd.Airway_Gargling;
                                model.AirwayStridor = medis_igd.Airway_Stridor;
                                model.AirwayWheezing = medis_igd.Airway_Wheezing;
                                model.AirwayRonchi = medis_igd.Airway_Ronchi;
                                model.AirwayTerintubasi = medis_igd.Airway_Terintubasi;
                                model.BreathingSpontan = medis_igd.Breathing_Spontan;
                                model.BreathingTachipneu = medis_igd.Breathing_Tachipneu;
                                model.BreathingDispneu = medis_igd.Breathing_Dispneu;
                                model.BreathingApneu = medis_igd.Breathing_Apneu;
                                model.BreathingVentilasiMekanik = medis_igd.Breathing_VentilasiMekanik;
                                model.CirculationNadi = medis_igd.Circulation_Nadi;
                                model.CirculationCRT = medis_igd.Circulation_CRT;
                                model.CirculationWarnaKulit = medis_igd.Circulation_WarnaKulit;
                                model.CirculationPendarahan = medis_igd.Circulation_Pendarahan;
                                model.CirculationTurgoKulit = medis_igd.Circulation_Kulit;
                                model.DisbilityResponAlert = medis_igd.DisabilityNeurogicalRespon_Alert;
                                model.DisbilityResponPain = medis_igd.DisabilityNeurogicalRespon_Pain;
                                model.DisbilityResponVerbal = medis_igd.DisabilityNeurogicalRespon_Verbal;
                                model.DisbilityResponUnrespon = medis_igd.DisabilityNeurogicalRespon_Unrespon;
                                model.DisbilityPupilIsokor = medis_igd.DisabilityNeurogicalPupil_Isokor;
                                model.DisbilityPupilAnisokor = medis_igd.DisabilityNeurogicalPupil_Anisokor;
                                model.DisbilityPupiPinPoint = medis_igd.DisabilityNeurogicalPupil_Pinpoint;
                                model.DisbilityPupiPinMidriasis = false;
                                model.DisbilityReflek = medis_igd.DisabilityNeurogicalReflek;
                                model.DisbilityGCSE = medis_igd.StatusPresent_LevelKesadaranCGS_E;
                                model.DisbilityGCSV = medis_igd.StatusPresent_LevelKesadaranCGS_V;
                                model.DisbilityGCSM = medis_igd.StatusPresent_LevelKesadaranCGS_M;
                                model.Rujukan = medis_igd.Rujukan;
                                model.RujukanDari = medis_igd.Rujukan + "  " + medis_igd.RujukanYaDrKeterangan + "  " + medis_igd.RujukanYaPuskesmasKeterangan + "  " + medis_igd.RujukanYaLainnyaKeterangan;
                                model.CirculationCRT = medis_igd.Circulation_CRT;
                                model.CirculationPendarahan = medis_igd.Circulation_Pendarahan;
                                model.Suhu = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_S;
                                model.BB = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_BB;
                                model.Nadi = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_N;
                                model.Respirasi = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_R;
                                model.SPO2 = medis_igd.StatusPresent_TandaTandaVitalDanKeadaanUmum_SpO2;
                                model.SkriningNyeri = medis_igd.SkriningNyeri;
                                model.SkriningPencelus = medis_igd.Pencetus;
                                model.SkriningPencelusKet = medis_igd.PencetusLainnya;
                                model.KualitasNyeri_Lainnya = medis_igd.KualitasNyeri_Lainnya;
                                model.KualitasNyeri_SakitBerdenyut = medis_igd.KualitasNyeri_SakitBerdenyut;
                                model.KualitasNyeri_SepertiDitusuk = medis_igd.KualitasNyeri_SepertiDitusuk;
                                model.SkriningNyeriKualitasLainnya = medis_igd.KualitasNyeri_LainnyaKeterangan;
                                model.SkriningLokasiNyeri = medis_igd.RegioNyeriLokasi;
                                model.SkriningIntensitasNyeri = medis_igd.IntensitasNyeri;
                                model.SkriningNyeriMenjalar = medis_igd.RegioNyeriMenjalar;
                                model.SkriningNyeriMenjalarKet = medis_igd.RegioNyeriMenjalarTidakKeterangan;
                                model.SkriningSkalaNyeri = medis_igd.SkalaNyeri;
                                model.SkriningNyeriDatangIstirahat = medis_igd.NyeriDatangSaatIstirahat;
                                model.SkriningNyeriDatangBeraktifitas = medis_igd.NyeriDatangSaatBeraktifitas;
                                model.SkriningNyeriDatangLainnya = medis_igd.NyeriDatangSaatLainnya;
                                model.SkriningNyeriDatangSaatLainnya = medis_igd.NyeriDatangSaatLainnyaKeterangan;
                                model.SkriningNyeriMembaikBilaIstirahat = medis_igd.NyeriMembaikBilaIstirahat;
                                model.SkriningNyeriMembaikBilaMendengarBerubahPosisiTidur = medis_igd.NyeriMembaikBilaBerubahPosisiAtauTidur;
                                model.SkriningNyeriMembaikBilaMendengarLainnya = medis_igd.NyeriMembaikBilaMendengarkanMusik;
                                model.SkriningNyeriMembaikBilaMendengarLainnyaSebutkan = "";
                                model.SkriningNyeriMembaikBilaMendengarMinumObat = medis_igd.NyeriMembaikBilaMinumObat;
                                model.SkriningNyeriMembaikBilaMendengarMinumObatKet = medis_igd.NyeriMembaikBilaMinumKeterangan;
                                model.SkriningNyeriMembaikBilaMendengarMusik = medis_igd.NyeriMembaikBilaMendengarkanMusik;
                            }

                        }
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRAsuhanKeperawatanAnakGawatDaruratViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.AsuhanKeperawatanAnakGawatDarurat.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AsuhanKeperawatanAnakGawatDarurat>(item);
                        s.AsuhanKeperawatanAnakGawatDarurat.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Asuhan Keperawatan Anak Gawat Darurat";
                    }
                    else
                    {
                        model = IConverter.Cast<AsuhanKeperawatanAnakGawatDarurat>(item);
                        s.AsuhanKeperawatanAnakGawatDarurat.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Asuhan Keperawatan Anak Gawat Darurat";
                    }
                    #region === Detail Observasi
                    if (item.Observasi_List == null) item.Observasi_List = new ListDetail<EMRAsuhanKeperawatanAnak_ObservasiModelDetail>();
                    item.Observasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Observasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Observasi_List;
                    var real_list = s.AsuhanKeperawatanAnakGawatDarurat_ObservasiCairan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsuhanKeperawatanAnakGawatDarurat_ObservasiCairan.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AsuhanKeperawatanAnakGawatDarurat_ObservasiCairan.Add(IConverter.Cast<AsuhanKeperawatanAnakGawatDarurat_ObservasiCairan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.JenisCairan = x.Model.JenisCairan;
                            _m.NoBotol = x.Model.NoBotol;
                            _m.IV = x.Model.IV;
                            _m.Oral = x.Model.Oral;
                            _m.Drain = x.Model.Drain;
                            _m.NGT = x.Model.NGT;
                            _m.Urin = x.Model.Urin;
                            _m.BAB = x.Model.BAB;
                        }
                    }
                    #endregion

                    #region === Detail Evaluasi
                    if (item.Evaluasi_List == null) item.Evaluasi_List = new ListDetail<EMRAsuhanKeperawatanAnak_EvaluasiPerkembanganPasienModelDetail>();
                    item.Evaluasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Evaluasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Evaluasi_List;
                    var real_list1 = s.AsuhanKeperawatanAnakGawatDarurat_EvaluasiPerkembangan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsuhanKeperawatanAnakGawatDarurat_EvaluasiPerkembangan.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AsuhanKeperawatanAnakGawatDarurat_EvaluasiPerkembangan.Add(IConverter.Cast<AsuhanKeperawatanAnakGawatDarurat_EvaluasiPerkembangan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.SOAP = x.Model.SOAP;
                            _m.Dokter = x.Model.Dokter;
                        }
                    }
                    #endregion

                    #region === Detail Tindakan
                    if (item.Tindakan_List == null) item.Tindakan_List = new ListDetail<EMRAsuhanKeperawatanAnakGawatDarurat_TindakanPerawatanModelDetail>();
                    item.Tindakan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Tindakan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list2 = item.Tindakan_List;
                    var real_list2 = s.AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan.Remove(x);
                    }

                    foreach (var x in new_list2)
                    {
                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan.Add(IConverter.Cast<AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan>(x.Model));

                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Evaluasi = x.Model.Evaluasi;
                            _m.TindakanPerawatan = x.Model.TindakanPerawatan;
                            _m.Petugas = x.Model.Petugas;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}