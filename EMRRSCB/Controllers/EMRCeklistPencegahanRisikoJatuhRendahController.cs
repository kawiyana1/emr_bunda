﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCeklistPencegahanRisikoJatuhRendahController : Controller
    {
        // GET: EMRCeklistPencegahanRisikoJatuhRendah
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRCeklistPencegahanRisikoJatuhRendahViewModel(); 
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);

                    model.NoBukti = id;
                    model.Nama = identitas.NamaPasien;
                    model.tglLahir = (identitas.TglLahir.Value.ToString("yyyy-MM-dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy-MM-dd"));
                    model.NRM = identitas.NRM;
                    model.MODEVIEW = view;
                    model.Tanggal = DateTime.Today;
                    model.Jam = DateTime.Now;
                    ViewBag.NoBukti = id;
                }

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        public string ListCeklistPencegahanRisikoJatuhRendah(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var nobukti = filter[1];
                        IQueryable<CeklistPencegahanRisikoJatuhRendah> p = s.CeklistPencegahanRisikoJatuhRendah.Where(x => x.NoBukti == nobukti);
                        //p = p.Where($"NoRegistrasi.Contains(@0)", filter[2]);
                        //p = p.Where($"NRM.Contains(@0)", filter[1]);
                        //p = p.Where($"NoResep.Contains(@0)", filter[4]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<EMRCeklistPencegahanRisikoJatuhRendahViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<EMRCeklistPencegahanRisikoJatuhRendahViewModel>(x);
                            m.Tanggal_View = x.Tanggal.Value.ToString("yyyy/MM/dd");

                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string editCPRJT(string id)
        {
            try
            {
                var model = new EMRCeklistPencegahanRisikoJatuhRendahViewModel();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        int idnya = int.Parse(id);
                        var dokumen = s.CeklistPencegahanRisikoJatuhRendah.FirstOrDefault(x => x.No == idnya);
                        var getnrm = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == dokumen.NoBukti);
                        var getpasien = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == getnrm.NRM);
                        model = IConverter.Cast<EMRCeklistPencegahanRisikoJatuhRendahViewModel>(dokumen);
                        model.Tanggal_View = model.Tanggal.Value.ToString("yyyy-MM-dd");
                        model.Jam_View = model.Jam.Value.ToString("HH:mm");
                        model.Pasien = (getpasien == null ? "" : getpasien.NamaPasien);
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Perawat);
                        if (dokter != null)
                        {
                            model.PerawatNama = dokter.NamaDOkter;
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("SecondCreate")]
        public string SecondCreate(EMRCeklistPencegahanRisikoJatuhRendahViewModel item, string img, string imgttd)
        {
            try
            {
                ResultSS result;
                using (var s = new EMREntities())
                {
                    //if (item.DPJP == null) return JsonHelper.JsonMsgError("Dokter DPJP tidak boleh kosong.");
                    //if (item.DPJP == null) throw new Exception("Dokter tidak boleh kosong.");

                    var model = s.CeklistPencegahanRisikoJatuhRendah.FirstOrDefault(x => x.NoBukti == item.NoBukti);

                    var activity = "";
                    var dokumenid = "";
                    if (item._METHOD == "CREATE")
                    {

                        var o = IConverter.Cast<CeklistPencegahanRisikoJatuhRendah>(item);
                        o.NoBukti = item.NoBukti;
                        //o.TTDDPJP = Convert.FromBase64String(imgttd);
                        s.CeklistPencegahanRisikoJatuhRendah.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Create Assesmen Ceklist Pencegahan Risiko Jatuh Rendah";

                        //if (item.templateName != null)
                        //{
                        //    var temp = new trDokumenTemplate();
                        //    temp.NoBukti = nobukti;
                        //    temp.NamaTemplate = item.templateName;
                        //    temp.DokterID = item.DPJP;
                        //    temp.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        //    temp.DokumenID = "CPPT";
                        //    s.trDokumenTemplate.Add(temp);
                        //}
                    }
                    else if (item._METHOD == "UPDATE")
                    {
                        model = IConverter.Cast<CeklistPencegahanRisikoJatuhRendah>(item);
                        model.No = item.No;
                        model.NoBukti = item.NoBukti;
                        s.CeklistPencegahanRisikoJatuhRendah.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Assesmen Ceklist Pencegahan Risiko Jatuh Rendah";

                        //if (item.templateName != null)
                        //{
                        //    var check_temp = s.trDokumenTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                        //    if (check_temp == null)
                        //    {
                        //        var temp = new trDokumenTemplate();
                        //        temp.NoBukti = item.NoBukti;
                        //        temp.NamaTemplate = item.templateName;
                        //        temp.DokterID = item.DPJP;
                        //        temp.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        //        temp.DokumenID = "CPPT";
                        //        s.trDokumenTemplate.Add(temp);
                        //    }
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var m = s.CeklistPencegahanRisikoJatuhRendah.FirstOrDefault(x => x.No == id);
                    s.CeklistPencegahanRisikoJatuhRendah.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}