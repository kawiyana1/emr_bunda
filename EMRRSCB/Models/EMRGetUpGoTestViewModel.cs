﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EMRRSCB.Models;

namespace EMRRSCB.Models
{
    public class EMRGetUpGoTestViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosa { get; set; }
        public string Pengkajian_A1 { get; set; }
        public string Pengkajian_A2 { get; set; }
        public string Pengkajian_B { get; set; }
        public string Hasil { get; set; }
        public string Hasil_Ket { get; set; }
        public string Tindakan { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public bool Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}