﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string CatatanPerkembanganPasien { get; set; }
        public string Intruksi { get; set; }
        public string Dokter { get; set; }
        public string Username { get; set; }
        public string DokterNama { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }

    }
}