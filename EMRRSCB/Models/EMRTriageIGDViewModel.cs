﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRTriageIGDViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TangggalKunjungan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKunjungan { get; set; }
        public string Anamnesa { get; set; }
        public string Instruksi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string TandaTangan { get; set; }
    }
}