﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormEdukasiTindakanAnestesiDanSedasiController : Controller
    {
        // GET: EMRFormEdukasiTindakanAnestesiDanSedasi
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRFormEdukasiTindakanAnestesiDanSedasiViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.FormEdukasiTindakanAnestesiDanSedasi.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormEdukasiTindakanAnestesiDanSedasiViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (dokter != null) model.DokterNama = dokter.NamaDOkter;

                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.NRM = (nrm == null ? "" : nrm);
                        model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                        model.Username = User.Identity.GetUserName();
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.NRM = (nrm == null ? "" : nrm);
                        model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));

                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRFormEdukasiTindakanAnestesiDanSedasiViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.FormEdukasiTindakanAnestesiDanSedasi.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormEdukasiTindakanAnestesiDanSedasi>(item);
                        s.FormEdukasiTindakanAnestesiDanSedasi.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create From edukasi tindakan";
                    }
                    else
                    {
                        model = IConverter.Cast<FormEdukasiTindakanAnestesiDanSedasi>(item);
                        s.FormEdukasiTindakanAnestesiDanSedasi.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update From edukasi tindakan";
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}