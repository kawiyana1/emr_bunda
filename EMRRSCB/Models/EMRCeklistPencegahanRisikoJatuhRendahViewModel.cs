﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCeklistPencegahanRisikoJatuhRendahViewModel
    {
        public int No { get; set; }
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosa { get; set; }
        public string No1 { get; set; }
        public string No2 { get; set; }
        public string No3 { get; set; }
        public string No4 { get; set; }
        public string No5 { get; set; }
        public string No6 { get; set; }
        public string No7 { get; set; }
        public string No8 { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Pasien { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDPasien { get; set; }
        public bool VerifikasiPerawat { get; set; }

        public string Nama { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public string tglLahir { get; set; }

        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public string _METHOD { get; set; } 

        public int MODEVIEW { get; set; }
    }
}