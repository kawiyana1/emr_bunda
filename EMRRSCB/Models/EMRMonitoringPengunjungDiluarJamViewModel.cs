﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringPengunjungDiluarJamViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Tanggal { get; set; }
        public string NamaPengunjung { get; set; }
        public string NoIdentitas { get; set; }
        public string NamaPasien { get; set; }
        public string KelasKeperawatan { get; set; }
        public string TglPenerimaan { get; set; }
        public string TglPengembalian { get; set; }
        public string TandaTangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}