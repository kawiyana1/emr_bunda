﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratBuktiTindakanViewModel
    {
        public string NoBukti { get; set; }
        public string Tindakan { get; set; }
        public string PerjalananPenyakit { get; set; }
        public string Dokter { get; set; }
        public string Dokter_Nama { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Umur { get; set; }
        public string Alamat { get; set; }
        public string TTDDokter { get; set; }
        public int MODEVIEW { get; set; }

    }
}