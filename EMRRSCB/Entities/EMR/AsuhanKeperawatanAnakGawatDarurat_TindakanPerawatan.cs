//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class AsuhanKeperawatanAnakGawatDarurat_TindakanPerawatan
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string TindakanPerawatan { get; set; }
        public string Evaluasi { get; set; }
        public string Petugas { get; set; }
        public string Username { get; set; }
    }
}
