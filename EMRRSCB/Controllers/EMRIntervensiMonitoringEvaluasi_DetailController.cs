﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRIntervensiMonitoringEvaluasi_DetailController : Controller
    {
        // GET: EMRIntervensiMonitoringEvaluasi_Detail
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRIntervensiMonitoringEvaluasiViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.IntervensiMonitoringEvaluasi.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIntervensiMonitoringEvaluasiViewDetail>(dokumen);

                        model.NoBukti = id;

                        var paraf = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Paraf);
                        if (paraf != null) model.ParafNama = paraf.NamaDOkter;

                        #region === Intervensi Detail
                        model.EMRIntervensi_List = new ListDetail<EMRIntervensiMonitoringEvaluasi_DetailViewModel>();
                        var detail_2 = s.IntervensiMonitoringEvaluasi_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRIntervensiMonitoringEvaluasi_DetailViewModel>(x);
                            var parafdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Paraf);
                            if (parafdetail != null)
                            {
                                y.ParafNama = parafdetail.NamaDOkter;
                            }
                            model.EMRIntervensi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRIntervensiMonitoringEvaluasiViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.IntervensiMonitoringEvaluasi.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<IntervensiMonitoringEvaluasi>(item);
                        s.IntervensiMonitoringEvaluasi.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Intervensi Monitoring";
                    }
                    else
                    {
                        model = IConverter.Cast<IntervensiMonitoringEvaluasi>(item);
                        s.IntervensiMonitoringEvaluasi.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Intervensi Monitoring";
                    }

                    #region === Detail Intervensi
                    if (item.EMRIntervensi_List == null) item.EMRIntervensi_List = new ListDetail<EMRIntervensiMonitoringEvaluasi_DetailViewModel>();
                    item.EMRIntervensi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRIntervensi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.EMRIntervensi_List;
                    var real_list1 = s.IntervensiMonitoringEvaluasi_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.IntervensiMonitoringEvaluasi_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.IntervensiMonitoringEvaluasi_Detail.Add(IConverter.Cast<IntervensiMonitoringEvaluasi_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.DietTetap = x.Model.DietTetap;
                            _m.PerubahanDiet = x.Model.PerubahanDiet;
                            _m.PerubahanDiet_Ket = x.Model.PerubahanDiet_Ket;
                            _m.E = x.Model.E;
                            _m.P = x.Model.P;
                            _m.AsupanMakanan = x.Model.AsupanMakanan;
                            _m.BB = x.Model.BB;
                            _m.Keterangan = x.Model.Keterangan;
                            _m.Paraf = x.Model.Paraf;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}