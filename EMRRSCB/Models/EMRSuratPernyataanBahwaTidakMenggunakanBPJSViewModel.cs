﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPernyataanBahwaTidakMenggunakanBPJSViewModel
    {
        public string NoBukti { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string BertindakSelaku { get; set; }
        public string BertindakSelakuText { get; set; }
        public string AtasPenderita { get; set; }
        public string TdkMenggunakanPilih { get; set; }
        public string TdkMenggunakanPilih1 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglSurat { get; set; }
        public string PihakRumahSakit { get; set; }
        public string YangMenyatakan { get; set; }
        public string SaksiKeluarga { get; set; }

        public string TTD_RUMAHSAKIT { get; set; }
        public string TTD_YANGMENYATAKAN { get; set; }
        public string TTD_SAKSIKELUARGA { get; set; }

        public int MODEVIEW { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }        
        public string JenisKelamin { get; set; }
        public string AlamatPasien { get; set; }
    }
}