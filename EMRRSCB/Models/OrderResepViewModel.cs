﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class OrderResepViewModel
    {
        public ListDetail<EMRRacikanObatDetailModel> EMRRacikanObat_List { get; set; }
        public int Nomor { get; set; }
        public string NoResep { get; set; }
        public string NoRegistrasi { get; set; } 
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string SectionID { get; set; }
        [Required]
        public string Farmasi_SectionID { get; set; }
        public string NamaFarmasi { get; set; }
        [Required]
        public string DokterID { get; set; }
        public string Dokter { get; set; }
        public decimal Jumlah { get; set; }
        public string Jumlah_View { get; set; }
        public bool Cyto { get; set; }
        public bool Realisasi { get; set; }
        public bool Batal { get; set; }
        public Nullable<int> NoAntri { get; set; }
        public string Keterangan { get; set; }
        public Nullable<bool> Puyer { get; set; }
        public int QtyPuyer { get; set; }
        public string SatuanPuyer { get; set; }
        public Nullable<double> BeratBadan { get; set; }
        public decimal KomisiDokter { get; set; }
        public string NoBukti { get; set; }
        public short User_ID { get; set; }
        public string JenisKerjasamaID { get; set; }
        public string CompanyID { get; set; }
        public string NRM { get; set; }
        public string NoKartu { get; set; }
        public string KelasID { get; set; }
        public bool KTP { get; set; }
        public string KerjasamaID { get; set; }
        public string PerusahaanID { get; set; }
        public bool RawatInap { get; set; }
        public Nullable<bool> Paket { get; set; }
        public string KodePaket { get; set; }
        public string NamaPaket { get; set; }
        public Nullable<bool> AmprahanRutin { get; set; }
        public Nullable<bool> IncludeJasa { get; set; }
        public string UserNameInput { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public Nullable<bool> RasioObat_ada { get; set; }
        public Nullable<bool> Rasio_KelebihanDibayarPasien { get; set; }
        public Nullable<bool> RasioUmum_Alert { get; set; }
        public Nullable<bool> RasioUmum_Blok { get; set; }
        public Nullable<bool> RasioSpesialis_Alert { get; set; }
        public Nullable<bool> RasioSPesialis_Blok { get; set; }
        public Nullable<bool> RasioSub_Alert { get; set; }
        public Nullable<bool> RasioSub_Blok { get; set; }
        public Nullable<decimal> RasioUmum_nilai { get; set; }
        public Nullable<decimal> RasioSpesialis_Nilai { get; set; }
        public Nullable<decimal> RasioSub_Nilai { get; set; }
        public Nullable<bool> SedangProses { get; set; }
        public string SedangProsesPC { get; set; }
        public string AlasanBatal { get; set; }
        public Nullable<int> UserID_Batal { get; set; }
        public bool ObatPulang { get; set; }
        public Nullable<bool> Pending { get; set; }
        public string AlasanPending { get; set; }
        public Nullable<int> UserID_Pending { get; set; }
        public bool ObatRacik { get; set; }
        public double? QtyRacik { get; set; }
        public string JenisResep { get; set; }


        public ListDetail<OrderResepDetailViewModel> Detail_List { get; set; }

        public string JedisResep { get; set; }
        public string SectionName { get; set; }
        public string Dosis1 { get; set; }
        public string Dosis2 { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListResepNonRacik> ListTemplate { get; set; }
    }

    public class OrderResepDetailViewModel
    {
        public string NoResep { get; set; }
        public int Barang_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan { get; set; }
        public string AturanPakai { get; set; }
        public string Dosis { get; set; }
        public double Qty { get; set; }
        public int? JmlRacikan { get; set; }
        public decimal Harga { get; set; }
        public string Harga_View { get; set; }
        public Nullable<double> Jumlah { get; set; }
        public string Jumlah_View { get; set; }
        public double Stok { get; set; }
        public bool Racik { get; set; }
        public bool Racikan1 { get; set; }
        public bool Racikan2 { get; set; }
        public bool Racikan3 { get; set; }
        public string KetDosis { get; set; }
        public string KetRacikan { get; set; }
    }

    public class OrderPaketResepViewModel
    {
        public string KodePaket { get; set; }
        public string NamaPaket { get; set; }
    }

    public class BarangViewModel
    {
        public string Kode_Barang { get; set; }
        public string Satuan_Stok { get; set; }
        public bool Aktif { get; set; }
        public string Kelompok { get; set; }
        public string SectionID { get; set; }
        public string Nama_Kategori { get; set; }
        public double Qty_Stok { get; set; }
        public Nullable<bool> BarangLokasiNew_Aktif { get; set; }
        public int Barang_ID { get; set; }
        public string Nama_Sub_Kategori { get; set; }
        public Nullable<decimal> Harga_Jual { get; set; }
        public string Harga_Jual_View { get; set; }

        public string NamaBarang { get; set; }
        public string Nama_Barang { get; set; }
    }

    public class ICDViewModel
    {
        public string KodeICD { get; set; }
        public string Descriptions { get; set; }
        
    }

    public class SectionViewModel
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }

    }

    public class SelectItemListResepNonRacik
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}