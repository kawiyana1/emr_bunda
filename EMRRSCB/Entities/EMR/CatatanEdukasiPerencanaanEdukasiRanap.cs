//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class CatatanEdukasiPerencanaanEdukasiRanap
    {
        public string NoBukti { get; set; }
        public string DokterSpesialis_F { get; set; }
        public Nullable<System.DateTime> DokterSpesialis_Tanggal { get; set; }
        public string DokterSpesialis_Metode { get; set; }
        public string DokterSpesialis_Edukator { get; set; }
        public string DokterSpesialis_Keluarga { get; set; }
        public bool DokterSpesialis_TidakRespon { get; set; }
        public bool DokterSpesialis_TidakPaham { get; set; }
        public bool DokterSpesialis_Paham { get; set; }
        public bool DokterSpesialis_MenjelaskanTanpaDiBantu { get; set; }
        public bool DokterSpesialis_MenjelaskanDiBantu { get; set; }
        public bool DokterSpesialis_MampuMendemontrasikan { get; set; }
        public string DokterSpesialis_MampuMendemontrasikanKet { get; set; }
        public string Nutrisi_C { get; set; }
        public Nullable<System.DateTime> Nutrisi_Tanggal { get; set; }
        public string Nutrisi_Metode { get; set; }
        public string Nutrisi_Edukator { get; set; }
        public string Nutrisi_Keluarga { get; set; }
        public bool Nutrisi_TidakRespon { get; set; }
        public bool Nutrisi_TidakPaham { get; set; }
        public bool Nutrisi_DapatMenjelaskan { get; set; }
        public bool Nutrisi_MampuMendemontrasikan { get; set; }
        public string Nutrisi_MampuMendemontrasikanKet { get; set; }
        public string DrSpesialis { get; set; }
        public Nullable<System.DateTime> DrSpesialis_Tanggal { get; set; }
        public string DrSpesialis_Metode { get; set; }
        public string DrSpesialis_Edukator { get; set; }
        public string DrSpesialis_Keluarga { get; set; }
        public bool DrSpesialis_TidakRespon { get; set; }
        public bool DrSpesialis_TidakPaham { get; set; }
        public bool DrSpesialis_Paham { get; set; }
        public bool DrSpesialis_MenjelaskanTanpaDiBantu { get; set; }
        public bool DrSpesialis_MenjelaskanDiBantu { get; set; }
        public bool DrSpesialis_MampuMendemontrasikan { get; set; }
        public string DrSpesialis_MampuMendemontrasikanKet { get; set; }
        public string ManajemenNyeri_C { get; set; }
        public string ManajemenNyeri_D { get; set; }
        public Nullable<System.DateTime> ManajemenNyeri_Tanggal { get; set; }
        public string ManajemenNyeri_Metode { get; set; }
        public string ManajemenNyeri_Edukator { get; set; }
        public string ManajemenNyeri_Keluarga { get; set; }
        public bool ManajemenNyeri_TidakRespon { get; set; }
        public bool ManajemenNyeri_Paham { get; set; }
        public bool ManajemenNyeri_DapatMenjelaskan { get; set; }
        public bool ManajemenNyeri_MampuMendemontrasikan { get; set; }
        public string ManajemenNyeri_MampuMendemontrasikanKet { get; set; }
        public string Rohaniawan_C { get; set; }
        public Nullable<System.DateTime> Rohaniawan_Tanggal { get; set; }
        public string Rohaniawan_Metode { get; set; }
        public string Rohaniawan_Edukator { get; set; }
        public string Rohaniawan_Keluarga { get; set; }
        public bool Rohaniawan_MampuMenjalankanBimbingan { get; set; }
        public string Farmasi_G { get; set; }
        public string Farmasi_H { get; set; }
        public Nullable<System.DateTime> Farmasi_Tanggal { get; set; }
        public string Farmasi_Metode { get; set; }
        public string Farmasi_Edukator { get; set; }
        public string Farmasi_Keluarga { get; set; }
        public bool Farmasi_TidakRespon { get; set; }
        public bool Farmasi_TidakPaham { get; set; }
        public bool Farmasi_Paham { get; set; }
        public bool Farmasi_MenjelaskanTanpaDiBantu { get; set; }
        public bool Farmasi_MenjelaskanDiBantu { get; set; }
        public bool Farmasi_MampuMendemontrasikan { get; set; }
        public string Farmasi_MampuMendemontrasikanKet { get; set; }
        public string Perawat_Pendidikan { get; set; }
        public string Perawat_Pendidikan2 { get; set; }
        public Nullable<System.DateTime> Perawat_Tanggal { get; set; }
        public string Perawat_Metode { get; set; }
        public string Perawat_Edukator { get; set; }
        public string Perawat_Keluarga { get; set; }
        public bool Perawat_TidakRespon { get; set; }
        public bool Perawat_TidakPaham { get; set; }
        public bool Perawat_Paham { get; set; }
        public bool Perawat_MenjelaskanTanpaDiBantu { get; set; }
        public bool Perawat_MenjelaskanDiBantu { get; set; }
        public bool Perawat_MampuMendemontrasikan { get; set; }
        public string Perawat_MampuMendemontrasikanKet { get; set; }
        public string Rehabilitasi { get; set; }
        public Nullable<System.DateTime> Rehabilitasi_Tanggal { get; set; }
        public string Rehabilitasi_Metode { get; set; }
        public string Rehabilitasi_Edukator { get; set; }
        public string Rehabilitasi_Keluarga { get; set; }
        public bool Rehabilitasi_TidakRespon { get; set; }
        public bool Rehabilitasi_TidakPaham { get; set; }
        public bool Rehabilitasi_Paham { get; set; }
        public bool Rehabilitasi_MenjelaskanTanpaDiBantu { get; set; }
        public bool Rehabilitasi_MenjelaskanDiBantu { get; set; }
        public bool Rehabilitasi_MampuMendemontrasikan { get; set; }
        public string Rehabilitasi_MampuMendemontrasikanKet { get; set; }
        public Nullable<bool> DokterSpesialis_PenjelasanPenyakit { get; set; }
        public Nullable<bool> DokterSpesialis_HasilPemeriksaan { get; set; }
        public Nullable<bool> DokterSpesialis_TindakanMedis { get; set; }
        public Nullable<bool> DokterSpesialis_PerkiraanHari { get; set; }
        public Nullable<bool> DokterSpesialis_PenjelasanKomplikasi { get; set; }
        public Nullable<bool> DokterSpesialis_Flainnya { get; set; }
        public Nullable<bool> Nutrisi_Diet { get; set; }
        public Nullable<bool> Nutrisi_Penyuluhan { get; set; }
        public Nullable<bool> Nutrisi_C_Lainnya { get; set; }
        public Nullable<bool> Farmasi_NamaObat { get; set; }
        public Nullable<bool> Farmasi_AturanPemakaian { get; set; }
        public Nullable<bool> Farmasi_JumlahObat { get; set; }
        public Nullable<bool> Farmasi_CaraPenyempianan { get; set; }
        public Nullable<bool> Farmasi_EfekSamping { get; set; }
        public Nullable<bool> Farmasi_Kontraindikasi { get; set; }
        public byte[] TTD_Edukator_1 { get; set; }
        public byte[] TTD_Pasien_1 { get; set; }
        public byte[] TTD_Edukator_2 { get; set; }
        public byte[] TTD_Pasien_2 { get; set; }
        public byte[] TTD_Edukator_3 { get; set; }
        public byte[] TTD_Pasien_3 { get; set; }
        public byte[] TTD_Edukator_4 { get; set; }
        public byte[] TTD_Pasien_4 { get; set; }
        public byte[] TTD_Edukator_5 { get; set; }
        public byte[] TTD_Pasien_5 { get; set; }
        public byte[] TTD_Edukator_6 { get; set; }
        public byte[] TTD_Pasien_6 { get; set; }
        public byte[] TTD_Edukator_7 { get; set; }
        public byte[] TTD_Pasien_7 { get; set; }
        public byte[] TTD_Edukator_8 { get; set; }
        public byte[] TTD_Pasien_8 { get; set; }
        public string DokterSpesialis_Metode1 { get; set; }
        public string Nutrisi_Metode1 { get; set; }
        public string DrSpesialis_Metode1 { get; set; }
        public string ManajemenNyeri_Metode1 { get; set; }
        public string Rohaniawan_Metode1 { get; set; }
        public string Farmasi_Metode1 { get; set; }
        public string Perawat_Metode1 { get; set; }
        public string Rehabilitasi_Metode1 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_1 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_2 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_3 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_4 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_5 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_6 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_7 { get; set; }
        public Nullable<bool> Verifikasi_Edukator_8 { get; set; }
        public Nullable<bool> DokterSpesialis_Glainnya { get; set; }
        public string DokterSpesialis_G { get; set; }
        public Nullable<bool> Nutrisi_D_Lainnya { get; set; }
        public string Nutrisi_D { get; set; }
        public Nullable<bool> Farmasi_Potensi { get; set; }
        public Nullable<bool> Farmasi_Penggunaan { get; set; }
        public Nullable<bool> Perawat_a { get; set; }
        public Nullable<bool> Perawat_b { get; set; }
        public Nullable<bool> Perawat_c { get; set; }
        public Nullable<bool> Perawat_d { get; set; }
        public string Perawat_b_Ket { get; set; }
    }
}
