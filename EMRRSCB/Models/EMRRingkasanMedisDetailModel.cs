﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRingkasanMedisDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Poliklinik { get; set; }
        public string Diagnosa { get; set; }
        public string ICD { get; set; }
        public string Pengobatan { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string TandaTangan { get; set; }
        public bool Verifikasi { get; set; }
    }
}