﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRLampiranDaftarPerawatanDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Lampiran { get; set; }
        public string Peralatan { get; set; }
        public string Keterangan { get; set; }
    }
}