//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class SuratPernyataanPulang
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Perawat { get; set; }
        public string Dokter { get; set; }
        public string YangMembuat { get; set; }
        public string Saksi { get; set; }
        public byte[] TTDPerawat { get; set; }
        public byte[] TTDYangMembuat { get; set; }
        public byte[] TTDDokter { get; set; }
        public byte[] TTDSaksi { get; set; }
        public Nullable<bool> VerfikasiPerawatJaga { get; set; }
        public Nullable<bool> VerifikasiDokter { get; set; }
    }
}
