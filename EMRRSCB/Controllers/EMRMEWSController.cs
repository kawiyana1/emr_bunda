﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRMEWSController : Controller
    {
        // GET: EMRMEWS
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.MEWS.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRMEWS_List = new ListDetail<EMRMEWSViewModel>();
                        var detail_1 = s.MEWS.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRMEWSViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == x.Paraf);
                            if (dokter != null) {
                                y.ParafNama = dokter.NamaDOkter;
                            }
                            model.EMRMEWS_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.MEWS.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create EWS ";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update EWS ";
                    }

                    if (item.EMRMEWS_List == null) item.EMRMEWS_List = new ListDetail<EMRMEWSViewModel>();
                    item.EMRMEWS_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRMEWS_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Tanggal = DateTime.Now;
                    }
                    var new_list = item.EMRMEWS_List;
                    var real_list = s.MEWS.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.MEWS.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.MEWS.Add(IConverter.Cast<MEWS>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.KesadaranHasil = x.Model.KesadaranHasil;
                            _m.KesadaranSkor = x.Model.KesadaranSkor;
                            _m.PernapasanHasil = x.Model.PernapasanHasil;
                            _m.PernapasanSkor = x.Model.PernapasanSkor;
                            _m.SaturasiOksigenHasil_1 = x.Model.SaturasiOksigenHasil_1;
                            _m.SaturasiOksigenHasil_2 = x.Model.SaturasiOksigenHasil_2;
                            _m.SaturasiOksigenSkor = x.Model.SaturasiOksigenSkor;
                            _m.TekananDarahHasil = x.Model.TekananDarahHasil;
                            _m.TekananDarahSkor = x.Model.TekananDarahSkor;
                            _m.HeartRateHasil = x.Model.HeartRateHasil;
                            _m.HeartRateSkor = x.Model.HeartRateSkor;
                            _m.SuhuAxillaHasil = x.Model.SuhuAxillaHasil;
                            _m.SuhuAxillaSkor = x.Model.SuhuAxillaSkor;
                            _m.TotalSkor = x.Model.TotalSkor;
                            _m.Paraf = x.Model.Paraf;
                            _m.Verifikasi = x.Model.Verifikasi;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}