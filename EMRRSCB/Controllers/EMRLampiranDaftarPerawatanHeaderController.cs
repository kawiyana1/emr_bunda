﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRLampiranDaftarPerawatanHeaderController : Controller
    {
        // GET: EMRLampiranDaftarPerawatanHeader
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRLampiranDaftarPerawatanHeaderViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.LampiranDaftarPerawatanHeader.FirstOrDefault(x => x.NoBukti == id);
                    var ceklampiran = s.LampiranDaftarPerawatanHeader.Where(z => z.NoBukti == id).FirstOrDefault();
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRLampiranDaftarPerawatanHeaderViewModel>(dokumen);
                        model.NoBukti = id;

                        #region === Detail Lampiran
                        model.Lampiran_List = new ListDetail<EMRLampiranDaftarPerawatanDetailModel>();
                        var detail_1 = s.LampiranDaftarPerawatan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRLampiranDaftarPerawatanDetailModel>(x);
                            model.Lampiran_List.Add(false, y);
                        }
                        #endregion
                        if (ceklampiran != null)
                        {
                            ViewBag.Lampiran = ceklampiran.Lampiran;
                        }
                        
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    //var dokumen_temp = s.LampiranDaftarPerawatan.Where(e => e.NoBukti == id).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    //model.ListTemplate = new List<SelectItemListPoliTHT>();
                    //foreach (var list in dokumen_temp)
                    //{
                    //    model.ListTemplate.Add(new SelectItemListPoliTHT()
                    //    {
                    //        Value = list.NoBukti,
                    //        Text = list.NamaTemplate
                    //    });
                    //}

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRLampiranDaftarPerawatanHeaderViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                   
                        model.NoBukti = id;
                    #region === Detail Lampiran
                        model.Lampiran_List = new ListDetail<EMRLampiranDaftarPerawatanDetailModel>();
                        var detail_1 = s.LampiranDaftarPerawatanMaster.Where(x => x.Lampiran == copy).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRLampiranDaftarPerawatanDetailModel>(x);
                            model.Lampiran_List.Add(false, y);
                        }
                        #endregion
                  
                    model.MODEVIEW = 0;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    ViewBag.Lampiran = copy;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRLampiranDaftarPerawatanHeaderViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.LampiranDaftarPerawatanHeader.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    if (model == null)
                    {
                        var o = IConverter.Cast<LampiranDaftarPerawatanHeader>(item);
                        s.LampiranDaftarPerawatanHeader.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                         
                        activity = "Create Lampiran Daftar Kerohanian";
                    }
                    else
                    {
                        model = IConverter.Cast<LampiranDaftarPerawatanHeader>(item);
                        s.LampiranDaftarPerawatanHeader.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Lampiran Daftar Kerohanian";
                    }

                    //if (item.save_template == true && item.nama_template != null)
                    //{
                    //    var temp = new trDokumenTemplate();
                    //    temp.NoBukti = item.NoBukti;
                    //    temp.NamaTemplate = item.nama_template;
                    //    temp.DokumenID = dokumenid;
                    //    temp.SectionID = sectionid;
                    //    s.trDokumenTemplate.Add(temp);
                    //}

                    #region === Detail Keperawatan
                    if (item.Lampiran_List == null) item.Lampiran_List = new ListDetail<EMRLampiranDaftarPerawatanDetailModel>();
                    item.Lampiran_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Lampiran_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Lampiran_List;
                    var real_list = s.LampiranDaftarPerawatan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.LampiranDaftarPerawatan.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.LampiranDaftarPerawatan.Add(IConverter.Cast<LampiranDaftarPerawatan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Lampiran = item.Lampiran;
                            _m.Peralatan = x.Model.Peralatan;
                            _m.Keterangan = x.Model.Keterangan;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}