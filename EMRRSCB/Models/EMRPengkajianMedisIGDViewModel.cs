﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisIGDViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TangalKedatangan { get; set; }
        public string KategoriTriage { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKedatangan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPengkajian { get; set; }
        public string Rujukan { get; set; }
        public bool RujukanYaRs { get; set; }
        public string RujukanYaKeterangan { get; set; }
        public bool RujukanYaPuskesmas { get; set; }
        public string RujukanYaPuskesmasKeterangan { get; set; }
        public bool RujukanYaDr { get; set; }
        public string RujukanYaDrKeterangan { get; set; }
        public bool RujukanYaLainnya { get; set; }
        public string RujukanYaLainnyaKeterangan { get; set; }
        public string RujukanYaDxRujukan { get; set; }
        public bool RujukanTidakDatangSendiri { get; set; }
        public string RujukanTidakDatangSendiriKeterangan { get; set; }
        public Nullable<bool> RujukanTidakDiantarOlehAtauStatus { get; set; }
        public string RujukanTidakDiantarOlehAtauStatusKeterangan { get; set; }
        public string IdentitasPengantar { get; set; }
        public string NoTelepon { get; set; }
        public string Kecelakaan { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKecelakaan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKecelakaan { get; set; }
        public string TempatKejadianKecelakaan { get; set; }
        public string PermintaanVisum { get; set; }
        public string PermintaanVisumDari { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> PermintaanVisumTanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PermintaanVisumJam { get; set; }
        public string PermintaanVisumNo { get; set; }
        public string KeadaanPasienTiba { get; set; }
        public string SumberData { get; set; }
        public string Anamnesis_KeluhanUtama { get; set; }
        public string Anamnesis_RiwayatPenyakitSekarang { get; set; }
        public string Anamnesis_RiwayatPenyakitDahulu { get; set; }
        public string Anamnesis_RiwayatAlergi { get; set; }
        public string Anamnesis_PenyakitKeluarga { get; set; }
        public string Anamnesis_RiwayatPengobatan1 { get; set; }
        public string Anamnesis_RiwayatPengobatan2 { get; set; }
        public string Anamnesis_RiwayatPengobatan3 { get; set; }
        public bool StatusPsikologis_Tenang { get; set; }
        public bool StatusPsikologis_Cemas { get; set; }
        public bool StatusPsikologis_Marah { get; set; }
        public bool StatusPsikologis_Depresi { get; set; }
        public bool StatusPsikologis_Gelisah { get; set; }
        public bool StatusPsikologis_Kooperatif { get; set; }
        public bool StatusPsikologis_TidakKooperatif { get; set; }
        public bool Airway_Bebas { get; set; }
        public bool Airway_Gargling { get; set; }
        public bool Airway_Stridor { get; set; }
        public bool Airway_Wheezing { get; set; }
        public bool Airway_Ronchi { get; set; }
        public bool Airway_Terintubasi { get; set; }
        public bool Breathing_Spontan { get; set; }
        public bool Breathing_Tachipneu { get; set; }
        public bool Breathing_Dispneu { get; set; }
        public bool Breathing_Apneu { get; set; }
        public bool Breathing_VentilasiMekanik { get; set; }
        public bool Breathing_MemakaiVentilator { get; set; }
        public string Circulation_Nadi { get; set; }
        public string Circulation_CRT { get; set; }
        public string Circulation_WarnaKulit { get; set; }
        public string Circulation_Pendarahan { get; set; }
        public string Circulation_Kulit { get; set; }
        public bool DisabilityNeurogicalRespon_Alert { get; set; }
        public bool DisabilityNeurogicalRespon_Pain { get; set; }
        public bool DisabilityNeurogicalRespon_Verbal { get; set; }
        public bool DisabilityNeurogicalRespon_Unrespon { get; set; }
        public bool DisabilityNeurogicalPupil_Isokor { get; set; }
        public bool DisabilityNeurogicalPupil_Anisokor { get; set; }
        public bool DisabilityNeurogicalPupil_Pinpoint { get; set; }
        public bool DisabilityNeurogicalPupil_Midriasis { get; set; }
        public string DisabilityNeurogicalReflek { get; set; }
        public string StatusPresent { get; set; }
        public string StatusPresent_KeadaanUmum { get; set; }
        public string StatusPresent_LevelKesadaran { get; set; }
        public string StatusPresent_LevelKesadaranCGS_E { get; set; }
        public string StatusPresent_LevelKesadaranCGS_V { get; set; }
        public string StatusPresent_LevelKesadaranCGS_M { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_TD { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_N { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_R { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_S { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_BB { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_SpO2 { get; set; }
        public string SkriningNyeri { get; set; }
        public string PengkajianNyeri { get; set; }
        public string IntensitasNyeri { get; set; }
        public string OnsetNyeri { get; set; }
        public string OnsetNyeriKronisSejakKapan { get; set; }
        public string Pencetus { get; set; }
        public string PencetusLainnya { get; set; }
        public bool KualitasNyeri_SepertiDitusuk { get; set; }
        public bool KualitasNyeri_SakitBerdenyut { get; set; }
        public bool KualitasNyeri_Lainnya { get; set; }
        public string KualitasNyeri_LainnyaKeterangan { get; set; }
        public string RegioNyeriLokasi { get; set; }
        public string RegioNyeriMenjalar { get; set; }
        public string RegioNyeriMenjalarTidakKeterangan { get; set; }
        public string SkalaNyeri { get; set; }
        public string TimingNyeri { get; set; }
        public bool NyeriDatangSaatIstirahat { get; set; }
        public bool NyeriDatangSaatBeraktifitas { get; set; }
        public bool NyeriDatangSaatLainnya { get; set; }
        public string NyeriDatangSaatLainnyaKeterangan { get; set; }
        public bool NyeriMembaikBilaIstirahat { get; set; }
        public bool NyeriMembaikBilaBeraktifitas { get; set; }
        public bool NyeriMembaikBilaMendengarkanMusik { get; set; }
        public bool NyeriMembaikBilaMinumObat { get; set; }
        public string NyeriMembaikBilaMinumKeterangan { get; set; }
        public bool NyeriMembaikBilaBerubahPosisiAtauTidur { get; set; }
        public bool NyeriMembaikBilaLainnya { get; set; }
        public string NyeriMembaikBilaLainnyaKeterangan { get; set; }
        public string FrekuensiNyeri { get; set; }
        public string NPAT_Keterangan { get; set; }
        public string NPAT { get; set; }
        public string FLACC_Keterangan { get; set; }
        public string FLACC { get; set; }
        public string PenilaianNyeriSkor_Keterangan { get; set; }
        public string PenilaianNyeriSkor { get; set; }
        public string PemeriksaanFisik_Kepala { get; set; }
        public string PemeriksaanFisik_Mata { get; set; }
        public string PemeriksaanFisik_THT { get; set; }
        public string PemeriksaanFisik_Leher { get; set; }
        public string PemeriksaanFisik_Dada { get; set; }
        public string PemeriksaanFisik_Abdomen { get; set; }
        public string PemeriksaanFisik_Punggung { get; set; }
        public string PemeriksaanFisik_Genetalia { get; set; }
        public string PemeriksaanFisik_Ekstremitas { get; set; }
        public byte[] StatusLokasiBedahDepan { get; set; }
        public string StatusLokasiBedahBelakang { get; set; }
        public string HasilPemeriksaanPenunjang_Laboratorium { get; set; }
        public string HasilPemeriksaanPenunjang_Xray { get; set; }
        public string HasilPemeriksaanPenunjang_EKG { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string DiagnosaKerjaDanBanding { get; set; }
        public string ICDX { get; set; }
        public string Terapi { get; set; }
        public string Konsultasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Konsultasi_Jam { get; set; }
        public string Konsultasi_Kepada { get; set; }
        public string Konsultasi_KepadaNama { get; set; }
        public string Konsultasi_Keterangan { get; set; }
        public string JawabanKonsultasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JawabanKonsultasi_Jam { get; set; }
        public string JawabanKonsultasi_Kepada { get; set; }
        public string JawabanKonsultasi_KepadaNama { get; set; }
        public string JawabanKonsultasi_Keterangan { get; set; }
        public string Konsultasi_HormatKami { get; set; }
        public string Konsultasi_HormatKamiNama { get; set; }
        public string JawabanKonsultasi_HormatKami { get; set; }
        public string JawabanKonsultasi_HormatKamiNama { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> KondisiKeluar_Jam { get; set; }
        public string KondisiKeluar_KeadaanUmum { get; set; }
        public string KondisiKeluar_Kesadaran { get; set; }
        public string KondisiKeluar_TD { get; set; }
        public string KondisiKeluar_Suhu { get; set; }
        public string KondisiKeluar_Nadi { get; set; }
        public string KondisiKeluar_Nafas { get; set; }
        public string KondisiKeluar_Catatan { get; set; }
        public string PemberianKomunikasi { get; set; }
        public string DokterJaga { get; set; }
        public string DokterJagaNama { get; set; }
        public string PasienKeluarga { get; set; }
        public bool Tindak_Dipulangkan { get; set; }
        public bool Tindak_Dipulangkan_KIE { get; set; }
        public bool Tindak_Dipulangkan_Obat { get; set; }
        public bool Tindak_Dipulangkan_Lab { get; set; }
        public bool Tindak_Dipulangkan_Ront { get; set; }
        public bool Tindak_Dipulangkan_Kontrol { get; set; }
        public bool Tindak_PulangPaksa { get; set; }
        public bool Tindak_PulangPaksa_KIE { get; set; }
        public bool Tindak_PulangPaksa_TandaTangan { get; set; }
        public bool Tindak_RawatInap { get; set; }
        public string Tindak_RawatInap_Ket { get; set; }
        public string Tindak_RawatInap_Indikasi { get; set; }
        public bool Tindak_Dirujukke { get; set; }
        public string Tindak_Dirujukke_Ket { get; set; }
        public bool Tindak_Meninggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Tindak_Meninggal_Jam { get; set; }
        public bool Tindak_Minggat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Tindak_Minggat_Jam { get; set; }
        public bool Tindak_Dilaporkan { get; set; }
        public bool Tindak_Dilaporkan_Satpam { get; set; }
        public bool Tindak_Dilaporkan_Supervisi { get; set; }
        public bool Tindak_Dilaporkan_MOD { get; set; }
        public bool Tindak_Dilaporkan_Humas { get; set; }
        public string DokterJagaID { get; set; }
        public string DokterJagaIDNama { get; set; }

        public string TTDKonsultasi_Kepada { get; set; }
        public string TTDJawabanKonsultasi_Kepada { get; set; }
        public string TTDJawabanKonsultasi_HormatKami { get; set; }
        public string TTDKonsultasi_HormatKami { get; set; }
        public string TTDDokterJaga { get; set; }
        public string TTDDokterJagaID { get; set; }
        public string TTDPasien { get; set; }
        public bool VerifikasiPasien { get; set; }
        public bool VerifikasiDokterJaga { get; set; }
        public bool VerifikasiKonsultasi_HormatKami { get; set; }
        public bool VerifikasiJawabanKonsultasi_HormatKami { get; set; }
        public bool VerifikasiDokterJagaID { get; set; }
       

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }


        public ListDetail<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel> RencanaKerja_List { get; set; }
        public ListDetail<EMRPengkajianMedisIGD_ObservasiDetailViewModel> Observasi_List { get; set; }
        public int MODEVIEW { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public List<SelectItemList> ListTemplate { get; set; }
    }

    public class SelectItemList
    {
        public string Text { get; set; }
        public string Value { get; set; } 
    }
}