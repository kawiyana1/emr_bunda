﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteController : Controller
    {
        // GET: EMRCatatanPerkembanganTerintegrasiIntegratedProgressNote
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRCatatanIntergrated_List = new ListDetail<EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteDetailModel>();
                        var detail_1 = s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteDetailModel>(x);

                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Verifikasi);
                            if (dokter != null)
                            {
                                y.VerifikasiNama = dokter.NamaDOkter;
                            }
                            model.EMRCatatanIntergrated_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Pengobatan RI";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Pengobatan RI";
                    }

                    if (item.EMRCatatanIntergrated_List == null) item.EMRCatatanIntergrated_List = new ListDetail<EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteDetailModel>();
                    item.EMRCatatanIntergrated_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRCatatanIntergrated_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.EMRCatatanIntergrated_List;
                    var real_list = s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.CatatanPerkembanganTerintegrasiIntegratedProgressNote.Add(IConverter.Cast<CatatanPerkembanganTerintegrasiIntegratedProgressNote>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.PPA = x.Model.PPA;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.SOAP = x.Model.SOAP;
                            _m.Intruksi = x.Model.Intruksi;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}