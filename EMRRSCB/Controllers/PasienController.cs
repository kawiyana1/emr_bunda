﻿//using EMRRSCB.Entities.EMR;
//using EMRRSCB.Models;
//using iHos.MVC.Converter;
//using iHos.MVC.Property;
//using Microsoft.AspNet.Identity;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Migrations;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Linq.Dynamic;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Script.Serialization;

//namespace EMRRSCB.Controllers
//{
//    public class PasienController : Controller
//    {
//        #region ====== I N D E X
//        public ActionResult Index()
//        {
//            return View();
//        }
//        #endregion

//        #region ====== T A B L E

//        [HttpPost]
//        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
//        {
//            try
//            {
//                ResultSS result;
//                using (var s = new EMREntities())
//                {
//                    IQueryable<mPasien> proses = s.mPasien;
//                    proses = proses.Where($"{nameof(mPasien.NRM)}.Contains(@0)", filter[0]);
//                    proses = proses.Where($"{nameof(mPasien.NamaPasien)}.Contains(@0)", filter[1]);
//                    if (!string.IsNullOrEmpty(filter[2]))
//                    {
//                        proses = proses.Where($"{nameof(mPasien.NoIdentitas)}.Contains(@0)", filter[2]);
//                    }
//                    proses = proses.Where($"{nameof(mPasien.Alamat)}.Contains(@0)", filter[5]);
//                    if (!string.IsNullOrEmpty(filter[6]))
//                    {
//                        proses = proses.Where($"{nameof(mPasien.Phone)}.Contains(@0)", filter[6]);
//                    }
//                    var totalcount = proses.Count();
//                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
//                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
//                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
//                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
//                    foreach (var x in m)
//                    {
//                        x.TglLahir_View = x.TglLahir.Value.ToString("dd/MM/yyyy");
//                    }
//                    result.Data = m;
//                }
//                return JsonConvert.SerializeObject(new TableList(result));
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }

//        #endregion

//        #region ====== C R E A T E
//        [HttpGet]
//        [ActionName("Create")]
//        public ActionResult Create_Get(string ajax)
//        {

//            var item = new PasienViewModel();
//            item.TglInput = DateTime.Today;
//            ViewBag.Ajax = ajax;
//            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
//            if (Request.IsAjaxRequest())
//                return PartialView();
//            else
//                return View();
//        }


//        [HttpPost]
//        [ActionName("Create")]
//        [ValidateAntiForgeryToken]
//        public string Create_Post()
//        {
//            try
//            {
//                var item = new PasienViewModel();
//                TryUpdateModel(item);

//                if (ModelState.IsValid)
//                {
//                    var m = new mPasien();
//                    ResultSS result;
//                    using (var s = new EMREntities())
//                    {
//                        var nrm = "";
//                        if (item.NRM == "" || item.NRM == null)
//                        {
//                            nrm = s.GetNRMNew().FirstOrDefault();
//                        }
//                        else
//                        {
//                            nrm = item.NRM;
//                        }
//                        m = IConverter.Cast<mPasien>(item);
//                        m.NRM = nrm;
//                        var insert = s.mPasien.Add(m);
//                        s.SaveChanges();
//                        result = new ResultSS(1, insert);

//                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
//                        {
//                            Activity = $"Pasien Create {item.NRM}"
//                        };
//                        UserActivity.InsertUserActivity(userActivity);
//                    }
//                    result.Data = item.NRM;
//                    return new JavaScriptSerializer().Serialize(new ResultStatus()
//                    {
//                        Data = m,
//                        IsSuccess = result.IsSuccess,
//                        Message = "Berhasil disimpan",
//                        Count = 1,
//                        Status = "success"
//                    });
//                }
//                else
//                    return JsonHelper.JsonMsgError(ViewData);
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }

//        #endregion

//        #region ====== E D I T

//        [HttpGet]
//        [ActionName("Edit")]
//        public ActionResult Edit_Get(string id)
//        {
//            PasienViewModel item;
//            try
//            {
//                using (var s = new EMREntities())
//                {
//                    var m = s.mPasien.FirstOrDefault(x => x.NRM == id);
//                    var mpasien = s.mPasien.FirstOrDefault(x => x.NRM == id);
//                    if (m == null) return HttpNotFound();
//                    if (mpasien == null) return HttpNotFound();
//                    item = IConverter.Cast<PasienViewModel>(mpasien);
//                }
//            }
//            catch (SqlException ex) { throw new Exception(ex.Message); }
//            catch (Exception ex) { throw new Exception(ex.Message); }

//            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
//            if (Request.IsAjaxRequest())
//                return PartialView(item);
//            else
//                return View(item);
//        }

//        [HttpPost]
//        [ActionName("Edit")]
//        [ValidateAntiForgeryToken]
//        public string Edit_Post(string id)
//        {
//            try
//            {
//                var item = new PasienViewModel();
//                TryUpdateModel(item);

//                if (ModelState.IsValid)
//                {
//                    ResultSS result;
//                    using (var s = new EMREntities())
//                    {
//                        TryUpdateModel(item);
//                        var update = s.SaveChanges();
//                        result = new ResultSS(1, update);

//                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
//                        {
//                            Activity = $"Pasien Edit {item.NRM}"
//                        };
//                        UserActivity.InsertUserActivity(userActivity);
//                    }
//                    return JsonHelper.JsonMsgEdit(result, -1);
//                }
//                else
//                    return JsonHelper.JsonMsgError(ViewData);
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }

//        #endregion

//        [HttpGet]
//        public string GetDataPasien(string id)
//        {
//            mPasien m;
//            var result = new PasienViewModel();
//            using (var s = new EMREntities())
//            {
//                m = s.mPasien.FirstOrDefault(x => x.NRM == id);
//                if (m != null)
//                {
//                    var cus = s.mCustomer.FirstOrDefault(x => x.CustomerID == m.CustomerKerjasamaID);
//                    if (cus != null)
//                    {
//                        result.CustomerKerjasamaIDNama = cus.NamaCustomer;
//                    }
//                    result.NRM = m.NRM;
//                    result.NamaPasien = m.NamaPasien;
//                    result.JenisKelamin = m.JenisKelamin;
//                    result.Alamat = m.Alamat;
//                    result.Phone = m.Phone;
//                    result.JenisKerjasamaID = m.JenisKerjasamaID;
//                    result.NoKartu = m.NoKartu;
//                    result.CustomerKerjasamaID = m.CustomerKerjasamaID;
//                    result.PenanggungNRM = m.PenanggungNRM;
//                    result.PenanggungNama = m.PenanggungNama;
//                    result.PenanggungAlamat = m.PenanggungAlamat;
//                    result.PenanggungPhone = m.PenanggungPhone;
//                    result.PenanggungHubungan = m.PenanggungHubungan;
//                    result.PasienKTP = m.PasienKTP;
//                }

//            }
//            return JsonConvert.SerializeObject(result);
//        }

//        #region ===== C H E C K  M A N U A L  N R M
//        [HttpPost]
//        public string check_manual_nrm(string nrm)
//        {
//            try
//            {
//                var item = new PasienViewModel();
//                item.NRM = nrm;

//                if (ModelState.IsValid)
//                {
//                    using (var s = new EMREntities())
//                    {
//                        if (item.NRM != null)
//                        {
//                            var pasien = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
//                            if (pasien != null)
//                            {
//                                return JsonHelper.JsonMsgInfo("NRM sudah digunakan pasien : " + pasien.NamaPasien, false);
//                            }
//                            else
//                            {
//                                return JsonHelper.JsonMsgInfo("Nomor NRM dapat digunakan", true);

//                            }
//                        }
//                        else
//                        {
//                            return JsonHelper.JsonMsgInfo("Nomor NRM tidak boleh kosong", false);
//                        }
//                    }
//                }
//                else
//                    return JsonHelper.JsonMsgError(ViewData);
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }
//        #endregion

//        [HttpPost]
//        public string ListPasienAll(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
//        {
//            try
//            {
//                ResultSS result;
//                using (var s = new EMREntities())
//                {
//                    IQueryable<mPasien> proses = s.mPasien;
//                    proses = proses.Where($"{nameof(mPasien.NRM)}.Contains(@0)", filter[0]);
//                    proses = proses.Where($"{nameof(mPasien.NamaPasien)}.Contains(@0)", filter[1]);
//                    proses = proses.Where($"{nameof(mPasien.Alamat)}.Contains(@0)", filter[2]);
//                    proses = proses.Where($"{nameof(mPasien.Phone)}.Contains(@0)", filter[3]);
                    
//                    var totalcount = proses.Count();
//                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
//                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
//                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
//                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));

//                }
//                return JsonConvert.SerializeObject(new TableList(result));
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }
//    }
//}