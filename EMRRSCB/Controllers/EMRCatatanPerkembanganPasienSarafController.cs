﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanPerkembanganPasienSarafController : Controller
    {
        // GET: EMRCatatanPerkembanganPasienSaraf
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var s = new EMREntities())
            {
                var dokumen = s.CatatanPerkembanganPasienSaraf.Where(x => x.NoBukti == id).ToList();
                if (dokumen != null)
                {
                    model.EMRPekembanganSaraf_List = new ListDetail<EMRCatatanPerkembanganPasienSarafDetailModel>();
                    var detail_1 = s.CatatanPerkembanganPasienSaraf.Where(x => x.NoBukti == id).ToList();
                    foreach (var x in detail_1)
                    {
                        var y = IConverter.Cast<EMRCatatanPerkembanganPasienSarafDetailModel>(x);
                        model.EMRPekembanganSaraf_List.Add(false, y);
                    }
                    model.MODEVIEW = view;
                    model.Report = 1;
                }
                model.NoBukti = id;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanPerkembanganPasienSaraf.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Perkembangan Pasien Saraf";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Perkembangan Pasien Saraf ";
                    }

                    if (item.EMRPekembanganSaraf_List == null) item.EMRPekembanganSaraf_List = new ListDetail<EMRCatatanPerkembanganPasienSarafDetailModel>();
                    item.EMRPekembanganSaraf_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRPekembanganSaraf_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        
                    }
                    var new_list = item.EMRPekembanganSaraf_List;
                    var real_list = s.CatatanPerkembanganPasienSaraf.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanPerkembanganPasienSaraf.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.CatatanPerkembanganPasienSaraf.Add(IConverter.Cast<CatatanPerkembanganPasienSaraf>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Keluhan = x.Model.Keluhan;
                            _m.Diagnosa = x.Model.Diagnosa;
                            _m.HasilPenunjang = x.Model.HasilPenunjang;
                            _m.Terapi = x.Model.Terapi;
                            _m.Keterangan = x.Model.Keterangan;
                            _m.Username = User.Identity.GetUserName();
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}