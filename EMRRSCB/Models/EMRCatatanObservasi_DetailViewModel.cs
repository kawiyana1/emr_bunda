﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanObservasi_DetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Menit { get; set; }
        public string JamManual { get; set; }
        public string TekananDarah { get; set; }
        public string FrekNapas { get; set; }
        public string FrekNadi { get; set; }
        public string Suhu { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string Cairan { get; set; }
        public string Username { get; set; }

        public int MODEVIEW { get; set; }
    }
}