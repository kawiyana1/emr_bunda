﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRRingkasanPulangController : Controller
    {
        // GET: EMRRingkasanPulang
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRRingkasanPulangViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.RingkasanPulang.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRRingkasanPulangViewModel>(dokumen);

                        model.NoBukti = id;
                        //model.JenisKerjasama = (identitas.JenisKerjasama == null ? "" : identitas.JenisKerjasama);
                        //model.Noreg = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KodeDokter);
                        if (dokter != null) model.KodeDokterNama = dokter.NamaDOkter;

                        var dokterintruksi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.InstruksiDrKet);
                        if (dokterintruksi != null) model.InstruksiDrKetNama = dokterintruksi.NamaDOkter;

                        var section = sim.SIMmSection.FirstOrDefault(e => e.SectionID == model.InstruksiPoliKet);
                        if (section != null) model.InstruksiPoliKetNama = section.SectionName;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.TanggalMRS = DateTime.Today;
                        model.TanggalKeluar = DateTime.Today;
                        model.JamMasuk = DateTime.Now;
                        model.JamKeluar = DateTime.Now;
                        model.InstruksiKontrolTgl = DateTime.Today;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRRingkasanPulangViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.RingkasanPulang.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<RingkasanPulang>(item);
                        s.RingkasanPulang.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Ringkasan Pulang";
                    }
                    else
                    {
                        model = IConverter.Cast<RingkasanPulang>(item);
                        s.RingkasanPulang.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Ringkasan Pulang";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}