﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRClinicalPathwaysParuController : Controller
    {
        // GET: EMRClinicalPathwaysParu
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRClinicalPathwaysParuViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.ClinicalPathwaysParu.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRClinicalPathwaysParuViewModel>(dokumen);

                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaDPJP);
                        if (dpjp != null) model.NamaDPJPNama = dpjp.NamaDOkter;

                        var ppjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaPPJP);
                        if (ppjp != null) model.NamaPPJPNama = ppjp.NamaDOkter;

                        #region === Detail Variasi
                        model.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysParu_DetailDetailModel>();
                        var detail_2 = s.VariasiClinicalPathwaysParu_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRVariasiClinicalPathwaysParu_DetailDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Variasi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;

                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListClinicalParu>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListClinicalParu()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRClinicalPathwaysParuViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.ClinicalPathwaysParu.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRClinicalPathwaysParuViewModel>(dokumen);
                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaDPJP);
                        if (dpjp != null) model.NamaDPJPNama = dpjp.NamaDOkter;

                        var ppjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaPPJP);
                        if (ppjp != null) model.NamaPPJPNama = ppjp.NamaDOkter;

                        #region === Detail Aspek
                        model.Aspek_List = new ListDetail<EMRAspekPelayananClinicalPathwaysParu_DetailDetailModel>();
                        var detail_1 = s.AspekPelayananClinicalPathwaysParu_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRAspekPelayananClinicalPathwaysParu_DetailDetailModel>(x);
                            model.Aspek_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Variasi
                        model.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysParu_DetailDetailModel>();
                        var detail_2 = s.VariasiClinicalPathwaysParu_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRVariasiClinicalPathwaysParu_DetailDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Variasi_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListClinicalParu>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListClinicalParu()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRClinicalPathwaysParuViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ClinicalPathwaysParu.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    if (model == null)
                    {
                        var o = IConverter.Cast<ClinicalPathwaysParu>(item);
                        s.ClinicalPathwaysParu.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Crinical Pathways Paru";
                    }
                    else
                    {
                        model = IConverter.Cast<ClinicalPathwaysParu>(item);
                        s.ClinicalPathwaysParu.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Crinical Pathways Paru";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.NamaDPJP;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    #region === Detail Aspek
                    if (item.Aspek_List == null) item.Aspek_List = new ListDetail<EMRAspekPelayananClinicalPathwaysParu_DetailDetailModel>();
                    item.Aspek_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Aspek_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Aspek_List;
                    var real_list = s.AspekPelayananClinicalPathwaysParu_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AspekPelayananClinicalPathwaysParu_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AspekPelayananClinicalPathwaysParu_Detail.Add(IConverter.Cast<AspekPelayananClinicalPathwaysParu_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Hari = x.Model.Hari;
                            _m.MenilaiMemeriksa_1 = x.Model.MenilaiMemeriksa_1;
                            _m.MenilaiFungsi_1 = x.Model.MenilaiFungsi_1;
                            _m.MenilaiTingkatKesadaran_1 = x.Model.MenilaiTingkatKesadaran_1;
                            _m.MenilaiKeparahan_1 = x.Model.MenilaiKeparahan_1;
                            _m.MenilaiTanda_2 = x.Model.MenilaiTanda_2;
                            _m.MenilaiEfektivitas_2 = x.Model.MenilaiEfektivitas_2;
                            _m.MenilaiKesadaran_2 = x.Model.MenilaiKesadaran_2;
                            _m.MemeriksaDL_3 = x.Model.MemeriksaDL_3;
                            _m.MemeriksaBUN_3 = x.Model.MemeriksaBUN_3;
                            _m.MemeriksaECG_3 = x.Model.MemeriksaECG_3;
                            _m.MemeriksaFoto_3 = x.Model.MemeriksaFoto_3;
                            _m.MemberiTerapi_4 = x.Model.MemberiTerapi_4;
                            _m.MemasangInfus_4 = x.Model.MemasangInfus_4;
                            _m.MelepasInfus_4 = x.Model.MelepasInfus_4;
                            _m.MemberiInhalasi_4 = x.Model.MemberiInhalasi_4;
                            _m.MemberiIntravena_4 = x.Model.MemberiIntravena_4;
                            _m.MemberiPernafasan_5 = x.Model.MemberiPernafasan_5;
                            _m.MemberiOksigen_5 = x.Model.MemberiOksigen_5;
                            _m.MemberiInhalasi_5 = x.Model.MemberiInhalasi_5;
                            _m.MemberiIntravena_5 = x.Model.MemberiIntravena_5;
                            _m.Osigen_6 = x.Model.Osigen_6;
                            _m.Infus_6 = x.Model.Infus_6;
                            _m.Steroid_6 = x.Model.Steroid_6;
                            _m.Mucolitik_6 = x.Model.Mucolitik_6;
                            _m.H2_6 = x.Model.H2_6;
                            _m.PPI_6 = x.Model.PPI_6;
                            _m.Antibiotik_6 = x.Model.Antibiotik_6;
                            _m.Bronkodilator_6 = x.Model.Bronkodilator_6;
                            _m.DietLunak_7 = x.Model.DietLunak_7;
                            _m.DietPadat_7 = x.Model.DietPadat_7;
                            _m.DietTambahan_7 = x.Model.DietTambahan_7;
                            _m.Cairan_7 = x.Model.Cairan_7;
                            _m.MobilisasiAktifTerbatas_8 = x.Model.MobilisasiAktifTerbatas_8;
                            _m.MobilisasiToileting_8 = x.Model.MobilisasiToileting_8;
                            _m.MobilisasiKeluarRuangan_8 = x.Model.MobilisasiKeluarRuangan_8;
                            _m.KonsultasiGizi_9 = x.Model.KonsultasiGizi_9;
                            _m.KonselingDiagnosa_10 = x.Model.KonselingDiagnosa_10;
                            _m.KonselingDukungan_10 = x.Model.KonselingDukungan_10;
                            _m.EdukasiDiet_11 = x.Model.EdukasiDiet_11;
                            _m.EdukasiCaraMinum_11 = x.Model.EdukasiCaraMinum_11;
                            _m.EdukasiControl_11 = x.Model.EdukasiControl_11;
                            _m.Simptom_12 = x.Model.Simptom_12;
                            _m.FungsiPernafasan_12 = x.Model.FungsiPernafasan_12;
                            _m.KeadaanUmum_12 = x.Model.KeadaanUmum_12;
                        }
                    }
                    #endregion

                    #region === Detail Variasi
                    if (item.Variasi_List == null) item.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysParu_DetailDetailModel>();
                    item.Variasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Variasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Variasi_List;
                    var real_list1 = s.VariasiClinicalPathwaysParu_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.VariasiClinicalPathwaysParu_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.VariasiClinicalPathwaysParu_Detail.Add(IConverter.Cast<VariasiClinicalPathwaysParu_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.VariasiPelayanan = x.Model.VariasiPelayanan;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Alasan = x.Model.Alasan;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}