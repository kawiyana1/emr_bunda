﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPenolakanTindakanViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DokterPelaksanaTIndakan { get; set; }
        public string DokterPelaksanaTIndakanNama { get; set; }
        public string PemberiInformasi { get; set; }
        public string PenerimaInformasi { get; set; }
        public string DiagnosaKet { get; set; }
        public bool Diagnosa { get; set; }
        public string DasarDiagnosaKet { get; set; }
        public bool DasarDiagnosa { get; set; }
        public string TindakanKedokteranKet { get; set; }
        public bool TindakanKedokteran { get; set; }
        public string IndikasiTindakanKet { get; set; }
        public bool IndikasiTindakan { get; set; }
        public string TataCaraKet { get; set; }
        public bool TataCara { get; set; }
        public string TujuanKet { get; set; }
        public bool Tujuan { get; set; }
        public string RisikoKet { get; set; }
        public bool Risiko { get; set; }
        public string KomplikasiKet { get; set; }
        public bool Komplikasi { get; set; }
        public string PrognosisKet { get; set; }
        public bool Prognosis { get; set; }
        public string AlternatifDanRisikoKet { get; set; }
        public bool AlternatifDanRisiko { get; set; }
        public string LainLainKet { get; set; }
        public bool LainLain { get; set; }
        public string TandaTangan1 { get; set; }
        public string TandaTangan2 { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Alamat { get; set; }
        public string Tindakan { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string Menyatakan { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi2 { get; set; }
        public string NamaPasien { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string HitungUmur { get; set; }
        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string TTD_Menyatakan { get; set; }
        public string TTD_Saksi1 { get; set; }
        public string TTD_Saksi2 { get; set; }
        public string TTD_TandaTangan1 { get; set; }
        public string TTD_TandaTangan2 { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}