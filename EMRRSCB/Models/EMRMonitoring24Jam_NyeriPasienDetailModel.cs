﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoring24Jam_NyeriPasienDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string EkpresiWajah { get; set; }
        public string EkstremitasAtas { get; set; }
        public string Compliance { get; set; }
        public string Nilai { get; set; }
    }
}