﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Newtonsoft.Json;
using EMRRSCB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMRRSCB.Helper;
using EMRRSCB.Entities.SIM;
using Microsoft.AspNet.Identity;

namespace EMRRSCB.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class ReportController : Controller
    {

        private string serverpath = "~/CrystalReports/";

        public ActionResult Index(string dir = "General_Report")
        {
            ViewBag.Dir = dir;
            var category = dir;
            var hreport = new HReport(Server, serverpath);
            var r = new ReportKasirViewModel()
            {
                Name = category,
                Reports = hreport.List(category)
            };
            r.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value; ;
            r.SectionName = Request.Cookies["EMRSectionNamaPelayanan"].Value; 
            using (var s = new SIM_Entities())
            {
                var section = s.SIMmSection.Where(x => x.SectionID == r.SectionID).FirstOrDefault();
                r.LokasiID = section.Lokasi_ID;
            }
            return View(r);
        }

        public ActionResult ExportPDF(string dir, string filename, string param)
        {
            try
            {
                var category = dir;
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["EMR_EntitiesManual"].ConnectionString.Split(';')) 
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportPDF(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult ExportExcel(string dir, string filename, string param)
        {
            try
            {
                var category = dir;
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["SIM_EntitiesManual"].ConnectionString.Split(';'))
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportExcel(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/vnd.ms-excel");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}