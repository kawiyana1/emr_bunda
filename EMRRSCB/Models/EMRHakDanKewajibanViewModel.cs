﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRHakDanKewajibanViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Pasien { get; set; }
        public string PemberiInformasi { get; set; }
        public string PemberiInformasiNama { get; set; }
        public string TandaTanganPasien { get; set; }
        public string TandaTanganPemberianInformasi { get; set; }
        public bool VerifikasiPetugas { get; set; }
        public bool VerifikasiPemberiInformasi { get; set; }

        public string AngotaKeluarga { get; set; }
        public string PermintaanKhusus { get; set; }
        public string PasienYangMasukNama { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string SayaSelaku { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Keluarga { get; set; }
        public string TandaTanganPetugas { get; set; }
        public string TandaTanganKeluarga { get; set; }
        public string Point14_1 { get; set; }
        public string Point14_2 { get; set; }
        public string Point14_3 { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}