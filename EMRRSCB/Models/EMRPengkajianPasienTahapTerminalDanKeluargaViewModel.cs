﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianPasienTahapTerminalDanKeluargaViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Username { get; set; }
        public string Suhu { get; set; }
        public string Tensi { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public bool Gejala_a_KesulitanNafas { get; set; }
        public bool Gejala_a_NafasTidakTeratur { get; set; }
        public bool Gejala_a_TerdapatSekret { get; set; }
        public bool Gejala_a_NafasCepat { get; set; }
        public bool Gejala_a_NafasMulaiMulut { get; set; }
        public bool Gejala_a_SpO2 { get; set; }
        public bool Gejala_a_NafasLambat { get; set; }
        public bool Gejala_a_MukosaOral { get; set; }
        public bool Gejala_a_TidakTerdapat { get; set; }
        public bool Gejala_b_Mual { get; set; }
        public bool Gejala_b_SulitMenelan { get; set; }
        public bool Gejala_b_GerakTubuh { get; set; }
        public bool Gejala_b_InkontinesiaFeses { get; set; }
        public bool Gejala_b_InkontinesiaUrine { get; set; }
        public bool Gejala_b_PerutKembung { get; set; }
        public bool Gejala_b_SulitBicara { get; set; }
        public bool Gejala_b_SulitBerjalan { get; set; }
        public bool Gejala_b_TidakTerdapat { get; set; }
        public string Gejala_c_Nyeri { get; set; }
        public string Gejala_c_Lokasi { get; set; }
        public string Gejala_c_SkalaNyeri { get; set; }
        public bool Gejala_d_BibirKebiruan { get; set; }
        public bool Gejala_d_Jari { get; set; }
        public bool Gejala_d_Gelisah { get; set; }
        public bool Gejala_d_Lemes { get; set; }
        public bool Gejala_d_KulitDingin { get; set; }
        public bool Gejala_d_TekananDarah { get; set; }
        public bool Gejala_d_NadaLambat { get; set; }
        public bool Gejala_d_Penurunan { get; set; }
        public bool Gejala_d_Tidak { get; set; }
        public bool Faktor_AktifitasFisik { get; set; }
        public bool Faktor_PindahPosisi { get; set; }
        public bool Faktor_Lain { get; set; }
        public string Faktor_LainKet { get; set; }
        public bool Masalah_Konstipasi { get; set; }
        public bool Masalah_NyeriAkut { get; set; }
        public bool Masalah_NyeriKronis { get; set; }
        public bool Masalah_PolaNafas { get; set; }
        public bool Masalah_Defisit { get; set; }
        public bool Masalah_Koping { get; set; }
        public bool Masalah_Perubahan { get; set; }
        public bool Masalah_Bersihan { get; set; }
        public bool Masalah_Distres { get; set; }
        public bool Masalah_Tidak { get; set; }
        public string DukunganSpiritual { get; set; }
        public string DukunganSpiritual_Oleh { get; set; }
        public string Urusan_Didoakan { get; set; }
        public string Urusan_Didoakan_Oleh { get; set; }
        public string Urusan_Bimbingan { get; set; }
        public string Urusan_BimbinganOleh { get; set; }
        public string Urusan_Pendamping { get; set; }
        public string Urusan_PendampingOleh { get; set; }
        public string Status_a_Apakah { get; set; }
        public string Status_a_Nama { get; set; }
        public string Status_a_Alamat { get; set; }
        public string Status_a_Telp { get; set; }
        public string Status_a_Hubungan { get; set; }
        public bool Status_b_Tetap { get; set; }
        public bool Status_b_Perawat { get; set; }
        public bool Status_c_Marah { get; set; }
        public bool Status_c_Sedih { get; set; }
        public bool Status_c_Rasa { get; set; }
        public bool Status_c_Perubahan { get; set; }
        public bool Status_c_Penurunan { get; set; }
        public bool Status_c_Gangguan { get; set; }
        public bool Status_c_TidakSesuai { get; set; }
        public bool Status_c_DIjauhiKeluarga { get; set; }
        public bool Status_c_MenerimaKeadaan { get; set; }
        public bool Kebutuhan_Pasien { get; set; }
        public bool Kebutuhan_Keluarga { get; set; }
        public bool Kebutuhan_Sahabat { get; set; }
        public bool Kebutuhan_Lainnya { get; set; }
        public string Kebutuhan_LainnyaKet { get; set; }
        public bool Alternatif_Autopsi { get; set; }
        public bool Alternatif_Donor { get; set; }
        public bool Alternatif_Tidak { get; set; }
        public bool Alternatif_Lainnya { get; set; }
        public string Alternatif_LainnyaKet { get; set; }
        public bool Resiko_Marah { get; set; }
        public bool Resiko_Sedih { get; set; }
        public bool Resiko_RasaBersalah { get; set; }
        public bool Resiko_PerubahanKebiasan { get; set; }
        public bool Resiko_PenurunanKosentrasi { get; set; }
        public bool Resiko_GangguanTidur { get; set; }
        public bool Resiko_TidakSesuai { get; set; }
        public bool Resiko_Dijauhi { get; set; }
        public bool Resiko_Menerima { get; set; }
        public string Keluarga { get; set; }
        public string Rohaniawan { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string TTDKeluarga { get; set; }
        public string TTDRohaniawan { get; set; }
        public bool VerifikasiDokter { get; set; }


        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
        public List<SelectItemListTahapTerminal> ListTemplate { get; set; }

        // disable all form 
        public int MODEVIEW { get; set; }
    }

    public class SelectItemListTahapTerminal
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}