﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanEdukasiRawatJalanViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string EdukasiDiberikan { get; set; }
        public string SiapaEdukasi { get; set; }
        public string Tempat { get; set; }
        public string MetodeEdukasi { get; set; }
        public string Respon { get; set; }
        public string Edukator { get; set; }
        public string EdukatorNama { get; set; }
        public string BidangDisiplin { get; set; }
        public string TandaTangan { get; set; }
        public Nullable<bool> Verifikasi { get; set; }
    }
}