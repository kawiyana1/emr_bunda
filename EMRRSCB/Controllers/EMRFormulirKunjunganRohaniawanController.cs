﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormulirKunjunganRohaniawanController : Controller
    {
        // GET: EMRFormulirKunjunganRohaniawan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.FormulirKunjunganRohaniawan.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRHeaderViewDetail>(dokumen);

                        model.NoBukti = id;

                        //var paraf = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Paraf);
                        //if (paraf != null) model.ParafNama = paraf.NamaDOkter;

                        #region ===  Detail
                        model.EMRFormKunjunganKerohanian_List = new ListDetail<EMRFormulirKunjunganRohaniawanViewModel>();
                        var detail_2 = s.FormulirKunjunganRohaniawan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRFormulirKunjunganRohaniawanViewModel>(x);
                            //var parafdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Nama);
                            //if (parafdetail != null)
                            //{
                            //    y.NamaNama = parafdetail.NamaDOkter;
                            //}
                            model.EMRFormKunjunganKerohanian_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.FormulirKunjunganRohaniawan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormulirKunjunganRohaniawan>(item);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Formulir Kunjungan Kerohanian";
                    }
                    else
                    {
                        model = IConverter.Cast<FormulirKunjunganRohaniawan>(item);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Formulir Kunjungan Kerohanian";
                    }

                    #region === Detail Intervensi
                    if (item.EMRFormKunjunganKerohanian_List == null) item.EMRFormKunjunganKerohanian_List = new ListDetail<EMRFormulirKunjunganRohaniawanViewModel>();
                    item.EMRFormKunjunganKerohanian_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRFormKunjunganKerohanian_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.EMRFormKunjunganKerohanian_List;
                    var real_list1 = s.FormulirKunjunganRohaniawan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormulirKunjunganRohaniawan.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {
                            _m = new FormulirKunjunganRohaniawan();
                            s.FormulirKunjunganRohaniawan.Add(IConverter.Cast<FormulirKunjunganRohaniawan>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<FormulirKunjunganRohaniawan>(x.Model);
                            s.FormulirKunjunganRohaniawan.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}