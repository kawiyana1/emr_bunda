﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRRingkasanMedisController : Controller
    {
        // GET: EMRRingkasanMedis
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRRingkasanMedisViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.RingkasanMedis.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRRingkasanMedisViewModel>(dokumen);

                        model.NoBukti = id;
                        model.Report = 1;

                        #region === Detail Keperawatan
                        model.Detail_List = new ListDetail<EMRRingkasanMedisDetailModel>();
                        var detail_1 = s.RingkasanMedis_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRRingkasanMedisDetailModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokter != null)
                            {
                                y.DokterNama = dokter.NamaDOkter;
                            }
                            model.Detail_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRRingkasanMedisViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.RingkasanMedis.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<RingkasanMedis>(item);
                        s.RingkasanMedis.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Poliklinik Anak";
                    }
                    else
                    {
                        model = IConverter.Cast<RingkasanMedis>(item);
                        s.RingkasanMedis.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Poliklinik Anak";
                    }
                    #region === Detail Keperawatan
                    if (item.Detail_List == null) item.Detail_List = new ListDetail<EMRRingkasanMedisDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Detail_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        //x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Detail_List;
                    var real_list = s.RingkasanMedis_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RingkasanMedis_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RingkasanMedis_Detail.Add(IConverter.Cast<RingkasanMedis_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Poliklinik = x.Model.Poliklinik;
                            _m.Diagnosa = x.Model.Diagnosa;
                            _m.ICD = x.Model.ICD;
                            _m.Pengobatan = x.Model.Pengobatan;
                            _m.Dokter = x.Model.Dokter;
                            _m.Verifikasi = x.Model.Verifikasi;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRingkasanMedis(string nobukti)
        {
            //sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RingkasanMedis.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"Rpt_RingkasanMedis", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_RingkasanMedis;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("RingkasanMedis_Detail", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["RingkasanMedis_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}