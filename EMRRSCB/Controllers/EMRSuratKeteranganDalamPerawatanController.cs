﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSuratKeteranganDalamPerawatanController : Controller
    {
        // GET: EMRSuratKeteranganDalamPerawatan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRSuratKeteranganDalamPerawatanViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.SuratKeteranganDalamPerawatan.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRSuratKeteranganDalamPerawatanViewModel>(dokumen);

                        model.NoBukti = id;
                        model.TglLahirYear = identitas.TglLahir.Value.ToString("yyyy");
                        model.TglLahirMounth = identitas.TglLahir.Value.ToString("MM");
                        model.TglLahirDay = identitas.TglLahir.Value.ToString("dd");
                        int a = Convert.ToInt32(Convert.ToDouble(model.TglLahirYear));
                        int b = Convert.ToInt32(Convert.ToDouble(model.TglLahirMounth));
                        int c = Convert.ToInt32(Convert.ToDouble(model.TglLahirDay));
                        DateTime Birth = new DateTime(a, b, c);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;
                        model.Saya = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.SayaUmur = Years.ToString();
                        model.SayaJK = (identitas.JenisKelamin == "M" ? "Laki" : "Perempuan");
                        model.SayaAlamat = identitas.Alamat;
                        model.JenisKerjasama = (identitas.JenisKerjasama == null ? "" : identitas.JenisKerjasama);
                        model.Noreg = id;
                        model.NRM = (nrm == null ? "" : nrm);
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));

                        var dokterpelaksana = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (dokterpelaksana != null) model.DokterNama = dokterpelaksana.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.TglLahir = identitas.TglLahir.Value.ToString("yyyy, MM, DD");
                        DateTime Birth = new DateTime(1954, 7, 30);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;
                        model.Saya = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.SayaUmur = Years.ToString();
                        model.SayaJK = (identitas.JenisKelamin == "M" ? "Laki" : "Perempuan");
                        model.SayaAlamat = identitas.Alamat;
                        model.JenisKerjasama = (identitas.JenisKerjasama == null ? "" : identitas.JenisKerjasama);
                        model.Noreg = id;
                        model.NRM = (nrm == null ? "" : nrm);
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                        if (soap != null)
                        {
                            model.Diagnosa = " SOAP A : " + soap.CPPT_A + "\n";
                            model.Tindakan = " SOAP P : " + soap.CPPT_P  + "\n";
                            model.Therapy = " Intruksi : " + soap.CPPT_I + "\n";
                        }
                        else
                        {
                            model.Diagnosa = "SOAP A : -";
                            model.Tindakan = "SOAP P : -";
                            model.Therapy = "Intruksi : -";
                        }
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRSuratKeteranganDalamPerawatanViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.SuratKeteranganDalamPerawatan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<SuratKeteranganDalamPerawatan>(item);
                        s.SuratKeteranganDalamPerawatan.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Surat Keterangan Dalam Perawatan";
                    }
                    else
                    {
                        model = IConverter.Cast<SuratKeteranganDalamPerawatan>(item);
                        s.SuratKeteranganDalamPerawatan.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Surat Keterangan Dalam Perawatan";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}