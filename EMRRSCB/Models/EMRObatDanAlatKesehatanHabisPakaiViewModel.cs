﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRObatDanAlatKesehatanHabisPakaiViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Diagnosa { get; set; }
        public string Tindakan { get; set; }
        public string Deskripsi { get; set; }
        public string DeskripsiNama { get; set; }
        public string Jenis { get; set; }
        public string Pemakaian { get; set; }
        public string Username { get; set; }

        public int MODEVIEW { get; set; }
    }
}