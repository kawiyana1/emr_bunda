﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class RegistrasiController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                // G E T  D A T A B A S E
                ResultSS result;
                using (var e = new EMREntities())
                {
                    using (var s = new SIM_Entities())
                    {
                        if (Request.Cookies["EMRTipePelayanan"].Value == "RI" && (Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC036" ||
                            Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC037" || Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC038" ||
                            Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC040" || Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC042"))
                        {
                            IQueryable<Pelayanan_DataRegPasienRI> p = s.Pelayanan_DataRegPasienRI/*.Where(x => x.RawatInap == true)*/;

                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["EMRSectionIDPelayanan"].Value);
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterRawatID.Contains(@0)", filter[26]);
                            }
                            

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiViewModel>(x);
                                var sudahinputemr = e.trDokumenAssesmenPasien.Where(y => y.NoReg == x.NoReg && y.SectionID == x.SectionID && y.Batal == false).FirstOrDefault();
                                if (sudahinputemr != null)
                                {
                                    m.SudahInputEMR = true;
                                }
                                else
                                {
                                    m.SudahInputEMR = false;
                                }
                                m.Jam_View = x.JamReg.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                                m.Nama_Supplier = x.DokterRawat;
                                m.JenisKelamin = x.NRM_JenisKelamin;
                                m.NoBed = x.NoBed;
                                m.Kamar = x.Kamar;
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else if (Request.Cookies["EMRTipePelayanan"].Value == "RJ" && Request.Cookies["EMRSectionIDPelayanan"].Value == "SEC002")
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg.Where(x => x.Batal == false).OrderBy(x => x.Jam);
                            if (filter[24] != "True")
                            {
                                p = p.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["EMRSectionIDPelayanan"].Value);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);
                            //if (filter[26] != null && filter[26] != "")
                            //{
                            //    p = p.Where($"DokterRawatID.Contains(@0)", filter[26]);
                            //}

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{"Jam"} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiViewModel>(x);
                                var sudahinputemr = e.trDokumenAssesmenPasien.Where(y => y.NoReg == x.NoReg && y.SectionID == x.SectionID && y.Batal == false).FirstOrDefault();
                                if (sudahinputemr != null)
                                {
                                    m.SudahInputEMR = true;
                                }
                                else
                                {
                                    m.SudahInputEMR = false;
                                }
                                m.Jam_View = x.Jam.ToString("dd/MM/yyyy" + " - " + "HH\":\"mm");
                                m.JamReg = x.Jam;
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.Nama_Supplier = x.NamaDOkter;
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else if (Request.Cookies["EMRTipePelayanan"].Value == "RJ")
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg.Where(x => x.Batal == false);
                            if (filter[24] != "True")
                            {
                                p = p.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["EMRSectionIDPelayanan"].Value);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterRawatID.Contains(@0)", filter[26]);
                            }

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiViewModel>(x);
                                var sudahinputemr = e.trDokumenAssesmenPasien.Where(y => y.NoReg == x.NoReg && y.SectionID == x.SectionID && y.Batal == false).FirstOrDefault();
                                if (sudahinputemr != null)
                                {
                                    m.SudahInputEMR = true;
                                }
                                else
                                {
                                    m.SudahInputEMR = false;
                                }
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.Nama_Supplier = x.NamaDOkter;
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg.Where(x => x.Batal == false);
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["EMRSectionIDPelayanan"].Value);
                            
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiViewModel>(x);
                                var sudahinputemr = e.trDokumenAssesmenPasien.Where(y => y.NoReg == x.NoReg && y.SectionID == x.SectionID && y.Batal == false).FirstOrDefault();
                                if (sudahinputemr != null)
                                {
                                    m.SudahInputEMR = true;
                                }
                                else
                                {
                                    m.SudahInputEMR = false;
                                }
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.Nama_Supplier = x.NamaDOkter;
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                    }
                }
                //return JsonConvert.SerializeObject(result);
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.Section = Request.Cookies["EMRSectionNamaPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new RegistrasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        //var noreg = s.GetRegistrasiBaru(DateTime.Today).FirstOrDefault();
                        var pasien = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
                        if (pasien == null) return JsonHelper.JsonMsgError("Pasien tidak ditemukan.");
                        pasien.PenanggungNRM = item.PenanggungNRM;
                        pasien.PenanggungNama = item.PenanggungNama;
                        pasien.PenanggungAlamat = item.PenanggungAlamat;
                        pasien.PenanggungPhone = item.PenanggungTelp;

                        var m = IConverter.Cast<SIMtrRegistrasi>(item);
                        //m.NoReg = noreg;
                        m.TglReg = DateTime.Today;
                        m.JamReg = DateTime.Now;
                        m.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        m.JenisKerjasamaID = pasien.JenisKerjasamaID;
                        m.CustomerKerjasamaID = pasien.CustomerKerjasamaID;
                        m.UmurThn = DateTime.Today.Year - pasien.TglLahir.Value.Year;
                        m.UmurBln = (Math.Abs(DateTime.Today.Month - pasien.TglLahir.Value.Month) + (m.UmurBln * 30) + (12 * (DateTime.Today.Year - pasien.TglLahir.Value.Year))) % 12;
                        m.UmurHr = (byte)((Math.Abs(DateTime.Today.Day - pasien.TglLahir.Value.Day) + (365 * (DateTime.Today.Year - pasien.TglLahir.Value.Year))) % 365);
                        s.SIMtrRegistrasi.Add(m);
                        s.SaveChanges();
                        result = new ResultSS(1);

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            //Activity = $"Registrasi Create {noreg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}