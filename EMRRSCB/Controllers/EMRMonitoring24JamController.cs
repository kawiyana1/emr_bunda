﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRMonitoring24JamController : Controller
    {
        // GET: EMRMonitoring24Jam
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRMonitoring24JamViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.Monitoring24Jam.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRMonitoring24JamViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokter1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter1);
                        if (dokter1 != null) model.Dokter1Nama = dokter1.NamaDOkter;

                        var dokter2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter2);
                        if (dokter2 != null) model.Dokter2Nama = dokter2.NamaDOkter;

                        var dokter3 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter3);
                        if (dokter3 != null) model.Dokter3Nama = dokter3.NamaDOkter;

                        var dokter4 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter4);
                        if (dokter4 != null) model.Dokter4Nama = dokter4.NamaDOkter;

                        var dokter5 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter5);
                        if (dokter5 != null) model.Dokter5Nama = dokter5.NamaDOkter;


                        #region === Detail Obat
                        model.Obat_List = new ListDetail<EMRMonitoring24Jam_DetailObatDetailModel>();
                        var detail_1 = s.Monitoring24Jam_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_DetailObatDetailModel>(x);
                            var obat1 = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.Enteral);
                            if (obat1 != null)
                            {
                                y.EnteralNama = obat1.Nama_Barang;
                            }

                            var obat2 = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.Parental);
                            if (obat2 != null)
                            {
                                y.ParentalNama = obat2.Nama_Barang;
                            }
                            model.Obat_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail
                        model.Detail_List = new ListDetail<EMRMonitoring24Jam_DetailDetailModel>();
                        var detail_2 = s.Monitoring24Jam_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_DetailDetailModel>(x);
                            //var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Edukator);
                            //if (dokter != null)
                            //{
                            //    y.EdukatorNama = dokter.NamaDOkter;
                            //}
                            model.Detail_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Catatan
                        model.CttPerkmbngan_List = new ListDetail<EMRMonitoring24Jam_CatatanPerkembanganDetailModel>();
                        var detail_3 = s.Monitoring24Jam_CatatanPerkembangan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_CatatanPerkembanganDetailModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Nama);
                            if (dokter != null)
                            {
                                y.NamaNama = dokter.NamaDOkter;
                            }
                            model.CttPerkmbngan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Tindakan
                        model.Tindakan_List = new ListDetail<EMRMonitoring24Jam_TindakanKeperawatanDetailModel>();
                        var detail_4 = s.Monitoring24Jam_TindakanKeperawatan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_TindakanKeperawatanDetailModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Nama);
                            if (dokter != null)
                            {
                                y.NamaNama = dokter.NamaDOkter;
                            }
                            model.Tindakan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Penilaian
                        model.Penilaian_List = new ListDetail<EMRMonitoring24Jam_PenilaianResikoDetailModel>();
                        var detail_5 = s.Monitoring24Jam_PenilaianResiko.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_5)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_PenilaianResikoDetailModel>(x);
                            model.Penilaian_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Nyeri
                        model.Nyeri_List = new ListDetail<EMRMonitoring24Jam_NyeriPasienDetailModel>();
                        var detail_6 = s.Monitoring24Jam_NyeriPasien.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_6)
                        {
                            var y = IConverter.Cast<EMRMonitoring24Jam_NyeriPasienDetailModel>(x);
                            model.Nyeri_List.Add(false, y);
                        }
                        #endregion
                       
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.ArteriLine_Tanggal = DateTime.Today;
                        model.CVC_Tanggal = DateTime.Today;
                        model.Drain_Tanggal = DateTime.Today;
                        model.IVLine_Tanggal = DateTime.Today;
                        model.Luka_Tanggal = DateTime.Today;
                        model.NGT_Tanggal = DateTime.Today;
                        model.OTT_Tanggal = DateTime.Today;
                        model.SwanzGanz_Tanggal = DateTime.Today;
                        model.UrinKateter_Tanggal = DateTime.Today;
                        model.WSD_Tanggal = DateTime.Today;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRMonitoring24JamViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.Monitoring24Jam.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<Monitoring24Jam>(item);
                        s.Monitoring24Jam.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Monitor 24 Jam";
                    }
                    else
                    {
                        model = IConverter.Cast<Monitoring24Jam>(item);
                        s.Monitoring24Jam.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Monitor 24 Jam";
                    }
                    #region === Detail Obat
                    if (item.Obat_List == null) item.Obat_List = new ListDetail<EMRMonitoring24Jam_DetailObatDetailModel>();
                    item.Obat_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Obat_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list = item.Obat_List;
                    var real_list = s.Monitoring24Jam_DetailObat.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_DetailObat.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_DetailObat();
                            s.Monitoring24Jam_DetailObat.Add(IConverter.Cast<Monitoring24Jam_DetailObat>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_DetailObat>(x.Model);
                            s.Monitoring24Jam_DetailObat.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    #region === Detail 
                    if (item.Detail_List == null) item.Detail_List = new ListDetail<EMRMonitoring24Jam_DetailDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Detail_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list1 = item.Detail_List;
                    var real_list1 = s.Monitoring24Jam_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_Detail();
                            s.Monitoring24Jam_Detail.Add(IConverter.Cast<Monitoring24Jam_Detail>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_Detail>(x.Model);
                            s.Monitoring24Jam_Detail.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    #region === Detail Catatan
                    if (item.CttPerkmbngan_List == null) item.CttPerkmbngan_List = new ListDetail<EMRMonitoring24Jam_CatatanPerkembanganDetailModel>();
                    item.CttPerkmbngan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.CttPerkmbngan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list2 = item.CttPerkmbngan_List;
                    var real_list2 = s.Monitoring24Jam_CatatanPerkembangan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_CatatanPerkembangan.Remove(x);
                    }

                    foreach (var x in new_list2)
                    {
                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_CatatanPerkembangan();
                            s.Monitoring24Jam_CatatanPerkembangan.Add(IConverter.Cast<Monitoring24Jam_CatatanPerkembangan>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_CatatanPerkembangan>(x.Model);
                            s.Monitoring24Jam_CatatanPerkembangan.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    #region === Detail Tindakan
                    if (item.Tindakan_List == null) item.Tindakan_List = new ListDetail<EMRMonitoring24Jam_TindakanKeperawatanDetailModel>();
                    item.Tindakan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Tindakan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list3 = item.Tindakan_List;
                    var real_list3 = s.Monitoring24Jam_TindakanKeperawatan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list3)
                    {
                        var m = new_list3.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_TindakanKeperawatan.Remove(x);
                    }

                    foreach (var x in new_list3)
                    {
                        var _m = real_list3.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_TindakanKeperawatan();
                            s.Monitoring24Jam_TindakanKeperawatan.Add(IConverter.Cast<Monitoring24Jam_TindakanKeperawatan>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_TindakanKeperawatan>(x.Model);
                            s.Monitoring24Jam_TindakanKeperawatan.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    #region === Detail Penilaian
                    if (item.Penilaian_List == null) item.Penilaian_List = new ListDetail<EMRMonitoring24Jam_PenilaianResikoDetailModel>();
                    item.Penilaian_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Penilaian_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list4 = item.Penilaian_List;
                    var real_list4 = s.Monitoring24Jam_PenilaianResiko.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list4)
                    {
                        var m = new_list4.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_PenilaianResiko.Remove(x);
                    }

                    foreach (var x in new_list4)
                    {
                        var _m = real_list4.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_PenilaianResiko();
                            s.Monitoring24Jam_PenilaianResiko.Add(IConverter.Cast<Monitoring24Jam_PenilaianResiko>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_PenilaianResiko>(x.Model);
                            s.Monitoring24Jam_PenilaianResiko.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    #region === Detail Nyeri
                    if (item.Nyeri_List == null) item.Nyeri_List = new ListDetail<EMRMonitoring24Jam_NyeriPasienDetailModel>();
                    item.Nyeri_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Nyeri_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName(); ;
                    }
                    var new_list5 = item.Nyeri_List;
                    var real_list5 = s.Monitoring24Jam_NyeriPasien.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list5)
                    {
                        var m = new_list5.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Monitoring24Jam_NyeriPasien.Remove(x);
                    }

                    foreach (var x in new_list5)
                    {
                        var _m = real_list5.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new Monitoring24Jam_NyeriPasien();
                            s.Monitoring24Jam_NyeriPasien.Add(IConverter.Cast<Monitoring24Jam_NyeriPasien>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<Monitoring24Jam_NyeriPasien>(x.Model);
                            s.Monitoring24Jam_NyeriPasien.AddOrUpdate(_m);
                        }
                    }
                    #endregion
                   
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}