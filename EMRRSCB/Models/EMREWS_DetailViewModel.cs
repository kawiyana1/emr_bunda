﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMREWS_DetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string TanggalManual { get; set; }
        public string JamManual { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public string Oksigen { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Kesadaran { get; set; }
        public string Suhu { get; set; }
        public string TotalSkor { get; set; }
        public string Username { get; set; }

        public int MODEVIEW { get; set; }
    }
}