﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRIntervensiDanPengkajianUlangNyeriController : Controller
    {
        // GET: EMRIntervensiDanPengkajianUlangNyeri
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.IntervensiDanPengkajianUlangNyeri.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.InterPengkajianNyeri_List = new ListDetail<EMRIntervensiDanPengkajianUlangNyeriModalDetail>();
                        var detail_1 = s.IntervensiDanPengkajianUlangNyeri.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRIntervensiDanPengkajianUlangNyeriModalDetail>(x);
                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == x.Petugas);
                            if (dokter != null)
                            {
                                y.PetugasNama = dokter.NamaDOkter;
                            }

                            var dokter1 = sim.mDokter.FirstOrDefault(xx => xx.DokterID == x.PetugasNonFarmakologi);
                            if (dokter1 != null)
                            {
                                y.PetugasNonFarmakologiNama = dokter1.NamaDOkter;
                            }
                            model.InterPengkajianNyeri_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.IntervensiDanPengkajianUlangNyeri.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Intervensi Dan Pengkajian Ulang Nyeri ";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Intervensi Dan Pengkajian Ulang Nyeri ";
                    }

                    if (item.InterPengkajianNyeri_List == null) item.InterPengkajianNyeri_List = new ListDetail<EMRIntervensiDanPengkajianUlangNyeriModalDetail>();
                    item.InterPengkajianNyeri_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.InterPengkajianNyeri_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Tanggal = DateTime.Now;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.InterPengkajianNyeri_List;
                    var real_list = s.IntervensiDanPengkajianUlangNyeri.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.IntervensiDanPengkajianUlangNyeri.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.IntervensiDanPengkajianUlangNyeri.Add(IConverter.Cast<IntervensiDanPengkajianUlangNyeri>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.SkorNyeri = x.Model.SkorNyeri;
                            _m.SkorSedasi = x.Model.SkorSedasi;
                            _m.TekananDarah = x.Model.TekananDarah;
                            _m.Suhu = x.Model.Suhu;
                            _m.Respitrasi = x.Model.Respitrasi;
                            _m.Petugas = x.Model.Petugas;
                            _m.VerifikasiPetugas = x.Model.VerifikasiPetugas;
                            _m.Tgl_Farma = x.Model.Tgl_Farma;
                            _m.Jam_Farma = x.Model.Jam_Farma;
                            _m.NamaObat = x.Model.NamaObat;
                            _m.Dosis = x.Model.Dosis;
                            _m.Rute = x.Model.Rute;
                            _m.IntervensiNonFarmakologi = x.Model.IntervensiNonFarmakologi;
                            _m.PetugasNonFarmakologi = x.Model.PetugasNonFarmakologi;
                            _m.VerifikasiNonFarmakologi = x.Model.VerifikasiNonFarmakologi;
                            _m.WaktuPengkajian = x.Model.WaktuPengkajian;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}