﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPoliklinikKandunganDanKebidananController : Controller
    {
        // GET: EMRPoliklinikKandunganDanKebidanan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRPoliklinikKandunganDanKebidananViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.PoliklinikKandunganDanKebidanan.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPoliklinikKandunganDanKebidananViewModel>(dokumen);

                        model.NoBukti = id;
                        model.Username = User.Identity.GetUserName();

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (perawat != null) model.BidanNama = perawat.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiTSDokter);
                        if (jawabankonsultasikpd != null) model.KonsultasiTSDokterNama = jawabankonsultasikpd.NamaDOkter;

                        var jawabankonsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiDokter);
                        if (jawabankonsultasihormatkami != null) model.KonsultasiDokterNama = jawabankonsultasihormatkami.NamaDOkter;

                        var konsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokter);
                        if (konsultasikpd != null) model.JawabanKonsultasiTSDokterNama = konsultasikpd.NamaDOkter;

                        var konsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiDokter);
                        if (konsultasihormatkami != null) model.JawabanKonsultasiDokterNama = konsultasihormatkami.NamaDOkter;

                        #region === Detail Riwayat Kehamilan
                        model.RiwayatKehamilan_List = new ListDetail<EMRRiwayatKehamilanModelDetail>();
                        var detail_1 = s.RiwayatKehamilan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRRiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Rencana Kerja
                        model.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRRencanaKerjaDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.RencanaKerja_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.BioPsikososialIstirahatTidurJam = DateTime.Now;
                        model.JawabanKonsultasiJam = DateTime.Now;
                        model.KosultasiJam = DateTime.Now;
                        model.KontrolTanggal = DateTime.Today;
                        model.Username = User.Identity.GetUserName();
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListPoliKandungan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPoliKandungan()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRPoliklinikKandunganDanKebidananViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PoliklinikKandunganDanKebidanan.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPoliklinikKandunganDanKebidananViewModel>(dokumen);
                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (perawat != null) model.BidanNama = perawat.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiTSDokter);
                        if (jawabankonsultasikpd != null) model.KonsultasiTSDokterNama = jawabankonsultasikpd.NamaDOkter;

                        var jawabankonsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiDokter);
                        if (jawabankonsultasihormatkami != null) model.KonsultasiDokterNama = jawabankonsultasihormatkami.NamaDOkter;

                        var konsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokter);
                        if (konsultasikpd != null) model.JawabanKonsultasiTSDokterNama = konsultasikpd.NamaDOkter;

                        var konsultasihormatkami = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiDokter);
                        if (konsultasihormatkami != null) model.JawabanKonsultasiDokterNama = konsultasihormatkami.NamaDOkter;

                        #region === Detail Riwayat Kehamilan
                        model.RiwayatKehamilan_List = new ListDetail<EMRRiwayatKehamilanModelDetail>();
                        var detail_1 = s.RiwayatKehamilan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRRiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Rencana Kerja
                        model.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRRencanaKerjaDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.RencanaKerja_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.VerifikasiDPJP = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListPoliKandungan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPoliKandungan()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPoliklinikKandunganDanKebidananViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.PoliklinikKandunganDanKebidanan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PoliklinikKandunganDanKebidanan>(item);
                        s.PoliklinikKandunganDanKebidanan.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Woi Create Poliklinik KandunganDanKebidanan";
                    }
                    else
                    {
                        model = IConverter.Cast<PoliklinikKandunganDanKebidanan>(item);
                        s.PoliklinikKandunganDanKebidanan.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Woi Update Poliklinik KandunganDanKebidanan";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJP;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    #region === Detail Riwayat Kandungan
                    if (item.RiwayatKehamilan_List == null) item.RiwayatKehamilan_List = new ListDetail<EMRRiwayatKehamilanModelDetail>();
                    item.RiwayatKehamilan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RiwayatKehamilan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.RiwayatKehamilan_List;
                    var real_list = s.RiwayatKehamilan_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RiwayatKehamilan_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RiwayatKehamilan_Detail.Add(IConverter.Cast<RiwayatKehamilan_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Abortus = x.Model.Abortus;
                            _m.Aterm = x.Model.Aterm;
                            _m.BBL = x.Model.BBL;
                            _m.Cacat = x.Model.Cacat;
                            _m.JenisKelaminLaki = x.Model.JenisKelaminLaki;
                            _m.JenisKelaminPerempuan = x.Model.JenisKelaminPerempuan;
                            _m.JenisPartus = x.Model.JenisPartus;
                            _m.Komplikasi = x.Model.Komplikasi;
                            _m.Meninggal = x.Model.Meninggal;
                            _m.Nakes = x.Model.Nakes;
                            _m.Normal = x.Model.Normal;
                            _m.Non = x.Model.Non;
                            _m.Prematur = x.Model.Prematur;
                        }
                    }
                    #endregion

                    #region === Detail Rencana Kerja
                    if (item.RencanaKerja_List == null) item.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                    item.RencanaKerja_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RencanaKerja_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.RencanaKerja_List;
                    var real_list1 = s.RencanaKerja_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RencanaKerja_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RencanaKerja_Detail.Add(IConverter.Cast<RencanaKerja_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = DateTime.Today;
                            _m.DaftarMasalah = x.Model.DaftarMasalah;
                            _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                            _m.Target = x.Model.Target;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}