﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRLembarObservasiKebidanananDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TanggalManual { get; set; }
        public string JamManual { get; set; }
        public string DJJ { get; set; }
        public string HIS { get; set; }
        public string Keterangan { get; set; }
    }
}