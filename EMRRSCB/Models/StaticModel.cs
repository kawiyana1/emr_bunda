﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSCB.Models
{
    public static class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        public static string ListSectionCustom
        {
            get
            {
                var r = "";
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        r += $"<optgroup label=\"{group.Text}\">";
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r += $"<option value=\"{item.SectionID}\">{item.SectionName}</option>";
                        }
                        r += $"</optgroup>";
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListStatusRacik
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.mStatusRacikan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Nama
                    });
                }
                r.Insert(0, new SelectListItem());
                return r;
            }
        }

        public static List<SelectListItem> ListKonjunctiva
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pucat", Value = "Pucat" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListSclera
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Putih", Value = "Putih" });
                result.Add(new SelectListItem() { Text = "Kuning", Value = "Kuning" });
                result.Add(new SelectListItem() { Text = "Merah", Value = "Merah" });
                return result;
            }
        }

        public static List<SelectListItem> ListBentuk
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Simetris", Value = "Simetris" });
                result.Add(new SelectListItem() { Text = "Asimetris", Value = "Asimetris" });
                return result;
            }
        }

        public static List<SelectListItem> ListPutingSusu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Menonjol", Value = "Menonjol" });
                result.Add(new SelectListItem() { Text = "Datar", Value = "Datar" });
                result.Add(new SelectListItem() { Text = "Masuk", Value = "Masuk" });
                return result;
            }
        }

        public static List<SelectListItem> ListTingkatPendidikan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "TK", Value = "TK" });
                result.Add(new SelectListItem() { Text = "SD", Value = "SD" });
                result.Add(new SelectListItem() { Text = "SMP", Value = "SMP" });
                result.Add(new SelectListItem() { Text = "SMA", Value = "SMA" });
                result.Add(new SelectListItem() { Text = "Akademi", Value = "Akademi" });
                result.Add(new SelectListItem() { Text = "Sarjana", Value = "Sarjana" });
                result.Add(new SelectListItem() { Text = "Lain- Lain", Value = "Lain- Lain" });
                return result;
            }
        }
        public static List<SelectListItem> ListPotensial
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Proses penyakit", Value = "Proses penyakit" });
                result.Add(new SelectListItem() { Text = "Pengobatan/Tindakan", Value = "Pengobatan/Tindakan" });
                result.Add(new SelectListItem() { Text = "Terapi/Obat", Value = "Terapi/Obat" });
                result.Add(new SelectListItem() { Text = "Nutrisi", Value = "Nutrisi" });
                result.Add(new SelectListItem() { Text = "Lain-Lain", Value = "Lain-Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdaTidakAda
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisOperasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Khusus", Value = "Khusus" });
                result.Add(new SelectListItem() { Text = "Besar", Value = "Besar" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Kecil", Value = "Kecil" });
                result.Add(new SelectListItem() { Text = "Emergency", Value = "Emergency" });
                result.Add(new SelectListItem() { Text = "Eloctive", Value = "Eloctive" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisAnestesi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Umum", Value = "Umum" });
                result.Add(new SelectListItem() { Text = "Spinal", Value = "Spinal" });
                result.Add(new SelectListItem() { Text = "Epidaral", Value = "Epidaral" });
                result.Add(new SelectListItem() { Text = "BSP*", Value = "BSP*" });
                result.Add(new SelectListItem() { Text = "CSE**", Value = "CSE**" });
                result.Add(new SelectListItem() { Text = "Lokal", Value = "Lokal" });
                return result;
            }
        }

        public static List<SelectListItem> ListJaringanDikirim
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "PA", Value = "PA" });
                result.Add(new SelectListItem() { Text = "KULTUR", Value = "KULTUR" });
                return result;
            }
        }

        public static List<SelectListItem> ListMOPAKULTUR
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "MO", Value = "MO" });
                result.Add(new SelectListItem() { Text = "PA", Value = "PA" });
                result.Add(new SelectListItem() { Text = "Kultur", Value = "Kultur" });
                return result;
            }
        }


        public static List<SelectListItem> ListUtuhRobek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Utuh", Value = "Utuh" });
                result.Add(new SelectListItem() { Text = "Robek", Value = "Robek" });
                result.Add(new SelectListItem() { Text = "Sampai Dasar Arah Robekan", Value = "Sampai Dasar Arah Robekan" });
                return result;
            }
        }

        public static List<SelectListItem> ListUtuhRapuh
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Utuh", Value = "Utuh" });
                result.Add(new SelectListItem() { Text = "Rapuh", Value = "Rapuh" });
                return result;
            }
        }


        public static List<SelectListItem> ListCukupKurang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cukup", Value = "Cukup" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                return result;
            }
        }

        public static List<SelectListItem> ListMinPlus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "+", Value = "+" });
                result.Add(new SelectListItem() { Text = "-", Value = "-" });

                return result;
            }
        }

        public static List<SelectListItem> ListKelainanAda
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bandl", Value = "Bandl" });
                result.Add(new SelectListItem() { Text = "Distensi", Value = "Distensi" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKelainan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Lecet", Value = "Lecet" });
                result.Add(new SelectListItem() { Text = "Bengkak", Value = "Bengkak" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListLakiPerempuan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Laki", Value = "Laki" });
                result.Add(new SelectListItem() { Text = "Perempuan", Value = "Perempuan" });
                return result;
            }
        }

        public static List<SelectListItem> ListLetakPunggung
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Punggung Kanan", Value = "Punggung Kanan" });
                result.Add(new SelectListItem() { Text = "Punggung Kiri", Value = "Punggung Kiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListBaikLembek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Lembek", Value = "Lembek" });
                return result;
            }
        }

        public static List<SelectListItem> ListSolidKistik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Solid", Value = "Solid" });
                result.Add(new SelectListItem() { Text = "Kistik", Value = "Kistik" });
                return result;
            }
        }

        public static List<SelectListItem> ListPalpasiKelaianan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Nyeri Tekanan", Value = "Nyeri Tekanan" });
                result.Add(new SelectListItem() { Text = "Cekungan Pada Perut", Value = "Cekungan Pada Perut" });
                result.Add(new SelectListItem() { Text = "Blass Penuh", Value = "Blass Penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListPresentasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kepala", Value = "Kepala" });
                result.Add(new SelectListItem() { Text = "Bokong", Value = "Bokong" });
                result.Add(new SelectListItem() { Text = "Kosong", Value = "Kosong" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengeluaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Colostrum", Value = "Colostrum" });
                result.Add(new SelectListItem() { Text = "ASI", Value = "ASI" });
                result.Add(new SelectListItem() { Text = "Nanah", Value = "Nanah" });
                result.Add(new SelectListItem() { Text = "Darah", Value = "Darah" });
                return result;
            }
        }

        public static List<SelectListItem> SelectListGender
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "", Value = ""},
                    new SelectListItem() { Text = "Male", Value = "M"},
                    new SelectListItem() { Text = "Female", Value = "F"},
                    new SelectListItem() { Text = "Other", Value = "O"}
                };
            }
        }
        public static List<SelectListItem> SelectListCheckList
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "", Value = ""},
                    new SelectListItem() { Text = "Dilakukan", Value = "Dilakukan"},
                    new SelectListItem() { Text = "Tidak Dilakukan", Value = "Tidak Dilakuan"},

                };
            }
        }

        public static List<SelectListItem> SelectListAgama
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mAgama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Agama,
                        Value = x.AgamaID.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectPilihICD
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mICD.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KodeICD,
                        Value = x.Descriptions.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        //public static List<SelectListItem> SelectListJenisKerjasama
        //{
        //    get
        //    {
        //        List<SelectListItem> result;
        //        using (var s = new EMREntities())
        //        {
        //            result = s.SIMmJenisKerjasama.ToList().ConvertAll(x => new SelectListItem()
        //            {
        //                Text = x.JenisKerjasama,
        //                Value = x.JenisKerjasamaID.ToString(),
        //                Selected = x.JenisKerjasamaID == 1
        //            });
        //            result.Insert(0, new SelectListItem());
        //        }
        //        return result;
        //    }
        //}

        public static List<SelectListItem> SelectListPerusahaan
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.mCustomer.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Customer,
                        Value = x.Customer_ID.ToString()
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListDokumenAssesmenLokal(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {

                    var get_section_all = s.vw_mDokumen_OK.Where(e => e.JenisDokumen == "LOKAL" ).ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }

        public static List<SelectListItem> SelectListDokumenAssesmenLengkap(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {

                    var get_section_all = s.vw_mDokumen_OK.Where(e => e.JenisDokumen == "GENERAL").ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }

        public static List<SelectListItem> SelectListDokumenAssesmen(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var get_section_tipepelayanan = sim.SIMmSection.FirstOrDefault(e => e.SectionID == section);
                    if (get_section_tipepelayanan != null)
                    {
                        if (section == "SEC002")
                        {
                            var qq = s.mDokumen.Where(e => e.DokumenID != "CPPT" && e.SectionID == "SEC002").ToList();
                            foreach (var x in qq)
                            {
                                dokumen.Add(new DokumenViewModel()
                                {
                                    Value = x.DokumenID,
                                    Text = x.NamaDokumen,
                                    No = (int)x.NoDokumen
                                });
                            }
                            r = dokumen.OrderBy("No ASC").ToList().ConvertAll(x => new SelectListItem()
                            {
                                Value = x.Value,
                                Text = x.Text
                            });
                        }
                        else
                        {
                            var get_section_id = s.mDokumen.Where(e => e.DokumenID != "CPPT" && e.SectionID == section).ToList();
                            foreach (var x in get_section_id)
                            {
                                dokumen.Add(new DokumenViewModel()
                                {
                                    Value = x.DokumenID,
                                    Text = x.NamaDokumen,
                                    No = (int)x.NoDokumen
                                });
                            }
                            var pp = s.mDokumen.Where(e => e.DokumenID != "CPPT" && e.SectionID == get_section_tipepelayanan.TipePelayanan).ToList();
                            foreach (var x in pp)
                            {
                                dokumen.Add(new DokumenViewModel()
                                {
                                    Value = x.DokumenID,
                                    Text = x.NamaDokumen,
                                    No = (int)x.NoDokumen
                                });
                            }
                            r = dokumen.OrderBy("No ASC").ToList().ConvertAll(x => new SelectListItem()
                            {
                                Value = x.Value,
                                Text = x.Text
                            });
                        }

                    }


                    //var get_section_all = s.mDokumen.Where(e => e.DokumenID != "CPPT" && e.SectionID == "RJ").ToList();
                    //foreach (var x in get_section_all)
                    //{
                    //    dokumen.Add(new DokumenViewModel()
                    //    {
                    //        Value = x.DokumenID,
                    //        Text = x.NamaDokumen,
                    //        No = (int)x.NoDokumen
                    //    });
                    //}

                    //r = dokumen.OrderBy("No ASC").ToList().ConvertAll(x => new SelectListItem()
                    //{
                    //    Value = x.Value,
                    //    Text = x.Text
                    //});
                }
            }
            return r;
        }

        public static List<SelectListItem> SelectListDokumenAssesmenUtama(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var tipe = sim.SIMmSection.FirstOrDefault(e => e.SectionID == section);
                    var get_section_all = s.VW_mDokumen.Where(e => e.JenisDokumen == "UTAMA").Where(vv => vv.SectionID == tipe.TipePelayanan || vv.SectionID == section).ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }

        public static List<SelectListItem> SelectListDokumenAssesmenTambahan(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var tipe = sim.SIMmSection.FirstOrDefault(e => e.SectionID == section);
                    var get_section_all = s.VW_mDokumen.Where(e => e.JenisDokumen == "TAMBAHAN" && e.SectionID != "----").Where(z => z.SectionID == tipe.TipePelayanan || z.SectionID == section).ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }

        public static List<SelectListItem> SelectListDokumenAssesmenTambahanIGD(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var tipe = sim.SIMmSection.FirstOrDefault(e => e.SectionID == section);
                    var get_section_all = s.VW_mDokumen.Where(e => e.JenisDokumen == "TAMBAHAN" && e.SectionID != "----").Where(z => z.SectionID == section).ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }
        public static List<SelectListItem> SelectListDokumenCPPT
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new EMREntities())
                {
                    r = s.mDokumen.Where(e => e.DokumenID == "CPPT").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.DokumenID,
                        Text = x.NamaDokumen
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> SelectListTemplateCPPT
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new EMREntities())
                {
                    r = s.trDokumenTemplate.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.NoBukti,
                        Text = x.NamaTemplate
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionPenunjang
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "PENUNJANG" && x.StatusAktif == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionFarmasi
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    //var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" & x.NoIP == GetLocalIPAddress).ToList();
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" && x.StatusAktif == true).ToList();
                    
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> SelectListPenanggungHubungan
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Anak", Value = "Anak"},
                    new SelectListItem(){ Text = "Orang Tua", Value = "Orang Tua"},
                    new SelectListItem(){ Text = "Saudara Kandung", Value = "Saudara Kandung"},
                    new SelectListItem(){ Text = "Suami/Istri", Value = "Suami/Istri"},
                    new SelectListItem(){ Text = "Teman", Value = "Teman"},
                    new SelectListItem(){ Text = "Pasien Sendiri", Value = "Pasien Sendiri"},
                    new SelectListItem(){ Text = "Lainnya", Value = "Lainnya"},
                };
                return r;
            }
        }
        public static List<SelectListItem> ListSatuanPuyer
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "KAPSUL", Value = "KAPSUL"},
                    new SelectListItem(){ Text = "PUYER", Value = "PUYER"},
                    new SelectListItem(){ Text = "CREAME", Value = "CREAME"},
                };
                return r;
            }
        }




        // EMR KAWI

        public static List<SelectListItem> ListYaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListSetujuMenolak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Setuju", Value = "Setuju" });
                result.Add(new SelectListItem() { Text = "Menolak", Value = "Menolak" });
                return result;
            }
        }

        public static List<SelectListItem> ListRekonsiliasiTransfer
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Stop", Value = "Stop" });
                result.Add(new SelectListItem() { Text = "Lanjut Aturan Pakai Sama", Value = "Lanjut Aturan Pakai Sama" });
                result.Add(new SelectListItem() { Text = "Lanjut Aturan Pakai Berubah", Value = "Lanjut Aturan Pakai Berubah" });
                return result;
            }
        }

        public static List<SelectListItem> ListKebutuhanKomunikasiOrtu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ayah", Value = "Ayah" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKompleksReaksi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kompleks", Value = "Kompleks" });
                result.Add(new SelectListItem() { Text = "Reaksi", Value = "Reaksi" });
                return result;
            }
        }

        public static List<SelectListItem> ListPositifNegatif
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Positif", Value = "Positif" });
                result.Add(new SelectListItem() { Text = "Negatif", Value = "Negatif" });
                return result;
            }
        }


        public static List<SelectListItem> ListBahasa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Aktif", Value = "Aktif" });
                result.Add(new SelectListItem() { Text = "Pasif", Value = "Pasif" });
                return result;
            }
        }

        public static List<SelectListItem> ListIndonesiaLainnya
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Indonesia", Value = "Indonesia" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListBicara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Serangan awal gangguan bicara", Value = "Serangan awal gangguan bicara" });
                return result;
            }
        }

        public static List<SelectListItem> List012
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                return result;
            }
        }
        public static List<SelectListItem> List0123
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                return result;
            }
        }

        public static List<SelectListItem> List01
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoAlatBantu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Berpegangan Pada Perabot", Value = "Berpegangan Pada Perabot" });
                result.Add(new SelectListItem() { Text = "Tongkat/Alat Penopang", Value = "Tongkat/Alat Penopang" });
                result.Add(new SelectListItem() { Text = "Tidak ada/kursi roda/perawat/tirah baring", Value = "Tidak ada/kursi roda/perawat/tirah baring" });
                return result;
            }
        }

        public static List<SelectListItem> ListGayaBerjalan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Terganggu", Value = "Terganggu" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                result.Add(new SelectListItem() { Text = "Normal/tirah baring/ imobilisasi", Value = "Normal/tirah baring/ imobilisasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListSiklus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sering lupa akan keterbatasan yang memiliki", Value = "Sering lupa akan keterbatasan yang memiliki" });
                result.Add(new SelectListItem() { Text = "Sadar akan kemampuan diri sendiri", Value = "Sadar akan kemampuan diri sendiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidakTidakYakin
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak Yakin", Value = "Tidak Yakin" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                return result;
            }
        }

        public static List<SelectListItem> ListBeratSkrining
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1-5 kg", Value = "1-5 kg" });
                result.Add(new SelectListItem() { Text = "6-10 kg", Value = "6-10 kg" });
                result.Add(new SelectListItem() { Text = "11-15 kg", Value = "11-15 kg" });
                result.Add(new SelectListItem() { Text = "> 15 kg", Value = "> 15 kg" });
                result.Add(new SelectListItem() { Text = "Tidak tahu berapa kilogram penurunannya", Value = "Tidak tahu berapa kilogram penurunannya" });
                return result;
            }
        }

        public static List<SelectListItem> ListTerusmenerus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Terus Menerus", Value = "Terus Menerus" });
                result.Add(new SelectListItem() { Text = "Hilang Timbul", Value = "Hilang Timbul" });
                return result;
            }
        }

        public static List<SelectListItem> ListStatusNikah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Single", Value = "Single" });
                result.Add(new SelectListItem() { Text = "Menikah", Value = "Menikah" });
                result.Add(new SelectListItem() { Text = "Bercerai", Value = "Bercerai" });
                result.Add(new SelectListItem() { Text = "Janda/ Duda", Value = "Janda/ Duda" });
                return result;
            }
        }

        public static List<SelectListItem> ListSumberData
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien", Value = "Pasien" });
                result.Add(new SelectListItem() { Text = "Keluarga", Value = "Keluarga" });
                result.Add(new SelectListItem() { Text = "Teman", Value = "Teman" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListPertelponDatang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pertelpon", Value = "Pertelpon" });
                result.Add(new SelectListItem() { Text = "Datang", Value = "Datang" });
                return result;
            }
        }

        public static List<SelectListItem> ListPekerjaan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "PNS", Value = "PNS" });
                result.Add(new SelectListItem() { Text = "Swasta", Value = "Swasta" });
                result.Add(new SelectListItem() { Text = "TNI/Polri", Value = "TNI/Polri" });
                result.Add(new SelectListItem() { Text = "Wirausaha", Value = "Wirausaha" });
                result.Add(new SelectListItem() { Text = "Petani", Value = "Petani" });
                result.Add(new SelectListItem() { Text = "Tidak Bekerja", Value = "Tidak Bekerja" });
                return result;
            }
        }
        public static List<SelectListItem> ListWargaNegara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "WNI", Value = "WNI" });
                result.Add(new SelectListItem() { Text = "WNA", Value = "WNA" });
                return result;
            }
        }

        public static List<SelectListItem> ListAgama
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hindu", Value = "Hindu" });
                result.Add(new SelectListItem() { Text = "Islam", Value = "Islam" });
                result.Add(new SelectListItem() { Text = "Budha", Value = "Budha" });
                result.Add(new SelectListItem() { Text = "Kristen", Value = "Kristen" });
                result.Add(new SelectListItem() { Text = "Katolik", Value = "Katolik" });
                return result;
            }
        }

        public static List<SelectListItem> ListKebiasaanMerokok
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Merokok", Value = "Merokok" });
                result.Add(new SelectListItem() { Text = "Alkohol", Value = "Alkohol" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListLaporkanke
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Satpam", Value = "Satpam" });
                result.Add(new SelectListItem() { Text = "Supervisi", Value = "Supervisi" });
                result.Add(new SelectListItem() { Text = "MOD", Value = "MOD" });
                result.Add(new SelectListItem() { Text = "Humas", Value = "Humas" });
                return result;
            }
        }

        public static List<SelectListItem> ListBau
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Amis", Value = "Amis" });
                result.Add(new SelectListItem() { Text = "Busuk", Value = "Busuk" });
                return result;
            }
        }

        public static List<SelectListItem> ListUtuhLaserasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Utuh", Value = "Utuh" });
                result.Add(new SelectListItem() { Text = "Laserasi", Value = "Laserasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListSudahBelum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sudah", Value = "Sudah" });
                result.Add(new SelectListItem() { Text = "Belum", Value = "Belum" });
                return result;
            }
        }

        public static List<SelectListItem> ListKewaspadaan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Standart", Value = "Standart" });
                result.Add(new SelectListItem() { Text = "Contact", Value = "Contact" });
                result.Add(new SelectListItem() { Text = "Contact", Value = "Contact" });
                result.Add(new SelectListItem() { Text = "Airbone", Value = "Airbone" });
                result.Add(new SelectListItem() { Text = "Droplet", Value = "Droplet" });
                return result;
            }
        }

        public static List<SelectListItem> ListTerabaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Teraba", Value = "Tidak Teraba" });
                result.Add(new SelectListItem() { Text = "Teraba", Value = "Teraba" });
                return result;
            }
        }

        public static List<SelectListItem> ListTeraturTidakTeratur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Tidak Teratur", Value = "Tidak Teratur" });
                return result;
            }
        }

        public static List<SelectListItem> ListRiwayatKawin
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Belum Kawin", Value = "Belum Kawin" });
                result.Add(new SelectListItem() { Text = "Cerai", Value = "Cerai" });
                result.Add(new SelectListItem() { Text = "Kawin", Value = "Kawin" });
                return result;
            }
        }

        public static List<SelectListItem> ListMenggunakanTidakMenggunakan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Menggunakan", Value = "Menggunakan" });
                result.Add(new SelectListItem() { Text = "Tidak Menggunakan", Value = "Tidak Menggunakan" });
                return result;
            }
        }

        public static List<SelectListItem> ListBenturanLainnya
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Benturan", Value = "Benturan" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> Listkawinke
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                return result;
            }
        }

        public static List<SelectListItem> ListLevelAsa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                result.Add(new SelectListItem() { Text = "6", Value = "6" });
                return result;
            }
        }

        public static List<SelectListItem> List1sampai10
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                result.Add(new SelectListItem() { Text = "6", Value = "6" });
                result.Add(new SelectListItem() { Text = "7", Value = "7" });
                result.Add(new SelectListItem() { Text = "8", Value = "8" });
                result.Add(new SelectListItem() { Text = "9", Value = "9" });
                result.Add(new SelectListItem() { Text = "10", Value = "10" });
                return result;
            }
        }

        public static List<SelectListItem> ListFrekuensi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1x", Value = "1x" });
                result.Add(new SelectListItem() { Text = "2x", Value = "2x" });
                result.Add(new SelectListItem() { Text = "3x", Value = "3x" });
                result.Add(new SelectListItem() { Text = ">3x", Value = ">3x" });
                return result;
            }
        }

        public static List<SelectListItem> ListDokterKandungan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dokter Kandungan", Value = "Dokter Kandungan" });
                result.Add(new SelectListItem() { Text = "Dokter Umum", Value = "Dokter Umum" });
                result.Add(new SelectListItem() { Text = "Bidan", Value = "Bidan" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKualitasNyeri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Seperti tertusuk benda tumpul/ tajam ditindih beban", Value = "Seperti tertusuk benda tumpul/ tajam ditindih beban" });
                result.Add(new SelectListItem() { Text = "Sakit Berdenyut", Value = "Sakit Berdenyut" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListUnset
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Nyeri Akut", Value = "Nyeri Akut" });
                result.Add(new SelectListItem() { Text = "Nyeri Kronis", Value = "Nyeri Kronis" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKuatLemah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kuat", Value = "Kuat" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                return result;
            }
        }

        public static List<SelectListItem> ListCRT
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 2", Value = "< 2" });
                result.Add(new SelectListItem() { Text = "> 2", Value = "> 2" });
                return result;
            }
        }

        public static List<SelectListItem> ListWarnaKulit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Kuning", Value = "Kuning" });
                result.Add(new SelectListItem() { Text = "Pucat", Value = "Pucat" });
                return result;
            }
        }
        public static List<SelectListItem> ListPendarahan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak Terkontrol", Value = "Tidak Terkontrol" });
                return result;
            }
        }

        public static List<SelectListItem> ListBaikBuruk
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Buruk", Value = "Buruk" });
                return result;
            }
        }

        public static List<SelectListItem> ListMenangisTidakMenangis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Segera Menangis", Value = "Segera Menangis" });
                result.Add(new SelectListItem() { Text = "Tidak Segera Menangis", Value = "Tidak Segera Menangis" });
                return result;
            }
        }

        public static List<SelectListItem> ListFlaccWajah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Ekpresi Khusus senyum", Value = "Tidak Ada Ekpresi Khusus senyum" });
                result.Add(new SelectListItem() { Text = "Menyeringai, Mengerutkan dahi, tampak tidak tertarik", Value = "Menyeringai, Mengerutkan dahi, tampak tidak tertarik" });
                result.Add(new SelectListItem() { Text = "Dagu Bergetar, gigi bergetar(sering)", Value = "Dagu Bergetar, gigi bergetar(sering)" });
                return result;
            }
        }

        public static List<SelectListItem> ListFlaccKaki
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal, Rileks", Value = "Normal, Rileks" });
                result.Add(new SelectListItem() { Text = "Gelisah, tegang", Value = "Gelisah, tegang" });
                result.Add(new SelectListItem() { Text = "Menendang, kaki tertekuk", Value = "Menendang, kaki tertekuk" });
                return result;
            }
        }

        public static List<SelectListItem> ListFlaccAktivitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Berbaring tenang posisi normal gerakan mudah", Value = "Berbaring tenang posisi normal gerakan mudah" });
                result.Add(new SelectListItem() { Text = "Menggeliat, tidak bisa diam tegang", Value = "Menggeliat, tidak bisa diam tegang" });
                result.Add(new SelectListItem() { Text = "Kaku Kejang", Value = "Kaku Kejang" });
                return result;
            }
        }

        public static List<SelectListItem> ListFlaccMenanggis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Menangis", Value = "Tidak Menangis" });
                result.Add(new SelectListItem() { Text = "Merintih, merengek, kadang mengeluh", Value = "Merintih, merengek, kadang mengeluh" });
                result.Add(new SelectListItem() { Text = "Terus Menangis, berteriak, sering mengeluh", Value = "Terus Menangis, berteriak, sering mengeluh" });
                return result;
            }
        }
        public static List<SelectListItem> ListFlaccConsolability
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Rileks", Value = "Rileks" });
                result.Add(new SelectListItem() { Text = "Dapat Ditenangkan dengan sentuhan, pelukan dan bujukan", Value = "Dapat Ditenangkan dengan sentuhan, pelukan dan bujukan" });
                result.Add(new SelectListItem() { Text = "Sulit di bujuk", Value = "Sulit di bujuk" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh1
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dibawah 3 tahun", Value = "Dibawah 3 tahun" });
                result.Add(new SelectListItem() { Text = "3 -7 Tahun", Value = "3 -7 Tahun" });
                result.Add(new SelectListItem() { Text = "7 - 13 tahun", Value = "7 - 13 tahun" });
                result.Add(new SelectListItem() { Text = "> 13 Tahun", Value = "> 13 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh2
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Laki-Laki", Value = "Laki-Laki" });
                result.Add(new SelectListItem() { Text = "Perempuan", Value = "Perempuan" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh3
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kelaianan Neurologi", Value = "Kelaianan Neurologi" });
                result.Add(new SelectListItem() { Text = "Perubahan dalam oksigensi(Masalah saluran Nafas, Dehidrasi, Anemia, Anoreksia, Sinkon,Sakit Kepala, dll)", Value = "Perubahan dalam oksigensi(Masalah saluran Nafas, Dehidrasi, Anemia, Anoreksia, Sinkon,Sakit Kepala, dll)" });
                result.Add(new SelectListItem() { Text = "Kelaianan Psikis / Prilaku", Value = "Kelaianan Psikis / Prilaku" });
                result.Add(new SelectListItem() { Text = "Diagnosa Lain", Value = "Diagnosa Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh4
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Sadar terhadap keterbatasan", Value = "Tidak Sadar terhadap keterbatasan" });
                result.Add(new SelectListItem() { Text = "Mengetahui kemampuan diri", Value = "Mengetahui kemampuan diri" });
                result.Add(new SelectListItem() { Text = "Lupa Keterbatasan", Value = "Lupa Keterbatasan" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh5
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Riwayat jatuh dari tempat tidur saat bayi - anak", Value = "Riwayat jatuh dari tempat tidur saat bayi - anak" });
                result.Add(new SelectListItem() { Text = "Pasien Menggunakan Alat Bantu atau Box/ mebel", Value = "Pasien Menggunakan Alat Bantu atau Box/ mebel" });
                result.Add(new SelectListItem() { Text = "Pasien Berada di tempat tidur", Value = "Pasien Menggunakan Alat Bantu atau Box/ mebel" });
                result.Add(new SelectListItem() { Text = "Di luar ruang rawat", Value = "Di luar ruang rawat" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh6
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dalam 24 Jam", Value = "Dalam 24 Jam" });
                result.Add(new SelectListItem() { Text = "Dalam 48 jam Rawat Jatuh", Value = "Dalam 48 jam Rawat Jatuh" });
                result.Add(new SelectListItem() { Text = "> 48 Jam", Value = "> 48 Jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuh7
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bermacam - macam obat yang digunakan obat sedative(kecuali pasien ICU yang menggunakan sedasi dana paralisis), Hipnotik barbiturat fenotiasih , Antidepresan, laksan/ diuretika, narkotik", Value = "Bermacam - macam obat yang digunakan obat sedative(kecuali pasien ICU yang menggunakan sedasi dana paralisis), Hipnotik barbiturat fenotiasih , Antidepresan, laksan/ diuretika, narkotik" });
                result.Add(new SelectListItem() { Text = "Salah satu dari pengobatan diatas", Value = "Salah satu dari pengobatan diatas" });
                result.Add(new SelectListItem() { Text = "Pengobatan lain", Value = "Pengobatan lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListKebutuhanKomunikasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ayah", Value = "Ayah" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        //Poli Umum
        public static List<SelectListItem> ListBaikSedangLemahJelek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                result.Add(new SelectListItem() { Text = "Jelek", Value = "Jelek" });
                return result;
            }
        }


        public static List<SelectListItem> ListBaikSedangLemahKurangBuruk
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Buruk", Value = "Buruk" });
                return result;
            }
        }

        public static List<SelectListItem> ListNormalAbnormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Abnormal", Value = "Abnormal" });
                return result;
            }
        }

        public static List<SelectListItem> ListRegularIlegural
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Regular", Value = "Regular" });
                result.Add(new SelectListItem() { Text = "Ilegular", Value = "Ilegular" });
                return result;
            }
        }

        public static List<SelectListItem> ListIXII
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "I", Value = "I" });
                result.Add(new SelectListItem() { Text = "II", Value = "II" });
                result.Add(new SelectListItem() { Text = "III", Value = "III" });
                result.Add(new SelectListItem() { Text = "IV", Value = "IV" });
                result.Add(new SelectListItem() { Text = "V", Value = "V" });
                result.Add(new SelectListItem() { Text = "VI", Value = "VI" });
                result.Add(new SelectListItem() { Text = "VI", Value = "VI" });
                result.Add(new SelectListItem() { Text = "VII", Value = "VII" });
                result.Add(new SelectListItem() { Text = "VII", Value = "VII" });
                result.Add(new SelectListItem() { Text = "VIII", Value = "VIII" });
                result.Add(new SelectListItem() { Text = "IX", Value = "IX" });
                result.Add(new SelectListItem() { Text = "X", Value = "X" });
                result.Add(new SelectListItem() { Text = "Xi", Value = "Xi" });
                result.Add(new SelectListItem() { Text = "XII", Value = "XII" });
                return result;
            }
        }

        public static List<SelectListItem> ListKananKiri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kanan", Value = "Kanan" });
                result.Add(new SelectListItem() { Text = "Kiri", Value = "Kiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMotorik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tenaga", Value = "Tenaga" });
                result.Add(new SelectListItem() { Text = "Tonus", Value = "Tonus" });
                result.Add(new SelectListItem() { Text = "Koordinasi", Value = "Koordinasi" });
                result.Add(new SelectListItem() { Text = "Gerakan Involunter", Value = "Gerakan Involunter" });
                result.Add(new SelectListItem() { Text = "Langkah", Value = "Langkah" });
                result.Add(new SelectListItem() { Text = "Gaya Jalan", Value = "Gaya Jalan" });
                return result;
            }
        }

        public static List<SelectListItem> ListRefleks
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Fisiologik", Value = "Fisiologik" });
                result.Add(new SelectListItem() { Text = "Dalam", Value = "Dalam" });
                return result;
            }
        }

        public static List<SelectListItem> ListSensorik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Permukaan", Value = "Permukaan" });
                result.Add(new SelectListItem() { Text = "Dalam", Value = "Dalam" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerawatanPascaOP
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ruangan", Value = "Ruangan" });
                result.Add(new SelectListItem() { Text = "HCU", Value = "HCU" });
                result.Add(new SelectListItem() { Text = "ICU", Value = "ICU" });
                result.Add(new SelectListItem() { Text = "NICU", Value = "NICU" });
                result.Add(new SelectListItem() { Text = "PICU", Value = "PICU" });
                return result;
            }
        }

        public static List<SelectListItem> ListTerhadapSaya
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Saya Sendiri", Value = "Saya Sendiri" });
                result.Add(new SelectListItem() { Text = "Istri", Value = "Istri" });
                result.Add(new SelectListItem() { Text = "Suami", Value = "Suami" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Ayah", Value = "Ayah" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                return result;
            }
        }

        public static List<SelectListItem> ListCara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "iv", Value = "iv" });
                result.Add(new SelectListItem() { Text = "im", Value = "im" });
                return result;
            }
        }

        public static List<SelectListItem> ListIIV
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "I", Value = "I" });
                result.Add(new SelectListItem() { Text = "II", Value = "II" });
                result.Add(new SelectListItem() { Text = "III", Value = "III" });
                result.Add(new SelectListItem() { Text = "IV", Value = "IV" });
                return result;
            }
        }

        public static List<SelectListItem> List14
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                return result;
            }
        }

        public static List<SelectListItem> ListBudayaSosialSpiritualKeyakinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Budaya", Value = "Budaya" });
                result.Add(new SelectListItem() { Text = "Sosial", Value = "Sosial" });
                result.Add(new SelectListItem() { Text = "Spiritual", Value = "Spiritual" });
                result.Add(new SelectListItem() { Text = "Keyakinan", Value = "Keyakinan" });
                return result;
            }
        }

        public static List<SelectListItem> VK_Kink
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "ETT Kink", Value = "ETT Kink" });
                result.Add(new SelectListItem() { Text = "Non Kink Oral", Value = "Non Kink Oral" });
                result.Add(new SelectListItem() { Text = "Nasal", Value = "Nasal" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidakDiperlukan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak diperlukan", Value = "Tidak diperlukan" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerluTidakPerlu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Perlu", Value = "Perlu" });
                result.Add(new SelectListItem() { Text = "PerluTIdakPerlu", Value = "PerluTIdakPerlu" });
                return result;
            }
        }

        public static List<SelectListItem> ListYadansudahTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya dan sudah direncanakan pemasangan infus line", Value = "Ya dan sudah direncanakan pemasangan infus line" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListAlkesObat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Alkes", Value = "Alkes" });
                result.Add(new SelectListItem() { Text = "Obat", Value = "Obat" });
                return result;
            }
        }

        public static List<SelectListItem> ListCatatanAntibiotik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Intramuskular", Value = "Intramuskular" });
                result.Add(new SelectListItem() { Text = "Subkutan", Value = "Subkutan" });
                result.Add(new SelectListItem() { Text = "Intravena", Value = "Intravena" });
                result.Add(new SelectListItem() { Text = "Oral", Value = "Oral" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerkembanganPenyakit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Membaik", Value = "Membaik" });
                result.Add(new SelectListItem() { Text = "Stabil", Value = "Stabil" });
                result.Add(new SelectListItem() { Text = "Memburuk", Value = "Memburuk" });
                result.Add(new SelectListItem() { Text = "Komplikasi", Value = "Komplikasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListPrognosis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik(Ad bonam)", Value = "Baik(Ad bonam)" });
                result.Add(new SelectListItem() { Text = "Buruk(Dubius Ad malam)", Value = "Buruk(Dubius Ad malam)" });
                return result;
            }
        }
        public static List<SelectListItem> ListDirujukRSKLinik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "RS", Value = "RS" });
                result.Add(new SelectListItem() { Text = "Klinik", Value = "Klinik" });
                return result;
            }
        }

        public static List<SelectListItem> ListStatusPulang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Atas Ijin Dokter", Value = "Atas Ijin Dokter" });
                result.Add(new SelectListItem() { Text = "Dirujuk", Value = "Dirujuk" });
                result.Add(new SelectListItem() { Text = "Melarikan Diri", Value = "Melarikan Diri" });
                result.Add(new SelectListItem() { Text = "Meninggal Dunia", Value = "Meninggal Dunia" });
                result.Add(new SelectListItem() { Text = "Atas Permintaan Sendiri", Value = "Atas Permintaan Sendiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListDirujukKe
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dokter Pribadi", Value = "Dokter Pribadi" });
                result.Add(new SelectListItem() { Text = "Rumah Sakit", Value = "Rumah Sakit" });
                result.Add(new SelectListItem() { Text = "Lain-lain", Value = "Lain-lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListRinganSedangBerat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ringan", Value = "Ringan" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Berat", Value = "Berat" });
                return result;
            }
        }

        public static List<SelectListItem> ListObligasiSaatPulang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Jalan", Value = "Jalan" });
                result.Add(new SelectListItem() { Text = "Tongkat", Value = "Tongkat" });
                result.Add(new SelectListItem() { Text = "Kursi Roda", Value = "Kursi Roda" });
                result.Add(new SelectListItem() { Text = "Brangkard", Value = "Brangkard" });
                return result;
            }
        }

        public static List<SelectListItem> ListSiapaYangDiEdukasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien", Value = "Pasien" });
                result.Add(new SelectListItem() { Text = "Ayah/Ibu", Value = "Ayah/Ibu" });
                result.Add(new SelectListItem() { Text = "Suami/Istri", Value = "Suami/Istri" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListMetodeEdukasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diskusi (D)", Value = "Diskusi (D)" });
                result.Add(new SelectListItem() { Text = "Demonstrasi (Demo)", Value = "Demonstrasi (Demo)" });
                result.Add(new SelectListItem() { Text = "Ceramah (C)", Value = "Ceramah (C)" });
                result.Add(new SelectListItem() { Text = "Simulasi (S)", Value = "Simulasi (S)" });
                result.Add(new SelectListItem() { Text = "Observasi (O)", Value = "Observasi (O)" });
                result.Add(new SelectListItem() { Text = "Praktek Langsung(L)", Value = "Praktek Langsung(L)" });
                return result;
            }
        }

        public static List<SelectListItem> ListRespon
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Respon Sama Sekali(Tidak Antusias dan keinginan belajar)", Value = "Tidak Respon Sama Sekali(Tidak Antusias dan keinginan belajar)" });
                result.Add(new SelectListItem() { Text = "Tidak Paham(Ingin Belajar Tapi Kesulitan mengerti)", Value = "Tidak Paham(Ingin Belajar Tapi Kesulitan mengerti)" });
                result.Add(new SelectListItem() { Text = "Paham hal yang di ajarkan, Tapi tidak bisa menjelaskan sendiri", Value = "Paham hal yang di ajarkan, Tapi tidak bisa menjelaskan sendiri" });
                result.Add(new SelectListItem() { Text = "Dapat menjelaskan apa yang telah diajarkan, tapi harus di bantu edukator", Value = "Dapat menjelaskan apa yang telah diajarkan, tapi harus di bantu edukator" });
                result.Add(new SelectListItem() { Text = "Dapat menjelaskan apa yang telah diajarkan tapi tanpa dibantu", Value = "Dapat menjelaskan apa yang telah diajarkan tapi tanpa dibantu" });
                return result;
            }
        }

        public static List<SelectListItem> ListMuntahMencretSisa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Muntah", Value = "Muntah" });
                result.Add(new SelectListItem() { Text = "Mencret", Value = "Mencret" });
                result.Add(new SelectListItem() { Text = "Sisa", Value = "Sisa" });
                return result;
            }
        }

        public static List<SelectListItem> ListPlantOfCare
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "MRS", Value = "MRS" });
                result.Add(new SelectListItem() { Text = "Operasi", Value = "Operasi" });
                result.Add(new SelectListItem() { Text = "Chemoterapy", Value = "Chemoterapy" });
                result.Add(new SelectListItem() { Text = "Persalinan", Value = "Persalinan" });
                result.Add(new SelectListItem() { Text = "ODC", Value = "ODC" });
                result.Add(new SelectListItem() { Text = "Perbaikan Kondisi", Value = "Perbaikan Kondisi" });
                return result;
            }
        }

        public static List<SelectListItem> ListCaraBayar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Umum", Value = "Umum" });
                result.Add(new SelectListItem() { Text = "JKBM", Value = "JKBM" });
                result.Add(new SelectListItem() { Text = "JKN", Value = "JKN" });
                result.Add(new SelectListItem() { Text = "Jaminan Lainnya", Value = "Jaminan Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListSiapTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Siap", Value = "Siap" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListBolehKirimTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Boleh Kirim", Value = "Boleh Kirim" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListKelasPerawatan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "III", Value = "III" });
                result.Add(new SelectListItem() { Text = "II", Value = "II" });
                result.Add(new SelectListItem() { Text = "I", Value = "I" });
                result.Add(new SelectListItem() { Text = "VIP III", Value = "VIP III" });
                result.Add(new SelectListItem() { Text = "VIP II", Value = "VIP II" });
                return result;
            }
        }

        public static List<SelectListItem> ListPoliIGDAdmission
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Poliklinik", Value = "Poliklinik" });
                result.Add(new SelectListItem() { Text = "IGD", Value = "IGD" });
                result.Add(new SelectListItem() { Text = "Admission", Value = "Admission" });
                return result;
            }
        }

        public static List<SelectListItem> ListKartuIdentitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "KTP", Value = "KTP" });
                result.Add(new SelectListItem() { Text = "SIM", Value = "SIM" });
                result.Add(new SelectListItem() { Text = "NRP", Value = "NRP" });
                result.Add(new SelectListItem() { Text = "NIP", Value = "NIP" });
                return result;
            }
        }

        public static List<SelectListItem> ListDikirimOleh
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dr. Poliklinik", Value = "Dr. Poliklinik" });
                result.Add(new SelectListItem() { Text = "Dr. Jaga IGD", Value = "Dr. Jaga IGD" });
                result.Add(new SelectListItem() { Text = "Rujukan Dari", Value = "Rujukan Dari" });
                return result;
            }
        }

        public static List<SelectListItem> ListProsedurMasukRS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "IGD", Value = "IGD" });
                result.Add(new SelectListItem() { Text = "Poliklinik", Value = "Poliklinik" });
                result.Add(new SelectListItem() { Text = "Langsung Rawat Inap", Value = "Langsung Rawat Inap" });
                return result;
            }
        }

        public static List<SelectListItem> ListSudahKBBElumKB
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sudah KB", Value = "Sudah KB" });
                result.Add(new SelectListItem() { Text = "Belum KB", Value = "Belum KB" });
                return result;
            }
        }

        public static List<SelectListItem> ListKeadaanKeluar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sembuh", Value = "Sembuh" });
                result.Add(new SelectListItem() { Text = "Membaik", Value = "Membaik" });
                result.Add(new SelectListItem() { Text = "Meninggal < 48 Jam", Value = "Meninggal < 48 Jam" });
                result.Add(new SelectListItem() { Text = "Meninggal > 48 Jam", Value = "Meninggal > 48 Jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListCaraKeluar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diijinkan Keluar", Value = "Diijinkan Keluar" });
                result.Add(new SelectListItem() { Text = "Pulang Paksa", Value = "Pulang Paksa" });
                result.Add(new SelectListItem() { Text = "Lari", Value = "Lari" });
                result.Add(new SelectListItem() { Text = "Pindah RS Lain", Value = "Pindah RS Lain" });
                result.Add(new SelectListItem() { Text = "Dirujuk Ke", Value = "Dirujuk Ke" });
                return result;
            }
        }

        public static List<SelectListItem> ListCaraPersalinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Spt B", Value = "Spt B" });
                result.Add(new SelectListItem() { Text = "Bracht", Value = "Bracht" });
                result.Add(new SelectListItem() { Text = "LM", Value = "LM" });
                result.Add(new SelectListItem() { Text = "VE", Value = "VE" });
                result.Add(new SelectListItem() { Text = "FE", Value = "FE" });
                result.Add(new SelectListItem() { Text = "SC", Value = "SC" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListTunggalKembar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tunggal", Value = "Tunggal" });
                result.Add(new SelectListItem() { Text = "Kembar", Value = "Kembar" });
                return result;
            }
        }

        public static List<SelectListItem> ListPostur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Fleksi dan atau Kaku Tagang", Value = "Fleksi dan atau Kaku Tagang" });
                result.Add(new SelectListItem() { Text = "Ekstensi", Value = "Ekstensi" });
                return result;
            }
        }

        public static List<SelectListItem> ListPolaTidur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Agitasi atau Lemas", Value = "Agitasi atau Lemas" });
                result.Add(new SelectListItem() { Text = "Relaks", Value = "Relaks" });
                return result;
            }
        }

        public static List<SelectListItem> ListEkspresi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Meringis", Value = "Meringis" });
                result.Add(new SelectListItem() { Text = "Mengerutkan Dahi", Value = "Mengerutkan Dahi" });
                return result;
            }
        }

        public static List<SelectListItem> ListWarna
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pucat atau Kehitaman atau Kemerahan", Value = "Pucat atau Kehitaman atau Kemerahan" });
                result.Add(new SelectListItem() { Text = "Merah Muda", Value = "Merah Muda" });
                return result;
            }
        }

        public static List<SelectListItem> ListNapas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Apnue", Value = "Apnue" });
                result.Add(new SelectListItem() { Text = "Takipne", Value = "Takipne" });
                return result;
            }
        }

        public static List<SelectListItem> ListDenyutJantung
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Fluktuasi", Value = "Fluktuasi" });
                result.Add(new SelectListItem() { Text = "Takikardi", Value = "Takikardi" });
                return result;
            }
        }

        public static List<SelectListItem> ListSaturasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Desaturasi", Value = "Desaturasi" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hipotensi atau Hipertensi", Value = "Hipotensi atau Hipertensi" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListPersepsiPerawat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ada Nyeri", Value = "Ada Nyeri" });
                result.Add(new SelectListItem() { Text = "Tidak Nyeri", Value = "Tidak Nyeri" });
                return result;
            }
        }

        public static List<SelectListItem> Listkeadaanlahir
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Segera menangis", Value = "Segera menangis" });
                result.Add(new SelectListItem() { Text = "Tidak segera mengangis", Value = "Tidak segera mengangis" });

                return result;
            }
        }

        public static List<SelectListItem> ListPersalinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Vacum", Value = "Vacum" });
                result.Add(new SelectListItem() { Text = "Forceps", Value = "Forceps" });
                result.Add(new SelectListItem() { Text = "SC", Value = "SC" });
                return result;
            }
        }

        public static List<SelectListItem> ListKeadaanUmum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                result.Add(new SelectListItem() { Text = "Jelek", Value = "Jelek" });
                return result;
            }
        }

        public static List<SelectListItem> ListThroaks
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Simetris", Value = "Simetris" });
                result.Add(new SelectListItem() { Text = "Asimetris", Value = "Asimetris" });
                return result;
            }
        }
        public static List<SelectListItem> ListDowneSkorFrekuensi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 60 / menit", Value = "< 60 / menit" });
                result.Add(new SelectListItem() { Text = "60 - 80 /  menit", Value = "60 - 80 /  menit" });
                result.Add(new SelectListItem() { Text = "> 80 / menit", Value = "> 80 / menit" });
                return result;
            }
        }

        public static List<SelectListItem> ListDowneSkorRetraksi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Retraksi", Value = "Tidak Ada Retraksi" });
                result.Add(new SelectListItem() { Text = "Retraksi Ringan", Value = "Retraksi Ringan" });
                result.Add(new SelectListItem() { Text = "Retraksi Berat", Value = "Retraksi Berat" });
                return result;
            }
        }

        public static List<SelectListItem> ListDowneSkorSianosis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Sianosis", Value = "Tidak Ada Sianosis" });
                result.Add(new SelectListItem() { Text = "Sianosis hilang dengan pemberian 02", Value = "Sianosis hilang dengan pemberian 02" });
                result.Add(new SelectListItem() { Text = "Sianosis menetap dengan pemberian 02", Value = "Sianosis menetap dengan pemberian 02" });
                return result;
            }
        }

        public static List<SelectListItem> ListDowneSkorSuaraNafas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Suara nafas di kedua paru baik", Value = "Suara nafas di kedua paru baik" });
                result.Add(new SelectListItem() { Text = "Suara nafas di kedua paru menurun", Value = "Suara nafas di kedua paru menurun" });
                result.Add(new SelectListItem() { Text = "Tidak ada suara napas di kedua paru", Value = "Tidak ada suara napas di kedua paru" });
                return result;
            }
        }

        public static List<SelectListItem> ListDowneSkorMerintih
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Merintih", Value = "Tidak Merintih" });
                result.Add(new SelectListItem() { Text = "Dapat didengar dengan stetoskop", Value = "Dapat didengar dengan stetoskop" });
                result.Add(new SelectListItem() { Text = "Dapat didengar tanpa alat bantu", Value = "Dapat didengar tanpa alat bantu" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorJumlah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1 Menit", Value = "1 Menit" });
                result.Add(new SelectListItem() { Text = "5 Menit", Value = "5 Menit" });
                result.Add(new SelectListItem() { Text = "10 Menit", Value = "10 Menit" });
                result.Add(new SelectListItem() { Text = "15 Menit", Value = "15 Menit" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorFrekuensi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tak ada", Value = "Tak ada" });
                result.Add(new SelectListItem() { Text = "< 100", Value = "< 100" });
                result.Add(new SelectListItem() { Text = "> 100", Value = "> 100" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorUsahaBernafas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak ada", Value = "Tidak ada" });
                result.Add(new SelectListItem() { Text = "Lambat", Value = "Lambat" });
                result.Add(new SelectListItem() { Text = "Menangis Kuat", Value = "Menangis Kuat" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorAktivitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Lumpuh", Value = "Lumpuh" });
                result.Add(new SelectListItem() { Text = "Gerak sedikit", Value = "Gerak sedikit" });
                result.Add(new SelectListItem() { Text = "Gerak aktif", Value = "Gerak aktif" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorReflek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak bereaksi", Value = "Tidak bereaksi" });
                result.Add(new SelectListItem() { Text = "Meringis", Value = "Meringis" });
                result.Add(new SelectListItem() { Text = "Terbatuk / Bersin", Value = "Terbatuk / Bersin" });
                return result;
            }
        }

        public static List<SelectListItem> ListAPGARSkorWarnaKulit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Biru / Pucat", Value = "Biru / Pucat" });
                result.Add(new SelectListItem() { Text = "Tubuh merah muda ekstremitas biru", Value = "Tubuh merah muda ekstremitas biru" });
                result.Add(new SelectListItem() { Text = "Seluruh tubuh kemerahan", Value = "Seluruh tubuh kemerahan" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisLensa(string defaultvalue = "")
        {
            var r = new List<SelectListItem>()
            {
                new SelectListItem() { Text ="Dekat", Value = "Dekat" , Selected = defaultvalue == "Dekat" },
                new SelectListItem() { Text ="Jauh", Value = "Jauh" , Selected = defaultvalue == "Jauh" },
                new SelectListItem() { Text ="Bifokal", Value = "Bifokal" , Selected = defaultvalue == "Bifokal" },
                new SelectListItem() { Text ="Progresif", Value = "Progresif" , Selected = defaultvalue == "Progresif" },
            };
            return r;
        }

        public static List<SelectListItem> ListBertindakSelaku
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "diri sendiri", Value = "diri sendiri" });
                result.Add(new SelectListItem() { Text = "orang tua", Value = "orang tua" });
                result.Add(new SelectListItem() { Text = "suami", Value = "suami" });
                result.Add(new SelectListItem() { Text = "istri", Value = "istri" });

                return result;
            }
        }

        public static List<SelectListItem> ListGetUpGoTest
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });


                return result;
            }
        }

        public static List<SelectListItem> ListProseduralPasienRencanaOperasiKirimanDokter
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Beli Sendiri", Value = "Beli Sendiri" });
                result.Add(new SelectListItem() { Text = "Digabung dengan biaya pulang", Value = "Digabung dengan biaya pulang" });


                return result;
            }
        }

        public static List<SelectListItem> ListSectionRJ
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RJ" && x.StatusAktif == true).ToList();
                    r.Add(new SelectListItem() { Value = "-", Text = "-" });
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionRI
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RI" && x.StatusAktif == true && x.RIOnly == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListRespirasiEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 8", Value = "<= 8" });
                result.Add(new SelectListItem() { Text = "9-11", Value = "9-11" });
                result.Add(new SelectListItem() { Text = "12-20", Value = "12-20" });
                result.Add(new SelectListItem() { Text = "21-24", Value = "21-24" });
                result.Add(new SelectListItem() { Text = ">= 25", Value = ">= 25" });
                return result;
            }
        }

        public static List<SelectListItem> ListSaturasiEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 91", Value = "<= 91" });
                result.Add(new SelectListItem() { Text = "92-93", Value = "92-93" });
                result.Add(new SelectListItem() { Text = "94-95", Value = "94-95" });
                result.Add(new SelectListItem() { Text = ">= 95", Value = ">= 95" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarahEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 85", Value = "<= 85" });
                result.Add(new SelectListItem() { Text = "86-95", Value = "86-95" });
                result.Add(new SelectListItem() { Text = "96-99", Value = "96-99" });
                result.Add(new SelectListItem() { Text = "100-179", Value = "100-179" });
                result.Add(new SelectListItem() { Text = "180-200", Value = "180-200" });
                result.Add(new SelectListItem() { Text = "201-219", Value = "201-219" });
                result.Add(new SelectListItem() { Text = ">= 220", Value = ">= 220" });
                return result;
            }
        }

        public static List<SelectListItem> ListNadiEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 40", Value = "<= 40" });
                result.Add(new SelectListItem() { Text = "41-50", Value = "41-50" });
                result.Add(new SelectListItem() { Text = "51-90", Value = "51-90" });
                result.Add(new SelectListItem() { Text = "91-110", Value = "91-110" });
                result.Add(new SelectListItem() { Text = "111-130", Value = "111-130" });
                result.Add(new SelectListItem() { Text = ">= 131", Value = ">= 131" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaranEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "A(Alert)", Value = "A(Alert)" });
                result.Add(new SelectListItem() { Text = "V, P, U(Verbal, Pain, Unresponsive)", Value = "V, P, U(Verbal, Pain, Unresponsive)" });
                return result;
            }
        }

        public static List<SelectListItem> ListSuhuEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 35", Value = "< 35" });
                result.Add(new SelectListItem() { Text = "36,1 - 38,0", Value = "36,1 - 38,0" });
                result.Add(new SelectListItem() { Text = "38,1 - 39,0", Value = "38,1 - 39,0" });
                result.Add(new SelectListItem() { Text = ">= 39,1", Value = ">= 39,1" });
                return result;
            }
        }
        public static List<SelectListItem> ListB_1
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 19", Value = "< 19" });
                result.Add(new SelectListItem() { Text = "19 - 21", Value = "19 - 21" });
                result.Add(new SelectListItem() { Text = "21 - 23", Value = "21 - 23" });
                result.Add(new SelectListItem() { Text = "> 23", Value = "> 23" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_2
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 21", Value = "< 21" });
                result.Add(new SelectListItem() { Text = "21 - 22", Value = "21 - 22" });
                result.Add(new SelectListItem() { Text = "> 22", Value = "> 22" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_3
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 31", Value = "<= 31" });
                result.Add(new SelectListItem() { Text = "> 31", Value = "> 31" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_4
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kehilangan > 3 kg", Value = "Kehilangan > 3 kg" });
                result.Add(new SelectListItem() { Text = "Tidak Tahu", Value = "Tidak Tahu" });
                result.Add(new SelectListItem() { Text = "Kehilangan antara 1 - 3kg", Value = "Kehilangan antara 1 - 3kg" });
                result.Add(new SelectListItem() { Text = "Tidak Kehilangan BB", Value = "Tidak Kehilangan BB" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_8
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hanya terbaring/ diatas kursi roda", Value = "Hanya terbaring/ diatas kursi roda" });
                result.Add(new SelectListItem() { Text = "Bisa bangkit dari tempat tidur tapi tidak keluar rumah", Value = "Bisa bangkit dari tempat tidur tapi tidak keluar rumah" });
                result.Add(new SelectListItem() { Text = "Bisa keluar rumah", Value = "Bisa keluar rumah" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_9
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Demensia berat dan depresi", Value = "Demensia berat dan depresi" });
                result.Add(new SelectListItem() { Text = "Demensia Ringan", Value = "Demensia Ringan" });
                result.Add(new SelectListItem() { Text = "Tidak ada masalah psikologis", Value = "Tidak ada masalah psikologis" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_11
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1 x makan", Value = "1 x makan" });
                result.Add(new SelectListItem() { Text = "2 x makan", Value = "2 x makan" });
                result.Add(new SelectListItem() { Text = "3 x makan", Value = "3 x makan" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_15
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 3 cangkir", Value = "< 3 cangkir" });
                result.Add(new SelectListItem() { Text = "3 - 5 cangkir", Value = "3 - 5 cangkir" });
                result.Add(new SelectListItem() { Text = "> 5 cangkir", Value = "> 5 cangkir" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_16
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak bisa makan tanpa bantuan", Value = "Tidak bisa makan tanpa bantuan" });
                result.Add(new SelectListItem() { Text = "Makan sendiri dengan sedikit kesulitan", Value = "Makan sendiri dengan sedikit kesulitan" });
                result.Add(new SelectListItem() { Text = "Makan sendiri tanpa kesulitan", Value = "Makan sendiri tanpa kesulitan" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_17
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Malnutrisi", Value = "Malnutrisi" });
                result.Add(new SelectListItem() { Text = "Tidak tahu / malnutrisi sedang", Value = "Tidak tahu / malnutrisi sedang" });
                result.Add(new SelectListItem() { Text = "Tidak ada masalah gizi", Value = "Tidak ada masalah gizi" });
                return result;
            }
        }

        public static List<SelectListItem> ListB_18
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak baik", Value = "Tidak baik" });
                result.Add(new SelectListItem() { Text = "Tidak tahu", Value = "Tidak tahu" });
                result.Add(new SelectListItem() { Text = "Sama baik", Value = "Sama baik" });
                result.Add(new SelectListItem() { Text = "Lebih baik", Value = "Lebih baik" });
                return result;
            }
        }

        public static List<SelectListItem> ListF_1
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak ada", Value = "Tidak ada" });
                result.Add(new SelectListItem() { Text = "Ringan", Value = "Ringan" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Berat", Value = "Berat" });
                result.Add(new SelectListItem() { Text = "Sangat Berat", Value = "Sangat Berat" });
                return result;
            }
        }

        public static List<SelectListItem> ListF_4
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sangat puas", Value = "Sangat puas" });
                result.Add(new SelectListItem() { Text = "Puas", Value = "Puas" });
                result.Add(new SelectListItem() { Text = "Sedikit puas", Value = "Sedikit puas" });
                result.Add(new SelectListItem() { Text = "Tidak puas", Value = "Tidak puas" });
                result.Add(new SelectListItem() { Text = "Sangat Tidak Puas", Value = "Sangat Tidak Puas" });
                return result;
            }
        }

        public static List<SelectListItem> ListF_5
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak jelas", Value = "Tidak jelas" });
                result.Add(new SelectListItem() { Text = "Sedikit", Value = "Sedikit" });
                result.Add(new SelectListItem() { Text = "Kadang - kadang", Value = "Kadang - kadang" });
                result.Add(new SelectListItem() { Text = "Jelas", Value = "Jelas" });
                result.Add(new SelectListItem() { Text = "Sangat Jelas", Value = "Sangat Jelas" });
                return result;
            }
        }

        public static List<SelectListItem> ListF_6
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Sedikit", Value = "Sedikit" });
                result.Add(new SelectListItem() { Text = "Kadang - kadang", Value = "Kadang - kadang" });
                result.Add(new SelectListItem() { Text = "Khawatir", Value = "Khawatir" });
                result.Add(new SelectListItem() { Text = "Sangat Khawatir", Value = "Sangat Khawatir" });
                return result;
            }
        }

        public static List<SelectListItem> ListF_7
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Sedikit", Value = "Sedikit" });
                result.Add(new SelectListItem() { Text = "Kadang - kadang", Value = "Kadang - kadang" });
                result.Add(new SelectListItem() { Text = "Banyak menggangu", Value = "Banyak menggangu" });
                result.Add(new SelectListItem() { Text = "Sangat terganggu", Value = "Sangat terganggu" });
                return result;
            }
        }

        public static List<SelectListItem> List_Usia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 3 Tahun", Value = "< 3 Tahun" });
                result.Add(new SelectListItem() { Text = "3 - 7 Tahun", Value = "3 - 7 Tahun" });
                result.Add(new SelectListItem() { Text = "7 - 13 Tahun", Value = "7 - 13 Tahun" });
                result.Add(new SelectListItem() { Text = "> 13 Tahun", Value = "> 13 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> List_Diagnosis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diagnosis Neurologi", Value = "Diagnosis Neurologi" });
                result.Add(new SelectListItem() { Text = "Perubahan oksigen (diagnosis, respiratorik, dehidrasi, anemia, anoreksi, sinkop, pusing, dsb)", Value = "Perubahan oksigen (diagnosis, respiratorik, dehidrasi, anemia, anoreksi, sinkop, pusing, dsb)" });
                result.Add(new SelectListItem() { Text = "Gangguan Psikis/ Perilaku", Value = "Gangguan Psikis/ Perilaku" });
                result.Add(new SelectListItem() { Text = "Diagnosis lain", Value = "Diagnosis lain" });
                return result;
            }
        }

        public static List<SelectListItem> List_GangguanKognitif
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak sadar terhadap keterbatasan", Value = "Tidak sadar terhadap keterbatasan" });
                result.Add(new SelectListItem() { Text = "Lupa keterbatasan", Value = "Lupa keterbatasan" });
                result.Add(new SelectListItem() { Text = "Mengetahui kemampuan diri", Value = "Mengetahui kemampuan diri" });
                return result;
            }
        }

        public static List<SelectListItem> List_FaktorLingkungan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Riwayat jatuh dari tempat tidur saat bayi - anak", Value = "Riwayat jatuh dari tempat tidur saat bayi - anak" });
                result.Add(new SelectListItem() { Text = "Pasien menggunakan alat bantu atau box atau mebel", Value = "Pasien menggunakan alat bantu atau box atau mebel" });
                result.Add(new SelectListItem() { Text = "Pasien berada di tempat tidur", Value = "Pasien berada di tempat tidur" });
                result.Add(new SelectListItem() { Text = "Diluar ruang rawat", Value = "Diluar ruang rawat" });
                return result;
            }
        }

        public static List<SelectListItem> List_ResponOprasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dalam 24 jam", Value = "Dalam 24 jam" });
                result.Add(new SelectListItem() { Text = "Dalam 48 jam riwayat jatuh", Value = "Dalam 48 jam riwayat jatuh" });
                result.Add(new SelectListItem() { Text = "> 48 jam", Value = "> 48 jam" });
                return result;
            }
        }

        public static List<SelectListItem> List_PenggunaanObat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Obat Sedative (kecuali pasien ICU yang menggunakan sedasi dan paralisis) Hipnotik, Barbiturat, Fenotiazin, Antidepresan, Laksans ? Diuretika, Narkotik", Value = "Obat Sedative (kecuali pasien ICU yang menggunakan sedasi dan paralisis) Hipnotik, Barbiturat, Fenotiazin, Antidepresan, Laksans ? Diuretika, Narkotik" });
                result.Add(new SelectListItem() { Text = "Salah satu dari pengobatan diatas", Value = "Salah satu dari pengobatan diatas" });
                result.Add(new SelectListItem() { Text = "Pengobatan lain", Value = "Pengobatan lain" });
                return result;
            }
        }

        public static List<SelectListItem> List_AlatBantu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Berpegangan pada perabot", Value = "Berpegangan pada perabot" });
                result.Add(new SelectListItem() { Text = "Tongkat/ Alat Penopang", Value = "Tongkat/ Alat Penopang" });
                result.Add(new SelectListItem() { Text = "Tidak Ada/ Kursi roda/ Perawat/ Tirah Baring", Value = "Tidak Ada/ Kursi roda/ Perawat/ Tirah Baring" });
                return result;
            }
        }

        public static List<SelectListItem> List_GayaBerjalan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Terganggu", Value = "Terganggu" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                result.Add(new SelectListItem() { Text = "Normal / Tirah Baring / Imobilisasi", Value = "Normal / Tirah Baring / Imobilisasi" });
                return result;
            }
        }

        public static List<SelectListItem> List_Siklus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sering lupa akan keterbatasan yang dimiliki", Value = "Sering lupa akan keterbatasan yang dimiliki" });
                result.Add(new SelectListItem() { Text = "Sadar akan kemampuan sendiri", Value = "Sadar akan kemampuan sendiri" });
                return result;
            }
        }

        public static List<SelectListItem> List_DilakukanTidakDilakukan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Lakukan", Value = "Lakukan" });
                result.Add(new SelectListItem() { Text = "Tidak Dilakukan", Value = "Tidak Dilakukan" });
                return result;
            }
        }

        public static List<SelectListItem> ListLampiran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Lampiran1", Value = "Lampiran1" });
                result.Add(new SelectListItem() { Text = "Lampiran2", Value = "Lampiran2" });
                result.Add(new SelectListItem() { Text = "Lampiran3", Value = "Lampiran3" });
                result.Add(new SelectListItem() { Text = "Lampiran4", Value = "Lampiran4" });
                result.Add(new SelectListItem() { Text = "Lampiran5", Value = "Lampiran5" });
                result.Add(new SelectListItem() { Text = "Lampiran6", Value = "Lampiran6" });
                return result;
            }
        }

        public static List<SelectListItem> ListVitalSign
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "N", Value = "N" });
                result.Add(new SelectListItem() { Text = "V Sis", Value = "V Sis" });
                result.Add(new SelectListItem() { Text = "Dis", Value = "Dis" });
                result.Add(new SelectListItem() { Text = "+ R", Value = "+ R" });
                result.Add(new SelectListItem() { Text = "TVS", Value = "TVS" });
                return result;
            }
        }

        public static List<SelectListItem> ListTVS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "25", Value = "25" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "15", Value = "15" });
                result.Add(new SelectListItem() { Text = "10", Value = "10" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                return result;
            }
        }

        public static List<SelectListItem> List_R_VitalSign
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "44", Value = "44" });
                result.Add(new SelectListItem() { Text = "45", Value = "45" });
                result.Add(new SelectListItem() { Text = "36", Value = "36" });
                result.Add(new SelectListItem() { Text = "32", Value = "32" });
                result.Add(new SelectListItem() { Text = "28", Value = "28" });
                result.Add(new SelectListItem() { Text = "24", Value = "24" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "16", Value = "16" });
                result.Add(new SelectListItem() { Text = "12", Value = "12" });
                result.Add(new SelectListItem() { Text = "8", Value = "8" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                return result;
            }
        }

        public static List<SelectListItem> List_N_VitalSign
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "200", Value = "200" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                return result;
            }
        }

        public static List<SelectListItem> List_TD_VitalSign
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "220", Value = "220" });
                result.Add(new SelectListItem() { Text = "230", Value = "230" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                return result;
            }
        }

        public static List<SelectListItem> ListPernafasan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Spontan", Value = "Spontan" });
                result.Add(new SelectListItem() { Text = "Dibantu", Value = "Dibantu" });
                return result;
            }
        }

        public static List<SelectListItem> ListBilaSpontan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Adekuat Bersuara", Value = "Adekuat Bersuara" });
                result.Add(new SelectListItem() { Text = "Penyumbatan", Value = "Penyumbatan" });
                result.Add(new SelectListItem() { Text = "Membutuhkan Bantuan Alat", Value = "Membutuhkan Bantuan Alat" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sadar Betul", Value = "Sadar Betul" });
                result.Add(new SelectListItem() { Text = "Belum Sadar Betul", Value = "Belum Sadar Betul" });
                result.Add(new SelectListItem() { Text = "Tidur Dalam", Value = "Tidur Dalam" });
                return result;
            }
        }

        public static List<SelectListItem> ListRespirasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Batuk / Menangis", Value = "Batuk / Menangis" });
                result.Add(new SelectListItem() { Text = "Berusaha Bernafas", Value = "Berusaha Bernafas" });
                result.Add(new SelectListItem() { Text = "Perlu Bantuan Bernafas", Value = "Perlu Bantuan Bernafas" });
                return result;
            }
        }
        public static List<SelectListItem> ListAktivitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Gerak Beraturan", Value = "Gerak Beraturan" });
                result.Add(new SelectListItem() { Text = "Gerak Tanpa Tujuan", Value = "Gerak Tanpa Tujuan" });
                result.Add(new SelectListItem() { Text = "Tidak Bergerak", Value = "Tidak Bergerak" });
                return result;
            }
        }

        public static List<SelectListItem> ListPindahKe
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ruang Rawat", Value = "Ruang Rawat" });
                result.Add(new SelectListItem() { Text = "ICU", Value = "ICU" });
                result.Add(new SelectListItem() { Text = "Boleh Pulang", Value = "Boleh Pulang" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Usia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 60 Tahun", Value = "< 60 Tahun" });
                result.Add(new SelectListItem() { Text = "> 60 Tahun", Value = "> 60 Tahun" });
                result.Add(new SelectListItem() { Text = "> 80 Tahun", Value = "> 80 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Riwayat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 1 Bulan", Value = "< 1 Bulan" });
                result.Add(new SelectListItem() { Text = "< 1 Tahun", Value = "< 1 Tahun" });
                result.Add(new SelectListItem() { Text = "Dirawat Sekarang", Value = "Dirawat Sekarang" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Aktifitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Dibantu Sebagai", Value = "Dibantu Sebagai" });
                result.Add(new SelectListItem() { Text = "Dibantu Penuh", Value = "Dibantu Penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Mobilitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Dengan Alat Bantu", Value = "Dengan Alat Bantu" });
                result.Add(new SelectListItem() { Text = "Koordinasi/Keseimbangan Buruk", Value = "Koordinasi/Keseimbangan Buruk" });
                result.Add(new SelectListItem() { Text = "Dibantu Sebagian", Value = "Dibantu Sebagian" });
                result.Add(new SelectListItem() { Text = "Dibantu Penuh Bed Rest / Nurse Assistent", Value = "Dibantu Penuh Bed Rest / Nurse Assistent" });
                result.Add(new SelectListItem() { Text = "Lingkungan dengan banyak furniture", Value = "Lingkungan dengan banyak furniture" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Defisit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kacamata Bukan Bifocal", Value = "Kacamata Bukan Bifocal" });
                result.Add(new SelectListItem() { Text = "Kacamata Bifocal", Value = "Kacamata Bifocal" });
                result.Add(new SelectListItem() { Text = "Gangguan Pendengaran", Value = "Gangguan Pendengaran" });
                result.Add(new SelectListItem() { Text = "Kacamata Multifocal", Value = "Kacamata Multifocal" });
                result.Add(new SelectListItem() { Text = "Katarak / Glaucoma", Value = "Katarak / Glaucoma" });
                result.Add(new SelectListItem() { Text = "Hampir tidak bisa / Melihat / Buta", Value = "Hampir tidak bisa / Melihat / Buta" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Kognitif
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Orientasi Baik", Value = "Orientasi Baik" });
                result.Add(new SelectListItem() { Text = "Kesulitan Mengerti Perintah", Value = "Kesulitan Mengerti Perintah" });
                result.Add(new SelectListItem() { Text = "Gangguan Memori", Value = "Gangguan Memori" });
                result.Add(new SelectListItem() { Text = "Kebinggunan", Value = "Kebinggunan" });
                result.Add(new SelectListItem() { Text = "Disorientasi", Value = "Disorientasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24PolaBab
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Inkontinesia Urine/Faeces", Value = "Inkontinesia Urine/Faeces" });
                result.Add(new SelectListItem() { Text = "Nokturia", Value = "Nokturia" });
                result.Add(new SelectListItem() { Text = "Urgensi / Frekwensi", Value = "Urgensi / Frekwensi" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Pengobatan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "> 4 Jenis", Value = "> 4 Jenis" });
                result.Add(new SelectListItem() { Text = "Anti Hipertensi / Hipoglikena /  Anti Depressan", Value = "Anti Hipertensi / Hipoglikena /  Anti Depressan" });
                result.Add(new SelectListItem() { Text = "Sedatif / Psikotropika / Narkotika", Value = "Sedatif / Psikotropika / Narkotika" });
                result.Add(new SelectListItem() { Text = "Infus Epidural / Spinal / DK / Traksi", Value = "Infus Epidural / Spinal / DK / Traksi" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Komorbiditas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diabetes / Penyakit Jantung / Stroke / ISK", Value = "Diabetes / Penyakit Jantung / Stroke / ISK" });
                result.Add(new SelectListItem() { Text = "Gangguan Saraf Pusat / Parkinson", Value = "Gangguan Saraf Pusat / Parkinson" });
                result.Add(new SelectListItem() { Text = "Paska Bedah 0 - 24 jam", Value = "Paska Bedah 0 - 24 jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24GambaranEKG
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sinus Ritme", Value = "Sinus Ritme" });
                result.Add(new SelectListItem() { Text = "Sinus Takikardia", Value = "Sinus Takikardia" });
                result.Add(new SelectListItem() { Text = "Sinus Bradikardi", Value = "Sinus Bradikardi" });
                result.Add(new SelectListItem() { Text = "Supra Vent. Takikardia", Value = "Supra Vent. Takikardia" });
                result.Add(new SelectListItem() { Text = "Blok Derajat", Value = "Blok Derajat" });
                result.Add(new SelectListItem() { Text = "Ventrikel Fibrilasi", Value = "Ventrikel Fibrilasi" });
                result.Add(new SelectListItem() { Text = "Ventrikel Takikardia", Value = "Ventrikel Takikardia" });
                result.Add(new SelectListItem() { Text = "Ventrikel Ekstra Sistol", Value = "Ventrikel Ekstra Sistol" });
                result.Add(new SelectListItem() { Text = "Pacing Capture", Value = "Pacing Capture" });
                result.Add(new SelectListItem() { Text = "Blok Derajat 2", Value = "Blok Derajat 2" });
                result.Add(new SelectListItem() { Text = "Supra Vent. Ekstra Sistole", Value = "Supra Vent. Ekstra Sistole" });
                result.Add(new SelectListItem() { Text = "Atrial Flutter", Value = "Supra Vent. Ekstra Sistole" });
                result.Add(new SelectListItem() { Text = "Atrial Fibrilasi", Value = "Atrial Fibrilasi" });
                result.Add(new SelectListItem() { Text = "Pacing Non Capture", Value = "Pacing Non Capture" });
                result.Add(new SelectListItem() { Text = "Blok Derajat 3", Value = "Blok Derajat 3" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24EskpresiWajah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Rileks", Value = "Rileks" });
                result.Add(new SelectListItem() { Text = "Tegang Partial", Value = "Tegang Partial" });
                result.Add(new SelectListItem() { Text = "Tegang", Value = "Tegang" });
                result.Add(new SelectListItem() { Text = "Meringis", Value = "Meringis" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24EkstremitasAtas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Bergerak", Value = "Tidak Bergerak" });
                result.Add(new SelectListItem() { Text = "Menekuk Partial", Value = "Menekuk Partial" });
                result.Add(new SelectListItem() { Text = "Menekuk Dengan Fleksi Jari", Value = "Menekuk Dengan Fleksi Jari" });
                result.Add(new SelectListItem() { Text = "Retraksi Permanen", Value = "Retraksi Permanen" });
                return result;
            }
        }

        public static List<SelectListItem> ListM24Compliance
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Toleransi Baik", Value = "Toleransi Baik" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGIrama
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sinus", Value = "Sinus" });
                result.Add(new SelectListItem() { Text = "Reguler", Value = "Reguler" });
                result.Add(new SelectListItem() { Text = "Ireguler", Value = "Ireguler" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGFrekuensi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Bradikardia", Value = "Bradikardia" });
                result.Add(new SelectListItem() { Text = "Takikardia", Value = "Takikardia" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGGelombang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Bertakik", Value = "Bertakik" });
                result.Add(new SelectListItem() { Text = "Tinggi", Value = "Tinggi" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGInterval
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Memanjang", Value = "Memanjang" });
                return result;
            }
        }


        public static List<SelectListItem> ListEKGAxis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "LAD", Value = "LAD" });
                result.Add(new SelectListItem() { Text = "RAD", Value = "RAD" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGAmplitudo
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "LVH", Value = "LVH" });
                result.Add(new SelectListItem() { Text = "RVH", Value = "RVH" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGMorfologi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Q Patologis", Value = "Q Patologis" });
                result.Add(new SelectListItem() { Text = "LBBB", Value = "LBBB" });
                result.Add(new SelectListItem() { Text = "RBBB", Value = "RBBB" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGSegmen
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "ST", Value = "ST" });
                result.Add(new SelectListItem() { Text = "T", Value = "T" });
                return result;
            }
        }

        public static List<SelectListItem> ListEKGGelombangT
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Inversi / Tinggi", Value = "Inversi / Tinggi" });
                return result;
            }
        }

        public static List<SelectListItem> ListMonitorRisikoJatuh
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sinus", Value = "Sinus" });
                result.Add(new SelectListItem() { Text = "Reguler", Value = "Reguler" });
                result.Add(new SelectListItem() { Text = "Ireguler", Value = "Ireguler" });
                return result;
            }
        }

        public static List<SelectListItem> ListMetodeDurasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diskusi (D)", Value = "Diskusi (D)" });
                result.Add(new SelectListItem() { Text = "Demonstrasi (Demo)", Value = "Demonstrasi (Demo)" });
                result.Add(new SelectListItem() { Text = "Ceramah (S)", Value = "Ceramah (S)" });
                result.Add(new SelectListItem() { Text = "Observasi (O)", Value = "Observasi (O)" });
                result.Add(new SelectListItem() { Text = "Praktek Langsung (PL)", Value = "Praktek Langsung (PL)" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaranMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Alert", Value = "Alert" });
                result.Add(new SelectListItem() { Text = "Verbal", Value = "Verbal" });
                result.Add(new SelectListItem() { Text = "Pain", Value = "Pain" });
                result.Add(new SelectListItem() { Text = "Unresponsive", Value = "Unresponsive" });
                return result;
            }
        }

        public static List<SelectListItem> ListPernapasanMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 8 x/mnt", Value = "<= 8 x/mnt" });
                result.Add(new SelectListItem() { Text = "9 - 11 x/mnt", Value = "9 - 11 x/mnt" });
                result.Add(new SelectListItem() { Text = "12 - 20 x/mnt", Value = "12 - 20 x/mnt" });
                result.Add(new SelectListItem() { Text = "21 - 24 x/mnt", Value = "21 - 24 x/mnt" });
                result.Add(new SelectListItem() { Text = ">= 25 x/mnt", Value = "≥ 25 x/mnt" });
                return result;
            }
        }

        public static List<SelectListItem> ListSaturasiMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 91%", Value = "<= 91%" });
                result.Add(new SelectListItem() { Text = "92 - 93%", Value = "92 - 93%" });
                result.Add(new SelectListItem() { Text = "94 - 95%", Value = "94 - 95%" });
                result.Add(new SelectListItem() { Text = "96%", Value = "96%" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarahMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 90", Value = "< 90" });
                result.Add(new SelectListItem() { Text = "90 - 99", Value = "90 - 99" });
                result.Add(new SelectListItem() { Text = "100 - 109", Value = "100 - 109" });
                result.Add(new SelectListItem() { Text = "110 - 219", Value = "110 - 219" });
                result.Add(new SelectListItem() { Text = ">= 220", Value = ">= 220" });
                return result;
            }
        }

        public static List<SelectListItem> ListDenyutJantungMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 40", Value = "<= 40" });
                result.Add(new SelectListItem() { Text = "41 - 50", Value = "41 - 50" });
                result.Add(new SelectListItem() { Text = "50 - 90", Value = "50 - 90" });
                result.Add(new SelectListItem() { Text = "91 - 110", Value = "91 - 110" });
                result.Add(new SelectListItem() { Text = "111 - 130", Value = "111 - 130" });
                result.Add(new SelectListItem() { Text = ">= 131", Value = ">= 131" });
                return result;
            }
        }

        public static List<SelectListItem> ListSuhuAxillaMEWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<= 35", Value = "<= 35" });
                result.Add(new SelectListItem() { Text = "35.1 - 36", Value = "35.1 - 36" });
                result.Add(new SelectListItem() { Text = "36.1 - 38.0", Value = "36.1 - 38.0" });
                result.Add(new SelectListItem() { Text = "38.1 - 39.0", Value = "38.1 - 39.0" });
                result.Add(new SelectListItem() { Text = ">= 39.1", Value = ">= 39.1" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaranMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "A", Value = "A" });
                result.Add(new SelectListItem() { Text = "V/P/U", Value = "V/P/U" });
                return result;
            }
        }

        public static List<SelectListItem> ListPernapasanMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<12", Value = "<12" });
                result.Add(new SelectListItem() { Text = "12-20", Value = "12-20" });
                result.Add(new SelectListItem() { Text = "21-25", Value = "21-25" });
                result.Add(new SelectListItem() { Text = ">25", Value = ">25" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarahSistolikMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<90", Value = "<90" });
                result.Add(new SelectListItem() { Text = "90-140", Value = "90-140" });
                result.Add(new SelectListItem() { Text = "141-150", Value = "141-150" });
                result.Add(new SelectListItem() { Text = "151-160", Value = "151-160" });
                result.Add(new SelectListItem() { Text = ">160", Value = ">160" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarahDiastolikMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "60-90", Value = "60-90" });
                result.Add(new SelectListItem() { Text = "91-100", Value = "91-100" });
                result.Add(new SelectListItem() { Text = "101-110", Value = "101-110" });
                result.Add(new SelectListItem() { Text = ">110", Value = ">110" });
                return result;
            }
        }

        public static List<SelectListItem> ListDenyutNadiMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<50", Value = "<50" });
                result.Add(new SelectListItem() { Text = "50-60", Value = "50-60" });
                result.Add(new SelectListItem() { Text = "61-100", Value = "61-100" });
                result.Add(new SelectListItem() { Text = "101-110", Value = "101-110" });
                result.Add(new SelectListItem() { Text = "111-120", Value = "111-120" });
                result.Add(new SelectListItem() { Text = ">120", Value = ">120" });
                return result;
            }
        }
        public static List<SelectListItem> ListSuhuAxillaMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<36", Value = "<36" });
                result.Add(new SelectListItem() { Text = "36.1-37.2", Value = "36.1-37.2" });
                result.Add(new SelectListItem() { Text = "37.5-37.7", Value = "37.5-37.7" });
                result.Add(new SelectListItem() { Text = ">37.7", Value = ">37.7" });
                return result;
            }
        }
        

        public static List<SelectListItem> ListSaturasiMEOWS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<92", Value = "<92" });
                result.Add(new SelectListItem() { Text = "92-95", Value = "92-95" });
                result.Add(new SelectListItem() { Text = ">95", Value = ">95" });
                return result;
            }
        }

        public static List<SelectListItem> ListProteinUrineAbnormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Negatif", Value = "Negatif" });
                result.Add(new SelectListItem() { Text = "+1", Value = "+1" });
                result.Add(new SelectListItem() { Text = ">= +2", Value = ">= +2" });
                return result;
            }
        }

        public static List<SelectListItem> paramReportSection
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmSection.Where(e => e.TipePelayanan == "RI" || e.TipePelayanan == "RJ" || e.TipePelayanan == "PENUNJANG" || e.TipePelayanan == "PENUNJANG2").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID
                    });
                }
                return r;
            }
        }

    }

}