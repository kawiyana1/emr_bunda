﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRGrafikTandaKardinalController : Controller
    {
        // GET: EMRGrafikTandaKardinal
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var s = new EMREntities())
            {
                var dokumen = s.GrafikTandaKardinal.Where(x => x.NoBukti == id).ToList();
                if (dokumen != null)
                {
                    model.EMRGrafikKardial_List = new ListDetail<EMRGrafikTandaKardinalViewModel>();
                    var detail_1 = s.GrafikTandaKardinal.Where(x => x.NoBukti == id).ToList();
                    foreach (var x in detail_1)
                    {
                        var y = IConverter.Cast<EMRGrafikTandaKardinalViewModel>(x);

                        //var t = new BridgingSIMRS();
                        //var getobat = t.GetObatByKode(y.KodeObat);
                        //if (getobat == null) y.NamaObat = "";
                        ////if (getobat.metadata.code != "200") new Exception(getobat.metadata.message);
                        //if (getobat != null) y.NamaObat = getobat.response.NamaBarang;

                        //var dokter = s.mDokter.FirstOrDefault(e => e.DokterID == y.DPJP);
                        //if (dokter != null)
                        //{
                        //    y.DPJPNama = dokter.NamaDokter;
                        //}
                        model.EMRGrafikKardial_List.Add(false, y);
                    }
                    model.MODEVIEW = view;
                    model.Report = 1;
                }
                model.NoBukti = id;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.GrafikTandaKardinal.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Grafik Tanda Tanda Kardial";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Grafik Tanda Tanda Kardial";
                    }

                    if (item.EMRGrafikKardial_List == null) item.EMRGrafikKardial_List = new ListDetail<EMRGrafikTandaKardinalViewModel>();
                    item.EMRGrafikKardial_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRGrafikKardial_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        //x.Model.Diagnosa = item.Diagnosa;
                        //x.Model.Alergi = item.Alergi;
                        x.Model.Section = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRGrafikKardial_List;
                    var real_list = s.GrafikTandaKardinal.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.GrafikTandaKardinal.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.GrafikTandaKardinal.Add(IConverter.Cast<GrafikTandaKardinal>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.Nadi = x.Model.Nadi;
                            _m.Suhu = x.Model.Suhu;
                            _m.Pernafasan = x.Model.Pernafasan;
                            _m.TekananDarah = x.Model.TekananDarah;
                          
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}