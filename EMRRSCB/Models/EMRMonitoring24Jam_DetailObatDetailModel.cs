﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoring24Jam_DetailObatDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Enteral { get; set; }
        public string EnteralNama { get; set; }
        public string Parental { get; set; }
        public string ParentalNama { get; set; }
        public string PemeriksaanPenunjang { get; set; }
    }
}