﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRDaftarKeluahanKomplinPasienViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Tanggal { get; set; }
        public string Keluhan { get; set; }
        public string Username { get; set; }
        public string Penyelesaian { get; set; }
        public string Keterangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}