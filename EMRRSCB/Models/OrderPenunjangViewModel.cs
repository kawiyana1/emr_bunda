﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class OrderPenunjangViewModel
    {
        public string NoBukti { get; set; }
        public string NoBuktiMemo { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string SectionTujuanNama { get; set; }
        public string DokterNama { get; set; }
        public string Memo { get; set; }
        public string SudahPeriksa { get; set; }
        public string NoReg { get; set; }
        public string NamaPasien { get; set; }
        public Nullable<bool> Hasil_Cito { get; set; }
        public Nullable<bool> Hasil_Rutin { get; set; }
        public string KodeICD { get; set; }
        public string DiagnosaIndikasi { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public string NamaVendor { get; set; }
        public string AlamatVendor { get; set; }
        public string TlpVendor { get; set; }
        public Nullable<System.DateTime> DiterimaTgl { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public Nullable<System.DateTime> JamSampling { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> JamSampling_Tgl { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamSampling_Jam { get; set; }
        public Nullable<System.DateTime> JamBilling { get; set; }
        public string KodePetugas { get; set; }
        public string NamaPetugas { get; set; }
        public string PemeriksaanTambahan { get; set; }
        public Nullable<bool> Realisasi { get; set; }
        public string UserId { get; set; }
        public string SectionID { get; set; }
        public string SectionAsalID { get; set; }
        public string SectionAsalIDNama { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<int> NomorDataREG { get; set; }
        public string KodeVendor { get; set; }

        public List<OrderPenunjang_OrderManual> OrderManual { get; set; }

        public List<OrderPenunjang_Jenis> Jenis { get; set; }
        public List<OrderPenunjang_Kelompok> Kelompok { get; set; }
        public List<OrderPenunjang_Jasa> Jasa { get; set; }
    }

    public class OrderPenunjang_Jenis
    {
        public int ID { get; set; }
        public string SectionID { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string Nama { get; set; }
        public string SectionID_Text { get; set; }
    }

    public class OrderPenunjang_Kelompok
    {
        public int ID { get; set; }
        public int JenisID { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string Nama { get; set; }
        public string JenisID_Text { get; set; }
    }

    public class OrderPenunjang_Jasa
    {
        public string NoBukti { get; set; }
        public Nullable<int> UrutJenis { get; set; }
        public string NamaJenis { get; set; }
        public Nullable<int> UrutKelompok { get; set; }
        public int ID_Kelompok { get; set; }
        public string NamaKelompok { get; set; }
        public Nullable<int> UrutJasa { get; set; }
        public string NamaTindakan { get; set; }
        public Nullable<bool> Dipilih { get; set; }
        public Nullable<bool> Realisasi { get; set; }
        public Nullable<bool> DiReferal { get; set; }

        public Nullable<int> Nomor { get; set; }
        public int Kelompok_ID { get; set; }
        public string JasaID { get; set; }
        public string NamaAlias { get; set; }
        public string NamaJasa { get; set; }
        public bool chckJasa { get; set; }
    }

    public class OrderPenunjang_OrderManual
    {
        public string NoBukti { get; set; }
        public Nullable<int> NoUrut { get; set; }
        public string JasaID { get; set; }
        public string NamaJasa { get; set; }
        public int Qty { get; set; }
        public Nullable<decimal> Tarif { get; set; }
        public string Tarif_View { get; set; }
        public string DokterID { get; set; }
        public string Keterangan { get; set; }
        public string NamaDokter { get; set; }
        public Nullable<bool> Realisasi { get; set; }
    }
}