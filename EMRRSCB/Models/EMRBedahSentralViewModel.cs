﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRBedahSentralViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string AhliBedah { get; set; }
        public string AhliBedahNama { get; set; }
        public string Asisten { get; set; }
        public string AsistenNama { get; set; }
        public string Instrumen { get; set; }
        public string InstrumenNama { get; set; }
        public string Sirkulasi { get; set; }
        public string SirkulasiNama { get; set; }
        public string AhliAnestesi { get; set; }
        public string AhliAnestesiNama { get; set; }
        public string PenataAnestesi { get; set; }
        public string PenataAnestesiNama { get; set; }
        public string JenisOperasi { get; set; }
        public string JenisAnestesi { get; set; }
        public string DiagnosaPreOperatif { get; set; }
        public string DiagnosaPostOperatif { get; set; }
        public string MacamOperasi { get; set; }
        public string KomplikasiPascaOperasi { get; set; }
        public string JaringanDikirim { get; set; }
        public string Kultur { get; set; }
        public string Pendarahan { get; set; }
        public string Implant { get; set; }
        public string Implant_Ket { get; set; }
        public string PerawatanPascaOperasi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglOperasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamMulaiOperasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamSelesaiOperasi { get; set; }
        public string LamaOperasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamMulaiAnestesi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamSelesaiAnestesi { get; set; }
        public string LamaAnestesi { get; set; }
        public string LaporanOperasi { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string NamaPasien { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string JamMulaiOperasiManual { get; set; }
        public string JamSelesaiOperasiManual { get; set; }
        public string JamMulaiAnestesiManual { get; set; }
        public string JamSelesaiAnestesiManual { get; set; }
        public string TTDDokter { get; set; }
        public bool VerifikasiDokter { get; set; }


        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
        public List<SelectItemListBedahCentral> ListTemplate { get; set; }
        public int MODEVIEW { get; set; }
      
    }

    public class SelectItemListBedahCentral
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}