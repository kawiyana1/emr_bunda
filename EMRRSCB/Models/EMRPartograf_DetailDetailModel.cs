﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPartograf_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string DenyutJantung { get; set; }
        public string AirKetuban { get; set; }
        public string PembukaanServik { get; set; }
        public string TurunanKepala { get; set; }
        public string Waktu { get; set; }
        public string KontraksiTiapMenit { get; set; }
        public string Oksitosin { get; set; }
        public string Tetes { get; set; }
        public string ObatDanCairan { get; set; }
        public string Nadi { get; set; }
        public string TekananDarah { get; set; }
        public string Temperatur { get; set; }
        public string Protein { get; set; }
        public string Aseton { get; set; }
        public string Volume { get; set; }
    }
}