﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringBalanceCarianViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string MinumJenis { get; set; }
        public string MinumJumlah { get; set; }
        public string IntravenaJenis { get; set; }
        public string IntravenaJumlah { get; set; }
        public string SisaJenis { get; set; }
        public string SisaJumlah { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam1 { get; set; }
        public string Urine { get; set; }
        public string LainLain { get; set; }


    }
}