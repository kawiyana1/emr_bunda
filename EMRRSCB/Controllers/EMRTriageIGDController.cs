﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRTriageIGDController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRHeaderViewDetail();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.TriageIGD.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRTriage_List = new ListDetail<EMRTriageIGDViewModel>();
                        var detail_1 = s.TriageIGD.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRTriageIGDViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Petugas);
                            if (dokter != null)
                            {
                                y.PetugasNama = dokter.NamaDOkter;
                            }
                            model.EMRTriage_List.Add(false, y);

                        }
                        model.MODEVIEW = view;
                    }
                    model.NoBukti = id;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.TriageIGD.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Triage IGD ";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Triage IGD ";
                    }

                    if (item.EMRTriage_List == null) item.EMRTriage_List = new ListDetail<EMRTriageIGDViewModel>();
                    item.EMRTriage_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRTriage_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.EMRTriage_List;
                    var real_list = s.TriageIGD.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.TriageIGD.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {
                            _m = new TriageIGD();
                            s.TriageIGD.Add(IConverter.Cast<TriageIGD>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<TriageIGD>(x.Model);
                            s.TriageIGD.AddOrUpdate(_m);
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}