﻿using iHos.MVC.Master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class SqlCon_EMR : MasterSqlCon
    {
        public SqlCon_EMR(string sp_name, string typeTable_name, DataTable table)
        {
            ConString = ConfigurationManager.ConnectionStrings["EMR_EntitiesManual"].ConnectionString;
            SP_Name = sp_name;
            TypeTable_Name = typeTable_name;
            Table = table;
        }

        public SqlCon_EMR()
        {
            ConString = ConfigurationManager.ConnectionStrings["EMR_EntitiesManual"].ConnectionString;
        }
    }
}