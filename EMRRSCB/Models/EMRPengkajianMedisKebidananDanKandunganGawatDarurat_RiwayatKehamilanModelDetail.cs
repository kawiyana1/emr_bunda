﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPatrus { get; set; }
        public bool Abortus { get; set; }
        public bool Prematur { get; set; }
        public bool Aterm { get; set; }
        public string JenisPartus { get; set; }
        public bool Nakes { get; set; }
        public bool Non { get; set; }
        public bool JenisKelaminLaki { get; set; }
        public bool JenisKelaminPerempuan { get; set; }
        public bool BBL { get; set; }
        public bool Normal { get; set; }
        public bool Cacat { get; set; }
        public bool Meninggal { get; set; }
        public string Komplikasi { get; set; }
        public string Username { get; set; }
        public string TglPartus { get; set; }
        public string BBLText { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }
    }
}