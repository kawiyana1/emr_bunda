﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class PengaturanDPJPController : Controller
    {
        // GET: PengaturanDPJP
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg)
        {
            var model = new PengaturanDPJPViewModel();

            using (var s = new SIM_Entities())
            {
                var dokumen = s.EMR_trDPJP.Where(x => x.Noreg == noreg).ToList();
                if (dokumen != null)
                {
                    model.Detail_List = new ListDetail<PengaturanDPJPDetailModel>();
                    var detail_1 = s.EMR_trDPJP.Where(x => x.Noreg == noreg).ToList();
                    foreach (var x in detail_1)
                    {
                        var y = IConverter.Cast<PengaturanDPJPDetailModel>(x);

                        var dokter = s.mDokter.Where(z => z.DokterID == x.DokterId).FirstOrDefault();
                        y.NamaDokter = dokter.NamaDOkter;

                        var status = s.EMR_mStatusDPJP.Where(z => z.StatusDPJP == x.Status).FirstOrDefault();
                        if (status != null) { 
                        y.StatusNama = status.StatusDPJP;
                        }

                        model.Detail_List.Add(false, y);
                    }
                }
                model.Noreg = noreg;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new PengaturanDPJPViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var activity = "Pengaturan DPJP";
                    if (item.Detail_List == null) item.Detail_List = new ListDetail<PengaturanDPJPDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    var d = s.EMR_trDPJP.Where(x => x.Noreg == item.Noreg).ToList();
                    foreach (var x in d)
                    {
                        var model = s.EMR_trDPJP.FirstOrDefault(z => z.Id == item.Id);
                        //delete
                        if (model == null)
                        {
                            s.EMR_DPJP_Delete((int)x.Id);
                        }
                    }
                    foreach (var x in item.Detail_List)
                    {
                        var _d = d.FirstOrDefault(y => y.Id == x.Model.Id);
                        var dokterdetail = s.mDokter.FirstOrDefault(c => c.DokterID == x.Model.DokterId);
                        if (_d == null)
                        {
                            // new
                            s.EMR_DPJP_Insert(
                                item.Noreg,
                                x.Model.DokterId,
                                dokterdetail.NamaDOkter,
                                x.Model.Status
                            );
                        }
                        else
                        {
                            // edit
                            s.EMR_DPJP_Update(
                               x.Model.Id,
                               x.Model.Status
                               );
                        }

                    }

                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.Noreg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result,-1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}