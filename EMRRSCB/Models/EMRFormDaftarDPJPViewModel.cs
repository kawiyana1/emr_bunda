﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormDaftarDPJPViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Diagnosa { get; set; }
        public string DPJPNama { get; set; }
        public string DPJPNamaNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJPTglMulai { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJPTglAkhir { get; set; }
        public string DPJPUtamaNama { get; set; }
        public string DPJPUtamaNamaNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJPUtamaTglMulai { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJPUtamaTglAkhir { get; set; }
        public string Keterangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}