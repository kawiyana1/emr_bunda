﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRujukanKeluarViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Kepada { get; set; }
        public string Di { get; set; }
        public string Anamnesa { get; set; }
        public string PemeriksaanFisik { get; set; }
        public string PemeriksaanPenunjang { get; set; }
        public string Diagnosa { get; set; }
        public string Therapy { get; set; }
        public string Disetujui { get; set; }
        public string DisetujuiNama { get; set; }
        public string Mengetahui { get; set; }
        public string MengetahuiNama { get; set; }
        public string Menyerahkan { get; set; }
        public string MenyerahkanNama { get; set; }
        public string TTDDisetujui { get; set; }
        public string TTDMengetahui { get; set; }
        public string TTDMenyerahkan { get; set; }



        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}