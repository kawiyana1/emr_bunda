﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NamaObat { get; set; }
        public string NamaObatNama { get; set; }
        public string Dosis { get; set; }
        public string BerapaLama { get; set; }
        public string AlasanMakanObat { get; set; }
        public string BerlanjutRanap { get; set; }
        public string Username { get; set; }
        public string AturanPakaiRute { get; set; }
        public string TindakLanjut { get; set; }
        public string PerubahanAturanPakai { get; set; }
        public string Indikasi { get; set; }
        public string BerlanjutRanap2 { get; set; }
    }
}