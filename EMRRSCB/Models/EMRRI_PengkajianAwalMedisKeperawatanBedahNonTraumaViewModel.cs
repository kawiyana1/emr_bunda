﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRI_PengkajianAwalMedisKeperawatanBedahNonTraumaViewModel
    {
        public ListDetail<EMRRI_RencanaKerja_DetailModel> RencanaKerja_List { get; set; }
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDatang { get; set; }
        public string PemeriksaanFisikTekananDarah { get; set; }
        public string PemeriksaanFisikRR { get; set; }
        public string PemeriksaanFisikNadi { get; set; }
        public string PemeriksaanFisikSuhu { get; set; }
        public string PemeriksaanFisikBB { get; set; }
        public string PemeriksaanFisikTB { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriPenilaianNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeriSkor { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPenyebab { get; set; }
        public string SkriningNyeriPenyebabKet { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string AnamnesaPenyakitKeluarga { get; set; }
        public string AnamnesaPengobatan { get; set; }
        public string AnamnesaRiwayatOperasi { get; set; }
        public string AnamnesaRiwayatTranfusi { get; set; }
        public string TandaVitalKeadaanUmum { get; set; }
        public string TandaVitalGizi { get; set; }
        public string TandaVitalGCS_E { get; set; }
        public string TandaVitalGCS_V { get; set; }
        public string TandaVitalGCS_M { get; set; }
        public string TandaVitalTindakanResusitasi { get; set; }
        public string TandaVitalTindakanResusitasi_Ket { get; set; }
        public string TandaVitalTensi { get; set; }
        public string TandaVitalRR { get; set; }
        public string TandaVitalNadi { get; set; }
        public string TandaVitalSuhuAxilla { get; set; }
        public string TandaVitalSuhuRectal { get; set; }
        public string TandaVitalBB { get; set; }
        public string PemeriksaanKepala { get; set; }
        public string PemeriksaanKepala_Ket { get; set; }
        public string PemeriksaanMata { get; set; }
        public string PemeriksaanMata_Ket { get; set; }
        public string PemeriksaanTHT { get; set; }
        public string PemeriksaanTHT_Ket { get; set; }
        public string PemeriksaanLeher { get; set; }
        public string PemeriksaanLeher_Ket { get; set; }
        public string PemeriksaanDada { get; set; }
        public string PemeriksaanDada_Ket { get; set; }
        public string PemeriksaanJantung { get; set; }
        public string PemeriksaanJantung_Ket { get; set; }
        public string PemeriksaanParu { get; set; }
        public string PemeriksaanParu_Ket { get; set; }
        public string PemeriksaanPerut { get; set; }
        public string PemeriksaanPerut_Ket { get; set; }
        public string PemeriksaanHeper { get; set; }
        public string PemeriksaanHeper_Ket { get; set; }
        public string PemeriksaanLien { get; set; }
        public string PemeriksaanLien_Ket { get; set; }
        public string PemeriksaanPunggung { get; set; }
        public string PemeriksaanPunggung_Ket { get; set; }
        public string PemeriksaanGenital { get; set; }
        public string PemeriksaanGenital_Ket { get; set; }
        public string PemeriksaanEktrimitas { get; set; }
        public string PemeriksaanEktrimitas_Ket { get; set; }
        public string PemeriksaanRectalToucher { get; set; }
        public string PemeriksaanRectalToucher_Ket { get; set; }
        public string PengkajianKebutuhanKhusus { get; set; }
        public string Laboratorium { get; set; }
        public string Xray { get; set; }
        public string EKG { get; set; }
        public string DiagnosaKerja { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string ICDX { get; set; }
        public string PengkajianKeperawatan { get; set; }
        public string Terapi { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakDitemukanHambatanBelajar { get; set; }
        public bool PemeriksaanUmum_Kepala { get; set; }
        public string PemeriksaanUmum_KepalaKet { get; set; }
        public bool PemeriksaanUmum_Maxillofacial { get; set; }
        public string PemeriksaanUmum_MaxillofacialKet { get; set; }
        public bool PemeriksaanUmum_CSpine { get; set; }
        public string PemeriksaanUmum_CSpineKet { get; set; }
        public bool PemeriksaanUmum_Chest { get; set; }
        public string PemeriksaanUmum_ChestKet { get; set; }
        public bool PemeriksaanUmum_Abdomen { get; set; }
        public string PemeriksaanUmum_AbdomenKet { get; set; }
        public bool PemeriksaanUmum_Genital { get; set; }
        public string PemeriksaanUmum_GenitalKet { get; set; }
        public bool PemeriksaanUmum_Rectal { get; set; }
        public string PemeriksaanUmum_RectalKet { get; set; }
        public bool PemeriksaanUmum_Musculoskeletel { get; set; }
        public string PemeriksaanUmum_MusculoskeletelKet { get; set; }
        public bool Tenang { get; set; }
        public bool Cemas { get; set; }
        public bool Marah { get; set; }
        public bool Depresi { get; set; }
        public bool Gelisah { get; set; }
        public bool Kooperatif { get; set; }
        public bool TidakKooperatif { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string TTDDPJP { get; set; }
        public bool VerifikasiDPJP { get; set; } 


        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListBedahNonTraumaRI> ListTemplate { get; set; } 

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
    }

    public class SelectItemListBedahNonTraumaRI
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}