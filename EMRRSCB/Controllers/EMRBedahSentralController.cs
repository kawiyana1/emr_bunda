﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRBedahSentralController : Controller
    {
        // GET: EMRBedahSentral
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRBedahSentralViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.BedahSentral.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRBedahSentralViewModel>(dokumen);

                        model.NoBukti = id;
                        model.JenisKerjasama = (identitas.JenisKerjasama == null ? "" : identitas.JenisKerjasama);
                        model.Noreg = id;


                        var ahlibedah = sim.mDokter.FirstOrDefault(e => e.DokterID == model.AhliBedah);
                        if (ahlibedah != null) model.AhliBedahNama = ahlibedah.NamaDOkter;

                        var asisten = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Asisten);
                        if (asisten != null) model.AsistenNama = asisten.NamaDOkter;

                        var instrumen = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Instrumen);
                        if (instrumen != null) model.InstrumenNama = instrumen.NamaDOkter;

                        var sirkulasi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Sirkulasi);
                        if (sirkulasi != null) model.SirkulasiNama = sirkulasi.NamaDOkter;

                        var ahlianestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.AhliAnestesi);
                        if (ahlianestesi != null) model.AhliAnestesiNama = ahlianestesi.NamaDOkter;

                        var penataanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PenataAnestesi);
                        if (penataanestesi != null) model.PenataAnestesiNama = penataanestesi.NamaDOkter;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (dokter != null) model.DokterNama = dokter.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        DateTime Birth = new DateTime(1954, 7, 30);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;

                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListBedahCentral>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListBedahCentral()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRBedahSentralViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var carinrm = s.trDokumenAssesmenPasien.Where(d => d.NoBukti == copy).FirstOrDefault();
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == carinrm.NRM);
                    var dokumen = s.BedahSentral.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRBedahSentralViewModel>(dokumen);

                        model.NoBukti = id;
                        model.JenisKerjasama = (identitas.JenisKerjasama == null ? "" : identitas.JenisKerjasama);
                        model.Noreg = id;


                        var ahlibedah = sim.mDokter.FirstOrDefault(e => e.DokterID == model.AhliBedah);
                        if (ahlibedah != null) model.AhliBedahNama = ahlibedah.NamaDOkter;

                        var asisten = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Asisten);
                        if (asisten != null) model.AsistenNama = asisten.NamaDOkter;

                        var instrumen = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Instrumen);
                        if (instrumen != null) model.InstrumenNama = instrumen.NamaDOkter;

                        var sirkulasi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Sirkulasi);
                        if (sirkulasi != null) model.SirkulasiNama = sirkulasi.NamaDOkter;

                        var ahlianestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.AhliAnestesi);
                        if (ahlianestesi != null) model.AhliAnestesiNama = ahlianestesi.NamaDOkter;

                        var penataanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PenataAnestesi);
                        if (penataanestesi != null) model.PenataAnestesiNama = penataanestesi.NamaDOkter;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (dokter != null) model.DokterNama = dokter.NamaDOkter;

                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        DateTime Birth = new DateTime(1954, 7, 30);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;

                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                    }
                    model.VerifikasiDokter = false;

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListBedahCentral>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListBedahCentral()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRBedahSentralViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumenid = "";
                    var model = s.BedahSentral.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<BedahSentral>(item);
                        s.BedahSentral.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Woi Create Bedah Sentral";
                    }
                    else
                    {
                        model = IConverter.Cast<BedahSentral>(item);
                        s.BedahSentral.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Woi Update Bedah Sentral";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.Dokter;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}