﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSCB.Models;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Entities.EMR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class OrderResepController : Controller
    {
        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string copy = "")
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = 0;
            var item = new OrderResepViewModel();
            using (var z = new EMREntities())
            {
                using (var s = new SIM_Entities())
                {
                    var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (e == null) return HttpNotFound();
                    item.DokterID = e.DokterID;
                    item.Dokter = e.NamaDOkter;
                    item.JenisResep = "ResepNonRacik";

                    if (copy != "")
                    {
                        var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == copy);
                        if (m != null)
                        {
                            item = IConverter.Cast<OrderResepViewModel>(m);
                            item.KodePaket = m.KodePaket;
                            //item.NamaPaket = m.paket
                            item.QtyRacik = m.QtyPuyer;
                            item.ObatRacik = m.Puyer ?? false;
                            item.SatuanPuyer = m.SatuanPuyer;
                            item.Jumlah_View = m.Jumlah.ToMoney();
                            item.DokterID = m.DokterID;
                            item.Dokter = m.Dokter;
                            var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var x in d)
                            {
                                var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                                n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                                n.Harga_View = x.Harga.ToMoney();
                                item.Detail_List.Add(false, n);
                            }
                        }

                        var bill = s.Pelayanan_RiwayatObat.FirstOrDefault(x => x.NoBukti == copy);
                        if (bill != null)
                        {
                            item.KodePaket = "";
                            //item.NamaPaket = m.paket
                            item.QtyRacik = double.Parse(bill.QtyPuyer);
                            item.ObatRacik = bill.Puyer == 1 ? true : false;
                            item.SatuanPuyer = bill.SatuanPuyer;
                            item.Jumlah_View = bill.Jumlah.Value.ToMoney();
                            item.DokterID = bill.DokterID;
                            item.Dokter = bill.Dokter;
                            var dd = s.Pelayanan_RiwayatObatDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoBukti == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var xx in dd)
                            {
                                var nn = IConverter.Cast<OrderResepDetailViewModel>(xx);
                                decimal jumlah = (decimal)xx.Qty * xx.Harga;
                                nn.Jumlah_View = ((decimal)(jumlah)).ToMoney();
                                nn.Harga_View = xx.Harga.ToMoney();
                                item.Detail_List.Add(false, nn);
                            }
                        }
                    }
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen_temp = z.trDokumenTemplate.Where(a => a.DokumenID == "ResepNonRacik" && a.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = "ResepNonRacik";
                    ViewBag.NamaDokumen = "OrderResep";
                    item.ListTemplate = new List<SelectItemListResepNonRacik>();
                    foreach (var list in dokumen_temp)
                    {
                        item.ListTemplate.Add(new SelectItemListResepNonRacik()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpGet]
        [ActionName("CreateNonRacik")]
        public ActionResult CreateNonRacik_Get(string noreg, string copy = "")
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = 0;
            var item = new OrderResepViewModel();
            using (var z = new EMREntities())
            {
                using (var s = new SIM_Entities())
                {
                    var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (e == null) return HttpNotFound();
                    item.DokterID = e.DokterID;
                    item.Dokter = e.NamaDOkter;
                    item.JenisResep = "ResepRacik";

                    if (copy != "")
                    {
                        var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == copy);
                        if (m != null)
                        {
                            item = IConverter.Cast<OrderResepViewModel>(m);
                            item.KodePaket = m.KodePaket;
                            //item.NamaPaket = m.paket
                            item.QtyRacik = m.QtyPuyer;
                            item.ObatRacik = m.Puyer ?? false;
                            item.SatuanPuyer = m.SatuanPuyer;
                            item.Jam_View = m.Jam.ToString("HH:mm");
                            item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                            item.Jumlah_View = m.Jumlah.ToMoney();
                            item.QtyPuyer = (int)m.QtyPuyer;
                            item.SatuanPuyer = m.SatuanPuyer;
                            //item.DokterID = m.DokterID;
                            //item.Dokter = m.Dokter;
                            var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var x in d)
                            {
                                var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                                n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                                n.Harga_View = x.Harga.ToMoney();

                                var dosis = s.SIMtrResepDetail.FirstOrDefault(q => q.NoResep == x.NoResep && q.Barang_ID == x.Barang_ID);
                                if (dosis != null)
                                {
                                    item.Dosis1 = dosis.Dosis ?? "-";
                                    item.Dosis2 = dosis.Dosis2 ?? "-";
                                    n.AturanPakai = dosis.KetDosis;
                                }
                                item.Detail_List.Add(false, n);
                            }
                        }

                    }
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen_temp = z.trDokumenTemplate.Where(a => a.DokumenID == "ResepRacik" && a.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = "ResepRacik";
                    ViewBag.NamaDokumen = "OrderResep";
                    item.ListTemplate = new List<SelectItemListResepNonRacik>();
                    foreach (var list in dokumen_temp)
                    {
                        item.ListTemplate.Add(new SelectItemListResepNonRacik()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult Create_GetCopy(string noreg, string id, string copy = "")
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = 0;
            var item = new OrderResepViewModel();
            using (var z = new EMREntities())
            {
                using (var s = new SIM_Entities())
                {
                    //var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    var getdokter = s.EMR_FN_GetHistoryResep().Where(x => x.NoResep == copy).FirstOrDefault();

                    var dokter = s.mDokter.Where(dkt => dkt.DokterID == getdokter.DokterID).FirstOrDefault();
                    item.DokterID = (dokter.DokterID);
                    item.Dokter = dokter.NamaDOkter;
                    item.JenisResep = "ResepNonRacik";
                    if (dokter == null) return HttpNotFound();


                    if (copy != "")
                    {
                        var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == copy);
                        if (m != null)
                        {
                            item = IConverter.Cast<OrderResepViewModel>(m);
                            item.KodePaket = m.KodePaket;
                            //item.NamaPaket = m.paket
                            item.QtyRacik = m.QtyPuyer;
                            item.ObatRacik = m.Puyer ?? false;
                            item.SatuanPuyer = m.SatuanPuyer;
                            item.Jumlah_View = m.Jumlah.ToMoney();
                            item.DokterID = m.DokterID;
                            item.Dokter = m.Dokter;
                            var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var x in d)
                            {
                                var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                                n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                                n.Harga_View = x.Harga.ToMoney();
                                item.Detail_List.Add(false, n);
                            }
                        }

                        var bill = s.Pelayanan_RiwayatObat.FirstOrDefault(x => x.NoBukti == copy);
                        if (bill != null)
                        {
                            item.KodePaket = "";
                            //item.NamaPaket = m.paket
                            item.QtyRacik = double.Parse(bill.QtyPuyer);
                            item.ObatRacik = bill.Puyer == 1 ? true : false;
                            item.SatuanPuyer = bill.SatuanPuyer;
                            item.Jumlah_View = bill.Jumlah.Value.ToMoney();
                            item.DokterID = bill.DokterID;
                            item.Dokter = bill.Dokter;
                            var dd = s.Pelayanan_RiwayatObatDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoBukti == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var xx in dd)
                            {
                                var nn = IConverter.Cast<OrderResepDetailViewModel>(xx);
                                decimal jumlah = (decimal)xx.Qty * xx.Harga;
                                nn.Jumlah_View = ((decimal)(jumlah)).ToMoney();
                                nn.Harga_View = xx.Harga.ToMoney();
                                item.Detail_List.Add(false, nn);
                            }
                        }
                    }
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen_temp = z.trDokumenTemplate.Where(a => a.DokumenID == "ResepNonRacik" && a.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = "ResepNonRacik";
                    ViewBag.NamaDokumen = "OrderResep";
                    item.ListTemplate = new List<SelectItemListResepNonRacik>();
                    foreach (var list in dokumen_temp)
                    {
                        item.ListTemplate.Add(new SelectItemListResepNonRacik()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpGet]
        [ActionName("CreateCopyNonResep")]
        public ActionResult Create_NonGetCopy(string noreg, string id, string copy = "")
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = 0;
            var item = new OrderResepViewModel();
            using (var z = new EMREntities())
            {
                using (var s = new SIM_Entities())
                {
                    //var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    var getdokter = s.EMR_FN_GetHistoryResep().Where(x => x.NoResep == copy).FirstOrDefault();

                    var dokter = s.mDokter.Where(dkt => dkt.DokterID == getdokter.DokterID).FirstOrDefault();
                    item.DokterID = (dokter.DokterID);
                    item.Dokter = dokter.NamaDOkter;
                    item.JenisResep = "ResepRacik";
                    if (dokter == null) return HttpNotFound();


                    if (copy != "")
                    {
                        var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == copy);
                        if (m != null)
                        {
                            item = IConverter.Cast<OrderResepViewModel>(m);
                            item.KodePaket = m.KodePaket;
                            //item.NamaPaket = m.paket
                            item.QtyRacik = m.QtyPuyer;
                            item.ObatRacik = m.Puyer ?? false;
                            item.SatuanPuyer = m.SatuanPuyer;
                            item.Jumlah_View = m.Jumlah.ToMoney();
                            item.DokterID = m.DokterID;
                            item.Dokter = m.Dokter;
                            var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == copy).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var x in d)
                            {
                                var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                                n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                                n.Harga_View = x.Harga.ToMoney();
                                item.Detail_List.Add(false, n);
                            }
                        }

                        var bill = s.Pelayanan_RiwayatObat.FirstOrDefault(x => x.NoBukti == copy);
                        if (bill != null)
                        {
                            item.KodePaket = "";
                            //item.NamaPaket = m.paket
                            item.QtyRacik = double.Parse(bill.QtyPuyer);
                            item.ObatRacik = bill.Puyer == 1 ? true : false;
                            item.SatuanPuyer = bill.SatuanPuyer;
                            item.Jumlah_View = bill.Jumlah.Value.ToMoney();
                            item.DokterID = bill.DokterID;
                            item.Dokter = bill.Dokter;
                            var dd = s.Pelayanan_RiwayatObatDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoBukti == copy).ToList();
                            var rck = s.SIMtrResepDetail.Where(x => x.NoResep == m.NoResep).ToList();
                            item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                            foreach (var xx in dd)
                            {
                                var nn = IConverter.Cast<OrderResepDetailViewModel>(xx);
                                decimal jumlah = (decimal)xx.Qty * xx.Harga;
                                nn.Jumlah_View = ((decimal)(jumlah)).ToMoney();
                                nn.Harga_View = xx.Harga.ToMoney();

                                foreach (var rcik in rck) { 
                                    nn.Racikan1 = rcik.Racikan1 ?? false;
                                    nn.Racikan2 = rcik.Racikan2 ?? false;
                                    nn.Racikan3 = rcik.Racikan3 ?? false;
                                    nn.KetRacikan = rcik.KetRacikan;
                                }
                                item.Detail_List.Add(false, nn);
                            }
                        }
                    }
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen_temp = z.trDokumenTemplate.Where(a => a.DokumenID == "ResepRacik" && a.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = "ResepRacik";
                    ViewBag.NamaDokumen = "OrderResep";
                    item.ListTemplate = new List<SelectItemListResepNonRacik>();
                    foreach (var list in dokumen_temp)
                    {
                        item.ListTemplate.Add(new SelectItemListResepNonRacik()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }


        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new OrderResepViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var z = new EMREntities())
                    {
                        using (var s = new SIM_Entities())
                        {
                            #region Validation
                            if (item.Detail_List == null) { throw new Exception("Detail tidak boleh kosong"); }

                            item.Detail_List.RemoveAll(x => x.Remove);
                            decimal jumlah = 0;
                            foreach (var x in item.Detail_List)
                            {
                                //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                var b2 = s.Pelayanan_GetListObat(item.Farmasi_SectionID, item.NoRegistrasi).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                //qty * harga
                                x.Model.Harga = b2.Harga_Jual ?? 0;
                                var _harga = b2.Harga_Jual ?? 0;
                                var qty = (decimal)x.Model.Qty;
                                var _subJumlah = qty * _harga;
                                jumlah += _subJumlah;
                            }
                            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
                            var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoRegistrasi && x.SectionID == section);
                            #endregion

                            using (var dbContextTransaction = s.Database.BeginTransaction())
                            {
                                try
                                {
                                    var id = s.SIMtrResep_AutoID().FirstOrDefault();

                                    var Puyer = false;
                                    var QtyPuyer = 0;
                                    var SatuanPuyer = "";
                                    var dosis1 = "";
                                    var dosis2 = "";
                                    if (item.Puyer == true)
                                    {
                                        Puyer = true;
                                        QtyPuyer = item.QtyPuyer;
                                        SatuanPuyer = item.SatuanPuyer;
                                        dosis1 = item.Dosis1;
                                        dosis2 = item.Dosis2;
                                    }
                                    else
                                    {
                                        Puyer = false;
                                        QtyPuyer = 0;
                                        SatuanPuyer = "";
                                    }

                                    var model = new SIMtrResep()
                                    {
                                        NoResep = id,
                                        NoRegistrasi = item.NoRegistrasi,
                                        SectionID = item.SectionID,
                                        Tanggal = DateTime.Today,
                                        Jam = DateTime.Now,
                                        DokterID = item.DokterID,
                                        Jumlah = jumlah,
                                        KomisiDokter = 0,
                                        Cyto = item.Cyto,
                                        NoBukti = null,
                                        Farmasi_SectionID = item.Farmasi_SectionID,
                                        User_ID = 490,
                                        JenisKerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                        CompanyID = datareg.COmpanyID,
                                        NRM = datareg.NRM,
                                        NoKartu = datareg.NoKartu,
                                        KelasID = datareg.KelasID,
                                        KTP = datareg.PasienKTP,
                                        KerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                        //Realisasi = "",
                                        //PerusahaanID = "",
                                        RawatInap = datareg.RawatInap,
                                        //Batal = "",
                                        Paket = !string.IsNullOrEmpty(item.KodePaket),
                                        KodePaket = item.KodePaket,
                                        AmprahanRutin = false,
                                        IncludeJasa = false,
                                        //NoAntri = auto,
                                        UserNameInput = "",
                                        Keterangan = item.Keterangan,
                                        Puyer = Puyer,
                                        QtyPuyer = (int?)QtyPuyer,
                                        SatuanPuyer = SatuanPuyer,
                                        CustomerKerjasamaID = (int?)datareg.CustomerKerjasamaID,
                                        RasioObat_ada = false,
                                        Rasio_KelebihanDibayarPasien = false,
                                        RasioUmum_Alert = false,
                                        RasioUmum_Blok = false,
                                        RasioSpesialis_Alert = false,
                                        RasioSPesialis_Blok = false,
                                        RasioSub_Alert = false,
                                        RasioSub_Blok = false,
                                        RasioUmum_nilai = 0,
                                        RasioSpesialis_Nilai = 0,
                                        RasioSub_Nilai = 0,
                                        BeratBadan = item.BeratBadan,
                                        SedangProses = null,
                                        SedangProsesPC = null,
                                        AlasanBatal = null,
                                        UserID_Batal = null,
                                        ObatPulang = item.ObatPulang,
                                        Pending = false,
                                        AlasanPending = null,
                                        UserID_Pending = null,
                                        JenisResep = item.JenisResep,
                                    };
                                    s.SIMtrResep.Add(model);

                                    #region Detail
                                    foreach (var x in item.Detail_List)
                                    {
                                        var racik = true;
                                        if (item.Puyer != true)
                                        {
                                            dosis1 = x.Model.AturanPakai;
                                            racik = false;
                                        }

                                        var blokasi = s.mBarangLokasiNew.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                        var d = new SIMtrResepDetail()
                                        {
                                            NoResep = id,
                                            Barang_ID = x.Model.Barang_ID,
                                            Satuan = x.Model.Satuan,
                                            Qty = x.Model.Qty,
                                            Harga_Satuan = x.Model.Harga,
                                            Disc_Persen = 0,
                                            Stok = blokasi.Qty_Stok,
                                            KomisiDokter = 0,
                                            THT = 0,
                                            Racik = racik,
                                            Plafon = 0,
                                            KelebihanPLafon = 0,
                                            KelasID = null,
                                            KTP = null,
                                            JenisKerjasamaID = null,
                                            PerusahaanID = null,
                                            DokterID = null,
                                            SectionID = null,
                                            PPN = 0,
                                            DosisID = 0,
                                            JenisBarangID = 0,
                                            Embalase = 0,
                                            JasaRacik = 0,
                                            Dosis = x.Model.Dosis,
                                            Dosis2 = dosis2,
                                            KetDosis = x.Model.AturanPakai,
                                            QtyRacik = item.QtyRacik,
                                            TermasukPaket = false,
                                            Racikan1 = x.Model.Racikan1,
                                            Racikan2 = x.Model.Racikan2,
                                            Racikan3 = x.Model.Racikan3,
                                            KetRacikan = x.Model.KetRacikan,
                                            QtyHasilRacik = x.Model.JmlRacikan,
                                            //NomorUrut = 0
                                        };
                                        s.SIMtrResepDetail.Add(d);
                                    }
                                    #endregion

                                    var dokumenid = "";
                                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                                    dokumenid = item.JenisResep;

                                    if (item.save_template == true && item.nama_template != null)
                                    {
                                        var temp = new trDokumenTemplate();
                                        temp.NoBukti = id;
                                        temp.NamaTemplate = item.nama_template;
                                        temp.DokterID = item.DokterID;
                                        temp.DokumenID = dokumenid;
                                        temp.SectionID = sectionid;
                                        z.trDokumenTemplate.Add(temp);
                                    }
                                    z.SaveChanges();
                                    result = new ResultSS(s.SaveChanges());
                                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                    {
                                        Activity = $"Resep Create {id}"
                                    };
                                    UserActivity.InsertUserActivity(userActivity);
                                    dbContextTransaction.Commit();
                                    result.Data = (new { id = id, farmasi = model.Farmasi_SectionID, qtypuyer = model.QtyPuyer, satuanpuyer = model.SatuanPuyer, keterengan = model.Keterangan, thisdetail = item.Detail_List });
                                }
                                catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                                catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                                catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, result.Data, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            var item = new OrderResepViewModel();
            using (var s = new SIM_Entities())
            {
                var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == id);

                if (m != null)
                {
                    var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == m.NoRegistrasi && x.SectionID == section);
                    if (e == null) return HttpNotFound();
                    item.DokterID = e.DokterID;
                    item.Dokter = e.NamaDOkter;

                    item = IConverter.Cast<OrderResepViewModel>(m);
                    item.KodePaket = m.KodePaket;
                    item.QtyRacik = m.QtyPuyer;
                    item.ObatRacik = m.Puyer ?? false;
                    item.SatuanPuyer = m.SatuanPuyer;
                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Jumlah.ToMoney();
                    var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == id).ToList();
                    item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                        n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                        n.Harga_View = x.Harga.ToMoney();
                        item.Detail_List.Add(false, n);
                    }
                }

            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpGet]
        [ActionName("EditRacik")]
        public ActionResult EditRacik_Get(string id)
        {
            var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            var item = new OrderResepViewModel();
            using (var s = new SIM_Entities())
            {
                var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == id);
                if (m != null)
                {
                    var e = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == m.NoRegistrasi && x.SectionID == section);
                    if (e == null) return HttpNotFound();
                    item.DokterID = e.DokterID;
                    item.Dokter = e.NamaDOkter;


                    item = IConverter.Cast<OrderResepViewModel>(m);
                    item.KodePaket = m.KodePaket;
                    item.QtyRacik = m.QtyPuyer;
                    item.ObatRacik = m.Puyer ?? false;
                    item.SatuanPuyer = m.SatuanPuyer;
                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Jumlah.ToMoney();
                    //item.DokterID = m.DokterID;
                    //item.Dokter = m.Dokter;
                    var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == id).ToList();
                    item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                        n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                        n.Harga_View = x.Harga.ToMoney();

                        var dosis = s.SIMtrResepDetail.FirstOrDefault(q => q.NoResep == x.NoResep && q.Barang_ID == x.Barang_ID);
                        if (dosis != null)
                        {
                            item.Dosis1 = dosis.Dosis ?? "-";
                            item.Dosis2 = dosis.Dosis2 ?? "-";
                            n.AturanPakai = dosis.KetDosis;
                        }
                        item.Detail_List.Add(false, n);
                    }
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new OrderResepViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        if (item.Detail_List == null) { throw new Exception("Detail tidak boleh kosong"); }

                        item.Detail_List.RemoveAll(x => x.Remove);
                        decimal jumlah = 0;
                        foreach (var x in item.Detail_List)
                        {
                            //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            var b2 = s.Pelayanan_GetListObat(item.Farmasi_SectionID, item.NoRegistrasi).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            //qty * harga
                            x.Model.Harga = b2.Harga_Jual ?? 0;
                            var _harga = b2.Harga_Jual ?? 0;
                            var qty = (decimal)x.Model.Qty;
                            var _subJumlah = qty * _harga;
                            jumlah += _subJumlah;
                        }
                        var section = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoRegistrasi && x.SectionID == section);
                        #endregion

                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var Resep = s.SIMtrResep.FirstOrDefault(x => x.NoResep == item.NoResep);
                                if (Resep == null) { throw new Exception("Resep tidak ditemukan."); }

                                var ResepDetail = s.SIMtrResepDetail.Where(x => x.NoResep == item.NoResep);
                                s.SIMtrResepDetail.RemoveRange(ResepDetail);


                                var Puyer = false;
                                var QtyPuyer = 0;
                                var SatuanPuyer = "";
                                var dosis1 = "";
                                var dosis2 = "";
                                if (item.Puyer == true)
                                {
                                    Puyer = true;
                                    QtyPuyer = item.QtyPuyer;
                                    SatuanPuyer = item.SatuanPuyer;
                                    dosis1 = item.Dosis1;
                                    dosis2 = item.Dosis2;
                                }
                                else
                                {
                                    Puyer = false;
                                    QtyPuyer = 0;
                                    SatuanPuyer = "";
                                }

                                Resep.Tanggal = DateTime.Today;
                                Resep.Jam = DateTime.Now;
                                Resep.DokterID = item.DokterID;
                                Resep.Jumlah = jumlah;
                                Resep.Cyto = item.Cyto;
                                Resep.Farmasi_SectionID = item.Farmasi_SectionID;
                                Resep.Puyer = Puyer;
                                Resep.QtyPuyer = (int?)QtyPuyer;
                                Resep.SatuanPuyer = SatuanPuyer;
                                Resep.BeratBadan = item.BeratBadan;
                                Resep.Keterangan = item.Keterangan;
                                Resep.ObatPulang = item.ObatPulang;

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    var racik = true;
                                    if (item.Puyer != true)
                                    {
                                        dosis1 = x.Model.AturanPakai;
                                        racik = false;
                                    }

                                    var blokasi = s.mBarangLokasiNew.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                    var d = new SIMtrResepDetail()
                                    {
                                        NoResep = item.NoResep,
                                        Barang_ID = x.Model.Barang_ID,
                                        Satuan = x.Model.Satuan,
                                        Qty = x.Model.Qty,
                                        Harga_Satuan = x.Model.Harga,
                                        Disc_Persen = 0,
                                        Stok = blokasi.Qty_Stok,
                                        KomisiDokter = 0,
                                        THT = 0,
                                        Racik = racik,
                                        Plafon = 0,
                                        KelebihanPLafon = 0,
                                        KelasID = null,
                                        KTP = null,
                                        JenisKerjasamaID = null,
                                        PerusahaanID = null,
                                        DokterID = null,
                                        SectionID = null,
                                        PPN = 0,
                                        DosisID = 0,
                                        JenisBarangID = 0,
                                        Embalase = 0,
                                        JasaRacik = 0,
                                        Dosis = dosis1,
                                        Dosis2 = dosis2,
                                        KetDosis = x.Model.AturanPakai,
                                        QtyRacik = item.QtyRacik,
                                        QtyHasilRacik = 0,
                                        TermasukPaket = false,
                                        NomorUrut = 0
                                    };
                                    s.SIMtrResepDetail.Add(d);
                                }
                                #endregion
                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Resep Edit {item.NoResep}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            OrderResepViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<OrderResepViewModel>(m);
                    item.KodePaket = m.KodePaket;
                    //item.NamaPaket = m.paket
                    item.QtyRacik = m.QtyPuyer;
                    item.ObatRacik = m.Puyer ?? false;
                    item.SatuanPuyer = m.SatuanPuyer;
                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Jumlah.ToMoney();
                    var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == nobukti).ToList();
                    item.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<OrderResepDetailViewModel>(x);
                        n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                        n.Harga_View = x.Harga.ToMoney();
                        item.Detail_List.Add(false, n);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                var nobukti = id;
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti && x.Realisasi == false);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.SIMtrResepDetail.RemoveRange(s.SIMtrResepDetail.Where(x => x.NoResep == nobukti));
                            s.SIMtrResep.Remove(m);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Resep delete {nobukti}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    IQueryable<Pelayanan_SIMtrResep> proses = s.Pelayanan_SIMtrResep;
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.NoRegistrasi)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.SectionID)}=@0", sectionid);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var model = new List<OrderResepViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<OrderResepViewModel>(x);
                        m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        m.Detail_List = new ListDetail<OrderResepDetailViewModel>();
                        var detailResep = s.Pelayanan_SIMtrResepDetail.Where(e => e.NoResep == x.NoResep).ToList();
                        foreach (var f in detailResep)
                        {
                            var n = IConverter.Cast<OrderResepDetailViewModel>(f);
                            n.Jumlah_View = ((decimal)(f.Jumlah ?? 0)).ToMoney();
                            n.Harga_View = f.Harga.ToMoney();

                            var dosis = s.SIMtrResepDetail.FirstOrDefault(q => q.NoResep == f.NoResep && q.Barang_ID == f.Barang_ID);
                            if (dosis != null)
                            {
                                m.Dosis1 = dosis.Dosis ?? "-";
                                m.Dosis2 = dosis.Dosis2 ?? "-";
                                n.KetDosis = dosis.KetDosis;
                            }
                            m.Detail_List.Add(false, n);
                        }

                        m.SatuanPuyer = x.SatuanPuyer ?? "-";
                        m.QtyPuyer = x.QtyPuyer ?? 0;
                        m.SectionName = Request.Cookies["EMRSectionNamaPelayanan"].Value;
                        model.Add(m);
                    }

                    result.Data = model;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - P A K E T - B H P

        [HttpPost]
        public string ListPaket(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Data_PaketObat_Result> proses = s.Pelayanan_Data_PaketObat(filter[3], filter[2]).AsQueryable();
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<OrderPaketResepViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string DetailPaket(string kode, string section, string noreg, int nomor)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_PaketObat.FirstOrDefault(x => x.KodePaket == kode);
                    var d = s.Pelayanan_PaketObatDetail.Where(x => x.KodePaket == kode).ToList();
                    var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    foreach (var x in d)
                    {
                        var y = s.GetHargaObatNew_WithStok(simtrreg.JenisKerjasamaID, "XX", (simtrreg.PasienKTP ? 1 : 0), x.Barang_ID, (int?)simtrreg.CustomerKerjasamaID, section, 0).FirstOrDefault();
                        x.Harga = y.TglHargaBaru == DateTime.Today ? y.Harga_Baru : y.Harga_Lama;
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Header = h,
                        Data = d.ConvertAll(x => new
                        {
                            Kode = x.KodePaket,
                            NamaPaket = x.NamaPaket,
                            Kode_Barang = x.Kode_Barang,
                            Barang_ID = x.Barang_ID,
                            Nama_Barang = x.Nama_Barang,
                            AturanPakai = x.AturanPakai,
                            Satuan_Stok = x.Nama_Satuan,
                            Qty = x.Qty,
                            Dosis = x.Dosis,
                            Harga_Jual = x.Harga ?? 0,
                            Harga_Jual_View = (x.Harga ?? 0).ToMoney(),
                            Jumlah_View = ((x.Harga ?? 0) * (decimal)(x.Qty)).ToMoney(),
                            Qty_Stok = x.QtyStok ?? 0
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - B A R A N G

        [HttpPost]
        public string ListBarang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var sectionTujuan = filter[8];
                    if (string.IsNullOrEmpty(filter[8]))
                    {
                        var lsfarmasi = StaticModel.ListSectionFarmasi.FirstOrDefault();
                        sectionTujuan = lsfarmasi.Value;
                    }
                    IQueryable<Pelayanan_GetListObat_Result> proses = s.Pelayanan_GetListObat(sectionTujuan, filter[10]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.NamaBarang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Satuan_Stok)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Qty_Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Sub_Kategori)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Aktif)}=@0", true);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[6].ToUpper() == "Y");
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Harga_Jual)}=@0", IFilter.F_Decimal(filter[7]));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<BarangViewModel>(x));
                    m.ForEach(x => x.Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney());
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E - O P T I K

        [HttpGet]
        [ActionName("CreateOptik")]
        public ActionResult CreateOptik_Get(string noreg, string section)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("CreateOptik")]
        [ValidateAntiForgeryToken]
        public string CreateOptik_Post()
        {
            try
            {
                var item = new ResepOptikViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        var datareg = s.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == item.NoReg);
                        #endregion

                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var id = s.AutoNumber_Pelayanan_BillOptik_Resep().FirstOrDefault();
                                var model = IConverter.Cast<BILLOPTIK_RESEP>(item);
                                model.Tanggal = DateTime.Now;
                                model.No_Bukti = id;
                                model.NRM = datareg.NRM;
                                model.NamaPasien = datareg.NamaPasien;
                                model.Alamat = datareg.Alamat;
                                model.NoTelp = "-";
                                model.JenisKelamin = datareg.JenisKelamin;
                                model.UmurBulan = datareg.UmurBln;
                                model.UmurTahun = datareg.UmurThn;
                                model.UmurHari = datareg.UmurHr;
                                model.JenisKerjasamaID = datareg.JenisKerjasamaID;
                                model.User_ID = 490;
                                model.Batal = false;
                                model.Realisasi = false;
                                model.DateUpdate = DateTime.Now;
                                s.BILLOPTIK_RESEP.Add(model);

                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                simtrreg.StatusResep = "MASUK";

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Resep optik Create {id}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L - O P T I K

        [HttpGet]
        [ActionName("DetailOptik")]
        public ActionResult DetailOptik(string nobukti)
        {
            ResepOptikViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.BILLOPTIK_RESEP.FirstOrDefault(x => x.No_Bukti == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ResepOptikViewModel>(m);
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                    if (dokter != null)
                        item.NamaDokter = dokter.NamaDOkter;
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion
    }
}