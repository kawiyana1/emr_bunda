﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRKunjunganPoliklinikController : Controller
    {
        // GET: EMRKunjunganPoliklinik
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.KunjunganPoliklinik.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model.EMRKunjunganPoliklinik_List = new ListDetail<EMRKunjunganPoliklinikViewModel>();
                        var detail_2 = s.KunjunganPoliklinik.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRKunjunganPoliklinikViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Petugas);
                            if (dokter != null)
                            {
                                y.PetugasNama = dokter.NamaDOkter;
                            }
                            model.EMRKunjunganPoliklinik_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                        model.NoBukti = id;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.KunjunganPoliklinik.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Kunjungan Poliklinik ";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Kunjungan Poliklinik ";
                    }

                    if (item.EMRKunjunganPoliklinik_List == null) item.EMRKunjunganPoliklinik_List = new ListDetail<EMRKunjunganPoliklinikViewModel>();
                    item.EMRKunjunganPoliklinik_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRKunjunganPoliklinik_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.EMRKunjunganPoliklinik_List;
                    var real_list = s.KunjunganPoliklinik.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.KunjunganPoliklinik.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.KunjunganPoliklinik.Add(IConverter.Cast<KunjunganPoliklinik>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

    }
}