﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSuratPernyataanBahwaTidakMenggunakanBPJSController : Controller
    {
        // GET: SuratPernyataanBahwaTidakMenggunakanBPJS
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRSuratPernyataanBahwaTidakMenggunakanBPJSViewModel();
            using (var sim = new SIM_Entities())
            {
                using(var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var soap = emr.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokument = emr.SuratPernyataanBahwaTidakMenggunakanBPJS.FirstOrDefault(x => x.NoBukti == id);
                    if( dokument != null)
                    {
                        model = IConverter.Cast<EMRSuratPernyataanBahwaTidakMenggunakanBPJSViewModel>(dokument);
                        model.NoBukti = id;
                        model.NamaPasien = identitas.NamaPasien;
                        model.Umur = identitas.UmurThn;
                       
                        model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                        model.AlamatPasien = identitas.Alamat;

                        model.MODEVIEW = view;

                    }
                    else
                    {
                        model.NoBukti = id;
                        model.NamaPasien = identitas.NamaPasien;
                        model.Umur = identitas.UmurThn;
                        model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                        model.AlamatPasien = identitas.Alamat;
                    }

                }

            }
        ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRSuratPernyataanBahwaTidakMenggunakanBPJSViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var emr = new EMREntities())
                {
                    var model = emr.SuratPernyataanBahwaTidakMenggunakanBPJS.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if(model == null)
                    {
                        var o = IConverter.Cast<SuratPernyataanBahwaTidakMenggunakanBPJS>(item);
                        emr.SuratPernyataanBahwaTidakMenggunakanBPJS.Add(o);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Surat Pernyatan Gak pake BPJS dan dll";

                    }
                    else
                    {
                        model = IConverter.Cast<SuratPernyataanBahwaTidakMenggunakanBPJS>(item);
                        emr.SuratPernyataanBahwaTidakMenggunakanBPJS.AddOrUpdate(model);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Update Surat Pernyatan Gak pake BPJS dan dll";
                    }
                    emr.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}