﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRI_PengkajianAwalMedisIlmuPenyakitMataViewModel
    {
        public ListDetail<EMRRI_RencanaKerja_DetailModel> RencanaKerja_List { get; set; }
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; } 
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDatang { get; set; }
        public string Rujukan { get; set; }
        public bool RS { get; set; }
        public string RSKet { get; set; }
        public bool DR { get; set; }
        public string DRKet { get; set; }
        public bool Puskesmas { get; set; }
        public string PuskesmasKet { get; set; }
        public bool Lainnya { get; set; }
        public string LainnyaKet { get; set; }
        public string DXLainnya { get; set; }
        public bool DatangSendiri { get; set; }
        public string DatangSendiriKet { get; set; }
        public bool Diantar { get; set; }
        public string DiantarKet { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string TandaVitalTekananDarah { get; set; }
        public string TandaVitalNadi { get; set; }
        public string TandaVitalSuhu { get; set; }
        public string TandaVitalRespirasi { get; set; }
        public string TandaVitalKeadaanUmum { get; set; }
        public string TandaVitalGCS_E { get; set; }
        public string TandaVitalGCS_V { get; set; }
        public string TandaVitalGCS_M { get; set; }
        public string PemeriksaanKeadaanUmum { get; set; }
        public string PemeriksaanCar { get; set; }
        public string PemeriksaanHepar { get; set; }
        public string PemeriksaanPulmo { get; set; }
        public string TestAnel { get; set; }
        public string TestButaWarna { get; set; }
        public string TestFlourecein { get; set; }
        public string PengkajianLain { get; set; }
        public string HasilPemeriksaanPenunjang { get; set; }
        public string Intruksi { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string DokterPengkajian { get; set; }
        public string DokterPengkajianNama { get; set; }
        public string VirusAwalOD_UCVA { get; set; }
        public string VirusAwalOD_BCVA { get; set; }
        public string VirusAwalOS_UCVA { get; set; }
        public string VirusAwalOS_BCVA { get; set; }
        public string KacamataOD_UCVA { get; set; }
        public string KacamataOD_BCVA { get; set; }
        public string KacamataOS_UCVA { get; set; }
        public string KacamataOS_BCVA { get; set; }
        public string TTDDPJP { get; set; }
        public string TTDDokterPengkajian { get; set; }
        public string PemeriksaanLain { get; set; }
        public string ICDX { get; set; }
        public bool VerifikasiDPJP { get; set; }
        public bool VerifikasiPengkaji { get; set; }


        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListIlmuMataRI> ListTemplate { get; set; } 

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
    }

    public class SelectItemListIlmuMataRI
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}