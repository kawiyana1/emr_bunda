﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRResumeMedisController : Controller
    {
        // GET: EMRResumeMedis
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRResumeMedisViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.ResumeMedis.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRResumeMedisViewModel>(dokumen);

                        model.NoBukti = id;
                        if (identitas != null)
                        {
                            model.NamaPasien = identitas.NamaPasien;
                            model.Umur = identitas.UmurThn.ToString() + " Thn"; 
                            model.Alamat = identitas.Alamat;
                            model.JenisKelamin = identitas.JenisKelamin == "M" ? "Laki" : "Perempuan";
                        }
                        var dokterpelaksana = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (dokterpelaksana != null) model.Dokter_Nama = dokterpelaksana.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        if (identitas != null)
                        {
                            model.NamaPasien = identitas.NamaPasien;
                            model.Umur = identitas.UmurThn.ToString() + " Thn";
                            model.Alamat = identitas.Alamat;
                            model.JenisKelamin = identitas.JenisKelamin == "M" ? "Laki" : "Perempuan";
                        }
                        model.Diagnosa = soap == null ? "" : soap.CPPT_A;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRResumeMedisViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ResumeMedis.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<ResumeMedis>(item);
                        s.ResumeMedis.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Resume Medis Poli Spesialis";
                    }
                    else
                    {
                        model = IConverter.Cast<ResumeMedis>(item);
                        s.ResumeMedis.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Resume Medis Poli Spesialis";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}