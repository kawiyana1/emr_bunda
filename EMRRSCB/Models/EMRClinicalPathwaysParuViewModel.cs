﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRClinicalPathwaysParuViewModel
    {
        public ListDetail<EMRAspekPelayananClinicalPathwaysParu_DetailDetailModel> Aspek_List { get; set; }
        public ListDetail<EMRVariasiClinicalPathwaysParu_DetailDetailModel> Variasi_List { get; set; }

        public bool bool1 { get; set; }
        public string NoBukti { get; set; }
        public string CatatanKhusus { get; set; }
        public string TanggalMasuk { get; set; }
        public string TanggalKeluar { get; set; }
        public string Diagnosa_PPOK { get; set; }
        public string Diagnosa_Utama { get; set; }
        public string DiagnosaPenyerta { get; set; }
        public string Komplikasi { get; set; }
        public string TindakanUtama { get; set; }
        public string TindakanLain { get; set; }
        public string NamaDPJP { get; set; }
        public string NamaDPJPNama { get; set; }
        public string NamaPPJP { get; set; }
        public string NamaPPJPNama { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTanganPPJP { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
        public List<SelectItemListClinicalParu> ListTemplate { get; set; }

        // disable all form 
        public int MODEVIEW { get; set; }
    }

    public class SelectItemListClinicalParu
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}