//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class SuratKeteranganKematian
    {
        public string NoBukti { get; set; }
        public string NomorSurat { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Alamat { get; set; }
        public string TanggalKematian { get; set; }
        public string JamKematian { get; set; }
        public string NamaKeluarga { get; set; }
        public Nullable<System.DateTime> TglKontrol { get; set; }
        public byte[] TandaTanganpasien { get; set; }
    }
}
