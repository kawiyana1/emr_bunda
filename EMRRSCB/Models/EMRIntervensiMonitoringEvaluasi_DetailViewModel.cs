﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRIntervensiMonitoringEvaluasi_DetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string DietAwal { get; set; }
        public string Tanggal { get; set; }
        public bool DietTetap { get; set; }
        public bool PerubahanDiet { get; set; }
        public string PerubahanDiet_Ket { get; set; }
        public string E { get; set; }
        public string P { get; set; }
        public string AsupanMakanan { get; set; }
        public string BB { get; set; }
        public string Keterangan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Username { get; set; }
        public string TandaTangan { get; set; }
        public bool Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}