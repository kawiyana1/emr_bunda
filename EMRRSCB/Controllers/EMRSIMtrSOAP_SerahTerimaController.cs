﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSIMtrSOAP_SerahTerimaController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0, int cppt = 0)
        {
            var model = new CPPTViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    if (cppt == 0) HttpNotFound();
                    var dokumen = s.CPPT.FirstOrDefault(x => x.NoBukti == id);
                    var data = s.CPPT.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<CPPTViewModel>(dokumen);
                        var petugas1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Petugas_1);
                        if (petugas1 != null) model.Petugas_1_Nama = petugas1.NamaDOkter;
                        var petugas2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Petugas_2);
                        if (petugas2 != null) model.Petugas_2_Nama = petugas2.NamaDOkter;
                        model.MODEVIEW = 1;
                    }
                    else
                    {
                        if (data != null)
                        {
                            model.NoBukti = id;
                        }
                        model.NoBukti = id;
                        model.MODEVIEW = view;
                    }

                }
            }
               
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new CPPTViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CPPT.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<CPPT>(item);
                        s.CPPT.Add(o);
                        activity = "Create Serah Terima CPPT";
                    }
                    else
                    {
                        model = IConverter.Cast<CPPT>(item);
                        s.CPPT.AddOrUpdate(model);
                        activity = "Update Serah Terima CPPT";
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti} {"Serah Terima CPPT :" + item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}