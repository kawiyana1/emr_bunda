﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPartografController : Controller
    {
        // GET: EMRPartograf
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string noreg, int view = 0)
        {
            var model = new EMRPartografViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.Partograf.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPartografViewModel>(dokumen);
                        var penolong = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Penolong);
                        if (penolong != null) model.PenolongNama = penolong.NamaDOkter;

                        model.Detail_List = new ListDetail<EMRPartograf_DetailDetailModel>();
                        var detail_1 = s.Partograf_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPartograf_DetailDetailModel>(x);
                            //var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Pengkajian);
                            //if (dokter_detail != null) y.PengkajianNama = dokter_detail.NamaDOkter;
                            model.Detail_List.Add(false, y);
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.MasukTanggal = DateTime.Today;
                        model.Pukul = DateTime.Now;
                        model.KetubanPecahPukul = DateTime.Now;
                        model.MulesSejakPukul = DateTime.Now;
                        model.MakananTerakhir_Pukul = DateTime.Now;
                        model.MinumanTerakhir_Pukul = DateTime.Now;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPartografViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.Partograf.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<Partograf>(item);
                        s.Partograf.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Pengkajian Catatan Pemindahan Pasien";
                    }
                    else
                    {
                        model = IConverter.Cast<Partograf>(item);
                        s.Partograf.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Pengkajian Catatan Pemindahan Pasien";
                    }

                    if (item.Detail_List == null) item.Detail_List = new ListDetail<EMRPartograf_DetailDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Detail_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Detail_List;
                    var real_list = s.Partograf_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Partograf_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.Partograf_Detail.Add(IConverter.Cast<Partograf_Detail>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}