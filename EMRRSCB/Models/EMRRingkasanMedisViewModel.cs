﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRingkasanMedisViewModel
    {
        public ListDetail<EMRRingkasanMedisDetailModel> Detail_List { get; set; }

        public string NoBukti { get; set; }
        public string Tindakan { get; set; }
        public string KodeOperasi { get; set; }
        public string Alergi { get; set; }
        public int Report { get; set; }


        // disable all form
        public int MODEVIEW { get; set; }
    }
}