﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoring24Jam_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string NoAcuan { get; set; }
        public string Hemodinamik_Suhu { get; set; }
        public string Hemodinamik_Nadi { get; set; }
        public string Hemodinamik_HR { get; set; }
        public string Hemodinamik_TD { get; set; }
        public string Hemodinamik_BP { get; set; }
        public string Hemodinamik_Map { get; set; }
        public string TipeVent { get; set; }
        public string Peep { get; set; }
        public string RR { get; set; }
        public string TV { get; set; }
        public string FlO2 { get; set; }
        public string Kesadaran { get; set; }
        public string Irama { get; set; }
        public string SkalaNyeri { get; set; }
        public string CVP { get; set; }
        public string SaO2 { get; set; }
        public string CuffPresure { get; set; }
        public string Mata_Ukuran_L { get; set; }
        public string Mata_Ukuran_R { get; set; }
        public string Mata_Reflek_L { get; set; }
        public string Mata_Reflek_R { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string ResikoJatuh { get; set; }
        public string Parental { get; set; }
        public string Jumlah_Parental { get; set; }
        public string Enteral { get; set; }
        public string Jumlah_Enteral { get; set; }
        public string TotalCairanMasuk { get; set; }
        public string IrigasiCM { get; set; }
        public string IrigasiCK { get; set; }
        public string Urine { get; set; }
        public string NGT { get; set; }
        public string DrainWSD { get; set; }
        public string DrainWSD2 { get; set; }
        public string TotalCairanKeluar { get; set; }
        public string Masalah { get; set; }
        public string Tindakan { get; set; }
    }
}