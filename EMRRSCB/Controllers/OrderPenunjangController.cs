﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Data;
using CrystalDecisions.Shared;

namespace EMRRSCB.Controllers
{
    public class OrderPenunjangController : Controller
    {
        #region ==== CREATE
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new OrderPenunjangViewModel();
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NoReg = noreg;
            ViewBag.Section = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                if (m == null) return HttpNotFound();

                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                if (item.SectionID == null) throw new Exception("Ruangan tidak boleh kosong");
                                if (item.DokterID == null) throw new Exception("Dokter tidak boleh kosong");

                                var user = User.Identity.GetUserId();
                                var nobukti = "";

                                item.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;

                                nobukti = s.trOrder_insert_New(
                                    DateTime.Now,
                                    item.NoReg,
                                    "",
                                    item.Hasil_Cito == null ? false : true,
                                    false,
                                    "",
                                    item.DiagnosaIndikasi,
                                    item.DokterID,
                                    "",
                                    "",
                                    "",
                                    null,
                                    item.JamSampling,
                                    null,
                                    "",
                                    "",
                                    item.PemeriksaanTambahan,
                                    false,
                                    user,
                                    item.SectionID,
                                    item.SectionAsalID,
                                    false
                                ).FirstOrDefault();

                                if (item.OrderManual != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.OrderManual)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetailManual>(item.OrderManual);
                                        m.NoBukti = nobukti;
                                        m.NoUrut = i;
                                        m.JasaID = x.JasaID;
                                        m.Qty = x.Qty;
                                        m.Tarif = x.Tarif;
                                        m.DokterID = x.DokterID;
                                        m.Keterangan = x.Keterangan;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetailManual.Add(m);

                                        i++;
                                    }
                                }

                                if (item.Jasa != null)
                                {
                                    foreach (var x in item.Jasa)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetail>(item.Jasa);
                                        m.NoBukti = nobukti;
                                        m.UrutJenis = x.UrutJenis;
                                        m.NamaJenis = x.NamaJenis;
                                        m.UrutKelompok = x.UrutKelompok;
                                        m.ID_Kelompok = x.ID_Kelompok;
                                        m.NamaKelompok = x.NamaKelompok;
                                        m.UrutJasa = x.UrutJasa;
                                        m.NamaTindakan = x.NamaTindakan;
                                        m.JasaID = x.JasaID;
                                        m.Dipilih = true;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetail.Add(m);
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.NoReg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== EDIT
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.NoReg = m.Noreg;
                model.SectionID = m.SectionID;
                model.JamSampling_Tgl = m.JamSampling.Value.Date;
                model.JamSampling_Jam = m.JamSampling;


            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region === VALIDASI
                                if (item.SectionID == null) throw new Exception("Ruangan tidak boleh kosong");
                                //if (item.OrderManual == null && item.Jasa == null)
                                //{
                                //    throw new Exception("Salah satu detail jasa harus diisi");
                                //}
                                var Order = s.trOrderPenunjang.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (Order == null)
                                {
                                    throw new Exception("Order Penunjang tidak ditemukan");
                                }
                                #endregion

                                #region === DELETE DETAIL
                                var trOrderPenunjangDetailManual = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetailManual != null)
                                {
                                    s.trOrderPenunjangDetailManual.RemoveRange(trOrderPenunjangDetailManual);
                                }

                                var trOrderPenunjangDetail = s.trOrderPenunjangDetail.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetail != null)
                                {
                                    s.trOrderPenunjangDetail.RemoveRange(trOrderPenunjangDetail);
                                }
                                #endregion

                                var user = User.Identity.GetUserId();
                                var nobukti = Order.NoBukti;

                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.DiagnosaIndikasi = item.DiagnosaIndikasi;
                                Order.DokterID = item.DokterID;
                                Order.PemeriksaanTambahan = item.PemeriksaanTambahan;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;
                                Order.UserId = user;

                                if (item.OrderManual != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.OrderManual)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetailManual>(item.OrderManual);
                                        m.NoBukti = nobukti;
                                        m.NoUrut = i;
                                        m.JasaID = x.JasaID;
                                        m.Qty = x.Qty;
                                        m.Tarif = x.Tarif;
                                        m.DokterID = x.DokterID;
                                        m.Keterangan = x.Keterangan;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetailManual.Add(m);

                                        i++;
                                    }
                                }

                                if (item.Jasa != null)
                                {
                                    foreach (var x in item.Jasa)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetail>(item.Jasa);
                                        m.NoBukti = nobukti;
                                        m.UrutJenis = x.UrutJenis;
                                        m.NamaJenis = x.NamaJenis;
                                        m.UrutKelompok = x.UrutKelompok;
                                        m.ID_Kelompok = x.ID_Kelompok;
                                        m.NamaKelompok = x.NamaKelompok;
                                        m.UrutJasa = x.UrutJasa;
                                        m.NamaTindakan = x.NamaTindakan;
                                        m.JasaID = x.JasaID;
                                        m.Dipilih = true;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetail.Add(m);
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.NoReg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.NoReg = m.Noreg;
                model.SectionID = m.SectionID;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region ==== GET DETAIL
        [HttpGet]
        public string get_JenisKelompokJasa(string section, string nobukti = null)
        {
            try
            {
                object Jenis = new Vw_mOrderPenunjang_Jenis();
                object Kelompok = new Vw_mOrderPenunjang_Kelompok();
                object Jasa = new Vw_mOrderPenunjang_DetailJasa();
                object JasaSave = new trOrderPenunjangDetail();
                using (var s = new SIM_Entities())
                {
                    Jenis = s.Vw_mOrderPenunjang_Jenis.Where(x => x.SectionID == section).ToList();
                    Kelompok = s.Vw_mOrderPenunjang_Kelompok.ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_Kelompok>(x));
                    Jasa = s.Vw_mOrderPenunjang_DetailJasa.ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_Jasa>(x));
                    if (nobukti != null)
                    {
                        JasaSave = s.trOrderPenunjangDetail.Where(x => x.NoBukti == nobukti).ToList();
                    }
                }
                if (nobukti == null)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Jenis = Jenis,
                        Kelompok = Kelompok,
                        Jasa = Jasa,
                        Message = "-"
                    });
                }
                else
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Jenis = Jenis,
                        Kelompok = Kelompok,
                        Jasa = Jasa,
                        JasaSave = JasaSave,
                        Message = "-"
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                    if (m == null) throw new Exception("Data Tidak ditemukan");

                    s.trOrder_insert_batal(nobukti);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Order Penunjang delete {nobukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);

                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== R E P O R T

        public ActionResult ExportPDF(string nobukti)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports/SIM"), $"Rpt_OrderTotal.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_Kosongan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_Kosongan;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand("Rpt_OrderPenunjangLab", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables["Rpt_OrderPenunjangLab;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand("Rpt_OrderPenunjangRadiologi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables["Rpt_OrderPenunjangRadiologi;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}