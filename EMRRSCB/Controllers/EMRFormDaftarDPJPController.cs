﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormDaftarDPJPController : Controller
    {
        // GET: EMRFormDaftarDPJP
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.FormDaftarDPJP.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRDaftarDPJP_List = new ListDetail<EMRFormDaftarDPJPViewModel>();
                        var detail_1 = s.FormDaftarDPJP.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRFormDaftarDPJPViewModel>(x);

                            var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == y.DPJPNama);
                            if (dpjp != null)
                            {
                                y.DPJPNamaNama = dpjp.NamaDOkter;
                            }

                            var dpjputama = sim.mDokter.FirstOrDefault(e => e.DokterID == y.DPJPUtamaNama);
                            if (dpjputama != null)
                            {
                                y.DPJPUtamaNamaNama = dpjputama.NamaDOkter;
                            }
                            model.EMRDaftarDPJP_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.FormDaftarDPJP.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Form Daftar DPJP";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Form Daftar DPJP";
                    }

                    if (item.EMRDaftarDPJP_List == null) item.EMRDaftarDPJP_List = new ListDetail<EMRFormDaftarDPJPViewModel>();
                    item.EMRDaftarDPJP_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRDaftarDPJP_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        //x.Model.Diagnosa = item.Diagnosa;
                        //x.Model.Alergi = item.Alergi;
                        //x.Model.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        //x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRDaftarDPJP_List;
                    var real_list = s.FormDaftarDPJP.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormDaftarDPJP.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.FormDaftarDPJP.Add(IConverter.Cast<FormDaftarDPJP>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.DPJPNama = x.Model.DPJPNama;
                            _m.Diagnosa = x.Model.Diagnosa;
                            _m.DPJPTglMulai = x.Model.DPJPTglMulai;
                            _m.DPJPTglAkhir = x.Model.DPJPTglAkhir;
                            _m.DPJPUtamaTglMulai = x.Model.DPJPUtamaTglMulai;
                            _m.DPJPUtamaTglAkhir = x.Model.DPJPUtamaTglAkhir;
                            _m.Keterangan = x.Model.Keterangan;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}