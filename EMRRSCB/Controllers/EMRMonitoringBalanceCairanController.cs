﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRMonitoringBalanceCairanController : Controller
    {
        // GET: MonitoringBalanceCairan

        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s =  new EMREntities())
                {
                    var dokumen = s.MonitoringBalanceCairan.Where(x => x.NoBukti == id).ToList();
                    if(dokumen != null)
                    {
                        model.EMRMoitoringBalanceCairan_List = new ListDetail<EMRMonitoringBalanceCarianViewModel>();
                        var detail_1 = s.MonitoringBalanceCairan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRMonitoringBalanceCarianViewModel>(x);
                            model.EMRMoitoringBalanceCairan_List.Add(false, y);
                        }


                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;
              
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {

                    var model = s.MonitoringBalanceCairan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Moitoring Balance Cairan";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Monitoring Baance Cauran";

                    }
                    if(item.EMRMoitoringBalanceCairan_List == null)
                    {
                        item.EMRMoitoringBalanceCairan_List = new ListDetail<EMRMonitoringBalanceCarianViewModel>();

                    }
                    item.EMRMoitoringBalanceCairan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRMoitoringBalanceCairan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
           
                    }
                    var new_list = item.EMRMoitoringBalanceCairan_List;
                    var real_list = s.MonitoringBalanceCairan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.MonitoringBalanceCairan.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.MonitoringBalanceCairan.Add(IConverter.Cast<MonitoringBalanceCairan>(x.Model));
                        else
                        {
                          
                            _m.Jam = x.Model.Jam;
                            _m.MinumJenis = x.Model.MinumJenis;
                            _m.MinumJumlah = x.Model.MinumJumlah;
                            _m.IntravenaJenis = x.Model.IntravenaJenis;
                            _m.IntravenaJumlah = x.Model.IntravenaJumlah;
                            _m.SisaJenis = x.Model.SisaJenis;
                            _m.SisaJumlah = x.Model.SisaJumlah;
                            _m.Jam1 = x.Model.Jam;
                            _m.Urine = x.Model.Urine;
                            _m.LainLain = x.Model.LainLain;

                            
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);

                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}