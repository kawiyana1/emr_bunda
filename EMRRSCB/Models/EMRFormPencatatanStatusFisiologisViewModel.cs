﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormPencatatanStatusFisiologisViewModel
    {
        public ListDetail<EMRFormPencatatanStatusFisiologis_DetailDetailModel> StatutsFisio_List { get; set; }
        public string NoBukti { get; set; }
        public string DiagnosaPreOp { get; set; }
        public string JenisTindakan { get; set; }
        public string DiagnosaPostOp { get; set; }
        public string DokterOperator { get; set; }
        public string DokterOperatorNama { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string TTDDokterOperator { get; set; }
        public string TTDPerawat { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TekananDarahNadiSebelum { get; set; }
        public string TekananDarahNadiSelama { get; set; }
        public string Catatan { get; set; }
        public bool VerifikasiDokterOperator { get; set; }
        public bool VerifikasiPerawat { get; set; }


        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
    }

}