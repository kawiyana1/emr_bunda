﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRObatDanAlatKesehatanHabisPakaiController : Controller
    {
        // GET: EMRObatDanAlatKesehatanHabisPakai
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.ObatDanAlatKesehatanHabisPakai.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRObatAlkes_List = new ListDetail<EMRObatDanAlatKesehatanHabisPakaiViewModel>();
                        var detail_1 = s.ObatDanAlatKesehatanHabisPakai.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRObatDanAlatKesehatanHabisPakaiViewModel>(x);
                            //var t = new BridgingSIMRS();
                            //var getobat = t.GetObatByKode(y.Deskripsi);
                            //if (getobat.metadata.code != "200") new Exception(getobat.metadata.message);
                            //if (getobat != null) y.Deskripsi = getobat.response.KodeBarang;
                            var barang = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.Deskripsi);
                            if (barang != null) y.DeskripsiNama = barang.Nama_Barang;

                            model.EMRObatAlkes_List.Add(false, y);
                            model.Tindakan = y.Tindakan;
                            model.Diagnosa = y.Diagnosa;
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplateObatAlkes = new List<SelectItemListObatAlkes>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplateObatAlkes.Add(new SelectItemListObatAlkes()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }


        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.ObatDanAlatKesehatanHabisPakai.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model.EMRObatAlkes_List = new ListDetail<EMRObatDanAlatKesehatanHabisPakaiViewModel>();
                        var detail_1 = s.ObatDanAlatKesehatanHabisPakai.Where(x => x.NoBukti == copy).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRObatDanAlatKesehatanHabisPakaiViewModel>(x);
                            //var t = new BridgingSIMRS();
                            //var getobat = t.GetObatByKode(y.Deskripsi);
                            //if (getobat.metadata.code != "200") new Exception(getobat.metadata.message);
                            //if (getobat != null) y.Deskripsi = getobat.response.KodeBarang;
                            var barang = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.Deskripsi);
                            if (barang != null) y.DeskripsiNama = barang.Nama_Barang;

                            model.EMRObatAlkes_List.Add(false, y);
                            model.Tindakan = y.Tindakan;
                            model.Diagnosa = y.Diagnosa;
                        }
                        model.MODEVIEW = 0;
                    }
                    model.NoBukti = id;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplateObatAlkes = new List<SelectItemListObatAlkes>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplateObatAlkes.Add(new SelectItemListObatAlkes()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ObatDanAlatKesehatanHabisPakai.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Obat dan Alkes";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Obat dan Alkes ";
                    }

                    if (item.EMRObatAlkes_List == null) item.EMRObatAlkes_List = new ListDetail<EMRObatDanAlatKesehatanHabisPakaiViewModel>();
                    item.EMRObatAlkes_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRObatAlkes_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Diagnosa = item.Diagnosa;
                        x.Model.Tindakan = item.Tindakan;
                    }
                    var new_list = item.EMRObatAlkes_List;
                    var real_list = s.ObatDanAlatKesehatanHabisPakai.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.ObatDanAlatKesehatanHabisPakai.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.ObatDanAlatKesehatanHabisPakai.Add(IConverter.Cast<ObatDanAlatKesehatanHabisPakai>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Diagnosa = item.Diagnosa;
                            _m.Tindakan = item.Tindakan;
                            _m.Deskripsi = x.Model.Deskripsi;
                            _m.DeskripsiNama = x.Model.DeskripsiNama;
                            _m.Jenis = x.Model.Jenis;
                            _m.Pemakaian = x.Model.Pemakaian;
                            _m.Username = User.Identity.GetUserName();
                        }
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = "";
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}