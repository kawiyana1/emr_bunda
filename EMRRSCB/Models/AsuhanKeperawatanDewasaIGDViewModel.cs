﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class AsuhanKeperawatanDewasaIGDViewModel
    {
        public ListDetail<AsuhanKeperawatanDewasaIGD_ObservasiCairanViewModel> Observasi_List { get; set; }
        public ListDetail<AsuhanKeperawatanDewasaIGD_EvaluasiPerkembanganViewModel> Evaluasi_List { get; set; }
        public ListDetail<AsuhanKeperawatanDewasaIGD_TindakanPerawatanViewModel> TindakanKeperawatan_List { get; set; }
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam_View { get; set; }
        public string PerawatPengkaji { get; set; }
        public string PerawatPengkaji_Nama { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string RiwayatAlergi { get; set; }
        public bool Airway_Bebas { get; set; }
        public bool Airway_Gargling { get; set; }
        public bool Airway_Stridor { get; set; }
        public bool Airway_Wheezing { get; set; }
        public bool Airway_Ronchi { get; set; }
        public bool Airway_Terintubasi { get; set; }
        public bool Breathing_Spontan { get; set; }
        public bool Breathing_Tachipneu { get; set; }
        public bool Breathing_Dispneu { get; set; }
        public bool Breathing_Apneu { get; set; }
        public bool Breathing_VentilasiMekanik { get; set; }
        public bool Breathing_MemakaiVentilator { get; set; }
        public string Circulation_Nadi { get; set; }
        public string Circulation_CRT { get; set; }
        public string Circulation_WarnaKulit { get; set; }
        public string Circulation_Pendarahan { get; set; }
        public string Circulation_Kulit { get; set; }
        public bool DisabilityNeurogicalRespon_Alert { get; set; }
        public bool DisabilityNeurogicalRespon_Pain { get; set; }
        public bool DisabilityNeurogicalRespon_Verbal { get; set; }
        public bool DisabilityNeurogicalRespon_Unrespon { get; set; }
        public bool DisabilityNeurogicalPupil_Isokor { get; set; }
        public bool DisabilityNeurogicalPupil_Anisokor { get; set; }
        public bool DisabilityNeurogicalPupil_Pinpoint { get; set; }
        public bool DisabilityNeurogicalPupil_Midriasis { get; set; }
        public string DisabilityNeurogicalReflek { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string StatusPresent_KeadaanUmum { get; set; }
        public string StatusPresent_LevelKesadaranCGS_E { get; set; }
        public string StatusPresent_LevelKesadaranCGS_V { get; set; }
        public string StatusPresent_LevelKesadaranCGS_M { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_TD { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_N { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_R { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_S { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_BB { get; set; }
        public string StatusPresent_TandaTandaVitalDanKeadaanUmum_SpO2 { get; set; }
        public string SkriningNyeri { get; set; }
        public string PengkajianNyeri { get; set; }
        public string IntensitasNyeri { get; set; }
        public string OnsetNyeri { get; set; }
        public string OnsetNyeriKronisSejakKapan { get; set; }
        public string Pencetus { get; set; }
        public string PencetusLainnya { get; set; }
        public bool KualitasNyeri_SepertiDitusuk { get; set; }
        public bool KualitasNyeri_SakitBerdenyut { get; set; }
        public bool KualitasNyeri_Lainnya { get; set; }
        public string KualitasNyeri_LainnyaKeterangan { get; set; }
        public string RegioNyeriLokasi { get; set; }
        public string RegioNyeriMenjalar { get; set; }
        public string RegioNyeriMenjalarTidakKeterangan { get; set; }
        public string SkalaNyeri { get; set; }
        public bool NyeriDatangSaatIstirahat { get; set; }
        public bool NyeriDatangSaatBeraktifitas { get; set; }
        public bool NyeriDatangSaatLainnya { get; set; }
        public string NyeriDatangSaatLainnyaKeterangan { get; set; }
        public bool NyeriMembaikBilaIstirahat { get; set; }
        public bool NyeriMembaikBilaBeraktifitas { get; set; }
        public bool NyeriMembaikBilaMendengarkanMusik { get; set; }
        public bool NyeriMembaikBilaMinumObat { get; set; }
        public string NyeriMembaikBilaMinumKeterangan { get; set; }
        public bool NyeriMembaikBilaBerubahPosisiAtauTidur { get; set; }
        public bool NyeriMembaikBilaLainnya { get; set; }
        public string NyeriMembaikBilaLainnyaKeterangan { get; set; }
        public string FrekuensiNyeri { get; set; }
        public string PenilaianNyeri { get; set; }
        public string SkriningNutrisi_BB { get; set; }
        public string SkriningNutrisi_TB { get; set; }
        public string SkriningNutrisi_1 { get; set; }
        public string SkriningNutrisi_1_Ket { get; set; }
        public string SkriningNutrisi_2 { get; set; }
        public string SkriningNutrisi_KriteriaGizi { get; set; }
        public string SkriningNutrisi_TotalSkor { get; set; }
        public string Fungsional_1 { get; set; }
        public string Fungsional_2 { get; set; }
        public string Fungsional_3 { get; set; }
        public string Fungsional_4 { get; set; }
        public string Fungsional_5 { get; set; }
        public string Fungsional_5_Ket { get; set; }
        public string Fungsional_6 { get; set; }
        public string Fungsional_7 { get; set; }
        public string Fungsional_8 { get; set; }
        public string Fungsional_9 { get; set; }
        public string Fungsional_10 { get; set; }
        public string Fungsional_TotalSkor { get; set; }
        public string Fungsional_Status { get; set; }
        public string RisikoJatuh_RiwayatJatuh { get; set; }
        public string RisikoJatuh_DiagosisSkunder { get; set; }
        public string RisikoJatuh_AlatBantu { get; set; }
        public string RisikoJatuh_Infus { get; set; }
        public string RisikoJatuh_GayaBerjalan { get; set; }
        public string RisikoJatuh_Siklusmenial { get; set; }
        public string RisikoJatuh_TotalSkor { get; set; }
        public string RisikoJatuh_Kategori { get; set; }
        public string Psikososial_Pernikahan { get; set; }
        public string Psikososial_Anak { get; set; }
        public string Psikososial_Anak_Jumlah { get; set; }
        public string Psikososial_WargaNegara { get; set; }
        public string Psikososial_WargaNegaraKet { get; set; }
        public string Psikososial_Pekerjaan { get; set; }
        public string Psikososial_TinggalBersama { get; set; }
        public string Psikososial_Nama1 { get; set; }
        public string Psikososial_Telp1 { get; set; }
        public string Psikososial_Nama2 { get; set; }
        public string Psikososial_Telp2 { get; set; }
        public bool Psikososial_Kebiasaan_Merokok { get; set; }
        public bool Psikososial_Kebiasaan_Alkohol { get; set; }
        public bool Psikososial_Kebiasaan_Lainnya { get; set; }
        public string Psikososial_Kebiasaan_Ket { get; set; }
        public string Psikososial_Kebiasaan_Jumlah { get; set; }
        public string Psikososial_Agama { get; set; }
        public string Psikososial_Kepercayaan { get; set; }
        public string Psikososial_Mencederai { get; set; }
        public string KebutuhanKomunikasiBicara { get; set; }
        public string KebutuhanKomunikasiBicaraKapan { get; set; }
        public bool KebutuhanKomunikasiBahasaIndonesia { get; set; }
        public string KebutuhanKomunikasiBahasaIndonesiaKet { get; set; }
        public bool KebutuhanKomunikasiBahasaInggris { get; set; }
        public string KebutuhanKomunikasiBahasaInggrisKet { get; set; }
        public bool KebutuhanKomunikasiBahasaDaerah { get; set; }
        public string KebutuhanKomunikasiBahasaDaerahKet { get; set; }
        public bool KebutuhanKomunikasiBahasaLainnya { get; set; }
        public string KebutuhanKomunikasiBahasaLainnyaKet { get; set; }
        public string KebutuhanKomunikasiPenerjemah { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasa { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasaIsyarat { get; set; }
        public bool KebutuhanKomunikasiHambatanBahasa { get; set; }
        public bool KebutuhanKomunikasiHambatanCemas { get; set; }
        public bool KebutuhanKomunikasiHambatanKognitif { get; set; }
        public bool KebutuhanKomunikasiHambatanEmosi { get; set; }
        public bool KebutuhanKomunikasiHambatanMenulis { get; set; }
        public bool KebutuhanKomunikasiHambatanPendengaran { get; set; }
        public bool KebutuhanKomunikasiHambatanKesulitanBicara { get; set; }
        public bool KebutuhanKomunikasiHambatanAudioVisual { get; set; }
        public bool KebutuhanKomunikasiHambatanHilangMemori { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakAdaPartisipasi { get; set; }
        public bool KebutuhanKomunikasiHambatanDiskusi { get; set; }
        public bool KebutuhanKomunikasiHambatanMotivasiBuruk { get; set; }
        public bool KebutuhanKomunikasiHambatanSecaraFisiologi { get; set; }
        public bool KebutuhanKomunikasiHambatanMembaca { get; set; }
        public bool KebutuhanKomunikasiHambatanMasalahPenglihatan { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakDitemukanHambatan { get; set; }
        public bool KebutuhanKomunikasiHambatanMendengar { get; set; }
        public bool KebutuhanKomunikasiHambatanDemontrasi { get; set; }
        public string KebutuhanKomunikasiTingkatkanPendidikan { get; set; }
        public bool KebutuhanKomunikasiPotensialKebutuhan_Penyakit { get; set; }
        public bool KebutuhanKomunikasiPotensialKebutuhan_Lainnya { get; set; }
        public bool KebutuhanKomunikasiPotensialKebutuhan_Pengobatan { get; set; }
        public bool KebutuhanKomunikasiPotensialKebutuhan_Terapi { get; set; }
        public bool KebutuhanKomunikasiPotensialKebutuhan_Nutrisi { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhan_Ket { get; set; }
        public bool KebutuhanPrivasiIdentitas { get; set; }
        public bool KebutuhanPrivasiRekamMedis { get; set; }
        public bool KebutuhanPrivasiSaatPemeriksaan { get; set; }
        public bool KebutuhanPrivasiSaatTindakanMedis { get; set; }
        public bool KebutuhanPrivasiTransportasi { get; set; }
        public bool DIagnosaKeperawatan_BersihanNafas { get; set; }
        public bool DIagnosaKeperawatan_TraumaWajah { get; set; }
        public bool DIagnosaKeperawatan_PertukaranGas { get; set; }
        public bool DIagnosaKeperawatan_Oksigen { get; set; }
        public bool DIagnosaKeperawatan_CurahJantung { get; set; }
        public bool DIagnosaKeperawatan_PerfusiJaringan { get; set; }
        public bool DIagnosaKeperawatan_KekuranganVolumeCairan { get; set; }
        public bool DIagnosaKeperawatan_KelebihanVolumeCairan { get; set; }
        public bool DIagnosaKeperawatan_ProsesInfeksi { get; set; }
        public bool DIagnosaKeperawatan_RetensiUrine { get; set; }
        public bool DIagnosaKeperawatan_NyeriAkut { get; set; }
        public bool DIagnosaKeperawatan_Hipertermia { get; set; }
        public bool DIagnosaKeperawatan_MobilitasFisik { get; set; }
        public bool DIagnosaKeperawatan_ResikoInfeksi { get; set; }
        public bool DIagnosaKeperawatan_Konstipasi { get; set; }
        public bool DIagnosaKeperawatan_ResikoJatuh { get; set; }
        public bool DIagnosaKeperawatan_StatusMental { get; set; }
        public bool DIagnosaKeperawatan_ResikoMencederaiDiri { get; set; }
        public bool RencanaKeperawatan_BendaAsing { get; set; }
        public bool RencanaKeperawatan_NPA { get; set; }
        public bool RencanaKeperawatan_NafasBuatan { get; set; }
        public bool RencanaKeperawatan_O2SesuaiKebutuhan { get; set; }
        public bool RencanaKeperawatan_MonitorO2 { get; set; }
        public bool RencanaKeperawatan_TandaVital { get; set; }
        public bool RencanaKeperawatan_Kesadaran { get; set; }
        public bool RencanaKeperawatan_EKG { get; set; }
        public bool RencanaKeperawatan_PasangInfus { get; set; }
        public bool RencanaKeperawatan_PendarahanKIE { get; set; }
        public bool RencanaKeperawatan_Semifowler { get; set; }
        public bool RencanaKeperawatan_DowerCateter { get; set; }
        public bool RencanaKeperawatan_CairanIntravena { get; set; }
        public bool RencanaKeperawatan_KajiTugorKulit { get; set; }
        public bool RencanaKeperawatan_TetesanCairan { get; set; }
        public bool RencanaKeperawatan_PasangNGT { get; set; }
        public bool RencanaKeperawatan_AtasiNyeri { get; set; }
        public bool RencanaKeperawatan_PerawatanLuka { get; set; }
        public bool RencanaKeperawatan_KompresHangat { get; set; }
        public bool RencanaKeperawatan_Kontraindikasi { get; set; }
        public bool RencanaKeperawatan_Delegtif { get; set; }
        public bool RencanaKeperawatan_MonitorIntake { get; set; }
        public bool RencanaKeperawatan_PasangPengaman { get; set; }
        public bool RencanaKeperawatan_TandaKompartemen { get; set; }
        public bool RencanaKeperawatan_Pengaman { get; set; }
        public bool RencanaKeperawatan_GelangKuning { get; set; }
        public bool RencanaKeperawatan_PengikatanPasien { get; set; }
        public bool PemindahanRuanganMRS { get; set; }
        public string PemindahanRuanganMRSDiRuangan { get; set; }
        public bool PemindahanRuanganMRSFotoRongen { get; set; }
        public string PemindahanRuanganMRSFotoRongenKet { get; set; }
        public bool PemindahanRuanganMRSLaboratorium { get; set; }
        public string PemindahanRuanganMRSLaboratoriumKet { get; set; }
        public bool PemindahanRuanganMRSEKG { get; set; }
        public string PemindahanRuanganMRSEKGKet { get; set; }
        public bool PemindahanRuanganMRSObatObatan { get; set; }
        public string PemindahanRuanganMRSObatObatanKet { get; set; }
        public bool PemindahanRuanganDipulangkan { get; set; }
        public bool PemindahanRuanganDipulangkanKIE { get; set; }
        public bool PemindahanRuanganDipulangkanObatPulang { get; set; }
        public bool PemindahanRuanganDipulangkanFotoRontgen { get; set; }
        public bool PemindahanRuanganDipulangkanLaboratorium { get; set; }
        public bool PemindahanRuanganDipulangkanKontrolPoliklinik { get; set; }
        public bool PemindahanRuanganPulangPaksa { get; set; }
        public bool PemindahanRuanganPulangPaksa_KIE { get; set; }
        public bool PemindahanRuanganPulangPaksa_TtdSurat { get; set; }
        public bool PemindahanRuanganMeninggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemindahanRuanganMeninggalJam { get; set; }
        public bool PemindahanRuanganMinggat { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemindahanRuanganMinggatKet { get; set; }
        public string PemindahanRuanganMinggatDilaporakanKe { get; set; }
        public string PemindahanRuanganDirujukKe { get; set; }
        public string Perawat { get; set; }
        public string Perawat_Nama { get; set; }
        public string TTDPerawatPengkaji { get; set; }
        public string TTDPerawat { get; set; }
        public bool Verifikasi_Perawat { get; set; }
        public int MDOEVEIW { get; set; }
    }

    public partial class AsuhanKeperawatanDewasaIGD_EvaluasiPerkembanganViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SOAP { get; set; }
        public string Dokter { get; set; }
        public string Dokter_Nama { get; set; }
        public string Username { get; set; }
    }

    public partial class AsuhanKeperawatanDewasaIGD_ObservasiCairanViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string JenisCairan { get; set; }
        public string NoBotol { get; set; }
        public bool IV { get; set; }
        public bool Oral { get; set; }
        public bool Drain { get; set; }
        public bool NGT { get; set; }
        public bool Urin { get; set; }
        public bool BAB { get; set; }
        public string Username { get; set; }
        
    }

    public partial class AsuhanKeperawatanDewasaIGD_TindakanPerawatanViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TindakanPerawatan { get; set; }
        public string Evaluasi { get; set; }
        public string Petugas { get; set; }
        public string Petugas_Nama { get; set; }
        public string Username { get; set; }
        
    }
}