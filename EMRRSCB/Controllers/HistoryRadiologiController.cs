﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class HistoryRadiologiController : Controller
    {
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    //var fromdate = DateTime.Parse(filter[36]);
                    //var todate = DateTime.Parse(filter[37]);
                    var section = "SECT0014";
                    var namasection = "RADIOLOGI";
                    IQueryable<EMR_GetHistoryPenunjang> p = s.EMR_GetHistoryPenunjang.Where(x => x.SectionID == section);
                    var totalcount = p.Count();
                    p = p.Where("Tanggal >= @0", DateTime.Parse(filter[36]).AddDays(-1));
                    p = p.Where("Tanggal <= @0", DateTime.Parse(filter[37]));
                    if (!string.IsNullOrEmpty(filter[28]))
                        p = p.Where($"NoBuktiBilling.Contains(@0)", filter[28]);
                    if (!string.IsNullOrEmpty(filter[8]))
                        p = p.Where($"NoReg.Contains(@0)", filter[8]);
                    if (!string.IsNullOrEmpty(filter[31]))
                        p = p.Where($"NoBuktiPermintaan.Contains(@0)", filter[33]);
                    //if (!string.IsNullOrEmpty(filter[36]))
                    //    p = p.Where($"SectionAsal.Contains(@0)", filter[36]);
                    totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<HasilBacaPenunjangViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<HasilBacaPenunjangViewModel>(x);
                        var lokasipdf = s.trHasilBacaPenunjangDetail.Where(v => v.NoBukti == m.NoBukti).FirstOrDefault();
                        if (lokasipdf != null) {
                            m.LokasiFilePDF = lokasipdf.PathDokumen;
                        }
                        else { 
                            m.LokasiFilePDF = "";
                        }
                        m.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                        m.UserIDWeb = User.Identity.GetUserId();
                        if (m.NoBuktiHasil == null)
                        {
                            m.Hasil = "Belum";
                        }
                        else
                        {
                            m.Hasil = "Sudah";
                        }
                        datas.Add(m);
                    }
                    result.Data = datas;

                    return JsonConvert.SerializeObject(new TableList(result));
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListModal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var fromdate = DateTime.Parse(filter[38]);
                    var todate = DateTime.Parse(filter[39]);
                    var section = "SECT0014";
                    var namasection = "RADIOLOGI";
                    IQueryable<EMR_GetListPxPenunjang_Result> p = s.EMR_GetListPxPenunjang(section, fromdate, todate).Where(x => x.Tipe == namasection);
                    var totalcount = p.Count();
                    if (!string.IsNullOrEmpty(filter[28]))
                        p = p.Where($"NoBill.Contains(@0)", filter[28]);
                    if (!string.IsNullOrEmpty(filter[8]))
                        p = p.Where($"NoReg.Contains(@0)", filter[8]);
                    if (!string.IsNullOrEmpty(filter[31]))
                        p = p.Where($"NoOrder.Contains(@0)", filter[33]);
                    if (!string.IsNullOrEmpty(filter[36]))
                        p = p.Where($"SectionAsal.Contains(@0)", filter[36]);
                    totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<HasilBacaPenunjangViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<HasilBacaPenunjangViewModel>(x);
                        var lokasipdf = s.trHasilBacaPenunjangDetail.Where(v => v.NoBukti == m.NoBuktiHasil).FirstOrDefault();
                        if (lokasipdf != null)
                        {
                            m.LokasiFilePDF = lokasipdf.PathDokumen;
                        }
                        else
                        {
                            m.LokasiFilePDF = "";
                        }
                        m.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                        m.UserIDWeb = User.Identity.GetUserId();
                        if (m.NoBuktiHasil == null)
                        {
                            m.Hasil = "Belum";
                        }
                        else
                        {
                            m.Hasil = "Sudah";
                        }
                        datas.Add(m);
                    }
                    result.Data = datas;

                    return JsonConvert.SerializeObject(new TableList(result));
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        //[HttpPost]
        //public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            var noreg = filter[11];
        //            var header = s.EMR_VIEW_RADIOLOGI(noreg).FirstOrDefault();
        //            if (header != null)
        //            {
        //                var proses = s.EMR_VIEW_RADIOLOGI_DETAIL(header.NoBukti).Where(x => x.NoBukti == header.NoBukti).ToList();
        //                //proses = proses.Where($"{nameof(EMR_VIEW_RADIOLOGI.RegNo)}=@0", filter[11]);
        //                var totalcount = proses.Count();
        //                var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                    .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //                result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //                var m = models.ToList().ConvertAll(x => IConverter.Cast<HistoryRadViewModel>(x));
        //                m.ForEach(x => x.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy"));
        //                result.Data = m;
        //            }
        //            else
        //            {
        //                result = new ResultSS(0, null, 0, 0);
        //                result.Data = new { };
        //            }
        //        }
        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        [HttpGet]
        public ActionResult Detail(string id, string noreg)
        {
            HistoryRadViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_RADIOLOGI(noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_RADIOLOGI_DETAIL(m.NoBukti).Where(x => x.NoBukti == id && x.RegNo == noreg).ToList();
                    result = IConverter.Cast<HistoryRadViewModel>(m);
                    result.Detail_List = new List<HistoryRadDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<HistoryRadDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.Value.ToString("dd/MM/yyyy");
                        t.TglInput_View = t.TglInput != null ? t.TglInput.Value.ToString("dd/MM/yyyy") : "";
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }


        public ActionResult ExportPDF(string nobukti)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports/HistoryReport"), $"Rpt_HasilRadiologi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand($"Rpt_HasilRadiologi", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }
    }
}