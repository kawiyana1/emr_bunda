﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormPencatatanStatusFisiologis_DetailDetailModel
    {
        public string NoBukti { get; set; } 
        public int No { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Username { get; set; }
        public string TensiSystole { get; set; }
        public string TensiDiastole { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string SoO2 { get; set; }
        public string CatatanObat { get; set; }
        public string TensiSystoleDatalist { get; set; }
        public string TensiDiastoleDatalist { get; set; }
        public string NadiDatalist { get; set; }
        public string RespirasiDatalist { get; set; }
    }
}