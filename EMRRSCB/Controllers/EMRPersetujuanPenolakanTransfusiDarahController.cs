﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPersetujuanPenolakanTransfusiDarahController : Controller
    {
        // GET: EMRPersetujuanPenolakanTransfusiDarah
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRPersetujuanPenolakanTransfusiDarahViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.PersetujuanPenolakanTransfusiDarah.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPersetujuanPenolakanTransfusiDarahViewModel>(dokumen);

                        model.NoBukti = id;

                        var petugas = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Petugas);
                        if (petugas != null) model.PetugasNama = petugas.NamaDOkter;
                        model.Nama = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.JKPasien = (identitas.JenisKelamin == "M" ? "Laki" : "Perempuan");
                        model.AlamatPasien = identitas.Alamat;
                        model.NRMPasien = nrm;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.DirawatPasien = Request.Cookies["EMRSectionNamaPelayanan"].Value; ;
                        model.TglLahirPasien = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.Nama = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.JKPasien = (identitas.JenisKelamin == "M" ? "Laki" : "Perempuan");
                        model.AlamatPasien = identitas.Alamat;
                        model.NRMPasien = nrm;
                        model.DirawatPasien = Request.Cookies["EMRSectionNamaPelayanan"].Value; ;
                        model.TglLahirPasien = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));

                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPersetujuanPenolakanTransfusiDarahViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.PersetujuanPenolakanTransfusiDarah.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PersetujuanPenolakanTransfusiDarah>(item);
                        s.PersetujuanPenolakanTransfusiDarah.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Persetujuan Penolakan Transfusi Darah";
                    }
                    else
                    {
                        model = IConverter.Cast<PersetujuanPenolakanTransfusiDarah>(item);
                        s.PersetujuanPenolakanTransfusiDarah.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Persetujuan Penolakan Transfusi Darah";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}