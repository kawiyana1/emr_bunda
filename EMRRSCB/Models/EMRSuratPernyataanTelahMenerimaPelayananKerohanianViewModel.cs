﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPernyataanTelahMenerimaPelayananKerohanianViewModel
    {
        public string NoBukti { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string TempatTglLahirUmur { get; set; }
        public string Agama { get; set; }
        public string KTPSIM { get; set; }
        public string NamaPernyataan { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string TTDNamaPernyataan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        public string NamaParagraf { get; set; }

        public string NRM { get; set; }
        public int MODEVIEW { get; set; }
    }
}