﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel
    {
        public ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail> Observasi_List { get; set; }
        public ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail> RencanaKerja_List { get; set; }
        public ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail> RiwayatKehamilan_List { get; set; }
        public string NoBukti { get; set; }
         
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Rujukan { get; set; }
        public bool RujukanRS { get; set; }
        public string RujukanRSKet { get; set; }
        public bool RujukanPuskesmas { get; set; }
        public string RujukanPuskesmasKet { get; set; }
        public bool RujukanDR { get; set; }
        public string RujukanDRKet { get; set; }
        public bool RujukanLainnya { get; set; }
        public string RujukanLainnyaKet { get; set; }
        public string RujukanDX { get; set; }
        public bool RujukanDatangSendiri { get; set; }
        public bool RujukanDatangDiantar { get; set; }
        public string RujukanDatangDiantarKet { get; set; }
        public string SumberData { get; set; }
        public string SumberDataLainnya { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string AnamnesaPenyakitKeluarga { get; set; }
        public string AnamnesaPengobatan { get; set; }
        public string RiwayatMenstruasiMenarcheUmur { get; set; } 
        public string RiwayatMenstruasiVolume { get; set; }
        public string RiwayatMenstruasiSiklus { get; set; }
        public string RiwayatMenstruasiSiklusKet { get; set; }
        public string RiwayatMenstruasiLama { get; set; }
        public string RiwayatMenstruasiKeluhanSaatHaid { get; set; }
        public string RiwayatPerkawinanStatus { get; set; }
        public string RiwayatPerkawinanStatusKet { get; set; }
        public string RiwayatPerkawinanUmurPertamaKawin { get; set; }
        public string RiwayatPerkawinanKawinDenganSuami1 { get; set; }
        public string RiwayatPerkawinanKawinDenganKe { get; set; }
        public string RiwayatPerkawinanKawinDenganKeKet { get; set; }
        public string RiwayatPemakaianKontrasepsi { get; set; }
        public string RiwayatPemakaianKontrasepsiJenis { get; set; }
        public string RiwayatPemakaianKontrasepsiLamaPemakaian { get; set; }
        public string RiwayatHamilHariPertamaHaid { get; set; }
        public string RiwayatHamilTafsiranPartus { get; set; }
        public string RiwayatHamilAnteNatalCare { get; set; }
        public string RiwayatHamilAnteNatalCareKet { get; set; }
        public string RiwayatHamilAnteNatalCareKetLainnya { get; set; }
        public string RiwayatHamilFrekuensi { get; set; }
        public string RiwayatHamilImunisasiTT { get; set; }
        public string RiwayatHamilImunisasiTTKet { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilMual { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilMuntah { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilPendarahan { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilPusing { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilSakitKepala { get; set; }
        public bool RiwayatHamilKeluhanSaatHamilLainnya { get; set; }
        public string RiwayatHamilKeluhanSaatHamilLainnyaKet { get; set; }
        public string RiwayatPenyakitGynekologi { get; set; }
        public string RiwayatPenyakitGynekologiKet { get; set; }
        public bool StatusPsikologisTenang { get; set; }
        public bool StatusPsikologisCemas { get; set; }
        public bool StatusPsikologisMarah { get; set; }
        public bool StatusPsikologisDepresi { get; set; }
        public bool StatusPsikologisGelisah { get; set; }
        public bool StatusPsikologisKooperatif { get; set; }
        public bool StatusPsikologisTidakKooperatif { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriPenilaianNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeriSkor { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPenyebab { get; set; }
        public string SkriningNyeriPenyebabLainnya { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public string PemeriksaanFisikKU { get; set; }
        public string PemeriksaanFisikKesadaran { get; set; }
        public string PemeriksaanFisikGCS_E { get; set; }
        public string PemeriksaanFisikGCS_V { get; set; }
        public string PemeriksaanFisikGCS_M { get; set; }
        public string PemeriksaanFisikBB { get; set; }
        public string PemeriksaanFisikTB { get; set; }
        public string PemeriksaanFisikTD { get; set; }
        public string PemeriksaanFisikHR { get; set; }
        public string PemeriksaanFisikRR { get; set; }
        public string PemeriksaanFisikSuhuAxilla { get; set; }
        public string PemeriksaanFisikSuhuRectal { get; set; }
        public string PemeriksaanFisikSaO2 { get; set; }
        public string PemeriksaanFisikMataKonjunctiva { get; set; }
        public string PemeriksaanFisikMataSclera { get; set; }
        public string PemeriksaanFisikLeherTypoid { get; set; }
        public string PemeriksaanFisikDadaJantung { get; set; }
        public string PemeriksaanFisikDadaParu { get; set; }
        public string PemeriksaanFisikMamaeBentuk { get; set; }
        public string PemeriksaanFisikMamaePuttingSusu { get; set; }
        public string PemeriksaanFisikMamaePengeluaran { get; set; }
        public string PemeriksaanFisikMamaeColostrom { get; set; }
        public string PemeriksaanFisikMamaeKebersihan { get; set; }
        public string PemeriksaanFisikMamaeKelainan { get; set; }
        public string PemeriksaanFisikMamaeKelainanLainnya { get; set; }
        public string PemeriksaanFisikExtermitasTungkai { get; set; }
        public string PemeriksaanFisikExtermitasOdema { get; set; }
        public string PemeriksaanFisikExtermitasReflex { get; set; }
        public string AbdomenInspeksiLukaBekasOperasi { get; set; }
        public string AbdomenInspeksiKelainan { get; set; }
        public string AbdomenInspeksiKelainanLainnya { get; set; }
        public string AbdomenInspeksiKelainanLainnyaKet { get; set; }
        public string AbdomenPalpasiTinggiFundasiUteri { get; set; }
        public string AbdomenPalpasiLingkarPanggul { get; set; }
        public string AbdomenPalpasiLetakPunggung { get; set; }
        public string AbdomenPalpasiPresentasi { get; set; }
        public string AbdomenPalpasiBagianTerendah { get; set; }
        public string AbdomenPalpasiOsbornTest { get; set; }
        public string AbdomenPalpasiKontraksiUterus { get; set; }
        public string AbdomenPalpasiKontraksiUterusAda { get; set; }
        public string AbdomenPalpasiKontraksiUterusLembek { get; set; }
        public string AbdomenPalpasiKontraksiUterusLembekHis { get; set; }
        public string AbdomenPalpasiKontraksiUterusLembekHisLama { get; set; }
        public string AbdomenPalpasiKelainan { get; set; }
        public string AbdomenPalpasiTerabaMassa { get; set; }
        public string AbdomenPalpasiTerabaMassaAda { get; set; }
        public string AbdomenPalpasiTerabaMassaAdaUkuran { get; set; }
        public string AbdomenAuskultasiBisingUsus { get; set; }
        public string AbdomenAuskultasiDenyutJantung { get; set; }
        public string AnogenitalInspeksiPengeluaranPervagina { get; set; }
        public bool AnogenitalInspeksiPengeluaranPervaginaDarah { get; set; }
        public bool AnogenitalInspeksiPengeluaranPervaginaLendir { get; set; }
        public bool AnogenitalInspeksiPengeluaranPervaginaNanah { get; set; }
        public bool AnogenitalInspeksiPengeluaranPervaginaAirKetuban { get; set; }
        public bool AnogenitalInspeksiPengeluaranPervaginaBagianKecilJanin { get; set; }
        public string AnogenitalInspeksiPengeluaranPervaginaBagianKecilJaninJelaskan { get; set; }
        public bool AnogenitalInspeksiLocheaRubra { get; set; }
        public bool AnogenitalInspeksiLocheaSanguinolenta { get; set; }
        public bool AnogenitalInspeksiLocheaAlba { get; set; }
        public bool AnogenitalInspeksiLocheaLainnya { get; set; }
        public string AnogenitalInspeksiLocheaLainnyaKet { get; set; }
        public string AnogenitalInspeksiVolume { get; set; }
        public string AnogenitalInspeksiVolumeBerbau { get; set; }
        public string AnogenitalInspeksiVolumeBau { get; set; }
        public string AnogenitalInspeksiPerineum { get; set; }
        public bool AnogenitalInspeksiJahitanBaik { get; set; }
        public bool AnogenitalInspeksiJahitanTerlepas { get; set; }
        public bool AnogenitalInspeksiJahitanHematom { get; set; }
        public bool AnogenitalInspeksiJahitanOedem { get; set; }
        public bool AnogenitalInspeksiJahitanEkimosis { get; set; }
        public bool AnogenitalInspeksiJahitanKemerahan { get; set; }
        public bool AnogenitalInspekuioVaginaKelainan { get; set; }
        public string AnogenitalInspekuioVaginaKelainanKet { get; set; }
        public bool AnogenitalInspekuioVaginaFistekl { get; set; }
        public bool AnogenitalInspekuioVaginaCondiloma { get; set; }
        public bool AnogenitalInspekuioVaginaSeptum { get; set; }
        public bool AnogenitalInspekuioVaginaVarises { get; set; }
        public string AnogenitalInspekuioHymen { get; set; }
        public string AnogenitalInspekuioHymenRobekJam { get; set; }
        public string AnogenitalInspekuioHymenRobekanSekitar { get; set; }
        public string AnogenitalInspekuioPortio { get; set; }
        public string AnogenitalInspekuioPortioLainnya { get; set; }
        public string AnogenitalInspekuioCavumDouglasiMenonjol { get; set; }
        public string AnogenitalInspekuioVaginaToucherOleh { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> AnogenitalInspekuioVaginaToucherOlehTanggal { get; set; }
        public string AnogenitalInspekuioVaginaToucherOlehKet { get; set; }
        public string AnogenitalInspekuioKesanPanggul { get; set; }
        public string PengkajianKebutuhanKhusus { get; set; }
        public string HasilPemeriksaanPenunjangLab { get; set; }
        public string HasilPemeriksaanPenunjangXRay { get; set; }
        public string HasilPemeriksaanPenunjangEKG { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string DiagnosaKerja { get; set; }
        public string ICD { get; set; }
        public string Terapi { get; set; }
        public string Kosultasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> KosultasiJam { get; set; }
        public string KonsultasiTSDokter { get; set; }
        public string KonsultasiTSDokterNama { get; set; }
        public string KonsultasiKamiKonsulkanPasien { get; set; }
        public string KonsultasiDokter { get; set; }
        public string KonsultasiDokterNama { get; set; }
        public string JawabanKonsultasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JawabanKonsultasiJam { get; set; }
        public string JawabanKonsultasiTSDokter { get; set; }
        public string JawabanKonsultasiTSDokterNama { get; set; }
        public string JawabanKonsultasiHasilPemeriksaan { get; set; }
        public string JawabanKonsultasiDokter { get; set; }
        public string JawabanKonsultasiDokterNama { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> KondisiSaatKeluarIGDJam { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmum { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumKesadaran { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumTekananDarah { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumSuhu { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumNadi { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumFrekuensiNafas { get; set; }
        public string KondisiSaatKeluarIGDKeadaanUmumCatatanPenting { get; set; }
        public string PemberianKomunikasi { get; set; }
        public string DokterJaga { get; set; }
        public bool PemindahanRuanganDipindahkeRuangan { get; set; }
        public string PemindahanRuanganDipindahkeRuanganKet { get; set; }
        public bool PemindahanRuanganDipulangkanKIE { get; set; }
        public bool PemindahanRuanganDipulangkanObatPulang { get; set; }
        public bool PemindahanRuanganDipulangkanLaboratorium { get; set; }
        public bool PemindahanRuanganDipulangkanKontrolPoliklinik { get; set; }
        public bool PemindahanRuanganMeninggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemindahanRuanganMeninggalJam { get; set; }
        public bool PemindahanRuanganMinggat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemindahanRuanganMinggatKet { get; set; }
        public string PemindahanRuanganMinggatDilaporakanKe { get; set; }
        public string PemindahanRuanganDirujukKe { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string DokterJagaNama { get; set; } 
        public string NamaPasien { get; set; } 

        public string TTDDokterJaga { get; set; }
        public string TTDDPJP { get; set; }
        public string TTDKonsultasiTS { get; set; }
        public string TTDJawabanKonsultasiTS { get; set; }
        public string TTDKonsultasi { get; set; }
        public string TTDJawabanKonsultasi { get; set; } 
        public string TTDPasien { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemPengakjianMedisKebidananKandunganIGD> ListTemplate { get; set; }
    }

    public class SelectItemPengakjianMedisKebidananKandunganIGD
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}