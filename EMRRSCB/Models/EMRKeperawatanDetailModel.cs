﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRKeperawatanDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Pengkajian { get; set; }
        public string DiagnosaKeperawatan { get; set; }
        public string Diagnosa { get; set; }
        public string RencanaTindakan { get; set; }
        public string Implementasi { get; set; }
        public string Evaluasi { get; set; }
        public string Username { get; set; }
    }
}