﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRencanaPemulanganPasienViewModel
    {
        public string NoBukti { get; set; }
        public string DiagnosaMedis { get; set; }
        public string SaatMRS { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMRS { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamMRS { get; set; }
        public string AlasanMRS { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPemulangan { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPemulangan { get; set; }
        public string EstimasiTanggal { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Keluarga { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string PasienDanKeluarga { get; set; }
        public string Pekerjaan { get; set; }
        public string Keuangan { get; set; }
        public string UmurLebih60 { get; set; }
        public string UmurLebih60Ket { get; set; }
        public string BayiBBLR { get; set; }
        public string BayiBBLRKet { get; set; }
        public string KeterbatasanMobilitas { get; set; }
        public string KeterbatasanMobilitasKet { get; set; }
        public string PerawatanLanjutan { get; set; }
        public string PerawatanLanjutanKet { get; set; }
        public string BantuanAktivitas { get; set; }
        public string BantuanAktivitasKet { get; set; }
        public bool MinumObat { get; set; }
        public bool Toileting { get; set; }
        public bool MenyiapkanMakanan { get; set; }
        public bool TenagaKhusus { get; set; }
        public bool PerawatanDiri { get; set; }
        public bool PemantauanDiet { get; set; }
        public bool Berpakaian { get; set; }
        public bool Transportasi { get; set; }
        public string MembantuKeperluan { get; set; }
        public string MembantuKeperluanKet { get; set; }
        public string PasienTinggalSendiri { get; set; }
        public string PasienTinggalSendiriKet { get; set; }
        public string MenggunakanPeralatanMedis { get; set; }
        public string MenggunakanPeralatanMedisKet { get; set; }
        public string MemerlukanAlatBantu { get; set; }
        public string MemerlukanAlatBantuKet { get; set; }
        public string MemerlukanBantuanPerawatan { get; set; }
        public string MemerlukanBantuanPerawatanKet { get; set; }
        public string BermasalahMemenuhiKebutuhan { get; set; }
        public string BermasalahMemenuhiKebutuhanKet { get; set; }
        public string MemilikiNyeriKronis { get; set; }
        public string MemilikiNyeriKronisKet { get; set; }
        public string MemerlukanEdukasi { get; set; }
        public string MemerlukanEdukasiKet { get; set; }
        public string MemerlukanKeterampilan { get; set; }
        public string MemerlukanKeterampilanKet { get; set; }
        public string TTDKeluarga { get; set; }
        public int MODEVIEW { get; set; }
        public int Report { get; set; }
      
    }
}