﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCapKakiBayiViewModel
    {
        public string NoBukti { get; set; }
        public string KakiKanan { get; set; }
        public string KakiKiri { get; set; }
        public int MODEVIEW { get; set; }
      
    }

}