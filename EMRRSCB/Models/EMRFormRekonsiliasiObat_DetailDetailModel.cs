﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormRekonsiliasiObat_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string DaftarObat { get; set; }
        public string DaftarObatNama { get; set; }
        public string SeberapaBeratAlergi { get; set; }
        public string ReaksiAlergi { get; set; }
        public string Username { get; set; }
    }
}