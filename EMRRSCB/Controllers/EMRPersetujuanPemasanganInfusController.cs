﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPersetujuanPemasanganInfusController : Controller
    {
        // GET: EMRPersetujuanPemasanganInfus
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRPersetujuanPemasanganInfusViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.PersetujuanPemasanganInfus.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPersetujuanPemasanganInfusViewModel>(dokumen);

                        model.NoBukti = id;

                        var pemberiinformasi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaPemberiInformasi);
                        if (pemberiinformasi != null) model.NamaPemberiInformasiNama = pemberiinformasi.NamaDOkter;

                        var dokterpenanggungjawab = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterPenanggungJawab);
                        if (dokterpenanggungjawab != null) model.DokterPenanggungJawabNama = dokterpenanggungjawab.NamaDOkter;

                        var menerangkan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.TelahMenerangkan);
                        if (menerangkan != null) model.TelahMenerangkanNama = menerangkan.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPersetujuanPemasanganInfusViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.PersetujuanPemasanganInfus.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PersetujuanPemasanganInfus>(item);
                        o.Username = User.Identity.GetUserName();
                        s.PersetujuanPemasanganInfus.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Persetujuan Pemasangan Infus";
                    }
                    else
                    {
                        model = IConverter.Cast<PersetujuanPemasanganInfus>(item);
                        model.Username = User.Identity.GetUserName();
                        s.PersetujuanPemasanganInfus.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Persetujuan Pemasangan Infus";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}