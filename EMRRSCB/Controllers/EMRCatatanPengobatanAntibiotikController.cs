﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanPengobatanAntibiotikController : Controller
    {
        // GET: EMRCatatanPengobatanAntibiotik
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanPengobatanAntibiotik.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRCatatanPengobatanAntibiotik_List = new ListDetail<EMRCatatanPengobatanAntibiotikViewModel>();
                        var detail_1 = s.CatatanPengobatanAntibiotik.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanPengobatanAntibiotikViewModel>(x);

                            var t = new BridgingSIMRS();
                            var getobat = sim.mBarang.Where(xx => xx.Kode_Barang == x.KodeObat).FirstOrDefault();
                            if (getobat == null) y.NamaObat = "";
                            if (getobat != null) y.NamaObat = getobat.Nama_Barang;

                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.DPJP);
                            if (dokter != null)
                            {
                                y.DPJPNama = dokter.NamaDOkter;
                            }
                            model.EMRCatatanPengobatanAntibiotik_List.Add(false, y);
                            model.Diagnosa = y.Diagnosa;
                            model.Alergi = y.Alergi;
                            model.TB = y.TB;
                            model.BB = y.BB;
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanPengobatanAntibiotik.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Pengobatan Antibiotik";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Pengobatan Antibiotik";
                    }

                    if (item.EMRCatatanPengobatanAntibiotik_List == null) item.EMRCatatanPengobatanAntibiotik_List = new ListDetail<EMRCatatanPengobatanAntibiotikViewModel>();
                    item.EMRCatatanPengobatanAntibiotik_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRCatatanPengobatanAntibiotik_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Diagnosa = item.Diagnosa;
                        x.Model.Alergi = item.Alergi;
                        x.Model.BB = item.BB;
                        x.Model.TB = item.TB;
                        x.Model.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRCatatanPengobatanAntibiotik_List;
                    var real_list = s.CatatanPengobatanAntibiotik.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanPengobatanAntibiotik.Remove(x);
                    }
                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new CatatanPengobatanAntibiotik();
                            s.CatatanPengobatanAntibiotik.Add(IConverter.Cast<CatatanPengobatanAntibiotik>(x.Model));

                        }
                        else
                        {
                            _m = IConverter.Cast<CatatanPengobatanAntibiotik>(x.Model);
                            s.CatatanPengobatanAntibiotik.AddOrUpdate(_m);
                        }
                        //if (_m == null) s.CatatanPengobatanAntibiotik.Add(IConverter.Cast<CatatanPengobatanAntibiotik>(x.Model));
                        //else
                        //{

                        //    _m.NoBukti = x.Model.NoBukti;
                        //    _m.No = x.Model.No;
                        //    _m.TglMulaiPemberian = x.Model.TglMulaiPemberian;
                        //    _m.TglResep = x.Model.TglResep;
                        //    _m.KodeObat = x.Model.KodeObat;
                        //    _m.NamaObat = x.Model.NamaObat;
                        //    _m.Profilaksia = x.Model.Profilaksia;
                        //    _m.JamPemeberianObat = x.Model.JamPemeberianObat;
                        //    _m.Empiris = x.Model.Empiris;
                        //    _m.Definitif = x.Model.Definitif;
                        //    _m.Rute = x.Model.Rute;
                        //    _m.Frekuensi = x.Model.Frekuensi;
                        //    _m.Catatan = x.Model.Catatan;
                        //    _m.Dosis = x.Model.Dosis;
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}