﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRencanaSedasi_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string ObatPremedikasi { get; set; }
        public string ObatPremedikasiNama { get; set; }
        public string Dosis { get; set; }
        public string Cara { get; set; }
        public string Username { get; set; }
    }
}