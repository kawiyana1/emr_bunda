﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianMedisIGDController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRPengkajianMedisIGDViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianMedisGawatDarurat.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianMedisIGDViewModel>(dokumen);

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_Kepada);
                        if (dokter != null) model.Konsultasi_KepadaNama = dokter.NamaDOkter;

                        var dokter1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_Kepada);
                        if (dokter1 != null) model.JawabanKonsultasi_KepadaNama = dokter1.NamaDOkter;

                        var dokter2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_HormatKami);
                        if (dokter2 != null) model.Konsultasi_HormatKamiNama = dokter2.NamaDOkter;

                        var dokter3 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_HormatKami);
                        if (dokter3 != null) model.JawabanKonsultasi_HormatKamiNama = dokter3.NamaDOkter;

                        var dokter4 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJaga);
                        if (dokter4 != null) model.DokterJagaNama = dokter4.NamaDOkter;

                        var dokter5 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJagaID);
                        if (dokter5 != null) model.DokterJagaIDNama = dokter5.NamaDOkter;


                        model.Observasi_List = new ListDetail<EMRPengkajianMedisIGD_ObservasiDetailViewModel>();
                        var detail_1 = s.PengkajianMedisGawatDarurat_Observasi.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisIGD_ObservasiDetailViewModel>(x);
                            var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Nama);
                            if (dokter_detail != null) y.NamaNama = dokter_detail.NamaDOkter;
                            model.Observasi_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel>();
                        var detail_2 = s.PengkajianMedisGawatDarurat_RencanaKerja.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemList>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemList()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianMedisIGDViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianMedisGawatDarurat.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianMedisIGDViewModel>(dokumen);
                        model.NoBukti = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_Kepada);
                        if (dokter != null) model.Konsultasi_KepadaNama = dokter.NamaDOkter;

                        var dokter1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_Kepada);
                        if (dokter1 != null) model.JawabanKonsultasi_KepadaNama = dokter1.NamaDOkter;

                        var dokter2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Konsultasi_HormatKami);
                        if (dokter2 != null) model.Konsultasi_HormatKamiNama = dokter2.NamaDOkter;

                        var dokter3 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasi_HormatKami);
                        if (dokter3 != null) model.JawabanKonsultasi_HormatKamiNama = dokter3.NamaDOkter;

                        var dokter4 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJaga);
                        if (dokter4 != null) model.DokterJagaNama = dokter4.NamaDOkter;

                        var dokter5 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJagaID);
                        if (dokter5 != null) model.DokterJagaIDNama = dokter5.NamaDOkter;


                        model.Observasi_List = new ListDetail<EMRPengkajianMedisIGD_ObservasiDetailViewModel>();
                        var detail_1 = s.PengkajianMedisGawatDarurat_Observasi.Where(x => x.NoBukti == copy).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisIGD_ObservasiDetailViewModel>(x);
                            var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Nama);
                            if (dokter_detail != null) y.NamaNama = dokter_detail.NamaDOkter;
                            model.Observasi_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel>();
                        var detail_2 = s.PengkajianMedisGawatDarurat_RencanaKerja.Where(x => x.NoBukti == copy).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.VerifikasiDokterJaga = false;
                    model.VerifikasiJawabanKonsultasi_HormatKami = false;
                    model.VerifikasiKonsultasi_HormatKami = false;
                    model.VerifikasiPasien = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemList>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemList()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                var item = new EMRPengkajianMedisIGDViewModel();
                TryUpdateModel(item);
                ResultSS result;
                item.NyeriMembaikBilaMinumKeterangan = (item.NyeriMembaikBilaMinumKeterangan == null) ? "-" : item.NyeriMembaikBilaMinumKeterangan;
                using (var s = new EMREntities())
                {
                    var model = s.PengkajianMedisGawatDarurat.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianMedisGawatDarurat>(item);
                        s.PengkajianMedisGawatDarurat.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Pengkajian Medis Gawat Darurat";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianMedisGawatDarurat>(item);
                        s.PengkajianMedisGawatDarurat.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Pengkajian Medis Gawat Darurat";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DokterJagaID;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }


                    if (item.Observasi_List == null) item.Observasi_List = new ListDetail<EMRPengkajianMedisIGD_ObservasiDetailViewModel>();
                    item.Observasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Observasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Observasi_List;
                    var real_list = s.PengkajianMedisGawatDarurat_Observasi.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisGawatDarurat_Observasi.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.PengkajianMedisGawatDarurat_Observasi.Add(IConverter.Cast<PengkajianMedisGawatDarurat_Observasi>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.Observasi = x.Model.Observasi;
                            _m.Instruksi = x.Model.Instruksi;
                        }
                    }

                    #region === Detail Rencana Kerja
                    if (item.RencanaKerja_List == null) item.RencanaKerja_List = new ListDetail<EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel>();
                    item.RencanaKerja_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RencanaKerja_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list1 = item.RencanaKerja_List;
                    var real_list1 = s.PengkajianMedisGawatDarurat_RencanaKerja.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisGawatDarurat_RencanaKerja.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianMedisGawatDarurat_RencanaKerja.Add(IConverter.Cast<PengkajianMedisGawatDarurat_RencanaKerja>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.DaftarMasalah = x.Model.DaftarMasalah;
                            _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                            _m.Target = x.Model.Target;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}