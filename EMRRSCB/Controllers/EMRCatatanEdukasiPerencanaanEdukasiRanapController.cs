﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanEdukasiPerencanaanEdukasiRanapController : Controller
    {
        // GET: EMRCatatanEdukasiPerencanaanEdukasiRanap
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRCatatanEdukasiPerencanaanEdukasiRanapViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanEdukasiPerencanaanEdukasiRanap.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRCatatanEdukasiPerencanaanEdukasiRanapViewModel>(dokumen);

                        model.NoBukti = id;
                        model.Username = User.Identity.GetUserName();

                        var edukator1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterSpesialis_Edukator);
                        if (edukator1 != null) model.DokterSpesialis_EdukatorNama = edukator1.NamaDOkter;

                        var edukator2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DrSpesialis_Edukator);
                        if (edukator2 != null) model.DrSpesialis_EdukatorNama = edukator2.NamaDOkter;

                        var edukator3 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Farmasi_Edukator);
                        if (edukator3 != null) model.Farmasi_EdukatorNama = edukator3.NamaDOkter;

                        var edukator4 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.ManajemenNyeri_Edukator);
                        if (edukator4 != null) model.ManajemenNyeri_EdukatorNama = edukator4.NamaDOkter;

                        var edukator5 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Nutrisi_Edukator);
                        if (edukator5 != null) model.Nutrisi_EdukatorNama = edukator5.NamaDOkter;

                        var edukator6 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat_Edukator);
                        if (edukator6 != null) model.Perawat_EdukatorNama = edukator6.NamaDOkter;

                        var edukator7 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Rehabilitasi_Edukator);
                        if (edukator7 != null) model.Rehabilitasi_EdukatorNama = edukator7.NamaDOkter;

                        var edukator8 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Rohaniawan_Edukator);
                        if (edukator8 != null) model.Rohaniawan_EdukatorNama = edukator8.NamaDOkter;

                        #region === Detail Catatan Edukasi
                        model.Edukasi_List = new ListDetail<EMRCatatanEdukasiRawatJalanViewModel>();
                        var detail_1 = s.CatatanEdukasiRawatJalan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanEdukasiRawatJalanViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Edukator);
                            if (dokter != null)
                            {
                                y.EdukatorNama = dokter.NamaDOkter;
                            }
                            model.Edukasi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.DokterSpesialis_Tanggal = DateTime.Today;
                        model.DrSpesialis_Tanggal = DateTime.Today;
                        model.Farmasi_Tanggal = DateTime.Today;
                        model.ManajemenNyeri_Tanggal = DateTime.Today;
                        model.Nutrisi_Tanggal = DateTime.Today;
                        model.Perawat_Tanggal = DateTime.Today;
                        model.Rehabilitasi_Tanggal = DateTime.Today;
                        model.Rohaniawan_Tanggal = DateTime.Today;
                        model.Username = User.Identity.GetUserName();
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRCatatanEdukasiPerencanaanEdukasiRanapViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanEdukasiPerencanaanEdukasiRanap.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<CatatanEdukasiPerencanaanEdukasiRanap>(item);
                        s.CatatanEdukasiPerencanaanEdukasiRanap.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Catatan Edukasi Ranap";
                    }
                    else
                    {
                        model = IConverter.Cast<CatatanEdukasiPerencanaanEdukasiRanap>(item);
                        s.CatatanEdukasiPerencanaanEdukasiRanap.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Catatan Edukasi Ranap";
                    }
                    #region === Detail Catatan Edukasi
                    if (item.Edukasi_List == null) item.Edukasi_List = new ListDetail<EMRCatatanEdukasiRawatJalanViewModel>();
                    item.Edukasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Edukasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Edukasi_List;
                    var real_list = s.CatatanEdukasiRawatJalan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanEdukasiRawatJalan.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new CatatanEdukasiRawatJalan();
                            s.CatatanEdukasiRawatJalan.Add(IConverter.Cast<CatatanEdukasiRawatJalan>(x.Model));
                            //s.CatatanEdukasiRawatJalan.Add(IConverter.Cast<CatatanEdukasiRawatJalan>(x.Model));
                        }
                        else
                        {
                            _m = IConverter.Cast<CatatanEdukasiRawatJalan>(x.Model);
                            s.CatatanEdukasiRawatJalan.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}