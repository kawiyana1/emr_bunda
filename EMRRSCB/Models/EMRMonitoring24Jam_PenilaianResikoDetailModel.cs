﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoring24Jam_PenilaianResikoDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Usia { get; set; }
        public string RiwayatJatuh { get; set; }
        public string Aktifitas { get; set; }
        public string Mobilitas { get; set; }
        public string DefisitSensoris { get; set; }
        public string Kognitif { get; set; }
        public string PolaBAB { get; set; }
        public string Pengobatan { get; set; }
        public string Komorbiditas { get; set; }
        public string Skor { get; set; }
    }
}