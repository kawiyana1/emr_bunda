﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class HistoryObatViewModel
    {
        public string NoBukti { get; set; }
        public string NoResep { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string DeskripsiObat { get; set; }
        public string NoReg { get; set; }

        public List<HistoryObatDetailViewModel> Detail_List { get; set; }
    }
    public partial class HistoryObatDetailViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string NoResep { get; set; }
        public string DeskripsiObat { get; set; }
        public string NoReg { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public double JmlObat { get; set; }
        public string Dosis { get; set; }
    }
    public class HistoryLabViewModel
    {
        public string NoSystem { get; set; }
        public string RegNo { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterPengirim { get; set; }
        public string SectionName { get; set; }
        public string PenanggungJawab { get; set; }
        public string Analis { get; set; }

        public List<HistoryLabDetailViewModel> Detail_List { get; set; }
    }
    public class HistoryLabDetailViewModel
    {
        public string NoSystem { get; set; }
        public string RegNo { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterPengirim { get; set; }
        public string SectionName { get; set; }
        public string KategoriTestNama { get; set; }
        public string NamaTest { get; set; }
        public string Nilai { get; set; }
        public string Satuan { get; set; }
        public string NilaiRujukan { get; set; }
        public string HasilTidakNormal_Flag { get; set; }
        public string Analis { get; set; }
        public string PenanggungJawab { get; set; }
    }


    public class HistoryRadViewModel
    {
        public string RegNo { get; set; }
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string RAD_AksesNumber { get; set; }
        public string StatusRad { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string DokterPengirim { get; set; }

        public List<HistoryRadDetailViewModel> Detail_List{ get; set; }
}
    public class HistoryRadDetailViewModel
    {
        public string RegNo { get; set; }
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string RAD_AksesNumber { get; set; }
        public string StatusRad { get; set; }
        public Nullable<int> Nomor { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }
        public string TglInput_View { get; set; }
        public string DokterPengirim { get; set; }
        public string Klinis { get; set; }
        public string Judul { get; set; }
        public string JawabanBody { get; set; }
        public string Kesan { get; set; }
    }
}