﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormBCatatanImplementasiViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Tanggal { get; set; }
        public string Catatan { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string TandaTangan { get; set; }
        public string Username { get; set; }
        public bool Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}