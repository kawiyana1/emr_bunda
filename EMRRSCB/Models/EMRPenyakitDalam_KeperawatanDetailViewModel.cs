﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPenyakitDalam_KeperawatanDetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Pengkajian { get; set; }
        public string PengkajianNama { get; set; }
        public string Diagnosa { get; set; }
        public string RencanaTindakan { get; set; }
        public string Implementasi { get; set; }
        public string Evaluasi { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Username { get; set; }
    }
}