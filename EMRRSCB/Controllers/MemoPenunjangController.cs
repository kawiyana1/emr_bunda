﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class MemoPenunjangController : Controller
    {
        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, int nomor)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NoReg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            using (var s = new SIM_Entities())
            {
                var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                if (m == null) return HttpNotFound();
                ViewBag.DokterID = m.DokterID;
                ViewBag.NamaDokter = m.NamaDOkter;
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new MemoPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        s.SIMtrMemoPenunjang_Insert(
                            item.Nomor,
                            item.DokterID,
                            item.SectionID,
                            DateTime.Today,
                            DateTime.Now,
                            item.SectionTujuanID,
                            item.Memo,
                            490,
                            item.SudahPeriksa,
                            item.NoReg
                        );
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"MemoPenunjang Create {item.NoReg} {item.SectionID} {item.Nomor}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            MemoPenunjangViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_GetMemoPenunjang.FirstOrDefault(x => x.NoBuktiMemo == id && x.SudahPeriksa == false);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<MemoPenunjangViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new MemoPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMtrMemoPenunjang.FirstOrDefault(x => x.NoBuktiMemo == item.NoBuktiMemo);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        model.Memo = item.Memo;

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"MemoPenunjang Edit {model.NoBuktiMemo}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var noreg = filter[0];
                    var section = filter[1];
                    IQueryable<Pelayanan_GetMemoPenunjang> proses = s.Pelayanan_GetMemoPenunjang.Where(x => x.NoReg == noreg && x.SectionID == section && x.Batal == 0);
                    proses = proses.Where($"{nameof(Pelayanan_GetMemoPenunjang.NoReg)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_GetMemoPenunjang.SectionID)}=@0", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<MemoPenunjangViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Jam_View = x.Jam.ToString("HH") + ":" + x.Jam.ToString("mm");
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : Request.Cookies["EMRSectionIDPelayanan"].Value);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.Kategori)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, (kategorioperasi == null ? 1 : kategorioperasi), section, noreg, "1").FirstOrDefault();
                    var m = new
                    {
                        jasa = jasa,
                        dokter = dokter,
                        cito = cito,
                        kategorioperasi = kategorioperasi,
                        section = section,
                        noreg = noreg
                    };
                    if (h == null) throw new Exception($"GetTarifBiaya_Global tidak ditemukan " + m.ToString());
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}