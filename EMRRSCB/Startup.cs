﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EMRRSCB.Startup))]
namespace EMRRSCB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
