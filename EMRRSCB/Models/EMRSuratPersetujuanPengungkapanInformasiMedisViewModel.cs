﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPersetujuanPengungkapanInformasiMedisViewModel
    {
        public string NoBukti { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }
        public string NomorKTPSIMPaspor { get; set; }
        public string NamaIden { get; set; }
        public string JenisKelaminIden { get; set; }
        public int?  UmurIden { get; set; }
        public string NRM { get; set; }
        public string UntukKepentingan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglSurat { get; set; }
        public string TTDNama { get; set; }
        public string TTDImage { get; set; }
        public int MODEVIEW { get; set; }

    }
}