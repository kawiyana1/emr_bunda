﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRI_PengkajianAwalMedisPenyakitKulitKelaminViewModel
    {
        public ListDetail<EMRRI_RencanaKerja_DetailModel> RencanaKerja_List { get; set; }
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDatang { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriPenilaianNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeriSkor { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPenyebab { get; set; }
        public string SkriningNyeriPenyebabKet { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public bool KebutuhanPrivasiIdentitas { get; set; }
        public bool KebutuhanPrivasiRekamMedis { get; set; }
        public bool KebutuhanPrivasiSaatPemeriksaan { get; set; }
        public bool KebutuhanPrivasiSaatTindakanMedis { get; set; }
        public bool KebutuhanPrivasiTransportasi { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string AnamnesaPenyakitKeluarga { get; set; }
        public string AnamnesaPengobatan { get; set; }
        public string AnamnesaRiwayatOperasi { get; set; }
        public string AnamnesaRiwayatTranfusi { get; set; }
        public string TandaVitalKeadaanUmum { get; set; }
        public string TandaVitalRespirasi { get; set; }
        public string TandaVitalSaturasi { get; set; }
        public string TandaVitalGCS_E { get; set; }
        public string TandaVitalSaturasiPada { get; set; }
        public string TandaVitalSaturasiPadaKet { get; set; }
        public string TandaVitalGCS_V { get; set; }
        public string TandaVitalGCS_M { get; set; }
        public string TandaVitalTindakanResusitasi { get; set; }
        public string TandaVitalTindakanResusitasi_Ket { get; set; }
        public string TandaVitalTensi { get; set; }
        public string TandaVitalRR { get; set; }
        public string TandaVitalNadi { get; set; }
        public string TandaVitalSuhuAxilla { get; set; }
        public string TandaVitalSuhuRectal { get; set; }
        public string TandaVitalBB { get; set; }
        public string PemeriksaanMata { get; set; }
        public string PemeriksaanMata_Ket { get; set; }
        public string PemeriksaanTHT { get; set; }
        public string PemeriksaanTHT_Ket { get; set; }
        public string PemeriksaanCor { get; set; }
        public string PemeriksaanCor_Ket { get; set; }
        public string PemeriksaanPulmo { get; set; }
        public string PemeriksaanPulmo_Ket { get; set; }
        public string PemeriksaanHepar { get; set; }
        public string PemeriksaanHepar_Ket { get; set; }
        public string PengkajianKebutuhanKhusus { get; set; }
        public string Laboratorium { get; set; }
        public string Xray { get; set; }
        public string EKG { get; set; }
        public string DiagnosaKerja { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string ICDX { get; set; }
        public string PengkajianKeperawatan { get; set; }
        public string Terapi { get; set; }
        public string DokterKonsul { get; set; }
        public string Catatan { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string PerawatNama { get; set; }
        public bool Tenang { get; set; }
        public bool Cemas { get; set; }
        public bool Marah { get; set; }
        public bool Depresi { get; set; }
        public bool Gelisah { get; set; }
        public bool Kooperatif { get; set; }
        public bool TidakKooperatif { get; set; }
        public string TTDDPJP { get; set; }
        public bool StatusDermatologiLokasi { get; set; }
        public string StatusDermatologiLokasiKet { get; set; }
        public bool StatusDermatologiBentukKelamin { get; set; }
        public string StatusDermatologiBentukKelaminKet { get; set; }
        public string Gambar { get; set; }
        public bool VerifikasiDPJP { get; set; }


        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListKulitkelaminRI> ListTemplate { get; set; } 

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
    }

    public class SelectItemListKulitkelaminRI
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}