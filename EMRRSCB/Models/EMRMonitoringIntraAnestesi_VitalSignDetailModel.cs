﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringIntraAnestesi_VitalSignDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Dosis { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }
        public string N2O { get; set; }
        public string Gas { get; set; }
        public string Jumlah { get; set; }
    }
}