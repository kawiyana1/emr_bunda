﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormEdukasiTindakanAnestesiDanSedasiViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string NoTelp { get; set; }
        public string NRM { get; set; }
        public string Diagnosis { get; set; }
        public string RencanaTindakan { get; set; }
        public string JenisAnestesi { get; set; }
        public string Negara { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string PihakDijelaskan { get; set; }
        public string NamaPasien { get; set; }
        public string TglLahir { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string Username { get; set; }
        public string Terhadap { get; set; }
        public bool Verifikasi { get; set; }
        public string TTDKeluarga { get; set; }
        public string NamaKeluarga { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}