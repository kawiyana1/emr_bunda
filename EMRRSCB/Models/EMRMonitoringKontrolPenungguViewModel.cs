﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringKontrolPenungguViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NamaPasien { get; set; }
        public string Umur { get; set; }
        public string NRM { get; set; }
        public string NamaPenunggu { get; set; }
        public string TglPenerimaan { get; set; }
        public string TglPengembalian { get; set; }
        public string NoIdentitas { get; set; }

        public int MODEVIEW { get; set; }
    }
}