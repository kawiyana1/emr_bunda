﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRChecklistPraBedahDanKelengkapanPraBedahController : Controller
    {
        // GET: EMRChecklistPraBedahDanKelengkapanPraBedah
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRChecklistPraBedahDanKelengkapanPraBedahViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
            {
                var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                var dokumen = s.ChecklistPraBedahDanKelengkapanPraBedah.FirstOrDefault(x => x.NoBukti == id);
                if (dokumen != null)
                {
                    model = IConverter.Cast<EMRChecklistPraBedahDanKelengkapanPraBedahViewModel>(dokumen);

                    model.NoBukti = id;
                    model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                    model.NRM = (nrm == null ? "" : nrm);
                    model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                    model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                    model.Username = User.Identity.GetUserName();

                    var petugaspre = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PreOperasi_Petugas);
                    if (petugaspre != null) model.PreOperasi_PetugasNama = petugaspre.NamaDOkter;

                    var petugaspost = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PostOperasi_Petugas);
                    if (petugaspost != null) model.PostOperasi_PetugasNama = petugaspost.NamaDOkter;

                    var petugasrawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PostOperasi_PetugasRuangRawat);
                    if (petugasrawat != null) model.PostOperasi_PetugasRuangRawatNama = petugasrawat.NamaDOkter;

                    var petugasibs = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PreOperasi_PetugasIBS);
                    if (petugasibs != null) model.PreOperasi_PetugasIBSNama = petugasibs.NamaDOkter;

                    model.MODEVIEW = view;
                }
                else
                {
                    model.NoBukti = id;
                    model.Tanggal = DateTime.Today;
                    model.Jam = DateTime.Now;
                    model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                    model.NRM = (nrm == null ? "" : nrm);
                    model.JenisKelamin = (identitas.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                    model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                    model.Username = User.Identity.GetUserName();
                }

            }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRChecklistPraBedahDanKelengkapanPraBedahViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ChecklistPraBedahDanKelengkapanPraBedah.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<ChecklistPraBedahDanKelengkapanPraBedah>(item);
                        s.ChecklistPraBedahDanKelengkapanPraBedah.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Checklist Prabedah dan Kelengkapan Pasien Pra Bedah";
                    }
                    else
                    {
                        model = IConverter.Cast<ChecklistPraBedahDanKelengkapanPraBedah>(item);
                        s.ChecklistPraBedahDanKelengkapanPraBedah.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Checklist Prabedah dan Kelengkapan Pasien Pra Bedah";
                    }

                   
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}