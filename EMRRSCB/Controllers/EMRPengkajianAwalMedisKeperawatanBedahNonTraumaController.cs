﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianAwalMedisKeperawatanBedahNonTraumaController : Controller
    {
        // GET: PengkajianAwalMedisKeperawatanBedahNonTrauma
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRPengkajianAwalMedisKeperawatanBedahNonTraumaViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianAwalMedisKeperawatanBedahNonTrauma.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalMedisKeperawatanBedahNonTraumaViewModel>(dokumen);

                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterKonsul);
                        if (jawabankonsultasikpd != null) model.DokterKonsulNama = jawabankonsultasikpd.NamaDOkter;

                        #region === Detail Keperawatan
                        model.Keperawatan_List = new ListDetail<EMRKeperawatanDetailModel>();
                        var detail_1 = s.Keperawatan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRKeperawatanDetailModel>(x);
                            model.Keperawatan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Rencana Kerja
                        model.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRRencanaKerjaDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.RencanaKerja_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.JamDatang = DateTime.Now;
                        model.JamPengkajian = DateTime.Now;
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListBedahNonTrauma>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListBedahNonTrauma()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRPengkajianAwalMedisKeperawatanBedahNonTraumaViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianAwalMedisKeperawatanBedahNonTrauma.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalMedisKeperawatanBedahNonTraumaViewModel>(dokumen);
                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                        var jawabankonsultasikpd = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterKonsul);
                        if (jawabankonsultasikpd != null) model.DokterKonsulNama = jawabankonsultasikpd.NamaDOkter;

                        #region === Detail Keperawatan
                        model.Keperawatan_List = new ListDetail<EMRKeperawatanDetailModel>();
                        var detail_1 = s.Keperawatan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRKeperawatanDetailModel>(x);
                            model.Keperawatan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Rencana Kerja
                        model.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                        var detail_2 = s.RencanaKerja_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRRencanaKerjaDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.RencanaKerja_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.VerifikasiDPJP = false;
                    model.VerifikasiPerawat = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListBedahNonTrauma>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListBedahNonTrauma()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPengkajianAwalMedisKeperawatanBedahNonTraumaViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.PengkajianAwalMedisKeperawatanBedahNonTrauma.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianAwalMedisKeperawatanBedahNonTrauma>(item);
                        s.PengkajianAwalMedisKeperawatanBedahNonTrauma.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Woi Create Poliklinik Bedah Non Trauma";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianAwalMedisKeperawatanBedahNonTrauma>(item);
                        s.PengkajianAwalMedisKeperawatanBedahNonTrauma.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Woi Update Poliklinik Bedah Non Trauma";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJP;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    #region === Detail Keperawatan
                    if (item.Keperawatan_List == null) item.Keperawatan_List = new ListDetail<EMRKeperawatanDetailModel>();
                    item.Keperawatan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Keperawatan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Keperawatan_List;
                    var real_list = s.Keperawatan_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Keperawatan_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.Keperawatan_Detail.Add(IConverter.Cast<Keperawatan_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Pengkajian = x.Model.Pengkajian;
                            _m.Diagnosa = x.Model.Diagnosa;
                            _m.RencanaTindakan = x.Model.RencanaTindakan;
                            _m.Implementasi = x.Model.Implementasi;
                            _m.Evaluasi = x.Model.Evaluasi;
                        }
                    }
                    #endregion

                    #region === Detail Rencana Kerja
                    if (item.RencanaKerja_List == null) item.RencanaKerja_List = new ListDetail<EMRRencanaKerjaDetailModel>();
                    item.RencanaKerja_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RencanaKerja_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.RencanaKerja_List;
                    var real_list1 = s.RencanaKerja_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RencanaKerja_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RencanaKerja_Detail.Add(IConverter.Cast<RencanaKerja_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = DateTime.Today;
                            _m.DaftarMasalah = x.Model.DaftarMasalah;
                            _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                            _m.Target = x.Model.Target;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}