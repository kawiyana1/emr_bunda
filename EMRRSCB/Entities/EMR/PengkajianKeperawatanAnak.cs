//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class PengkajianKeperawatanAnak
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string SumberData { get; set; }
        public string SumberDataLainnya { get; set; }
        public string Rujukan { get; set; }
        public bool RujukanRS { get; set; }
        public string RujukanRSKet { get; set; }
        public bool RujukanPuskesmas { get; set; }
        public string RujukanPuskesmasKet { get; set; }
        public bool RujukanDR { get; set; }
        public string RujukanDRKet { get; set; }
        public bool RujukanLainnya { get; set; }
        public string RujukanLainnyaKet { get; set; }
        public string RujukanDX { get; set; }
        public bool RujukanDatangSendiri { get; set; }
        public bool RujukanDatangDiantar { get; set; }
        public string RujukanDatangDiantarKet { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TlpKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string SubyektifKeluhanUtama { get; set; }
        public string SubyektifPenyakitSekarang { get; set; }
        public string SubyektifDiagnosaMedis { get; set; }
        public string SubyektifPenyakitDahulu { get; set; }
        public string SubyektifMRS { get; set; }
        public string SubyektifRiwayatOperasi { get; set; }
        public string AnamnesaPenyakitKeluarga { get; set; }
        public string SubyektifRiwayatAlergi { get; set; }
        public string SubyektifRiwayatImunisasi { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriAsesment { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPencetus { get; set; }
        public string SkriningNyeriPencetusKet { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri1 { get; set; }
        public string SkalaFLACC_WajahSkor { get; set; }
        public string SkalaFLACC_KakiSkor { get; set; }
        public string SkalaFLACC_AktifitasSkor { get; set; }
        public string SkalaFLACC_MenangisSkor { get; set; }
        public string SkalaFLACC_Consolability { get; set; }
        public string SkalaFLACC_TotalSkor { get; set; }
        public string SkalaFLACC_Kriteria { get; set; }
        public bool KeadaanKesadaran_CM { get; set; }
        public bool KeadaanKesadaran_Apatis { get; set; }
        public bool KeadaanKesadaran_Somnolen { get; set; }
        public bool KeadaanKesadaran_Stupar { get; set; }
        public bool KeadaanKesadaran_Coma { get; set; }
        public string KeadaanGCS_E { get; set; }
        public string KeadaanGCS_V { get; set; }
        public string KeadaanGCS_M { get; set; }
        public string KeadaanSuhu { get; set; }
        public string KeadaanNadi { get; set; }
        public string KeadaanRR { get; set; }
        public string KeadaanTensi { get; set; }
        public string KeadaanSpO2 { get; set; }
        public bool KeadaanPada_Suhu { get; set; }
        public bool KeadaanPada_Nasal { get; set; }
        public bool KeadaanPada_NRB { get; set; }
        public bool KeadaanPada_Lainnya { get; set; }
        public string KeadaanPada_LainnyaKet { get; set; }
        public string PolaNafas_KesulitanNafas { get; set; }
        public string PolaNafas_O2 { get; set; }
        public bool PolaNafas_Nasal { get; set; }
        public bool PolaNafas_Sungkup { get; set; }
        public bool PolaNafas_Lainnya { get; set; }
        public string PolaNafas_LainnyaKet { get; set; }
        public string SkriningGiziJawab1 { get; set; }
        public string SkriningGiziSkor1 { get; set; }
        public string SkriningGiziJawab2 { get; set; }
        public string SkriningGiziSkor2 { get; set; }
        public string SkriningGiziJawab3 { get; set; }
        public string SkriningGiziSkor3 { get; set; }
        public string SkriningGiziJawab4 { get; set; }
        public string SkriningGiziSkor4 { get; set; }
        public string SkriningGiziTotalSkor { get; set; }
        public string SkriningGiziKriteriaGizi { get; set; }
        public bool DaftarPenyakit_AnorexiaNervosa { get; set; }
        public bool DaftarPenyakit_Dismaturitas { get; set; }
        public bool DaftarPenyakit_HatiKronik { get; set; }
        public bool DaftarPenyakit_Metabolik { get; set; }
        public bool DaftarPenyakit_LukaBakar { get; set; }
        public bool DaftarPenyakit_JantungKronik { get; set; }
        public bool DaftarPenyakit_GinjalKronik { get; set; }
        public bool DaftarPenyakit_Trauma { get; set; }
        public bool DaftarPenyakit_Displasia { get; set; }
        public bool DaftarPenyakit_InfeksiHIV { get; set; }
        public bool DaftarPenyakit_Prankreatitis { get; set; }
        public bool DaftarPenyakit_RetardasiMental { get; set; }
        public bool DaftarPenyakit_PenyakitCellac { get; set; }
        public bool DaftarPenyakit_Inflammatory { get; set; }
        public bool DaftarPenyakit_ShartBowel { get; set; }
        public bool DaftarPenyakit_CystioFlbrosis { get; set; }
        public bool DaftarPenyakit_Kangker { get; set; }
        public bool DaftarPenyakit_Otot { get; set; }
        public bool DaftarPenyakit_Lainnya { get; set; }
        public string DaftarPenyakit_LainnyaKet { get; set; }
        public bool ProsedurInfus { get; set; }
        public string ProsedurInfusKet { get; set; }
        public Nullable<System.DateTime> ProsedurInfusTanggal { get; set; }
        public bool ProsedurDower { get; set; }
        public string ProsedurDowerKet { get; set; }
        public Nullable<System.DateTime> ProsedurDowerTanggal { get; set; }
        public bool ProsedurCrystosmotomy { get; set; }
        public string ProsedurCrystosmotomyKet { get; set; }
        public Nullable<System.DateTime> ProsedurCrystosmotomyTanggal { get; set; }
        public bool ProsedurCentral { get; set; }
        public string ProsedurCentralKet { get; set; }
        public Nullable<System.DateTime> ProsedurCentralTanggal { get; set; }
        public bool ProsedurSelang { get; set; }
        public string ProsedurSelangKet { get; set; }
        public Nullable<System.DateTime> ProsedurSelangTanggal { get; set; }
        public bool ProsedurTracheostomy { get; set; }
        public string ProsedurTracheostomyKet { get; set; }
        public Nullable<System.DateTime> ProsedurTracheostomyTanggal { get; set; }
        public bool ProsedurLainnya { get; set; }
        public string ProsedurLainnyaKet { get; set; }
        public string ProsedurLainnyaDipasang { get; set; }
        public Nullable<System.DateTime> ProsedurLainnyaTanggal { get; set; }
        public string AsesmenFungsional1Skor { get; set; }
        public string AsesmenFungsional1Keterangan { get; set; }
        public string AsesmenFungsional1NilaiSkor { get; set; }
        public string AsesmenFungsional2Skor { get; set; }
        public string AsesmenFungsional2Keterangan { get; set; }
        public string AsesmenFungsional2NilaiSkor { get; set; }
        public string AsesmenFungsional3Skor { get; set; }
        public string AsesmenFungsional3Keterangan { get; set; }
        public string AsesmenFungsional3NilaiSkor { get; set; }
        public string AsesmenFungsional4Skor { get; set; }
        public string AsesmenFungsional4Keterangan { get; set; }
        public string AsesmenFungsional4NilaiSkor { get; set; }
        public string AsesmenFungsional5Skor { get; set; }
        public string AsesmenFungsional5Keterangan { get; set; }
        public string AsesmenFungsional5NilaiSkor { get; set; }
        public string AsesmenFungsional6Skor { get; set; }
        public string AsesmenFungsional6Keterangan { get; set; }
        public string AsesmenFungsional6NilaiSkor { get; set; }
        public string AsesmenFungsional7Skor { get; set; }
        public string AsesmenFungsional7Keterangan { get; set; }
        public string AsesmenFungsional7NilaiSkor { get; set; }
        public string AsesmenFungsional8Skor { get; set; }
        public string AsesmenFungsional8Keterangan { get; set; }
        public string AsesmenFungsional8NilaiSkor { get; set; }
        public string AsesmenFungsional9Skor { get; set; }
        public string AsesmenFungsional9Keterangan { get; set; }
        public string AsesmenFungsional9NilaiSkor { get; set; }
        public string AsesmenFungsional10Skor { get; set; }
        public string AsesmenFungsional10Keterangan { get; set; }
        public string AsesmenFungsional10NilaiSkor { get; set; }
        public string AsesmenFungsionalTotalSkor { get; set; }
        public string AsesmenFungsionalKreteriaStatus { get; set; }
        public string AsesmenResikoJatuh_UmurSkor { get; set; }
        public string AsesmenResikoJatuh_JenisKelaminSkor { get; set; }
        public string AsesmenResikoJatuh_DiagnoseSkor { get; set; }
        public string AsesmenResikoJatuh_GagguanKognitif { get; set; }
        public string AsesmenResikoJatuh_FaktorLingkungan { get; set; }
        public string AsesmenResikoJatuh_Respon { get; set; }
        public string AsesmenResikoJatuh_PenggunaanObat { get; set; }
        public string AsesmenResikoJatuh_TotalSkor { get; set; }
        public string AsesmenResikoJatuh_Kriteria { get; set; }
        public bool Nosokomial_Phlebitis { get; set; }
        public bool Nosokomial_ISK { get; set; }
        public bool Nosokomial_Pnemonia { get; set; }
        public bool Nosokomial_ILO { get; set; }
        public bool Nosokomial_Sepsis { get; set; }
        public bool Nosokomial_Dekubitus { get; set; }
        public bool Status_TidakDiketahui { get; set; }
        public bool Status_Suspect { get; set; }
        public bool Status_Siketahui { get; set; }
        public bool Status_MRSA { get; set; }
        public bool Status_VRE { get; set; }
        public bool Status_TB { get; set; }
        public bool Status_Ifeksi { get; set; }
        public bool Status_Lainnya { get; set; }
        public string Status_LainnyaKet { get; set; }
        public bool Addition_Droplet { get; set; }
        public bool Addition_Airborn { get; set; }
        public bool Addition_Contact { get; set; }
        public bool Addition_Skin { get; set; }
        public bool Addition_ContactMulti { get; set; }
        public string IntergritasKulit { get; set; }
        public bool Masalah_Rash { get; set; }
        public bool Masalah_Lesi { get; set; }
        public bool Masalah_Perut { get; set; }
        public bool Masalah_Memar { get; set; }
        public bool Masalah_Pucat { get; set; }
        public bool Masalah_Kuning { get; set; }
        public bool Masalah_Sianotik { get; set; }
        public bool Masalah_Berkeringant { get; set; }
        public string ResikoDekubitas { get; set; }
        public string Luka { get; set; }
        public string Luka_Lokasi { get; set; }
        public string Luka_Ket { get; set; }
        public string PolaTidur { get; set; }
        public string PolaTidurJam { get; set; }
        public string Kuantitas { get; set; }
        public string BioPsikososialAyahIbu { get; set; }
        public string BioPsikososialStatusPernikahan { get; set; }
        public string BioPsikososialAnak { get; set; }
        public string BioPsikososialAnakJumlahAnak { get; set; }
        public string BioPsikososialPekerjaan { get; set; }
        public string BioPsikososialTinggalBersama { get; set; }
        public string BioPsikososialTinggalBersamaLainnyaNama { get; set; }
        public string BioPsikososialTinggalBersamaLainnyaTelp { get; set; }
        public string BioPsikososialWarganegara { get; set; }
        public string BioPsikososialWarganegaraSuku { get; set; }
        public string BioPsikososialAgama { get; set; }
        public string BioPsikososialPercayaKeyakinan { get; set; }
        public string BioPsikososialKebiasaan { get; set; }
        public string BioPsikososialKebiasaanLainnya { get; set; }
        public string BioPsikososialResikoMencederai { get; set; }
        public bool KebutuhanPrivasiIdentitas { get; set; }
        public bool KebutuhanPrivasiRekamMedis { get; set; }
        public bool KebutuhanPrivasiSaatPemeriksaan { get; set; }
        public bool KebutuhanPrivasiSaatTindakanMedis { get; set; }
        public bool KebutuhanPrivasiTransportasi { get; set; }
        public string KebutuhanKomunikasiBicara { get; set; }
        public string KebutuhanKomunikasiBicaraKapan { get; set; }
        public bool KebutuhanKomunikasiBahasaIndonesia { get; set; }
        public string KebutuhanKomunikasiBahasaIndonesiaKet { get; set; }
        public bool KebutuhanKomunikasiBahasaInggris { get; set; }
        public string KebutuhanKomunikasiBahasaInggrisKet { get; set; }
        public bool KebutuhanKomunikasiBahasaDaerah { get; set; }
        public string KebutuhanKomunikasiBahasaDaerahKet { get; set; }
        public bool KebutuhanKomunikasiBahasaLainnya { get; set; }
        public string KebutuhanKomunikasiBahasaLainnyaKet { get; set; }
        public string KebutuhanKomunikasiPenerjemah { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasa { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasaIsyarat { get; set; }
        public bool KebutuhanKomunikasiHambatanBahasa { get; set; }
        public bool KebutuhanKomunikasiHambatanCemas { get; set; }
        public bool KebutuhanKomunikasiHambatanKognitif { get; set; }
        public bool KebutuhanKomunikasiHambatanEmosi { get; set; }
        public bool KebutuhanKomunikasiHambatanMenulis { get; set; }
        public bool KebutuhanKomunikasiHambatanPendengaran { get; set; }
        public bool KebutuhanKomunikasiHambatanKesulitanBicara { get; set; }
        public bool KebutuhanKomunikasiHambatanAudioVisual { get; set; }
        public bool KebutuhanKomunikasiHambatanHilangMemori { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakAdaPartisipasi { get; set; }
        public bool KebutuhanKomunikasiHambatanDiskusi { get; set; }
        public bool KebutuhanKomunikasiHambatanMotivasiBuruk { get; set; }
        public bool KebutuhanKomunikasiHambatanSecaraFisiologi { get; set; }
        public bool KebutuhanKomunikasiHambatanMembaca { get; set; }
        public bool KebutuhanKomunikasiHambatanMasalahPenglihatan { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakDitemukanHambatan { get; set; }
        public bool KebutuhanKomunikasiHambatanMendengar { get; set; }
        public bool KebutuhanKomunikasiHambatanDemontrasi { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhan { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhanLainnya { get; set; }
        public string Username { get; set; }
        public string SectionID { get; set; }
        public string SpO2 { get; set; }
        public bool SuhuRuangan { get; set; }
        public bool NasalCenule { get; set; }
        public bool NRB { get; set; }
        public bool Lainnya { get; set; }
        public string KesulitanBernafas { get; set; }
        public bool Infusintravena { get; set; }
        public string Infusintravena_Dipasang { get; set; }
        public Nullable<System.DateTime> Infusintravena_Tanggal { get; set; }
        public string CentralLine { get; set; }
        public string CentralLine_Dipasang { get; set; }
        public Nullable<System.DateTime> CentralLine_Tanggal { get; set; }
        public bool DowerChateter { get; set; }
        public string DowerChateter_Dipasang { get; set; }
        public Nullable<System.DateTime> DowerChateter_Tanggal { get; set; }
        public bool SelangNGT { get; set; }
        public string SelangNGT_Dipasang { get; set; }
        public Nullable<System.DateTime> SelangNGT_Tanggal { get; set; }
        public bool CystostomyChat { get; set; }
        public string CystostomyChat_Dipasang { get; set; }
        public Nullable<System.DateTime> CystostomyChat_Tanggal { get; set; }
        public bool Tracheostomy { get; set; }
        public string Tracheostomy_Dipasang { get; set; }
        public Nullable<System.DateTime> Tracheostomy_Tanggal { get; set; }
        public bool LainLainya { get; set; }
        public string LainLainya_Dipasang { get; set; }
        public Nullable<System.DateTime> LainLainya_Tanggal { get; set; }
        public bool Dipasangdi { get; set; }
        public string Dipasangdi_Dipasang { get; set; }
        public Nullable<System.DateTime> Dipasangdi_Tanggal { get; set; }
        public bool Phiebities { get; set; }
        public bool ISK { get; set; }
        public bool Pnemonia { get; set; }
        public bool ILO { get; set; }
        public bool Sepsis { get; set; }
        public bool Dekubitus { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string Perawat { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaPenyakitDahulu { get; set; }
        public string AnamnesaDiagnosaMedis { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string RiwayatImunisasi { get; set; }
        public string TandaVitalKeadaanUmum { get; set; }
        public string PemeriksaanFisikSuhu { get; set; }
        public string PemeriksaanFisikNadi { get; set; }
        public string TandaVitalRR { get; set; }
        public string TandaVitalTensi { get; set; }
        public bool KebutuhanKomunikasiTidakDitemukanHambatan { get; set; }
        public string KebutuhanKomunikasiTingkatkanPendidikan { get; set; }
        public byte[] TTDPerawat { get; set; }
        public string KebutuhanKomunikasiORTU { get; set; }
        public string KebutuhanKomunikasiORTUKet { get; set; }
        public Nullable<bool> VerifikasiPerawat { get; set; }
        public string ResikoJatuh1Skor { get; set; }
        public string ResikoJatuh2Skor { get; set; }
        public string ResikoJatuh3Skor { get; set; }
        public string ResikoJatuh4Skor { get; set; }
        public string ResikoJatuh5Skor { get; set; }
        public string ResikoJatuh6Skor { get; set; }
        public string ResikoJatuh7Skor { get; set; }
    }
}
