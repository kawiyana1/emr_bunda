﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace EMRRSCB.Models
{
    public class EMRSuratPernyataanPulangViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string YangMembuat { get; set; }
        public string Saksi { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDYangMembuat { get; set; }
        public string TTDDokter { get; set; }
        public string TTDSaksi { get; set; }
        public bool VerifikasiDokter { get; set; }
        public bool VerfikasiPerawatJaga { get; set; }

        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string NamaSaya { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public int MODEVIEW { get; set; }

    }
}