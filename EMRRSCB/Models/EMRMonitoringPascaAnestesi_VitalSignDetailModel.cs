﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringPascaAnestesi_VitalSignDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string VitalSign { get; set; }
        public string TVS { get; set; }
        public string R { get; set; }
        public string N { get; set; }
        public string TD { get; set; }
        public string Waktu { get; set; }
        public string SPO2 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Username { get; set; }
    }
}