﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class PengaturanDPJPViewModel
    {
        public long Id { get; set; }
        public string Noreg { get; set; }
        public ListDetail<PengaturanDPJPDetailModel> Detail_List { get; set; }
    }
}