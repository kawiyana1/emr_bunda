﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanObservasi_DetailController : Controller
    {
        // GET: EMRCatatanObservasi_Detail
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanObservasi_Detail.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRObservasi_List = new ListDetail<EMRCatatanObservasi_DetailViewModel>();
                        var detail_1 = s.CatatanObservasi_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanObservasi_DetailViewModel>(x);

                            model.EMRObservasi_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanObservasi_Detail.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Observasi";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Observasi";
                    }

                    if (item.EMRObservasi_List == null) item.EMRObservasi_List = new ListDetail<EMRCatatanObservasi_DetailViewModel>();
                    item.EMRObservasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRObservasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRObservasi_List;
                    var real_list = s.CatatanObservasi_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanObservasi_Detail.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.CatatanObservasi_Detail.Add(IConverter.Cast<CatatanObservasi_Detail>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.JamManual = x.Model.JamManual;
                            _m.Menit = x.Model.Menit;
                            _m.TekananDarah = x.Model.TekananDarah;
                            _m.FrekNapas = x.Model.FrekNapas;
                            _m.FrekNadi = x.Model.FrekNadi;
                            _m.Suhu = x.Model.Suhu;
                            _m.GCS_E = x.Model.GCS_E;
                            _m.GCS_M = x.Model.GCS_M;
                            _m.GCS_V = x.Model.GCS_V;
                            _m.Cairan = x.Model.Cairan;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}