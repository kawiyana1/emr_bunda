﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRResumePasienPulangViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DirawatSejak { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DirawatSampai { get; set; }
        public string DiagnosaMedikPulang { get; set; }
        public string StatusPulang { get; set; }
        public string Suhu { get; set; }
        public string Nadi { get; set; }
        public string Tensi { get; set; }
        public string Pernafasan { get; set; }
        public string BB { get; set; }
        public string Kesadaran { get; set; }
        public string PasienDirujukKe { get; set; }
        public string PasienDirujukKet { get; set; }
        public bool AlatBantuTidakAda { get; set; }
        public bool AlatBantuInfus { get; set; }
        public bool AlatBantuKateter { get; set; }
        public bool AlatBantuNGT { get; set; }
        public bool AlatBantuOksigen { get; set; }
        public bool AlatBantuLain { get; set; }
        public string AlatBantuLainKet { get; set; }
        public string ObligasiSaatPulang { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string TindakanKeperawatan { get; set; }
        public string MasalahKeperawatanTidakPerlu { get; set; }
        public bool CaraPemberianMakan { get; set; }
        public bool CaraPerawatanLuka { get; set; }
        public bool CaraBatukEfektif { get; set; }
        public bool CaraPengaturanDiet { get; set; }
        public bool CaraPengeceran { get; set; }
        public bool MerawatBayi { get; set; }
        public bool PerawatanTaliPusat { get; set; }
        public bool CaraPemberianObat { get; set; }
        public bool CaraTeknikRelaksasi { get; set; }
        public bool CaraFisioterapi { get; set; }
        public bool CaraMenjemur { get; set; }
        public bool MemandikanBayi { get; set; }
        public bool MerawatTaliPusat { get; set; }
        public bool LainLain { get; set; }
        public string LainLainKet { get; set; }
        public string PasienMengatakanMengerti { get; set; }
        public string SetelahDiberikanPenyuluhan { get; set; }
        public string MencocokanGelangIbu { get; set; }
        public string NomorGelangIbu { get; set; }
        public string DisaksikanOrangTua { get; set; }
        public string ObatYangDibawaPulang { get; set; }
        public string HasilPemeriksaan { get; set; }
        public string SuratKontrol { get; set; }
        public string SuratRujukan { get; set; }
        public string SuratIstirahat { get; set; }
        public string KartuImunisasi { get; set; }
        public string SuratKeteranganLahir { get; set; }
        public string SuratAdministrasi { get; set; }
        public string PulangKeAlamat { get; set; }
        public string NamaPenjemput { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string KodePerawat { get; set; }
        public string KodePerawatNama { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDPasien { get; set; }
        public bool VerifikasiPrwt { get; set; }
        public string NamaTandaTangan { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}