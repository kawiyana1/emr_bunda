﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace EMRRSCB.Models
{
    public class EMRMonitorPelaksaanPencegahanRisikoJatuhViewModel
    {
        public string NoBukti { get; set; }
        public string CaraBejalanPasien { get; set; }
        public string TidakSeimbang { get; set; }
        public string JalanMenggunakanAlatBantu { get; set; }
        public string MenompangSaatAkanDududk { get; set; }
        public string Validasi { get; set; }

        public int MODEVIEW { get; set; }

        public ListDetail<EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel> EMRMonitor_List { get; set; }

    }
}