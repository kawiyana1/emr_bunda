﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class PengaturanDPJPDetailModel
    {
        public int Id { get; set; }
        public string Noreg { get; set; }

        [Required]
        public string DokterId { get; set; }
        public string NamaDokter { get; set; }
        public string Status { get; set; }
        public string StatusNama { get; set; }
    }
}