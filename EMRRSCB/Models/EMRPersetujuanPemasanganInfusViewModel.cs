﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPersetujuanPemasanganInfusViewModel
    {
        public string NoBukti { get; set; }
        public string DokterPenanggungJawab { get; set; }
        public string DokterPenanggungJawabNama { get; set; }
        public string NamaPenerimaInformasi { get; set; }
        public string NamaPemberiInformasi { get; set; }
        public string NamaPemberiInformasiNama { get; set; }
        public string HubunganDenganPasien { get; set; }
        public bool MemasangAlat { get; set; }
        public bool MemberikanCairan { get; set; }
        public bool MemberikanObat { get; set; }
        public string Tujuan_Ket { get; set; }
        public bool MencuciTangan { get; set; }
        public bool MempersiapkanAlatInfus { get; set; }
        public bool MenentukanLokasi { get; set; }
        public bool MemasangHandscoun { get; set; }
        public bool MembersihkanKulit { get; set; }
        public bool MenusukPembuluhDarah { get; set; }
        public bool MengalirkanCairanInfus { get; set; }
        public bool MelakukanFiksasasi { get; set; }
        public bool MengaturTetesanInfus { get; set; }
        public bool PendarahanDiDaerahPemasangan { get; set; }
        public bool Infeksi { get; set; }
        public string LainLain { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TelahMenerangkan { get; set; }
        public string TelahMenerangkanNama { get; set; }
        public string TelahMenerima { get; set; }
        public string TTDTelahMenerima { get; set; }
        public bool VerifikasiTelahMenerangkan { get; set; }
        public string Username { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}