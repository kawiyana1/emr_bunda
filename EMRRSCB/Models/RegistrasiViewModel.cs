﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class RegistrasiViewModel
    {
        public string NoReg { get; set; }
        public short NoAntri { get; set; }
        public string NoBed { get; set; }
        public string Kamar { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public string CustomerKerjasamaIDNama { get; set; }
        public string NoKartu { get; set; }
        [Required]
        public string NRM { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public string Nama_Supplier { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public System.DateTime TglReg { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string TglReg_View { get; set; }
        public string Tanggal_View { get; set; }
        public string TglLahir { get; set; }
        public string TipePasien { get; set; }
        public string Alergi { get; set; }
        public string Jam_View { get; set; }
        public System.DateTime JamReg { get; set; }
        public string SectionID { get; set; }
        public string Alamat { get; set; }
        public bool SudahInputEMR { get; set; }
        public string Phone { get; set; }
        [Required]
        public string DokterID { get; set; }
        public string DokterIDNama { get; set; }
        public string DokterRawatID { get; set; }
        public bool PasienBaru { get; set; }
        public bool PasienKTP { get; set; }
        public bool PenanggungIsPasien { get; set; }
        public string PenanggungNRM { get; set; }
        [Required]
        public string PenanggungNama { get; set; }
        [Required]
        public string PenanggungAlamat { get; set; }
        [Required]
        public string PenanggungHubungan { get; set; }
        [Required]
        public string PenanggungTelp { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public bool Batal { get; set; }
        public string StatusBayar { get; set; }
        public string SudahPeriksa { get; set; }
        public string CPPT_OGambar { get; set; }
        public string TandaTangan { get; set; }
        public string TTDDPJP { get; set; }
        public string TTDCPPT { get; set; }

        public string MenyerahkanPasien { get; set; }
        public string MenerimaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamBacaUlang { get; set; }
        public string PetugasBacaUlang { get; set; }
        public string PetugasBacaUlangNama { get; set; } 

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }

        [DataType(DataType.Time)]

        public Nullable<System.DateTime> JamKonfirmasi { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public string PetugasKonfirmasiNama { get; set; }
        public string PetugasBacaUlangTTD { get; set; }
        public string PetugasKonfirmasiTTD { get; set; }
        public string MenerimaPasienTTD { get; set; }
        public string MenyerahkanPasienTTD { get; set; }
        public Nullable<bool> VerifikasiPetugasCPPT { get; set; }
        public Nullable<bool> VerifikasiPetugasBacaUlangCPPT { get; set; }
        public Nullable<bool> VerifikasiPetugasKonfirmasiCPPT { get; set; }
        public Nullable<bool> VerifikasiMenyerahkanPasienCPPT { get; set; }
        public Nullable<bool> VerifikasiMenerimaPasienCPPT { get; set; }


        public string Template { get; set; }
    }
}