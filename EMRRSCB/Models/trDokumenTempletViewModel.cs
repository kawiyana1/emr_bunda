﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class trDokumenTempletViewModel
    {
        public string NoBukti { get; set; }
        public string NamaTemplate { get; set; }
        public string DokterID { get; set; }
        public string DokumenID { get; set; }
        public string SectionID { get; set; }
    }
}