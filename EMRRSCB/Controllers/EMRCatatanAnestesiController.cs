﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanAnestesiController : Controller
    {
        // GET: EMRCatatanAnestesi
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRCatatanAnestesiViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanAnestesi.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRCatatanAnestesiViewModel>(dokumen);

                        model.NoBukti = id;

                        var spesialisanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.SpesialisAnestesi);
                        if (spesialisanestesi != null) model.SpesialisAnestesiNama = spesialisanestesi.NamaDOkter;

                        var spesialisbedah = sim.mDokter.FirstOrDefault(e => e.DokterID == model.SpesialisBedah);
                        if (spesialisbedah != null) model.SpesialisBedahNama = spesialisbedah.NamaDOkter;

                        var penataanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PenataAnestesi);
                        if (penataanestesi != null) model.PenataAnestesiNama = penataanestesi.NamaDOkter;

                        var penyusun = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Penyusun);
                        if (penyusun != null) model.PenyusunNama = penyusun.NamaDOkter;

                        var dktanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi);
                        if (dktanestesi != null) model.DokterAnestesiNama = dktanestesi.NamaDOkter;

                        var dktanestesiintra = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi_MonitoringIntra);
                        if (dktanestesiintra != null) model.DokterAnestesi_MonitoringIntraNama = dktanestesiintra.NamaDOkter;

                        var dktanestesipascaopr = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi_PascaOperasi);
                        if (dktanestesipascaopr != null) model.DokterAnestesi_PascaOperasiNama = dktanestesipascaopr.NamaDOkter;

                        var dktanestesipetugas = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi_Petugas);
                        if (dktanestesipetugas != null) model.DokterAnestesi_PetugasNama = dktanestesipetugas.NamaDOkter;

                        #region === Detail Rencana Sedasi
                        model.RencanaSedasi_List = new ListDetail<EMRRencanaSedasi_DetailDetailModel>();
                        var detail_1 = s.RencanaSedasi_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRRencanaSedasi_DetailDetailModel>(x);
                            var obatdetail = sim.mBarang.Where(e => e.Kode_Barang == y.ObatPremedikasi).FirstOrDefault();
                            if (obatdetail != null)
                            {
                                y.ObatPremedikasiNama = obatdetail.Nama_Barang;
                            }
                            model.RencanaSedasi_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Obat Premedikasi
                        model.AssesmentPraIndukasi_List = new ListDetail<EMRAsesmentPraIndukasi_DetailDetailModel>();
                        var detail_2 = s.AsesmentPraIndukasi_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRAsesmentPraIndukasi_DetailDetailModel>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Pelaksana);
                            if (dokterdetail != null)
                            {
                                y.PelaksanaNama = dokterdetail.NamaDOkter;
                            }

                            var obatdetail = sim.mBarang.Where(e => e.Kode_Barang == y.ObatPremedikasi).FirstOrDefault();
                            if (obatdetail != null)
                            {
                                y.ObatPremedikasiNama = obatdetail.Nama_Barang;
                            }
                            model.AssesmentPraIndukasi_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Obat Monitoring Intra Anestesi
                        model.ObatIntra_List = new ListDetail<EMRMonitoringIntraAnestesi_VitalSignDetailModel>();
                        var detail_3 = s.MonitoringIntraAnestesi_VitalSign.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRMonitoringIntraAnestesi_VitalSignDetailModel>(x);
                            var obatdetail = sim.mBarang.Where(e => e.Kode_Barang == y.Obat).FirstOrDefault();
                            if (obatdetail != null)
                            {
                                y.ObatNama = obatdetail.Nama_Barang;
                            }
                            model.ObatIntra_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Vital Sign Monitoring Intra Anestesi
                        model.VitalSignIntra_List = new ListDetail<EMRMonitoringIntraAnestesi_ObatDetailModel>();
                        var detail_4 = s.MonitoringIntraAnestesi_Obat.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRMonitoringIntraAnestesi_ObatDetailModel>(x);
                            model.VitalSignIntra_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Vital Sign Monitoring Pasca Anestesi
                        model.VitalSignPascaAnests_List = new ListDetail<EMRMonitoringPascaAnestesi_VitalSignDetailModel>();
                        var detail_5 = s.MonitoringPascaAnestesi_VitalSign.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_5)
                        {
                            var y = IConverter.Cast<EMRMonitoringPascaAnestesi_VitalSignDetailModel>(x);
                            model.VitalSignPascaAnests_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRCatatanAnestesiViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanAnestesi.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<CatatanAnestesi>(item);
                        s.CatatanAnestesi.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Anestesi";
                    }
                    else
                    {
                        model = IConverter.Cast<CatatanAnestesi>(item);
                        s.CatatanAnestesi.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Anestesi";
                    }
                    #region === Detail Keperawatan
                    if (item.RencanaSedasi_List == null) item.RencanaSedasi_List = new ListDetail<EMRRencanaSedasi_DetailDetailModel>();
                    item.RencanaSedasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RencanaSedasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Tanggal = DateTime.Today;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.RencanaSedasi_List;
                    var real_list = s.RencanaSedasi_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RencanaSedasi_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RencanaSedasi_Detail.Add(IConverter.Cast<RencanaSedasi_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.ObatPremedikasi = x.Model.ObatPremedikasi;
                            _m.Dosis = x.Model.Dosis;
                            _m.Cara = x.Model.Cara;
                            _m.Username = x.Model.Username;
                        }
                    }
                    #endregion

                    #region === Detail Obar Premedikasi
                    if (item.AssesmentPraIndukasi_List == null) item.AssesmentPraIndukasi_List = new ListDetail<EMRAsesmentPraIndukasi_DetailDetailModel>();
                    item.AssesmentPraIndukasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.AssesmentPraIndukasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.AssesmentPraIndukasi_List;
                    var real_list1 = s.AsesmentPraIndukasi_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsesmentPraIndukasi_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AsesmentPraIndukasi_Detail.Add(IConverter.Cast<AsesmentPraIndukasi_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = DateTime.Today;
                            _m.ObatPremedikasi = x.Model.ObatPremedikasi;
                            _m.Dosis = x.Model.Dosis;
                            _m.Jam = x.Model.Jam;
                            _m.Pelaksana = x.Model.Pelaksana;
                        }
                    }
                    #endregion

                    #region === Detail Obat Intra Anestesi
                    if (item.ObatIntra_List == null) item.ObatIntra_List = new ListDetail<EMRMonitoringIntraAnestesi_VitalSignDetailModel>();
                    item.ObatIntra_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.ObatIntra_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        //x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list2 = item.ObatIntra_List;
                    var real_list2 = s.MonitoringIntraAnestesi_VitalSign.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.MonitoringIntraAnestesi_VitalSign.Remove(x);
                    }

                    foreach (var x in new_list2)
                    {
                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.MonitoringIntraAnestesi_VitalSign.Add(IConverter.Cast<MonitoringIntraAnestesi_VitalSign>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Obat = x.Model.Obat;
                            _m.Jam = x.Model.Jam;
                            _m.N2O = x.Model.N2O;
                            _m.Dosis = x.Model.Dosis;
                            _m.Gas = x.Model.Gas;
                            _m.Jumlah = x.Model.Jumlah;
                        }
                    }
                    #endregion

                    #region === Detail Vital Sign Intra Anestesi
                    if (item.VitalSignIntra_List == null) item.VitalSignIntra_List = new ListDetail<EMRMonitoringIntraAnestesi_ObatDetailModel>();
                    item.VitalSignIntra_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.VitalSignIntra_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list3 = item.VitalSignIntra_List;
                    var real_list3 = s.MonitoringIntraAnestesi_Obat.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list3)
                    {
                        var m = new_list3.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.MonitoringIntraAnestesi_Obat.Remove(x);
                    }

                    foreach (var x in new_list3)
                    {
                        var _m = real_list3.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.MonitoringIntraAnestesi_Obat.Add(IConverter.Cast<MonitoringIntraAnestesi_Obat>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.VitalSign = x.Model.VitalSign;
                            _m.Jam = x.Model.Jam;
                            _m.TVS = x.Model.TVS;
                            _m.RR = x.Model.RR;
                            _m.N = x.Model.N;
                            _m.TD = x.Model.TD;
                            _m.SPO2 = x.Model.SPO2;
                            _m.PECo2 = x.Model.PECo2;
                            _m.FiO2 = x.Model.FiO2;
                            _m.TekananNafas = x.Model.TekananNafas;
                            _m.CairanInfus = x.Model.CairanInfus;
                            _m.Darah = x.Model.Darah;
                            _m.Urin = x.Model.Urin;
                            _m.Pendarahan = x.Model.Pendarahan;
                        }
                    }
                    #endregion

                    #region === Detail Vital Sign Pasca Anestesi
                    if (item.VitalSignPascaAnests_List == null) item.VitalSignPascaAnests_List = new ListDetail<EMRMonitoringPascaAnestesi_VitalSignDetailModel>();
                    item.VitalSignPascaAnests_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.VitalSignPascaAnests_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list4 = item.VitalSignPascaAnests_List;
                    var real_list4 = s.MonitoringPascaAnestesi_VitalSign.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list4)
                    {
                        var m = new_list4.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.MonitoringPascaAnestesi_VitalSign.Remove(x);
                    }

                    foreach (var x in new_list4)
                    {
                        var _m = real_list4.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.MonitoringPascaAnestesi_VitalSign.Add(IConverter.Cast<MonitoringPascaAnestesi_VitalSign>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.VitalSign = x.Model.VitalSign;
                            _m.Jam = x.Model.Jam;
                            _m.Waktu = x.Model.Waktu;
                            _m.TVS = x.Model.TVS;
                            _m.R = x.Model.R;
                            _m.N = x.Model.N;
                            _m.TD = x.Model.TD;
                            _m.SPO2 = x.Model.SPO2;
                        }
                    }
                    #endregion
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}