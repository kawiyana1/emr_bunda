﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSCB.Controllers
{
    public class HistoryAssesmenController : Controller
    {

        public ActionResult Index(string nrm)
        {
            ViewBag.NRM = nrm;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var nrm = filter[0];
                    IQueryable<vw_DokumenAssesmenPasien> p = s.vw_DokumenAssesmenPasien.Where(x => x.NRM == nrm && x.Simpan == true && x.DokumenID != "CPPT" && x.Batal == false);
                    if (filter[24] != "True")
                    {
                        var fromdate = (filter[22] == "")? DateTime.Today.ToString() : filter[22];
                        var todate = (filter[23] == "")? DateTime.Today.ToString() : filter[23];
                        p = p.Where("TanggalCreate >= @0", DateTime.Parse(fromdate).AddDays(-1));
                        p = p.Where("TanggalCreate <= @0", DateTime.Parse(todate));
                    }
                    if (!string.IsNullOrEmpty(filter[1])) p = p.Where($"NoBukti.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) p = p.Where($"NamaDokumen.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) p = p.Where($"SectionName.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) p = p.Where($"CretaedBy.Contains(@0)", filter[4]);

                    var totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<DokumenAssesmenPasienViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                        m.TanggalCreate_View = x.TanggalCreate.Value.ToString("dd-MM-yyyy HH:mm");
                        m.TanggalUpdate_View = x.TanggalUpdate.Value.ToString("dd-MM-yyyy HH:mm");
                        datas.Add(m);
                    }
                    result.Data = datas;

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}