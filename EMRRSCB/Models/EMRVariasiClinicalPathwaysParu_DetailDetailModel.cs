﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRVariasiClinicalPathwaysParu_DetailDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string VariasiPelayanan { get; set; }
        public string Tanggal { get; set; }
        public string Alasan { get; set; }
        public string Username { get; set; }
    }
}