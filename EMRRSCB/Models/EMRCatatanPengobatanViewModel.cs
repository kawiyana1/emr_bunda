﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanPengobatanViewModel 
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Diagnosa { get; set; }
        public string Alergi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglMulaiPemberian { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglResep { get; set; }
        public string JamPemberianObat { get; set; }
        public string Username { get; set; }
        public string SectionID { get; set; }
        public string NamaObat { get; set; }
        public string KodeObat { get; set; }
        public string Dosis { get; set; }
        public string Indikasi { get; set; }
        public string Rute { get; set; }
        public string Frekuensi { get; set; }
        public string Catatan { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string TandaTangan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal1 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam11 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam12 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam13 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam14 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam15 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam16 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam17 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal2 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam21 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam22 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam23 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam24 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam25 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam26 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam27 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal3 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam31 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam32 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam33 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam34 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam35 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam36 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam37 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal4 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam41 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam42 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam43 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam44 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam45 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam46 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam47 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal5 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam51 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam52 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam53 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam54 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam55 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam56 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam57 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal6 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam61 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam62 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam63 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam64 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam65 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam66 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam67 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal7 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam71 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam72 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam73 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam74 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam75 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam76 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam77 { get; set; }
        public Nullable<bool> Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}