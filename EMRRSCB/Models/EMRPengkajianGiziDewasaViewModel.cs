﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianGiziDewasaViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggaal { get; set; }
        public string Antropometri { get; set; }
        public string TB { get; set; }
        public string BB { get; set; }
        public string IMT { get; set; }
        public string ULA { get; set; }
        public string iIMED { get; set; }
        public string TinggiLutut { get; set; }
        public bool Biokimia_Albumin { get; set; }
        public bool Biokimia_Hb { get; set; }
        public bool Biokimia_BUN { get; set; }
        public bool Biokimia_GDP100 { get; set; }
        public bool Biokimia_GDP2Jam { get; set; }
        public bool Biokimia_GDP200 { get; set; }
        public bool Biokimia_AsamUrat { get; set; }
        public bool Biokimia_Kreatinin { get; set; }
        public bool Biokimia_Kalium { get; set; }
        public bool Biokimia_Ureum { get; set; }
        public bool Biokimia_AsamUrat_L { get; set; }
        public bool Biokimia_AsamUrat_Pr { get; set; }
        public bool Biokimia_Kreatinin_L { get; set; }
        public bool Biokimia_Kreatinin_Pr { get; set; }
        public bool Biokimia_Kalium_Tinggi { get; set; }
        public bool Biokimia_Kalium_Rendah { get; set; }
        public bool Biokimia_Hamil { get; set; }
        public bool Biokimia_Ederma { get; set; }
        public bool Biokimia_SGOT { get; set; }
        public bool Biokimia_SPGT { get; set; }
        public bool Gangguan_Mual { get; set; }
        public bool Gangguan_Muntah { get; set; }
        public bool Gangguan_Diare { get; set; }
        public bool Gangguan_Sembelit { get; set; }
        public bool Gangguan_KesulitanMengunyah { get; set; }
        public bool Gangguan_Menelan { get; set; }
        public bool Diactary_a_DiRumah { get; set; }
        public bool Diactary_a_DiRumah_51 { get; set; }
        public bool Diactary_a_DiRumah_51_79 { get; set; }
        public bool Diactary_a_DiRumah_80 { get; set; }
        public bool Diactary_a_DirumahSakit { get; set; }
        public bool Diactary_a_DirumahSakit_51 { get; set; }
        public bool Diactary_a_DirumahSakit_51_59 { get; set; }
        public bool Diactary_a_DirumahSakit_80 { get; set; }
        public bool Diactary_a_AdaPerubahan { get; set; }
        public string Diactary_b_Alergi { get; set; }
        public string Diactary_b_Alergi_Jelaskan { get; set; }
        public string Diactary_c_Gizi { get; set; }
        public string DiagnoseMedis { get; set; }
        public bool DiagnoseGizi_KurusBerat { get; set; }
        public bool DiagnoseGizi_GemukBerat { get; set; }
        public bool DiagnoseGizi_Hipo { get; set; }
        public bool DiagnoseGizi_KurusRingan { get; set; }
        public bool DiagnoseGizi_GemukRingan { get; set; }
        public bool DiagnoseGizi_Normal { get; set; }
        public bool DiagnoseGizi_LainLain { get; set; }
        public string DiagnoseGizi_Lain_Ket { get; set; }
        public string Nutrisi_Energi { get; set; }
        public string Nutrisi_Protein { get; set; }
        public string Nutrisi_Lain { get; set; }
        public bool JenisDiet_Standar { get; set; }
        public bool JenisDiet_TKTP { get; set; }
        public bool JenisDiet_DM { get; set; }
        public string JenisDiet_DM_Ket { get; set; }
        public bool JenisDiet_Ginjal { get; set; }
        public string JenisDiet_Ginjal_Ket { get; set; }
        public bool JenisDiet_Jantung { get; set; }
        public string JenisDiet_Jantung_Ket { get; set; }
        public bool JenisDiet_Hatic { get; set; }
        public string JenisDiet_Hatic_Ket { get; set; }
        public bool JenisDiet_Lain { get; set; }
        public string JenisDiet_Lain_Ket { get; set; }
        public bool BentukMakan_Nasi { get; set; }
        public bool BentukMakan_Bubur { get; set; }
        public bool BentukMakan_BuburSaring { get; set; }
        public bool BentukMakan_Cair { get; set; }
        public bool CaraPemberian_Oral { get; set; }
        public bool CaraPemberian_Eternal { get; set; }
        public string Catatan { get; set; }
        public string AhliGizi { get; set; }
        public string AhliGizi_Nama { get; set; }
        public string TTDAhliGizi { get; set; }
        public int MODEVIEW { get; set; }
    }
}