﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianAsuhanKebidananVKController : Controller
    {
        // GET: EMRPengkajianAsuhanKebidananVK
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRPengkajianAsuhanKebidananVKViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianAsuhanKebidananVK.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAsuhanKebidananVKViewModel>(dokumen);

                        model.NoBukti = id;

                        var bidan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (bidan != null) model.BidanNama = bidan.NamaDOkter;

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_4 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.BioPsikososialIstirahatTidurJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.PemindahanRuanganMeninggalJam = DateTime.Now;
                        model.PemindahanRuanganMinggatKet = DateTime.Now;
                    }


                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemAsuhanRI>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemAsuhanRI()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianAsuhanKebidananVKViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianAsuhanKebidananVK.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAsuhanKebidananVKViewModel>(dokumen);

                        model.Tanggal = DateTime.Today;
                        model.NoBukti = id;
                        model.Jam = DateTime.Now;

                        var bidan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (bidan != null) model.BidanNama = bidan.NamaDOkter;

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_4 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.BioPsikososialIstirahatTidurJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.PemindahanRuanganMeninggalJam = DateTime.Now;
                        model.PemindahanRuanganMinggatKet = DateTime.Now;
                    }

                    model.MODEVIEW = 0;
                    model.VerifikasiBidan = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemAsuhanRI>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemAsuhanRI()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPengkajianAsuhanKebidananVKViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.PengkajianAsuhanKebidananVK.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianAsuhanKebidananVK>(item);
                        s.PengkajianAsuhanKebidananVK.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Assesmen PengkajianAsuhanKebidananRI";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianAsuhanKebidananVK>(item);
                        s.PengkajianAsuhanKebidananVK.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Update Assesmen PengkajianAsuhanKebidananRI";
                    }

                    #region === Detail Riwayat Kehamilan
                    if (item.RiwayatKehamilan_List == null) item.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                    item.RiwayatKehamilan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RiwayatKehamilan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list3 = item.RiwayatKehamilan_List;
                    var real_list3 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list3)
                    {
                        var m = new_list3.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Remove(x);
                    }

                    foreach (var x in new_list3)
                    {
                        var _m = real_list3.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Add(IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Abortus = x.Model.Abortus;
                            _m.Aterm = x.Model.Aterm;
                            _m.BBL = x.Model.BBL;
                            _m.Cacat = x.Model.Cacat;
                            _m.JenisKelaminLaki = x.Model.JenisKelaminLaki;
                            _m.JenisKelaminPerempuan = x.Model.JenisKelaminPerempuan;
                            _m.JenisPartus = x.Model.JenisPartus;
                            _m.Komplikasi = x.Model.Komplikasi;
                            _m.Meninggal = x.Model.Meninggal;
                            _m.Nakes = x.Model.Nakes;
                            _m.Normal = x.Model.Normal;
                            _m.Non = x.Model.Non;
                            _m.Prematur = x.Model.Prematur;
                            _m.TglPartus = x.Model.TglPartus;
                            _m.BBLText = x.Model.BBLText;
                        }
                    }
                    #endregion

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.Bidan;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}