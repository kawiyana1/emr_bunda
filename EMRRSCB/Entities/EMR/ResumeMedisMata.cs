//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class ResumeMedisMata
    {
        public string NoBukti { get; set; }
        public string Anamnesa { get; set; }
        public string PemeriksaanFisik { get; set; }
        public string Diagnosis { get; set; }
        public string Therapy { get; set; }
        public Nullable<bool> Visus { get; set; }
        public Nullable<bool> SlitLamp { get; set; }
        public Nullable<bool> ResepKacaMata { get; set; }
        public Nullable<bool> RawatLuka { get; set; }
        public Nullable<bool> Founduscopy { get; set; }
        public string PemeriksaanLain { get; set; }
        public string Dokter { get; set; }
        public byte[] TTDDokter { get; set; }
    }
}
