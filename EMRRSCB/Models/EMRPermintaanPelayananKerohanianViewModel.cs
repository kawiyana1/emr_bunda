﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPermintaanPelayananKerohanianViewModel
    {

        public string NoBukti { get; set; }
        public string Agama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> PermintaanTanggal { get; set; }

        [DataType(DataType.Time)]

        public Nullable<System.DateTime> PermintaanJam { get; set; }

        public string NamaPetugasKerohanian { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> PermintaanTanggalKerohanian { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PermintaanJamKerohanian { get; set; }
        public string NoTelpKerohanian { get; set; }
        public string TTDPerawatNama { get; set; }
        public string TTTDPetugasKerohanianNama { get; set; }

        public string TTDPasienNama { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDPetugas { get; set; }
        public string TTDPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglSurat { get; set; }

        public string NamaPasien { get; set; }
        public string TglLahir { get; set; }
        public string NRM { get; set; }

        public string JenisKelamin { get; set; }
        
      

        public int MODEVIEW { get; set; }
    }
}