﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPersetujuanPenolakanTransfusiDarahViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string KTP { get; set; }
        public string Setuju { get; set; }
        public string Terhadap { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string TujuanPemberianDarahKet { get; set; }
        public string TTDPasien { get; set; }
        public bool VerifikasiPasien { get; set; }
        public bool TujuanPemberianDarah { get; set; }
        public bool ManfaatPemberianDarah { get; set; }
        public bool PerkiraanJumlah { get; set; }
        public bool ResikoKomplikasi { get; set; }
        public bool AlternatifLain { get; set; }
        public bool PerkiraanBiaya { get; set; }
        public bool TindakanTambahan { get; set; }
        public bool TindakanIniKemungkinan { get; set; }

        public string NamaPasien { get; set; }
        public string NRMPasien { get; set; }
        public string TanggalLahirPasien { get; set; }
        public string JKPasien { get; set; }
        public string TglLahirPasien { get; set; }
        public string AlamatPasien { get; set; }
        public string DirawatPasien { get; set; }
        
        public int MODEVIEW { get; set; }
      
    }
}