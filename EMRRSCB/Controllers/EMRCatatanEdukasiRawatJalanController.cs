﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanEdukasiRawatJalanController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanEdukasiRawatJalan.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMREdukasi_List = new ListDetail<EMRCatatanEdukasiRawatJalanViewModel>();
                        var detail_1 = s.CatatanEdukasiRawatJalan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanEdukasiRawatJalanViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Edukator);
                            if (dokter != null)
                            {
                                y.EdukatorNama = dokter.NamaDOkter;
                            }
                            model.EMREdukasi_List.Add(false, y);

                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanEdukasiRawatJalan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Edukasi Rawat Jalan";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Edukasi Rawat Jalan ";
                    }

                    if (item.EMREdukasi_List == null) item.EMREdukasi_List = new ListDetail<EMRCatatanEdukasiRawatJalanViewModel>();
                    item.EMREdukasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMREdukasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.EMREdukasi_List;
                    var real_list = s.CatatanEdukasiRawatJalan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanEdukasiRawatJalan.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.CatatanEdukasiRawatJalan.Add(IConverter.Cast<CatatanEdukasiRawatJalan>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.EdukasiDiberikan = x.Model.EdukasiDiberikan;
                            _m.SiapaEdukasi = x.Model.SiapaEdukasi;
                            _m.Tempat = x.Model.Tempat;
                            _m.MetodeEdukasi = x.Model.MetodeEdukasi;
                            _m.Respon = x.Model.Respon;
                            _m.Edukator = x.Model.Edukator;
                            _m.BidangDisiplin = x.Model.BidangDisiplin;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportEdukasiTerintegrasi(string nobukti)
        {
            //sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanEdukasiRawatJalan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"Rpt_CatatanEdukasiRawatJalan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanEdukasiRawatJalan;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("ImplementasiRisikoJatuhAnak", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["ImplementasiRisikoJatuhAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}


