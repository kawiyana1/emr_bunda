﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPermohonanRawatInapViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string IRD { get; set; }
        public string IRD_Nama { get; set; }
        public string Diagnosa { get; set; }
        public string Dirawat { get; set; }
        public string Dirawat_Nama { get; set; }
        public string Dokter { get; set; }
        public string Dokter_Nama { get; set; }
        public string TTDDokter { get; set; }
        public bool Verifikasi { get; set; }
        public string IRDKet { get; set; }


        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Umur { get; set; }
        public string Alamat { get; set; }
        public string NRM { get; set; }
        public int MODEVIEW { get; set; }
    }
}