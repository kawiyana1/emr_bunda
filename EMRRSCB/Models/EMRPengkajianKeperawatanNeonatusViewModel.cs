﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianKeperawatanNeonatusViewModel
    {
        public string NoBukti { get; set; } 

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Rujukan { get; set; }
        public bool RujukanRS { get; set; }
        public string RujukanRSKet { get; set; }
        public bool RujukanPuskesmas { get; set; }
        public string RujukanPuskesmasKet { get; set; }
        public bool RujukanDR { get; set; }
        public string RujukanDRKet { get; set; }
        public bool RujukanLainnya { get; set; }
        public string RujukanLainnyaKet { get; set; }
        public string RujukanDX { get; set; }
        public bool RujukanDatangSendiri { get; set; }
        public bool RujukanDatangDiantar { get; set; }
        public string RujukanDatangDiantarKet { get; set; }
        public string SumberData { get; set; }
        public string SumberDataLainnya { get; set; }
        public string AnamnesaKeluhanUtama { get; set; }
        public string AnamnesaPenyakitSekarang { get; set; }
        public string AnamnesaRiwayatPrenatalAnakKe { get; set; }
        public string AnamnesaRiwayatPrenatalUmurIbu { get; set; }
        public string AnamnesaRiwayatPrenatalUmurKehamilan { get; set; }
        public bool AnamnesaRiwayatPrenatalDM { get; set; }
        public bool AnamnesaRiwayatPrenatalB24 { get; set; }
        public bool AnamnesaRiwayatPrenatalHT { get; set; }
        public bool AnamnesaRiwayatPrenatalJantung { get; set; }
        public bool AnamnesaRiwayatPrenatalTB { get; set; }
        public bool AnamnesaRiwayatPrenatalHepB { get; set; }
        public bool AnamnesaRiwayatPrenatalAsma { get; set; }
        public bool AnamnesaRiwayatPrenatalPMS { get; set; }
        public bool AnamnesaRiwayatPrenatalAnemia { get; set; }
        public bool AnamnesaRiwayatPrenatalLainnya { get; set; }
        public string AnamnesaRiwayatPrenatalLainnyaKet { get; set; }
        public string AnamnesaRiwayatPrenatalRiwayatPenyakitIbu { get; set; }
        public string AnamnesaRiwayatPrenatalPengobatanIbu { get; set; }
        public string AnamnesaRiwayatPrenatalDiagnosaIbu { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> AnamnesaRiwayatInternatalTgl { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> AnamnesaRiwayatInternatalTime { get; set; }
        public string AnamnesaRiwayatInternatalPKT { get; set; }
        public string AnamnesaRiwayatInternatalAS { get; set; }
        public string AnamnesaRiwayatInternatalCaraPersalinan { get; set; }
        public string AnamnesaRiwayatInternatalCaraPersalinanKet { get; set; }
        public string AnamnesaRiwayatInternatalLahir { get; set; }
        public string AnamnesaRiwayatInternatalKetubanPecah { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> AnamnesaRiwayatInternatalKetubanPecahJam { get; set; }
        public bool AnamnesaRiwayatInternatalJernih { get; set; }
        public bool AnamnesaRiwayatInternatalHijau { get; set; }
        public bool AnamnesaRiwayatInternatalMerah { get; set; }
        public bool AnamnesaRiwayatInternatalKeruh { get; set; }
        public bool AnamnesaRiwayatInternatalKental { get; set; }
        public bool AnamnesaRiwayatInternatalMekonium { get; set; }
        public bool AnamnesaRiwayatInternatalSegar { get; set; }
        public bool AnamnesaRiwayatInternatalLayu { get; set; }
        public bool AnamnesaRiwayatInternatalSimpul { get; set; }
        public bool AnamnesaRiwayatInternatalKomplit { get; set; }
        public bool AnamnesaRiwayatInternatalKlasifikasi { get; set; }
        public bool AnamnesaRiwayatInternatalKelaianan { get; set; }
        public string AnamnesaRiwayatInternatalKelainanKet { get; set; }
        public bool AnamnesaDataBiologisASI { get; set; }
        public bool AnamnesaDataBiologisSusuFormula { get; set; }
        public bool AnamnesaDataBiologisLainnya { get; set; }
        public string AnamnesaDataBiologisLainnyaKet { get; set; }
        public bool AnamnesaDataBiologisFrekuensi { get; set; }
        public bool AnamnesaDataBiologisBAK { get; set; }
        public string AnamnesaDataBiologisBAKKet { get; set; }
        public bool AnamnesaDataBiologisBAB { get; set; }
        public string AnamnesaDataBiologisBABKet { get; set; }
        public bool AnamnesaFaktorResikoInfeksiIbuDemamLebih35 { get; set; }
        public bool AnamnesaFaktorResikoInfeksiKPDLebih35 { get; set; }
        public bool AnamnesaFaktorResikoInfeksiKorioam { get; set; }
        public bool AnamnesaFaktorResikoInfeksiKetubanHijau { get; set; }
        public bool AnamnesaFaktorResikoInfeksiFatalDistres { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorKPDLebih24 { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorAsifiksia { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorBBLSR { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorUK { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorGemia { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorKeputihan { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorTersangka { get; set; }
        public bool AnamnesaFaktorResikoInfeksiMinorIbuDemam { get; set; }
        public bool AnamnesaOksigenasiUdara { get; set; }
        public bool AnamnesaOksigenasiO2nasal { get; set; }
        public bool AnamnesaOksigenasiO2Low { get; set; }
        public bool AnamnesaOksigenasiO2High { get; set; }
        public bool AnamnesaOksigenasiO2Headbox { get; set; }
        public bool AnamnesaOksigenasiCPAP { get; set; }
        public bool AnamnesaOksigenasiLainnya { get; set; }
        public string AnamnesaOksigenasiLainnyaKet { get; set; }
        public string PenilaianNyeri { get; set; }
        public string AnamnesaSkriningNutrisi { get; set; }
        public string NPATFisikPostur { get; set; }
        public string NPATFisikPosturNilai { get; set; }
        public string NPATFisikPolaTidur { get; set; }
        public string NPATFisikPolaTidurNilai { get; set; }
        public string NPATFisikEkpresi { get; set; }
        public string NPATFisikEkpresiNilai { get; set; }
        public string NPATFisikTangis { get; set; }
        public string NPATFisikTangisNilai { get; set; }
        public string NPATFisikWarna { get; set; }
        public string NPATFisikWarnaNilai { get; set; }
        public string NPATPsikologiNapas { get; set; }
        public string NPATPsikologiNapasNilai { get; set; }
        public string NPATPsikologiDenyutJantung { get; set; }
        public string NPATPsikologiDenyutJantungNilai { get; set; }
        public string NPATPsikologiSaturasi { get; set; }
        public string NPATPsikologiSaturasiNilai { get; set; }
        public string NPATPsikologiTekananDarah { get; set; }
        public string NPATPsikologiTekananDarahNilai { get; set; }
        public string NPATPersepsiPerawat { get; set; }
        public string NPATPersepsiPerawatNilai { get; set; }
        public string NPATIntervensi { get; set; }
        public string NPATTotalSkor { get; set; }
        public string AsesmenFungsional1Skor { get; set; }
        public string AsesmenFungsional1Keterangan { get; set; }
        public string AsesmenFungsional1NilaiSkor { get; set; }
        public string AsesmenFungsional2Skor { get; set; }
        public string AsesmenFungsional2Keterangan { get; set; }
        public string AsesmenFungsional2NilaiSkor { get; set; }
        public string AsesmenFungsional3Skor { get; set; }
        public string AsesmenFungsional3Keterangan { get; set; }
        public string AsesmenFungsional3NilaiSkor { get; set; }
        public string AsesmenFungsional4Skor { get; set; }
        public string AsesmenFungsional4Keterangan { get; set; }
        public string AsesmenFungsional4NilaiSkor { get; set; }
        public string AsesmenFungsional5Skor { get; set; }
        public string AsesmenFungsional5Keterangan { get; set; }
        public string AsesmenFungsional5NilaiSkor { get; set; }
        public string AsesmenFungsional6Skor { get; set; }
        public string AsesmenFungsional6Keterangan { get; set; }
        public string AsesmenFungsional6NilaiSkor { get; set; }
        public string AsesmenFungsional7Skor { get; set; }
        public string AsesmenFungsional7Keterangan { get; set; }
        public string AsesmenFungsional7NilaiSkor { get; set; }
        public string AsesmenFungsional8Skor { get; set; }
        public string AsesmenFungsional8Keterangan { get; set; }
        public string AsesmenFungsional8NilaiSkor { get; set; }
        public string AsesmenFungsional9Skor { get; set; }
        public string AsesmenFungsional9Keterangan { get; set; }
        public string AsesmenFungsional9NilaiSkor { get; set; }
        public string AsesmenFungsional10Skor { get; set; }
        public string AsesmenFungsional10Keterangan { get; set; }
        public string AsesmenFungsional10NilaiSkor { get; set; }
        public string AsesmenFungsionalTotalSkor { get; set; }
        public string AsesmenFungsionalKreteriaStatus { get; set; }
        public string PengkajianKebutuhanKhusus { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }

        public string DiagnosisMedis { get; set; }
        public string RiwayatKeluhan { get; set; }
        public string AnamnesaPenyakitTerdahulu { get; set; }
        public string AnamnesaPenyakitOperasi { get; set; }
        public string AnamnesaPenyakitkelainan { get; set; }
        public string AnamnesaRiwayatAlergi { get; set; }
        public string KetubanPecah_Jam { get; set; }
        public bool RiwayatTerapiVitaminK { get; set; }
        public bool RiwayatBCG { get; set; }
        public bool RiwayatHepatitisB0 { get; set; }
        public bool Polio1 { get; set; }
        public string AntropometriBB { get; set; }
        public string AntropometriPB { get; set; }
        public string AntropometriLK { get; set; }
        public string AntropometriLD { get; set; }
        public string KeadaanUmumGerak { get; set; }
        public string KeadaanUmumTangis { get; set; }
        public string KeadaanUmumWarnaKulit { get; set; }
        public bool KeadaanUmumKemerahan { get; set; }
        public bool KeadaanUmumSianosis { get; set; }
        public bool KeadaanUmumPucat { get; set; }
        public bool KeadaanUmumIkterusKramer { get; set; }
        public string KeadaanUmumIkterusKramerKet { get; set; }
        public string KeadaanUmumHR { get; set; }
        public string KeadaanUmumSuhu { get; set; }
        public string KeadaanUmumRR { get; set; }
        public string KeadaanUmumSatO2 { get; set; }
        public string DowneSkorFrekuensi { get; set; }
        public string DowneSkorFrekuensiSkor { get; set; }
        public string DowneSkorRetraksi { get; set; }
        public string DowneSkorRetraksiSkor { get; set; }
        public string DowneSkorSianosis { get; set; }
        public string DowneSkorSianosisSkor { get; set; }
        public string DowneSkorSuaraNafas { get; set; }
        public string DowneSkorSuaraNafasSkor { get; set; }
        public string DowneSkorMerintih { get; set; }
        public string DowneSkorMerintihSkor { get; set; }
        public string DowneSkorTotalSkor { get; set; }
        public string DowneSkorDiagnosaSkor { get; set; }
        public string PemeriksaanKepala { get; set; }
        public string PemeriksaanKepala_Ket { get; set; }
        public string PemeriksaanUUB { get; set; }
        public string PemeriksaanUUB_Ket { get; set; }
        public string PemeriksaanMata { get; set; }
        public string PemeriksaanMata_Ket { get; set; }
        public string PemeriksaanTHT { get; set; }
        public string PemeriksaanTHT_Ket { get; set; }
        public string PemeriksaanMulut { get; set; }
        public string PemeriksaanMulut_Ket { get; set; }
        public string PemeriksaanThroax { get; set; }
        public string PemeriksaanThroax_Ket { get; set; }
        public string PemeriksaanAbdomen { get; set; }
        public string PemeriksaanAbdomen_Ket { get; set; }
        public string PemeriksaanTaliPusat { get; set; }
        public string PemeriksaanTaliPusat_Ket { get; set; }
        public string PemeriksaanPunggung { get; set; }
        public string PemeriksaanPunggung_Ket { get; set; }
        public string PemeriksaanGenetalia { get; set; }
        public string PemeriksaanGenetalia_Ket { get; set; }
        public string PemeriksaanAnus { get; set; }
        public string PemeriksaanAnus_Ket { get; set; }
        public string PemeriksaanEkstremitas { get; set; }
        public string PemeriksaanEkstremitas_Ket { get; set; }
        public string PemeriksaanKulit { get; set; }
        public string PemeriksaanKulit_Ket { get; set; }
        public string PemeriksaanReflek { get; set; }
        public string PemeriksaanReflek_Ket { get; set; }
        public bool Moro { get; set; }
        public bool Grasping { get; set; }
        public bool Sucking { get; set; }
        public bool Stepping { get; set; }
        public bool Babinski { get; set; }
        public bool Rooting { get; set; }
        public bool Swallowing { get; set; }
        public bool Tonick { get; set; }
        public bool Glabela { get; set; }
        public bool Lainnya { get; set; }
        public string LainnyaKet { get; set; }
        public bool VerifikasiDPJP { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListKeperawatanNeo> ListTemplate { get; set; }

    }

    public class SelectItemListKeperawatanNeo
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}