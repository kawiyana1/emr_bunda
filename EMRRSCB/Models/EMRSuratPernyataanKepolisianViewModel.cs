﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPernyataanKepolisianViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Pekerjaan { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string Berupa { get; set; }
        public string YangMenyatakan { get; set; }
        public string YangMenyatakanNama { get; set; }
        public string TTDYangMenyatakan { get; set; }
        public string Daerah { get; set; }
        public string Resort { get; set; }
        public string Nomor { get; set; }

        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }

        public int MODEVIEW { get; set; }
    }
}