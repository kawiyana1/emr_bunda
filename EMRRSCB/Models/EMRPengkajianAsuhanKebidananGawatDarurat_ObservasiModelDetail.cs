﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string JenisCairan { get; set; }
        public string NoBotol { get; set; }
        public bool IV { get; set; }
        public bool Oral { get; set; }
        public bool Drain { get; set; }
        public bool NGT { get; set; }
        public bool Urin { get; set; }
        public bool BAB { get; set; }
        public string Username { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }

    }
}