﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel
    {

        public string NoBukti { get; set; }
        public int No { get; set; }
        public string No1 { get; set; }
        public string No2 { get; set; }
        public string No3 { get; set; }
        public string No4 { get; set; }
        public string No5 { get; set; }
        public string No6 { get; set; }
    }
}