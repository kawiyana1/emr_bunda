﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPermohonanOperasiTubektomiViewModel
    {
        public string NoBukti { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Klinik { get; set; }
        public string Istri { get; set; }
        public string Suami { get; set; }
        public string Saksi { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string NamaYangDioperasi { get; set; }
        public string NamaKlinik { get; set; }
        public string TTDIstri { get; set; }
        public string TTDSuami { get; set; }
        public string TTDSaksi { get; set; }
        public string TTDDokter { get; set; }

        public bool VerifikasiDokter { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}