﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanPengobatanController : Controller
    {
        // GET: EMRCatatanPengobatan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanPengobatan.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRCatatanPengobatan_List = new ListDetail<EMRCatatanPengobatanViewModel>();
                        var detail_1 = s.CatatanPengobatan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanPengobatanViewModel>(x);

                            var getobat = sim.mBarang.Where(xx => xx.Kode_Barang == x.KodeObat).FirstOrDefault();
                           

                            var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == y.DPJP);
                            if (dokter != null)
                            {
                                y.DPJPNama = dokter.NamaDOkter;
                            }
                            model.EMRCatatanPengobatan_List.Add(false, y);
                            model.Diagnosa = y.Diagnosa;
                            model.Alergi = y.Alergi;
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanPengobatan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Catatan Pengobatan RI";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Catatan Pengobatan RI";
                    }

                    if (item.EMRCatatanPengobatan_List == null) item.EMRCatatanPengobatan_List = new ListDetail<EMRCatatanPengobatanViewModel>();
                    item.EMRCatatanPengobatan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRCatatanPengobatan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Diagnosa = item.Diagnosa;
                        x.Model.Alergi = item.Alergi;
                        x.Model.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRCatatanPengobatan_List;
                    var real_list = s.CatatanPengobatan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanPengobatan.Remove(x);
                    }
                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            _m = new CatatanPengobatan();
                            s.CatatanPengobatan.Add(IConverter.Cast<CatatanPengobatan>(x.Model));

                        }
                        else
                        {
                            _m = IConverter.Cast<CatatanPengobatan>(x.Model);
                            s.CatatanPengobatan.AddOrUpdate(_m);
                        }

                        //if (_m == null) s.CatatanPengobatan.Add(IConverter.Cast<CatatanPengobatan>(x.Model));
                        //else
                        //{

                        //    _m.NoBukti = x.Model.NoBukti;
                        //    _m.No = x.Model.No;
                        //    _m.TglMulaiPemberian = x.Model.TglMulaiPemberian;
                        //    _m.TglResep = x.Model.TglResep;
                        //    _m.JamPemberianObat = x.Model.JamPemberianObat;
                        //    _m.KodeObat = x.Model.KodeObat;
                        //    _m.NamaObat = x.Model.NamaObat;
                        //    _m.Dosis = x.Model.Dosis;
                        //    _m.Indikasi = x.Model.Indikasi;
                        //    _m.Rute = x.Model.Rute;
                        //    _m.Frekuensi = x.Model.Frekuensi;
                        //    _m.Catatan = x.Model.Catatan;
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}