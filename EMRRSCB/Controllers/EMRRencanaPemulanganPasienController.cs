﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;


namespace EMRRSCB.Controllers
{
    public class EMRRencanaPemulanganPasienController : Controller
    {
        // GET: EMRRencanaPemulanganPasien
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRRencanaPemulanganPasienViewModel();
            using (var sim = new SIM_Entities())
            { 
                using (var s = new EMREntities())
            {
                var dokumen = s.RencanaPemulanganPasien.FirstOrDefault(x => x.NoBukti == id);
                if (dokumen != null)
                {
                    model = IConverter.Cast<EMRRencanaPemulanganPasienViewModel>(dokumen);

                    var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                    if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                    model.NoBukti = id;
                    model.Report = 1;
                    model.MODEVIEW = view;
                }
                else
                {
                    model.NoBukti = id;
                }

            }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRRencanaPemulanganPasienViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.RencanaPemulanganPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<RencanaPemulanganPasien>(item);
                        s.RencanaPemulanganPasien.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Rencana Pemulangan Pasien";
                    }
                    else
                    {
                        model = IConverter.Cast<RencanaPemulanganPasien>(item);
                        s.RencanaPemulanganPasien.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Rencana Pemulangan Pasien";
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}