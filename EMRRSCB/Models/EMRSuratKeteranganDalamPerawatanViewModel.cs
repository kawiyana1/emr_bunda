﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratKeteranganDalamPerawatanViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKontrol { get; set; }
        public string Diagnosa { get; set; }
        public string Tindakan { get; set; }
        public string Therapy { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string TTDDokter { get; set; }



        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}