﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPemasanganNGTViewModel
    {
        public string NoBukti { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public bool VerifikasiPetugas { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}