﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.TanggalDari = DateTime.Today;
            ViewBag.TanggalSampai = DateTime.Today;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string GetDiagnosa(string fromdate, string todate)
        {
            try
            {
                var model = new ListDetail<DashboardViewModel>();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var tipe = Request.Cookies["EMRTipePelayanan"].Value;
                        var detail_2 = s.EMR_FN_Dashboard_Diagnosa(fromdate, todate, tipe).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<DashboardViewModel>(x);
                            y.Deskripsi = x.Descriptions;
                            model.Add(false, y);
                        }

                        //var dt = s.EMR_FN_Dashboard_Diagnosa("17/Dec/2022", "18/Dec/2022", "RI").ToList();
                        //if (dt != null)
                        //{

                        //    foreach()
                        //    model = IConverter.Cast<DashboardViewModel>(dt);
                        //    model.Deskripsi = dt.Descriptions;
                        //}
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string GetDPJP(string fromdate, string todate)
        {
            try
            {
                var model = new ListDetail<DashboardViewModel>();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var tipe = Request.Cookies["EMRTipePelayanan"].Value;
                        var detail_2 = s.EMR_FN_Dashboard_DPJP(fromdate, todate, tipe).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<DashboardViewModel>(x);
                            y.Deskripsi = x.Descriptions;
                            model.Add(false, y);
                        }

                        //var dt = s.EMR_FN_Dashboard_Diagnosa("17/Dec/2022", "18/Dec/2022", "RI").ToList();
                        //if (dt != null)
                        //{

                        //    foreach()
                        //    model = IConverter.Cast<DashboardViewModel>(dt);
                        //    model.Deskripsi = dt.Descriptions;
                        //}
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string GetDasboard(string fromdate, string todate)
        {
            try
            {
                var model = new ListDetail<DashboardViewModel>();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var tipe = Request.Cookies["EMRTipePelayanan"].Value;
                        var detail_2 = s.EMR_FN_Dashboard(fromdate, todate, tipe).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<DashboardViewModel>(x);
                            y.Deskripsi = x.Descriptions;
                            model.Add(false, y);
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string GetCaraPulang(string fromdate, string todate)
        {
            try
            {
                var model = new ListDetail<DashboardViewModel>();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var tipe = Request.Cookies["EMRTipePelayanan"].Value;
                        var detail_2 = s.EMR_FN_Dashboard_CaraPasienPulang(fromdate, todate, tipe).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<DashboardViewModel>(x);
                            y.Deskripsi = x.Descriptions;
                            model.Add(false, y);
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}