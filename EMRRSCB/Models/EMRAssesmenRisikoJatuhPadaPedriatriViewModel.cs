﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRAssesmenRisikoJatuhPadaPedriatriViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Usia { get; set; }
        public string Usia_Ket { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKelamin_Ket { get; set; }
        public string Diagnosis { get; set; }
        public string Diagnosis_Ket { get; set; }
        public string GangguanKognitif { get; set; }
        public string GangguanKognitif_Ket { get; set; }
        public string FaktorLingkungan { get; set; }
        public string FaktorLingkungan_Ket { get; set; }
        public string ResponOprasi { get; set; }
        public string ResponOprasi_Ket { get; set; }
        public string PenggunaanObat { get; set; }
        public string PenggunaanObat_Ket { get; set; }
        public string Total { get; set; }
        public string Tanggal { get; set; }
        public string Username { get; set; }
        public string Keterangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}