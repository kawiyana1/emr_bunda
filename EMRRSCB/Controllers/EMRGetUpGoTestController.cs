﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace EMRRSCB.Controllers
{
    public class EMRGetUpGoTestController : Controller
    {
        // GET: EMRGetUpGoTest
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRGetUpGoTestViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokument = emr.GetUpGoTest.FirstOrDefault(x => x.NoBukti == id);
                    if (dokument != null)
                    {
                        model = IConverter.Cast<EMRGetUpGoTestViewModel>(dokument);
                        model.NoBukti = id;
                        var petugas = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Petugas);
                        if (petugas != null) model.PetugasNama = petugas.NamaDOkter;
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            {
                return View(model);
            }
        }


        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]

        public string CreatePost()
        {
            try
            {
                var item = new EMRGetUpGoTestViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var emr = new EMREntities())
                {
                    var model = emr.GetUpGoTest.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<GetUpGoTest>(item);
                        emr.GetUpGoTest.Add(o);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Surat GetUpGoTestnya";

                    }
                    else
                    {
                        model = IConverter.Cast<GetUpGoTest>(item);
                        emr.GetUpGoTest.AddOrUpdate(model);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Update Surat GetUpGoTestnya";
                    }
                    emr.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

    }
}
