//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class PemberianInformasiHasilPelayananPengobatan
    {
        public string NoBukti { get; set; }
        public string Dokter { get; set; }
        public string PemberianInformasi { get; set; }
        public string PenerimaInformasi { get; set; }
        public string No1 { get; set; }
        public bool No1Tanda { get; set; }
        public string No2 { get; set; }
        public bool No2Tanda { get; set; }
        public string No3 { get; set; }
        public bool No3Tanda { get; set; }
        public string No4 { get; set; }
        public bool No4Tanda { get; set; }
        public string No5 { get; set; }
        public bool No5Tanda { get; set; }
        public string No6 { get; set; }
        public bool No6Tanda { get; set; }
        public string No7 { get; set; }
        public bool No7Tanda { get; set; }
        public string No8 { get; set; }
        public bool No8Tanda { get; set; }
        public byte[] TTD1 { get; set; }
        public byte[] TTD2 { get; set; }
    }
}
