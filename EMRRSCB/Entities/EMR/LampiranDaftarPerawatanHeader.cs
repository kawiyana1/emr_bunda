//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class LampiranDaftarPerawatanHeader
    {
        public string NoBukti { get; set; }
        public string PenanggungJawab { get; set; }
        public string AlamatTempatIbadah { get; set; }
        public string PemimpinAcara { get; set; }
        public string PelayananDoa { get; set; }
        public string Lampiran { get; set; }
    }
}
