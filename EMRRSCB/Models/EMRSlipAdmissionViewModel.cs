﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSlipAdmissionViewModel
    {
        public string NoBukti { get; set; }
        public string NamaPasien { get; set; }
        public string Umur { get; set; }
        public string TanggalLahir { get; set; }
        public string AlamatPasien { get; set; }
        public string PenanggungJawab { get; set; }
        public string PekerjaanPenanggungJawab { get; set; }
        public string PlantOfCare { get; set; }
        public string DokterMerawat { get; set; }
        public string DokterMerawatNama { get; set; }
        public string CaraBayar { get; set; }
        public string JKNGol { get; set; }
        public string JKNGolKet { get; set; }
        public string HakPerawatan { get; set; }
        public string KelasPerawatan { get; set; }
        public string Intermedite { get; set; }
        public string Ruangan { get; set; }
        public string KonfirmasiTempat { get; set; }
        public string PetugasRuangan { get; set; }
        public string PetugasRuanganNama { get; set; }
        public string Ketersediaan { get; set; }
        public string SituasiKamar { get; set; }
        public string KesiapanKamar { get; set; }
        public string JenisTindakan { get; set; }
        public string BiayaTindakan { get; set; }
        public string InformasiKetersediaan { get; set; }
        public string InfromasiHakKewajiban { get; set; }
        public string InformasiGeneral { get; set; }
        public string InformasiCaraBayar { get; set; }
        public string RekamMedikLama { get; set; }
        public string PengantaranListPasien { get; set; }
        public string PetugasAdmission { get; set; }
        public string PetugasAdmissionNama { get; set; }
        public bool VerifikasiPetugasAdmission { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }
    }
}