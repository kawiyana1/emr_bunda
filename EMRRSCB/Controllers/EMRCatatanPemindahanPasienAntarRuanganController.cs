﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCatatanPemindahanPasienAntarRuanganController : Controller
    {
        // GET: EMRCatatanPemindahanPasienAntarRuangan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string noreg, int view = 0)
        {
            var model = new EMRCatatanPemindahanPasienAntarRuanganViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.CatatanPemindahanPasienAntarRuangan.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRCatatanPemindahanPasienAntarRuanganViewModel>(dokumen);
                        var dokter1 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterRawat_1);
                        if (dokter1 != null) model.DokterRawat_1Nama = dokter1.NamaDOkter;

                        var dokter2 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterRawat_2);
                        if (dokter2 != null) model.DokterRawat_2Nama = dokter2.NamaDOkter;

                        var dokter3 = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterRawat_3);
                        if (dokter3 != null) model.DokterRawat_3Nama = dokter3.NamaDOkter;

                        var diserahkan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Diserahkan_Perawat);
                        if (diserahkan != null) model.Diserahkan_PerawatNama = diserahkan.NamaDOkter;

                        var diterima = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Diterima_Perawat);
                        if (diterima != null) model.Diterima_PerawatNama = diterima.NamaDOkter;

                        model.Detail_List = new ListDetail<EMRCatatanPemindahanPasienAntarRuangan_DetailDetailModel>();
                        var detail_1 = s.CatatanPemindahanPasienAntarRuangan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRCatatanPemindahanPasienAntarRuangan_DetailDetailModel>(x);
                            //var dokter_detail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Pengkajian);
                            //if (dokter_detail != null) y.PengkajianNama = dokter_detail.NamaDOkter;
                            model.Detail_List.Add(false, y);
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.ProsedurPembedahan_Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.JamSerahTerima = DateTime.Now;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRCatatanPemindahanPasienAntarRuanganViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.CatatanPemindahanPasienAntarRuangan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<CatatanPemindahanPasienAntarRuangan>(item);
                        s.CatatanPemindahanPasienAntarRuangan.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Pengkajian Catatan Pemindahan Pasien";
                    }
                    else
                    {
                        model = IConverter.Cast<CatatanPemindahanPasienAntarRuangan>(item);
                        s.CatatanPemindahanPasienAntarRuangan.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Pengkajian Catatan Pemindahan Pasien";
                    }

                    if (item.Detail_List == null) item.Detail_List = new ListDetail<EMRCatatanPemindahanPasienAntarRuangan_DetailDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Detail_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Detail_List;
                    var real_list = s.CatatanPemindahanPasienAntarRuangan_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.CatatanPemindahanPasienAntarRuangan_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.CatatanPemindahanPasienAntarRuangan_Detail.Add(IConverter.Cast<CatatanPemindahanPasienAntarRuangan_Detail>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}