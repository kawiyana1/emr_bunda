﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianMedisKebidananDanKandunganGawatDaruratController : Controller
    {
        // GET: EMRPengkajianMedisKebidananDanKandunganGawatDarurat
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianMedisKebidananDanKandunganGawatDarurat.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJaga);
                        if (dokter != null) model.DokterJagaNama = dokter.NamaDOkter;

                        var DPJP = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (DPJP != null) model.DPJPNama = DPJP.NamaDOkter;

                        var konsultasidokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiTSDokter);
                        if (konsultasidokter != null) model.KonsultasiTSDokterNama = konsultasidokter.NamaDOkter;

                        var jwbkonsultasidokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokter);
                        if (jwbkonsultasidokter != null) model.JawabanKonsultasiTSDokterNama = jwbkonsultasidokter.NamaDOkter;

                        var knsltsdokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiDokter);
                        if (knsltsdokter != null) model.KonsultasiDokterNama = knsltsdokter.NamaDOkter;

                        var jwbknsltsdokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokterNama);
                        if (jwbknsltsdokter != null) model.JawabanKonsultasiTSDokterNama = jwbknsltsdokter.NamaDOkter;

                        model.NamaPasien = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm).NamaPasien;

                        model.Observasi_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail>();
                        var detail_1 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Observasi_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail>();
                        var detail_2 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_3 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.KosultasiJam = DateTime.Now;
                        model.JawabanKonsultasiJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.NamaPasien = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm).NamaPasien;
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.NRM = nrm;
                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemPengakjianMedisKebidananKandunganIGD>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemPengakjianMedisKebidananKandunganIGD()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy, string nrm)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianMedisKebidananDanKandunganGawatDarurat.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterJaga);
                        if (dokter != null) model.DokterJagaNama = dokter.NamaDOkter;

                        var DPJP = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (DPJP != null) model.DPJPNama = DPJP.NamaDOkter;

                        var konsultasidokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiTSDokter);
                        if (konsultasidokter != null) model.KonsultasiTSDokterNama = konsultasidokter.NamaDOkter;

                        var jwbkonsultasidokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokter);
                        if (jwbkonsultasidokter != null) model.JawabanKonsultasiTSDokterNama = jwbkonsultasidokter.NamaDOkter;

                        var knsltsdokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.KonsultasiDokter);
                        if (knsltsdokter != null) model.KonsultasiDokterNama = knsltsdokter.NamaDOkter;

                        var jwbknsltsdokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.JawabanKonsultasiTSDokterNama);
                        if (jwbknsltsdokter != null) model.JawabanKonsultasiTSDokterNama = jwbknsltsdokter.NamaDOkter;

                        model.NamaPasien = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm).NamaPasien;

                        model.Observasi_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail>();
                        var detail_1 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Observasi_List.Add(false, y);
                        }

                        model.RencanaKerja_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail>();
                        var detail_2 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail>(x);
                            model.RencanaKerja_List.Add(false, y);
                        }

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_3 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.KosultasiJam = DateTime.Now;
                        model.JawabanKonsultasiJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.NamaPasien = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm).NamaPasien;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemPengakjianMedisKebidananKandunganIGD>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemPengakjianMedisKebidananKandunganIGD()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPengkajianMedisKebidananDanKandunganGawatDaruratViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.PengkajianMedisKebidananDanKandunganGawatDarurat.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat>(item);
                        s.PengkajianMedisKebidananDanKandunganGawatDarurat.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Assesmen Rekonsiliasi Obat Adminsi";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat>(item);
                        s.PengkajianMedisKebidananDanKandunganGawatDarurat.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Assesmen Rekonsiliasi Obat Adminsi";
                    }
                    #region === Detail Observasi
                    if (item.Observasi_List == null) item.Observasi_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_ObservasiModelDetail>();
                    item.Observasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Observasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list = item.Observasi_List;
                    var real_list = s.PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi.Add(IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat_Observasi>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Intruksi = x.Model.Intruksi;
                        }
                    }
                    #endregion

                    #region === Detail Rencana Kerja
                    if (item.RencanaKerja_List == null) item.RencanaKerja_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail>();
                    item.RencanaKerja_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RencanaKerja_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list1 = item.RencanaKerja_List;
                    var real_list1 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis.Add(IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedis>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.DaftarMasalah = x.Model.DaftarMasalah;
                            _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                        }
                    }
                    #endregion

                    #region === Detail Riwayat Kehamilan
                    if (item.RiwayatKehamilan_List == null) item.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                    item.RiwayatKehamilan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RiwayatKehamilan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                    }
                    var new_list2 = item.RiwayatKehamilan_List;
                    var real_list2 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Remove(x);
                    }

                    foreach (var x in new_list2)
                    {
                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Add(IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Abortus = x.Model.Abortus;
                            _m.Aterm = x.Model.Aterm;
                            _m.BBL = x.Model.BBL;
                            _m.Cacat = x.Model.Cacat;
                            _m.JenisKelaminLaki = x.Model.JenisKelaminLaki;
                            _m.JenisKelaminPerempuan = x.Model.JenisKelaminPerempuan;
                            _m.JenisPartus = x.Model.JenisPartus;
                            _m.Komplikasi = x.Model.Komplikasi;
                            _m.Meninggal = x.Model.Meninggal;
                            _m.Nakes = x.Model.Nakes;
                            _m.Normal = x.Model.Normal;
                            _m.Non = x.Model.Non;
                            _m.Prematur = x.Model.Prematur;
                        }
                    }
                    #endregion
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}