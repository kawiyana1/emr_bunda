//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class CPPT
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Profesi { get; set; }
        public string CPPT_S { get; set; }
        public string CPPT_O { get; set; }
        public string CPPT_A { get; set; }
        public string CPPT_P { get; set; }
        public string CPPT_I { get; set; }
        public string DPJP { get; set; }
        public byte[] CPPT_OGambar { get; set; }
        public byte[] TTDDPJP { get; set; }
        public byte[] TTDCPPT { get; set; }
        public string Petugas_1 { get; set; }
        public string Petugas_2 { get; set; }
        public Nullable<System.DateTime> Tanggal_SerahTerima { get; set; }
        public byte[] Petugas_1_TTD { get; set; }
        public byte[] Petugas_2_TTD { get; set; }
        public string MenyerahkanPasien { get; set; }
        public string MenerimaPasien { get; set; }
        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }
        public Nullable<System.DateTime> JamBacaUlang { get; set; }
        public string PetugasBacaUlang { get; set; }
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }
        public Nullable<System.DateTime> JamKonfirmasi { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public byte[] PetugasBacaUlangTTD { get; set; }
        public byte[] PetugasKonfirmasiTTD { get; set; }
        public byte[] MenyerahkanPasienTTD { get; set; }
        public byte[] MenerimaPasienTTD { get; set; }
        public Nullable<bool> VerifikasiPetugasCPPT { get; set; }
        public Nullable<bool> VerifikasiPetugasBacaUlangCPPT { get; set; }
        public Nullable<bool> VerifikasiPetugasKonfirmasiCPPT { get; set; }
        public Nullable<bool> VerifikasiMenyerahkanPasienCPPT { get; set; }
        public Nullable<bool> VerifikasiMenerimaPasienCPPT { get; set; }
    }
}
