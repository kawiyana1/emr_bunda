﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRAssesmenRisikoJatuhMorseViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Diagnosa { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalHeader { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamHeader { get; set; }
        public string RiwayatJatuh { get; set; }
        public string RiwayatJatuhPoin { get; set; }
        public string Diagnosis { get; set; }
        public string DiagnosisPoin { get; set; }
        public string AlatBantu { get; set; }
        public string AlatBantuPoin { get; set; }
        public string TerpasangInfus { get; set; }
        public string TerpasangInfusPoin { get; set; }
        public string GayaBerjalan { get; set; }
        public string GayaBerjalanPoin { get; set; }
        public string Siklus { get; set; }
        public string SiklusPoin { get; set; }
        public string TotalSkor { get; set; }
        public string CederaJatuh { get; set; }
        public string TotalSkorHasil { get; set; }
        public string Tanggal { get; set; }
        public string Username { get; set; }

        public int MODEVIEW { get; set; }
    }
}