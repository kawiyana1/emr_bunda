﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoring24Jam_CatatanPerkembanganDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SOAP { get; set; }
        public string Intruksi { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string TandaTangan { get; set; }
        public string TanggalManual { get; set; }
        public string JamManual { get; set; }
        public string Profesi { get; set; }
    }
}