﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRIntervensiMonitoringEvaluasiViewDetail
    {
        public string NoBukti { get; set; }
        public string BB { get; set; }
        public int Report { get; set; }
        public int MODEVIEW { get; set; }
        public string DietAwal { get; set; }
        public string E { get; set; }
        public string P { get; set; }
        public string AsupanMakan { get; set; }
        public string Keterangan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Tanggal { get; set; }
        public string TandaTangan { get; set; }
        public bool Verifikasi { get; set; }
        public ListDetail<EMRIntervensiMonitoringEvaluasi_DetailViewModel> EMRIntervensi_List { get; set; }
    }
}