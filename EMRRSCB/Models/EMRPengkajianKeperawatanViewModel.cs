﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianKeperawatanViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SumberData { get; set; }
        public string SumberDataLainnya { get; set; }
        public string Agama { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public string Kewarganegaraan { get; set; }
        public string AlamatSaaitini { get; set; }
        public bool ComposMontis { get; set; }
        public bool Apatis { get; set; }
        public bool Somnoler { get; set; }
        public bool Slupor { get; set; }
        public bool Coma { get; set; }
        public string GCS { get; set; }
        public string VitalSign { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Suhu { get; set; }
        public string Pernafasan { get; set; }
        public string SpO2 { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatKeluhan { get; set; }
        public string DiagnosaMedis { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string RiwayatMRS { get; set; }
        public string RiwayatDioperasi { get; set; }
        public string RiwayatPenyakitKeluarga { get; set; }
        public string RiwayatAlergi { get; set; }
        public string SkriningNyeriNyeri { get; set; }
        public string SkriningNyeriPengkajianNyeri { get; set; }
        public string SkriningNyeriAsesment { get; set; }
        public string SkriningNyeriOnset { get; set; }
        public string SkriningNyeriOnsetSejakKapan { get; set; }
        public string SkriningNyeriPencetus { get; set; }
        public string SkriningNyeriPencetusKet { get; set; }
        public string SkriningNyeriKualitasNyeri { get; set; }
        public string SkriningNyeriKualitasNyeriLainnya { get; set; }
        public string SkriningNyeriRegioNyeriLokasi { get; set; }
        public string SkriningNyeriNyeriMenjalar { get; set; }
        public string SkriningNyeriNyeriMenjalarKemana { get; set; }
        public string SkriningNyeriNyeriSkalaNyeri { get; set; }
        public string SkriningNyeriNyeriTimingNyeri { get; set; }
        public bool SkriningNyeriNyeriDatangSaatIstirahat { get; set; }
        public bool SkriningNyeriNyeriDatangSaatBeraktifitas { get; set; }
        public bool SkriningNyeriNyeriDatangSaatLainnya { get; set; }
        public string SkriningNyeriNyeriDatangSaatLainnyaSebutkan { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaIstirahat { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMusik { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarMinumObat { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarMinumObatKet { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarBerubahPosisiTidur { get; set; }
        public bool SkriningNyeriNyeriMembaikBilaMendengarLainnya { get; set; }
        public string SkriningNyeriNyeriMembaikBilaMendengarLainnyaSebutkan { get; set; }
        public string SkriningNyeriFrekuensiNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri { get; set; }
        public string SkriningNyeriIntensitasNyeri1 { get; set; }
        public string SkriningGiziApakahAsupanMengalami { get; set; }
        public string SkriningGiziApakahAsupanMengalamiKet { get; set; }
        public string SkriningGiziApakahAsupanMakan { get; set; }
        public string SkriningGizi1 { get; set; }
        public string SkriningGizi2 { get; set; }
        public string AsesmenFungsionalTotal { get; set; }
        public string SkriningGiziTotalSkor { get; set; }
        public string SkriningGiziTotalSkor1 { get; set; }
        public string SkriningGiziKriteriaGizi { get; set; }
        public string SkriningGiziKriteriaGizi1 { get; set; }
        public string AsesmenFungsional1Skor { get; set; }
        public string AsesmenFungsional1Keterangan { get; set; }
        public string AsesmenFungsional1NilaiSkor { get; set; }
        public string AsesmenFungsional2Skor { get; set; }
        public string AsesmenFungsional2Keterangan { get; set; }
        public string AsesmenFungsional2NilaiSkor { get; set; }
        public string AsesmenFungsional3Skor { get; set; }
        public string AsesmenFungsional3Keterangan { get; set; }
        public string AsesmenFungsional3NilaiSkor { get; set; }
        public string AsesmenFungsional4Skor { get; set; }
        public string AsesmenFungsional4Keterangan { get; set; }
        public string AsesmenFungsional4NilaiSkor { get; set; }
        public string AsesmenFungsional5Skor { get; set; }
        public string AsesmenFungsional5Keterangan { get; set; }
        public string AsesmenFungsional5NilaiSkor { get; set; }
        public string AsesmenFungsional6Skor { get; set; }
        public string AsesmenFungsional6Keterangan { get; set; }
        public string AsesmenFungsional6NilaiSkor { get; set; }
        public string AsesmenFungsional7Skor { get; set; }
        public string AsesmenFungsional7Keterangan { get; set; }
        public string AsesmenFungsional7NilaiSkor { get; set; }
        public string AsesmenFungsional8Skor { get; set; }
        public string AsesmenFungsional8Keterangan { get; set; }
        public string AsesmenFungsional8NilaiSkor { get; set; }
        public string AsesmenFungsional9Skor { get; set; }
        public string AsesmenFungsional9Keterangan { get; set; }
        public string AsesmenFungsional9NilaiSkor { get; set; }
        public string AsesmenFungsional10Skor { get; set; }
        public string AsesmenFungsional10Keterangan { get; set; }
        public string AsesmenFungsional10NilaiSkor { get; set; }
        public string AsesmenFungsionalTotalSkor { get; set; }
        public string AsesmenFungsionalKreteriaStatus { get; set; }
        public string AsesmenResikoFatorResiko { get; set; }
        public string AsesmenResikoFatorResikoSkala { get; set; }
        public string AsesmenResikoFatorResikoSkor { get; set; }
        public string AsesmenResikoRiwayatJatuh { get; set; }
        public string AsesmenResikoRiwayatJatuhSkala { get; set; }
        public string AsesmenResikoRiwayatJatuhSkor { get; set; }
        public string AsesmenResikoDiagnosisSekunder { get; set; }
        public string AsesmenResikoDiagnosisSekunderSkala { get; set; }
        public string AsesmenResikoDiagnosisSekunderSkor { get; set; }
        public string AsesmenResikoAlatBantu { get; set; }
        public string AsesmenResikoAlatBantuSkala { get; set; }
        public string AsesmenResikoAlatBantuSkor { get; set; }
        public string AsesmenResikoTerpasangInfus { get; set; }
        public string AsesmenResikoTerpasangInfusSkala { get; set; }
        public string AsesmenResikoTerpasangInfusSkor { get; set; }
        public string AsesmenResikoGayaBerjalan { get; set; }
        public string AsesmenResikoGayaBerjalanSkala { get; set; }
        public string AsesmenResikoGayaBerjalanSkor { get; set; }
        public string AsesmenResikoSiklusMental { get; set; }
        public string AsesmenResikoSiklusMentalSkala { get; set; }
        public string AsesmenResikoSiklusMentalSkor { get; set; }
        public string AsesmenResikoTotalSkor { get; set; }
        public string AsesmenResikoKategori { get; set; }
        public string Psikososial_StatusNikah { get; set; }
        public string Psikososial_TinggalBersama { get; set; }
        public string Psikososial_TinggalBersamaKet { get; set; }
        public string Psikososial_RiwayatKebiasaan { get; set; }
        public string Psikososial_RiwayatKebiasaanLainnya { get; set; }
        public string Psikososial_RiwayatKebiasaanJenis { get; set; }
        public string Psikososial_ResikoMencederai { get; set; }
        public string Psikososial_Kepercayaan { get; set; }
        public bool Observasi_VitalSign { get; set; }
        public bool Observasi_Neurogical { get; set; }
        public bool Observasi_Neurovaskular { get; set; }
        public bool Observasi_GulaDarah { get; set; }
        public bool Observasi_BeratBadan { get; set; }
        public bool Observasi_TinggiBadan { get; set; }
        public bool Observasi_Urinalisys { get; set; }
        public bool Observasi_DL { get; set; }
        public bool Observasi_Lainnya { get; set; }
        public string Observasi_LainnyaKet { get; set; }
        public bool InfusIntravena { get; set; }
        public string InfusIntravenaDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> InfusIntravenaTanggal { get; set; }
        public bool Dower { get; set; }
        public string DowerDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DowerTanggal { get; set; }
        public bool Cystostomy { get; set; }
        public string CystostomyDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CystostomyTanggal { get; set; }
        public bool LainLain { get; set; }
        public string LainLainDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> LainLainTanggal { get; set; }
        public bool CentraLine { get; set; }
        public string CentraLineDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CentraLineTanggal { get; set; }
        public bool SelangNGT { get; set; }
        public string SelangNGTDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> SelangNGTTanggal { get; set; }
        public bool Tracheostomy { get; set; }
        public string TracheostomyDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TracheostomyTanggal { get; set; }
        public bool Status_TidakDiketahui { get; set; }
        public bool Status_Suspect { get; set; }
        public bool Status_Diketahui { get; set; }
        public bool Status_MRSA { get; set; }
        public bool Status_VRE { get; set; }
        public bool Status_TB { get; set; }
        public bool Status_InfeksiOpprtunistik { get; set; }
        public bool Status_Lainnya { get; set; }
        public string Status_LainnyaKet { get; set; }
        public bool Droplet { get; set; }
        public bool Airborn { get; set; }
        public bool Contact { get; set; }
        public bool Skin { get; set; }
        public bool ContactMultiResistent { get; set; }
        public bool Phlebitis { get; set; }
        public bool ISK { get; set; }
        public bool Pnemonia { get; set; }
        public bool ILO { get; set; }
        public bool Sepsis { get; set; }
        public bool Dekubitus { get; set; }
        public string KebutuhanKomunikasiBicara { get; set; }
        public string KebutuhanKomunikasiBicaraKapan { get; set; }
        public bool KebutuhanKomunikasiBahasaIndonesia { get; set; }
        public string KebutuhanKomunikasiBahasaIndonesiaKet { get; set; }
        public bool KebutuhanKomunikasiBahasaInggris { get; set; }
        public string KebutuhanKomunikasiBahasaInggrisKet { get; set; }
        public bool KebutuhanKomunikasiBahasaDaerah { get; set; }
        public string KebutuhanKomunikasiBahasaDaerahKet { get; set; }
        public bool KebutuhanKomunikasiBahasaLainnya { get; set; }
        public string KebutuhanKomunikasiBahasaLainnyaKet { get; set; }
        public string KebutuhanKomunikasiPenerjemah { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasa { get; set; }
        public string KebutuhanKomunikasiPenerjemahBahasaIsyarat { get; set; }
        public bool KebutuhanKomunikasiHambatanBahasa { get; set; }
        public bool KebutuhanKomunikasiHambatanCemas { get; set; }
        public bool KebutuhanKomunikasiHambatanKognitif { get; set; }
        public bool KebutuhanKomunikasiHambatanEmosi { get; set; }
        public bool KebutuhanKomunikasiHambatanMenulis { get; set; }
        public bool KebutuhanKomunikasiHambatanPendengaran { get; set; }
        public bool KebutuhanKomunikasiHambatanKesulitanBicara { get; set; }
        public bool KebutuhanKomunikasiHambatanAudioVisual { get; set; }
        public bool KebutuhanKomunikasiHambatanHilangMemori { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakAdaPartisipasi { get; set; }
        public bool KebutuhanKomunikasiHambatanDiskusi { get; set; }
        public bool KebutuhanKomunikasiHambatanMotivasiBuruk { get; set; }
        public bool KebutuhanKomunikasiHambatanSecaraFisiologi { get; set; }
        public bool KebutuhanKomunikasiHambatanMembaca { get; set; }
        public bool KebutuhanKomunikasiHambatanMasalahPenglihatan { get; set; }
        public bool KebutuhanKomunikasiHambatanTidakDitemukanHambatan { get; set; }
        public bool KebutuhanKomunikasiHambatanMendengar { get; set; }
        public bool KebutuhanKomunikasiHambatanDemontrasi { get; set; }
        public string KebutuhanKomunikasiTingkatkanPendidikan { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhan { get; set; }
        public string KebutuhanKomunikasiPotensialKebutuhanLainnya { get; set; }
        public bool KebutuhanPrivasiIdentitas { get; set; }
        public bool KebutuhanPrivasiRekamMedis { get; set; }
        public bool KebutuhanPrivasiSaatPemeriksaan { get; set; }
        public bool KebutuhanPrivasiSaatTindakanMedis { get; set; }
        public bool KebutuhanPrivasiTransportasi { get; set; }
        public string KesulitanBernafas { get; set; }
        public bool KesulitanBernafasNasalCanule { get; set; }
        public bool KesulitanBernafasSungkup { get; set; }
        public bool KesulitanBernafasReBrething { get; set; }
        public bool IntegritasKulit_Resh { get; set; }
        public bool IntegritasKulit_Lesi { get; set; }
        public bool IntegritasKulit_Parut { get; set; }
        public bool IntegritasKulit_Memar { get; set; }
        public bool IntegritasKulit_Pucat { get; set; }
        public bool IntegritasKulit_Kuning { get; set; }
        public bool IntegritasKulit_Sinotik { get; set; }
        public bool IntegritasKulit_Berkeringat { get; set; }
        public string IntegritasKulit_ResikoDekubitus { get; set; }
        public string IntegritasKulit_ResikoDekubitusSkore { get; set; }
        public string IntegritasKulit_Luka { get; set; }
        public string IntegritasKulit_LukaLokasi { get; set; }
        public string IntegritasKulit_LukaPengisianFormLuka { get; set; }
        public string EliminasiPerkemihan { get; set; }
        public bool EliminasiPerkemihan_Stoma { get; set; }
        public bool EliminasiPerkemihan_Stricture { get; set; }
        public bool EliminasiPerkemihan_Retensi { get; set; }
        public bool EliminasiPerkemihan_InkontinesiaUrine { get; set; }
        public bool EliminasiPerkemihan_Dialysis { get; set; }
        public string EliminasiDefekasi { get; set; }
        public bool EliminasiDefekasi_Atoma { get; set; }
        public bool EliminasiDefekasi_Atheresia { get; set; }
        public bool EliminasiDefekasi_Konstipasi { get; set; }
        public bool EliminasiDefekasi_InkontinesiaAlvi { get; set; }
        public bool EliminasiDefekasi_Diarea { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> EstimasiTanggalPemulangan { get; set; }
        public string PasienPulangKe { get; set; }
        public string EdukasiPasienPulang { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string TTDPasien { get; set; }
        public string TTDPerawat { get; set; }
        public bool LokasiRuangan { get; set; }
        public bool KemananRuangan { get; set; }
        public bool TatatertibRuangan { get; set; }
        public bool WaktuDokterVisite { get; set; }
        public bool JamBerkunjung { get; set; }
        public bool Administrasi { get; set; }
        public bool TempatIbadah { get; set; }
        public bool FasilitasRuangan { get; set; }
        public bool PelayananGizi { get; set; }
        public bool KebersihanKamar { get; set; }
        public bool RencanaPerawat { get; set; }
        public bool KebutuhanKomunikasiTidakDitemukanHambatan { get; set; }
        public bool VerifikasiPerawat { get; set; }

        public List<SelectItemListPengkajianKeperawatan> ListTemplate { get; set; }

        // disable all form

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
        public int MODEVIEW { get; set; }
    }

    public class SelectItemListPengkajianKeperawatan
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}