﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRClinicalPathwaysDemamTeroid_DetailController : Controller
    {
        // GET: EMRClinicalPathwaysDemamTeroid_Detail
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRClinicalPathwaysDemamTiroidViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.ClinicalPathwaysDemamTiroid.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRClinicalPathwaysDemamTiroidViewModel>(dokumen);

                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaDPJP);
                        if (dpjp != null) model.NamaDPJPNama = dpjp.NamaDOkter;

                        var ppjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaPPJP);
                        if (ppjp != null) model.NamaPPJPNama = ppjp.NamaDOkter;

                        #region === Detail Aspek
                        model.Aspek_List = new ListDetail<EMRAspekPelayananClinicalPathwaysDemamTeroid_DetailDetailModel>();
                        var detail_1 = s.AspekPelayananClinicalPathwaysDemamTeroid_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRAspekPelayananClinicalPathwaysDemamTeroid_DetailDetailModel>(x);
                            model.Aspek_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Variasi
                        model.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysDemamTeroid_DetailDetailModel>();
                        var detail_2 = s.VariasiClinicalPathwaysDemamTiroid_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRVariasiClinicalPathwaysDemamTeroid_DetailDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Variasi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;

                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListClinicalDemamTiroid>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListClinicalDemamTiroid()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRClinicalPathwaysDemamTiroidViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.ClinicalPathwaysDemamTiroid.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRClinicalPathwaysDemamTiroidViewModel>(dokumen);
                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaDPJP);
                        if (dpjp != null) model.NamaDPJPNama = dpjp.NamaDOkter;

                        var ppjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaPPJP);
                        if (ppjp != null) model.NamaPPJPNama = ppjp.NamaDOkter;

                        #region === Detail Aspek
                        model.Aspek_List = new ListDetail<EMRAspekPelayananClinicalPathwaysDemamTeroid_DetailDetailModel>();
                        var detail_1 = s.AspekPelayananClinicalPathwaysDemamTeroid_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRAspekPelayananClinicalPathwaysDemamTeroid_DetailDetailModel>(x);
                            model.Aspek_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Variasi
                        model.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysDemamTeroid_DetailDetailModel>();
                        var detail_2 = s.VariasiClinicalPathwaysDemamTiroid_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRVariasiClinicalPathwaysDemamTeroid_DetailDetailModel>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Variasi_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListClinicalDemamTiroid>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListClinicalDemamTiroid()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRClinicalPathwaysDemamTiroidViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ClinicalPathwaysDemamTiroid.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    if (model == null)
                    {
                        var o = IConverter.Cast<ClinicalPathwaysDemamTiroid>(item);
                        s.ClinicalPathwaysDemamTiroid.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Crinical Pathways DemamTiroid";
                    }
                    else
                    {
                        model = IConverter.Cast<ClinicalPathwaysDemamTiroid>(item);
                        s.ClinicalPathwaysDemamTiroid.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Crinical Pathways DemamTiroid";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.NamaDPJP;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    #region === Detail Aspek
                    if (item.Aspek_List == null) item.Aspek_List = new ListDetail<EMRAspekPelayananClinicalPathwaysDemamTeroid_DetailDetailModel>();
                    item.Aspek_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Aspek_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Aspek_List;
                    var real_list = s.AspekPelayananClinicalPathwaysDemamTeroid_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AspekPelayananClinicalPathwaysDemamTeroid_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AspekPelayananClinicalPathwaysDemamTeroid_Detail.Add(IConverter.Cast<AspekPelayananClinicalPathwaysDemamTeroid_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Hari = x.Model.Hari;
                            _m.MenilaiMemeriksa_1 = x.Model.MenilaiMemeriksa_1;
                            _m.MenilaiFungsi_1 = x.Model.MenilaiFungsi_1;
                            _m.MenilaiTingkatKesadaran_1 = x.Model.MenilaiTingkatKesadaran_1;
                            _m.MenilaiKeparahan_1 = x.Model.MenilaiKeparahan_1;
                            _m.MenilaiTanda_2 = x.Model.MenilaiTanda_2;
                            _m.MenilaiEfektivitas_2 = x.Model.MenilaiEfektivitas_2;
                            _m.MenilaiKesadaran_2 = x.Model.MenilaiKesadaran_2;
                            _m.MemeriksaDL_3 = x.Model.MemeriksaDL_3;
                            _m.MemeriksaBUN_3 = x.Model.MemeriksaBUN_3;
                            _m.MemasangInfus_4 = x.Model.MemasangInfus_4;
                            _m.MelepasInfus_4 = x.Model.MelepasInfus_4;
                            _m.MemberiIntravena_4 = x.Model.MemberiIntravena_4;
                            _m.MemberiNutrisi_5 = x.Model.MemberiNutrisi_5;
                            _m.MemberiCairan_5 = x.Model.MemberiCairan_5;
                            _m.MemberiIntravena_5 = x.Model.MemberiIntravena_5;
                            _m.InfusNormal_6 = x.Model.InfusNormal_6;
                            _m.Antimetic_6 = x.Model.Antimetic_6;
                            _m.Mukoprotektor_6 = x.Model.Mukoprotektor_6;
                            _m.Antipiretik_6 = x.Model.Antipiretik_6;
                            _m.Antibiotic_6 = x.Model.Antibiotic_6;
                            _m.PPI_6 = x.Model.PPI_6;
                            _m.DietLunak_7 = x.Model.DietLunak_7;
                            _m.DietPadat_7 = x.Model.DietPadat_7;
                            _m.Intake_7 = x.Model.Intake_7;
                            _m.Cairan_7 = x.Model.Cairan_7;
                            _m.MobilisasiAktifTerbatas_8 = x.Model.MobilisasiAktifTerbatas_8;
                            _m.MobilisasiToileting_8 = x.Model.MobilisasiToileting_8;
                            _m.MobilisasiKeluarRuangan_8 = x.Model.MobilisasiKeluarRuangan_8;
                            _m.KonsultasiGizi_9 = x.Model.KonsultasiGizi_9;
                            _m.KonselingDiagnosa_10 = x.Model.KonselingDiagnosa_10;
                            _m.KonselingDukungan_10 = x.Model.KonselingDukungan_10;
                            _m.EdukasiDiet_11 = x.Model.EdukasiDiet_11;
                            _m.EdukasiCaraMinum_11 = x.Model.EdukasiCaraMinum_11;
                            _m.EdukasiControl_11 = x.Model.EdukasiControl_11;
                            _m.Simptom_12 = x.Model.Simptom_12;
                            _m.PerbaikanIntake_12 = x.Model.PerbaikanIntake_12;
                            _m.KeadaanUmum_12 = x.Model.KeadaanUmum_12;
                        }
                    }
                    #endregion

                    #region === Detail Variasi
                    if (item.Variasi_List == null) item.Variasi_List = new ListDetail<EMRVariasiClinicalPathwaysDemamTeroid_DetailDetailModel>();
                    item.Variasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Variasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Variasi_List;
                    var real_list1 = s.VariasiClinicalPathwaysDemamTiroid_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.VariasiClinicalPathwaysDemamTiroid_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.VariasiClinicalPathwaysDemamTiroid_Detail.Add(IConverter.Cast<VariasiClinicalPathwaysDemamTiroid_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.VariasiPelayanan = x.Model.VariasiPelayanan;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Alasan = x.Model.Alasan;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}