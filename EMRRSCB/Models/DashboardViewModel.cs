﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class DashboardViewModel
    {
        public string Deskripsi { get; set; }
        public string Nilai { get; set; }
        public string Tipe { get; set; }
    }
}