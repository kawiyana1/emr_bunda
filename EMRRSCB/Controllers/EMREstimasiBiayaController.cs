﻿//using EMRRSCB.Entities.EMR;
//using EMRRSCB.Models;
//using iHos.MVC.Converter;
//using iHos.MVC.Property;
//using Microsoft.AspNet.Identity;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Migrations;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Linq.Dynamic;
//using System.Web;
//using System.Web.Mvc;

//namespace EMRRSCB.Controllers
//{
//    public class EMREstimasiBiayaController : Controller
//    {
//        // GET: EMREstimasiBiaya
//        [HttpGet]
//        [ActionName("Create")]
//        public ActionResult CreateGet(string id, int view = 0)
//        {
//            var model = new EMREstimasiBiayaViewModel();

//            using (var s = new EMREntities())
//            {
//                var dokumen = s.EstimasiBiaya.FirstOrDefault(x => x.NoBukti == id);
//                if (dokumen != null)
//                {
//                    model = IConverter.Cast<EMREstimasiBiayaViewModel>(dokumen);
//                    var dpjp = s.mDokter.FirstOrDefault(e => e.DokterID == model.NamaTandaTanganPetugasNama);
//                    if (dpjp != null) model.NamaTandaTanganPetugasNama = dpjp.NamaDokter;

//                    model.MODEVIEW = view;
//                }
//                else
//                {
//                    model.Tanggal = DateTime.Today;
//                    model.Jam = DateTime.Now;
//                    model.NoBukti = id;
//                }

//            }
//            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
//            if (Request.IsAjaxRequest())
//                return PartialView(model);
//            else
//                return View(model);
//        }

//        [HttpPost]
//        [ActionName("Create")]
//        [ValidateAntiForgeryToken]
//        public string CreatePost()
//        {
//            try
//            {
//                var item = new EMRPenyakitDalamViewModel();
//                TryUpdateModel(item);
//                ResultSS result;
//                using (var s = new EMREntities())
//                {
//                    var model = s.EstimasiBiaya.FirstOrDefault(x => x.NoBukti == item.NoBukti);
//                    var activity = "";
//                    if (model == null)
//                    {
//                        var o = IConverter.Cast<EstimasiBiaya>(item);
//                        s.EstimasiBiaya.Add(o);

//                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
//                        header.Simpan = true;
//                        header.TanggalCreate = DateTime.Now;
//                        header.CretaedBy = User.Identity.GetUserName();
//                        header.UserIdWeb_Created = User.Identity.GetUserId();

//                        activity = "Create Estimasi Biaya";
//                    }
//                    else
//                    {
//                        model = IConverter.Cast<EstimasiBiaya>(item);
//                        s.EstimasiBiaya.AddOrUpdate(model);

//                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
//                        header.Simpan = true;
//                        header.TanggalUpdate = DateTime.Now;
//                        header.UpdatedBy = User.Identity.GetUserName();
//                        header.UserIdWeb_Updated = User.Identity.GetUserId();

//                        activity = "Update Estimasi Biaya";
//                    }

//                    s.SaveChanges();

//                    result = new ResultSS(1);

//                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
//                    {
//                        Activity = $" {activity} {item.NoBukti}"
//                    };
//                    UserActivity.InsertUserActivity(userActivity);
//                }
//                return JsonHelper.JsonMsgCreate(result);
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }
//    }
//}