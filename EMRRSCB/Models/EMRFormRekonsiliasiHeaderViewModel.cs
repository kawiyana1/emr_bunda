﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormRekonsiliasiHeaderViewModel
    {
        public string NoBukti { get; set; }
        public string PetugasAdmisi { get; set; }
        public string PetugasAdmisiNama { get; set; }
        public bool VerifikasiPetugasAdmisi { get; set; }
        public string Username { get; set; }
        public string PetugasTransfer { get; set; }
        public string PetugasTransferNama { get; set; }
        public bool VerifikasiPetugasTransfer { get; set; }
        public ListDetail<EMRFormRekonsiliasiObat_DetailDetailModel> EMRFormRekonsiliasiObat_List { get; set; }
        public ListDetail<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel> EMRRekonsiliasiBHP_List { get; set; }
        public ListDetail<EMRFormRekonsiliasiObat_SaatTransferDetailModel> EMRRekonsiliasiTransfer_List { get; set; }
        public ListDetail<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel> EMRRekonsiliasiKeluarRS_List { get; set; }
        public int MODEVIEW { get; set; }
        public int Report { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMRS { get; set; }
        public List<SelectItemList_RekonsiliasiObat> ListTemplate { get; set; }
    }

    public class SelectItemList_RekonsiliasiObat
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}