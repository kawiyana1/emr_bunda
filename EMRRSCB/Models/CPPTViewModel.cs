﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class CPPTViewModel
    {
        public string _METHOD { get; set; }
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Jam_View { get; set; }
        public string Profesi { get; set; }
        public string CPPT_S { get; set; }
        public string CPPT_O { get; set; }
        public string CPPT_A { get; set; }
        public string CPPT_P { get; set; }
        public string CPPT_I { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Edit { get; set; }
        public bool saveTemplate { get; set; }
        public string templateName { get; set; }
        public string CPPT_OGambar { get; set; }
        public string TandaTangan { get; set; }
        public string TTDDPJP { get; set; }
        public string TTDCPPT { get; set; }
        public string Petugas_1 { get; set; }
        public string Petugas_2 { get; set; }
        public string MenyerahkanPasien { get; set; }
        public string MenyerahkanPasienNama { get; set; }
        public string MenerimaPasien { get; set; }
        public string MenerimaPasienNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamBacaUlang { get; set; }
        public string PetugasBacaUlang { get; set; }
        public string PetugasBacaUlangNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }

        [DataType(DataType.Time)]

        public Nullable<System.DateTime> JamKonfirmasi { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public string PetugasKonfirmasiNama { get; set; }
        public string PetugasBacaUlangTTD { get; set; }
        public string PetugasKonfirmasiTTD { get; set; }
        public string MenyerahkanPasienTTD { get; set; }
        public string MenerimaPasienTTD { get; set; }


        public Nullable<System.DateTime> TanggalInput { get; set; }
        public string UserName { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> T_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> T_Tanggal { get; set; }
        public string T_Petugas { get; set; }
        public string T_Petugas_Nama { get; set; }
        public string T_TTD { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> B_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> B_Tanggal { get; set; }
        public string B_TTD { get; set; }
        public string B_Petugas { get; set; }
        public string B_Petugas_Nama { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> K_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> K_Tanggal { get; set; }
        public string K_Petugas { get; set; }
        public string K_Petugas_Nama { get; set; }
        public string K_TTD { get; set; }

        public string Petugas_1_Nama { get; set; }
        public string Petugas_2_Nama { get; set; }
        public string Petugas_1_TTD { get; set; }
        public string Petugas_2_TTD { get; set; }
        public string SudahTBK { get; set; }
        public bool VerifikasiPetugasCPPT { get; set; }
        public bool VerifikasiPetugasBacaUlangCPPT { get; set; }
        public bool VerifikasiPetugasKonfirmasiCPPT { get; set; }
        


        public int MODEVIEW { get; set; }

    }
}