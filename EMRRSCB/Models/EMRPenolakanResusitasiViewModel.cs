﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPenolakanResusitasiViewModel
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nama { get; set; }
        public string MR { get; set; }
        public string Kamar { get; set; }
        public string Nama1 { get; set; }
        public string UmurTahun1 { get; set; }
        public string UmurBulan1 { get; set; }
        public string JenisKelamin1 { get; set; }
        public string Alamat1 { get; set; }
        public string Nama2 { get; set; }
        public string UmurTahun2 { get; set; }
        public string UmurBulan2 { get; set; }
        public string JenisKelamin2 { get; set; }
        public string Alamat2 { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Saksi { get; set; }
        public string Pasien { get; set; }
        public string TTDPetugas { get; set; }
        public string TTDSaksi { get; set; }
        public string TTDPasien { get; set; }


        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public int MODEVIEW { get; set; }
    }
}