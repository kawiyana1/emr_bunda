﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanPerkembanganPasienSarafDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Keluhan { get; set; }
        public string Diagnosa { get; set; }
        public string HasilPenunjang { get; set; }
        public string Terapi { get; set; }
        public string Keterangan { get; set; }
        public string Username { get; set; }
    }
}