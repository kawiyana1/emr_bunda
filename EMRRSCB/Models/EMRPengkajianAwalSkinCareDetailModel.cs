﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianAwalSkinCareDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Anamanesa { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
        public string Alergi { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Username { get; set; }
    }
}