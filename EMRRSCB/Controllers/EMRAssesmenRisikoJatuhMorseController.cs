﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRAssesmenRisikoJatuhMorseController : Controller
    {
        // GET: EMRAssesmenRisikoJatuhMorse
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.AssesmenRisikoJatuhMorse.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRHeaderViewDetail>(dokumen);

                        model.NoBukti = id;
                        
                        model.Diagnosa = dokumen.Diagnosa;
                        #region === Intervensi Detail
                        model.EMRAssMorse_List = new ListDetail<EMRAssesmenRisikoJatuhMorseViewModel>();
                        var detail_2 = s.AssesmenRisikoJatuhMorse.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRAssesmenRisikoJatuhMorseViewModel>(x);
                           
                            model.EMRAssMorse_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        if (soap != null)
                        {
                            model.Diagnosa = " SOAP A : " + soap.CPPT_A + "\n";
                        }
                        else
                        {
                            model.Diagnosa = "SOAP A : -";
                        }
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.AssesmenRisikoJatuhMorse.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AssesmenRisikoJatuhMorse>(item);
                        s.AssesmenRisikoJatuhMorse.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Form A";
                    }
                    else
                    {
                        model = IConverter.Cast<AssesmenRisikoJatuhMorse>(item);
                        s.AssesmenRisikoJatuhMorse.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Form A";
                    }

                    #region === Detail Intervensi
                    if (item.EMRAssMorse_List == null) item.EMRAssMorse_List = new ListDetail<EMRAssesmenRisikoJatuhMorseViewModel>();
                    item.EMRAssMorse_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRAssMorse_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                        x.Model.TanggalHeader = DateTime.Today;
                        x.Model.JamHeader = DateTime.Now;
                        x.Model.Diagnosa = item.Diagnosa;
                    }
                    var new_list1 = item.EMRAssMorse_List;
                    var real_list1 = s.AssesmenRisikoJatuhMorse.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AssesmenRisikoJatuhMorse.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        // add | add where (new_list not_in raal_list)
                        if (_m == null)
                        {
                            _m = new AssesmenRisikoJatuhMorse();
                            s.AssesmenRisikoJatuhMorse.Add(IConverter.Cast<AssesmenRisikoJatuhMorse>(x.Model));

                        }
                        // edit | where (new_list in raal_list)
                        else
                        {
                            //update
                            _m = IConverter.Cast<AssesmenRisikoJatuhMorse>(x.Model);
                            s.AssesmenRisikoJatuhMorse.AddOrUpdate(_m);
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}