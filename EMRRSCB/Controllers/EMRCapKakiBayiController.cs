﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRCapKakiBayiController : Controller
    {
        // GET: EMRCapKakiBayi
        public ActionResult Index()
        {
            return View();
        }

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRCapKakiBayiViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.CapKakiBayi.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRCapKakiBayiViewModel>(dokumen);

                        ViewBag.NoBukti = id;
                        ViewBag.KakiKiri = Convert.ToBase64String(dokumen.KakiKiri); 
                        ViewBag.KakiKanan = Convert.ToBase64String(dokumen.KakiKanan);
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        ViewBag.NoBukti = id;
                        model.NoBukti = id;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post(string nobutki, HttpPostedFileBase fileKakiKiri, HttpPostedFileBase fileKakiKanan)
        {
            try
            {
                var result = new ResultSS();
                using (var s = new EMREntities())
                {
                    var m = s.CapKakiBayi.FirstOrDefault(x => x.NoBukti == nobutki);
                    if (m != null)
                    {
                        if(fileKakiKiri == null || fileKakiKanan == null) throw new Exception("Ambil Ulang Gambar kedua kaki");
                        m.KakiKiri = GetByteArrayFromImageKiri(fileKakiKiri);
                        m.KakiKanan = GetByteArrayFromImageKanan(fileKakiKanan);
                        result = new ResultSS(s.SaveChanges());
                        return JsonHelper.JsonMsgEdit(result, -1);
                    }
                    else
                    {
                        s.CapKakiBayi.Add(new CapKakiBayi()
                        {
                            KakiKiri = GetByteArrayFromImageKiri(fileKakiKiri),
                            KakiKanan = GetByteArrayFromImageKanan(fileKakiKanan),
                            NoBukti = nobutki
                        });
                        result = new ResultSS(s.SaveChanges());
                        return JsonHelper.JsonMsgCreate(result);
                    }
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        public static byte[] GetByteArrayFromImageKiri(HttpPostedFileBase fileKakiKiri)
        {
            using (var target = new MemoryStream())
            {
                fileKakiKiri.InputStream.CopyTo(target);
                return target.ToArray();
            }
        }

        public static byte[] GetByteArrayFromImageKanan(HttpPostedFileBase fileKakiKanan)
        {
            using (var target = new MemoryStream())
            {
                fileKakiKanan.InputStream.CopyTo(target);
                return target.ToArray();
            }
        }
    }
}