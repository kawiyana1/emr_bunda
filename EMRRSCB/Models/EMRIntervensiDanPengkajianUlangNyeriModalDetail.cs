﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRIntervensiDanPengkajianUlangNyeriModalDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SkorNyeri { get; set; }
        public string SkorSedasi { get; set; }
        public string TekananDarah { get; set; }
        public string Suhu { get; set; }
        public string Respitrasi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public bool VerifikasiPetugas { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_Farma { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_Farma { get; set; }
        public string NamaObat { get; set; }
        public string Dosis { get; set; }
        public string Rute { get; set; }
        public string IntervensiNonFarmakologi { get; set; }
        public string PetugasNonFarmakologi { get; set; }
        public string PetugasNonFarmakologiNama { get; set; }
        public bool VerifikasiNonFarmakologi { get; set; }
        public string WaktuPengkajian { get; set; }
    }
}