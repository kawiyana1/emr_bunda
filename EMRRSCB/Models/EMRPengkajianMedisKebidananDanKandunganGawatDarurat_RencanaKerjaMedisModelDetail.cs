﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RencanaKerjaMedisModelDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string DaftarMasalah { get; set; }
        public string RencanaIntervensi { get; set; }
        public string Target { get; set; }
        public string Username { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }

    }
}