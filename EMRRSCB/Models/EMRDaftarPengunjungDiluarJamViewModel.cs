﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRDaftarPengunjungDiluarJamViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string NamaPengunjung { get; set; }
        public string Keterangan { get; set; }

        public int MODEVIEW { get; set; }
    }
}