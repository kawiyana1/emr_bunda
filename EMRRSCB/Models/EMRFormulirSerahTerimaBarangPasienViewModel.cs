﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormulirSerahTerimaBarangPasienViewModel
    {
        public ListDetail<EMRFormulirSerahTerimaBarangPasienDetailDetailModel> Detail_List { get; set; }

        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DataPasienNama { get; set; }
        public string DataPasienUmur { get; set; }
        public string DataPasienNomorRM { get; set; }
        public string DataPetugasNama { get; set; }
        public string DataPetugasAlamat { get; set; }
        public string DataPetugasNoHp { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Satpam { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi2 { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDSatpam { get; set; }
        public string TTDSaksi { get; set; }
        public string TTDSaksi2 { get; set; }


        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }

        //public string nama_template { get; set; }
        //public bool save_template { get; set; }
        //public int dokumenid { get; set; }
        //public string PilihICD { get; set; }
        //public string PilihICDNama { get; set; }
        //public List<SelectItemListPoliTHT> ListTemplate { get; set; }

        // disable all form 
        public int MODEVIEW { get; set; }
    }

    //public class SelectItemListPoliTHT
    //{
    //    public string Text { get; set; }
    //    public string Value { get; set; }
    //}
}