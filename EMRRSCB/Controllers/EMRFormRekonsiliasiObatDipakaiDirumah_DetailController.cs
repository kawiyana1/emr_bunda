﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormRekonsiliasiObatDipakaiDirumah_DetailController : Controller
    {
        // GET: EMRFormRekonsiliasiObatDipakaiDirumah_Detail
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRFormRekonsiliasiHeaderViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.FormRekonsiliasiHeader.Where(x => x.NoBukti == id).FirstOrDefault(x => x.NoBukti == id);
                    var tglMRS = sim.VW_Registrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormRekonsiliasiHeaderViewModel>(dokumen);
                        model.NoBukti = id;

                        var petugasadmisi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PetugasAdmisi);
                        if (petugasadmisi != null) model.PetugasAdmisiNama = petugasadmisi.NamaDOkter;

                        var petugastransfer = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PetugasTransfer);
                        if (petugastransfer != null) model.PetugasTransferNama = petugastransfer.NamaDOkter;

                        #region Detail Form Obat
                        model.EMRFormRekonsiliasiObat_List = new ListDetail<EMRFormRekonsiliasiObat_DetailDetailModel>();
                        var detail_1 = s.FormRekonsiliasiObat_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_DetailDetailModel>(x);


                            var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.DaftarObat);
                            if (getobat != null) y.DaftarObatNama = getobat.Nama_Barang;


                            model.EMRFormRekonsiliasiObat_List.Add(false, y);
                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form BHP
                        model.EMRRekonsiliasiBHP_List = new ListDetail<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel>();
                        var detail_2 = s.FormRekonsiliasiObatDipakaiDirumah_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel>(x);


                            var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.NamaObat);
                            if (getobat != null) y.NamaObatNama = getobat.Nama_Barang;

                            model.EMRRekonsiliasiBHP_List.Add(false, y);
                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form Rekonsiliasi Transfer
                        model.EMRRekonsiliasiTransfer_List = new ListDetail<EMRFormRekonsiliasiObat_SaatTransferDetailModel>();
                        var detail_3 = s.FormRekonsiliasiObat_SaatTransfer.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_SaatTransferDetailModel>(x);
                            model.EMRRekonsiliasiTransfer_List.Add(false, y);


                            //var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.NamaObat);
                            //if (getobat != null) y.NamaObatNama = getobat.Nama_Barang;

                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form Rekonsiliasi Keluar RS
                        model.EMRRekonsiliasiKeluarRS_List = new ListDetail<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel>();
                        var detail_4 = s.FormRekonsiliasiObat_SaatKeluarRumahSakit.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel>(x);
                            model.EMRRekonsiliasiKeluarRS_List.Add(false, y);


                            //var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.NamaObat);
                            //if (getobat != null) y.NamaObatNama = getobat.Nama_Barang;

                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;
                    if (tglMRS == null)
                    {
                        model.TanggalMRS = DateTime.Today;
                    }
                    else { 
                        model.TanggalMRS = tglMRS.TglReg;
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemList_RekonsiliasiObat>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemList_RekonsiliasiObat()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var model = new EMRFormRekonsiliasiHeaderViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = s.FormRekonsiliasiHeader.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormRekonsiliasiHeaderViewModel>(dokumen);
                        model.NoBukti = id;

                        var petugasadmisi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PetugasAdmisi);
                        if (petugasadmisi != null) model.PetugasAdmisiNama = petugasadmisi.NamaDOkter;

                        var petugastransfer = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PetugasTransfer);
                        if (petugastransfer != null) model.PetugasTransferNama = petugastransfer.NamaDOkter;

                        #region Detail Form Obat
                        model.EMRFormRekonsiliasiObat_List = new ListDetail<EMRFormRekonsiliasiObat_DetailDetailModel>();
                        var detail_1 = s.FormRekonsiliasiObat_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_DetailDetailModel>(x);


                            var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.DaftarObat);
                            if (getobat != null) y.DaftarObatNama = getobat.Nama_Barang;


                            model.EMRFormRekonsiliasiObat_List.Add(false, y);
                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form BHP
                        model.EMRRekonsiliasiBHP_List = new ListDetail<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel>();
                        var detail_2 = s.FormRekonsiliasiObatDipakaiDirumah_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel>(x);


                            var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.NamaObat);
                            if (getobat != null) y.NamaObatNama = getobat.Nama_Barang;

                            model.EMRRekonsiliasiBHP_List.Add(false, y);
                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form Rekonsiliasi Transfer
                        model.EMRRekonsiliasiTransfer_List = new ListDetail<EMRFormRekonsiliasiObat_SaatTransferDetailModel>();
                        var detail_3 = s.FormRekonsiliasiObat_SaatTransfer.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_SaatTransferDetailModel>(x);
                            model.EMRRekonsiliasiTransfer_List.Add(false, y);


                            //var getobat = sim.mBarang.FirstOrDefault(e => e.Kode_Barang == y.NamaObat);
                            //if (getobat != null) y.NamaObatNama = getobat.Nama_Barang;

                            //model.Diagnosa = y.Diagnosa;
                            //model.Alergi = y.Alergi;
                            //model.TB = y.TB;
                            //model.BB = y.BB;
                        }
                        #endregion

                        #region Detail Form Rekonsiliasi Keluar RS
                        model.EMRRekonsiliasiKeluarRS_List = new ListDetail<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel>();
                        var detail_4 = s.FormRekonsiliasiObat_SaatKeluarRumahSakit.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel>(x);
                            model.EMRRekonsiliasiKeluarRS_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.VerifikasiPetugasAdmisi = false;
                    model.VerifikasiPetugasTransfer = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemList_RekonsiliasiObat>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemList_RekonsiliasiObat()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRFormRekonsiliasiHeaderViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.FormRekonsiliasiHeader.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormRekonsiliasiHeader>(item);
                        s.FormRekonsiliasiHeader.Add(o);
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Form Rekonsiliasi Obat RI";
                    }
                    else
                    {
                        model = IConverter.Cast<FormRekonsiliasiHeader>(item);
                        s.FormRekonsiliasiHeader.AddOrUpdate(model);
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Form Rekonsiliasi Obat RI";
                    }
                    #region Detail From Rekonsiliasi Obat
                    if (item.EMRFormRekonsiliasiObat_List == null) item.EMRFormRekonsiliasiObat_List = new ListDetail<EMRFormRekonsiliasiObat_DetailDetailModel>();
                    item.EMRFormRekonsiliasiObat_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRFormRekonsiliasiObat_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRFormRekonsiliasiObat_List;
                    var real_list = s.FormRekonsiliasiObat_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormRekonsiliasiObat_Detail.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.FormRekonsiliasiObat_Detail.Add(IConverter.Cast<FormRekonsiliasiObat_Detail>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.DaftarObat = x.Model.DaftarObat;
                            _m.DaftarObatNama = x.Model.DaftarObatNama;
                            _m.SeberapaBeratAlergi = x.Model.SeberapaBeratAlergi;
                            _m.ReaksiAlergi = x.Model.ReaksiAlergi;
                        }
                    }
                    #endregion

                    #region Detail From Rekonsiliasi BHP
                    if (item.EMRRekonsiliasiBHP_List == null) item.EMRRekonsiliasiBHP_List = new ListDetail<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel>();
                    item.EMRRekonsiliasiBHP_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRRekonsiliasiBHP_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.EMRRekonsiliasiBHP_List;
                    var real_list1 = s.FormRekonsiliasiObatDipakaiDirumah_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormRekonsiliasiObatDipakaiDirumah_Detail.Remove(x);
                    }
                    foreach (var x in new_list1)
                    {


                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.FormRekonsiliasiObatDipakaiDirumah_Detail.Add(IConverter.Cast<FormRekonsiliasiObatDipakaiDirumah_Detail>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.NamaObat = x.Model.NamaObat;
                            _m.AturanPakaiRute = x.Model.AturanPakaiRute;
                            _m.PerubahanAturanPakai = x.Model.PerubahanAturanPakai;
                            _m.TindakLanjut = x.Model.TindakLanjut;
                            _m.BerlanjutRanap = x.Model.BerlanjutRanap;
                            _m.BerlanjutRanap2 = x.Model.BerlanjutRanap2;
                        }
                    }
                    #endregion

                    #region Detail From Transfer
                    if (item.EMRRekonsiliasiTransfer_List == null) item.EMRRekonsiliasiTransfer_List = new ListDetail<EMRFormRekonsiliasiObat_SaatTransferDetailModel>();
                    item.EMRRekonsiliasiTransfer_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRRekonsiliasiTransfer_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list2 = item.EMRRekonsiliasiTransfer_List;
                    var real_list2 = s.FormRekonsiliasiObat_SaatTransfer.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormRekonsiliasiObat_SaatTransfer.Remove(x);
                    }
                    foreach (var x in new_list2)
                    {


                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.FormRekonsiliasiObat_SaatTransfer.Add(IConverter.Cast<FormRekonsiliasiObat_SaatTransfer>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.NamaObat = x.Model.NamaObat;
                            _m.AturanPakai = x.Model.AturanPakai;
                            _m.TindakLanjutDPJP = x.Model.TindakLanjutDPJP;
                            _m.PerubahanAturanPakai = x.Model.PerubahanAturanPakai;
                        }
                    }
                    #endregion

                    #region Detail From Keluar RS
                    if (item.EMRRekonsiliasiKeluarRS_List == null) item.EMRRekonsiliasiKeluarRS_List = new ListDetail<EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel>();
                    item.EMRRekonsiliasiKeluarRS_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRRekonsiliasiKeluarRS_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list3 = item.EMRRekonsiliasiKeluarRS_List;
                    var real_list3 = s.FormRekonsiliasiObat_SaatKeluarRumahSakit.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list3)
                    {
                        var m = new_list3.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormRekonsiliasiObat_SaatKeluarRumahSakit.Remove(x);
                    }
                    foreach (var x in new_list3)
                    {


                        var _m = real_list3.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.FormRekonsiliasiObat_SaatKeluarRumahSakit.Add(IConverter.Cast<FormRekonsiliasiObat_SaatKeluarRumahSakit>(x.Model));
                        else
                        {

                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.NamaObat = x.Model.NamaObat;
                            _m.AturanPakaiDanRute = x.Model.AturanPakaiDanRute;
                            _m.TindakLanjut = x.Model.TindakLanjut;
                            _m.PerubahanAturanPakai = x.Model.PerubahanAturanPakai;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}