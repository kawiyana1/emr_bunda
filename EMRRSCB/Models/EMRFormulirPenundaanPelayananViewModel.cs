﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace EMRRSCB.Models
{
    public class EMRFormulirPenundaanPelayananViewModel
    {

        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPukul { get; set; }
        public string Ruangan { get; set; }
        public bool PenundaanPelayananDokter1 { get; set; }
        public bool PenundaanPelayananGizi { get; set; }
        public bool PenundaanPelayananFarmasi { get; set; }
        public bool PenundaanPelayananLaboratorium { get; set; }
        public bool PenundaanPelayananPerawat { get; set; }
        public bool PenundaanPelayananTindakanOperasi { get; set; }
        public bool DokterBelumDatang { get; set; }
        public bool FasilitasTidakAda { get; set; }
        public bool KeluargaMenolak { get; set; }
        public bool DokterBerhalangan { get; set; }
        public bool FasilitasRusak { get; set; }
        public bool PetugasBelumDatang { get; set; }
        public bool LainLainBit { get; set; }
        public string LainLainText { get; set; }
        public string AlternatifYangDitawarkan { get; set; }
        public string ManfaatTindakanAlternative { get; set; }
        public string ManfaatPenundaan { get; set; }
        public string PernyataanPasien { get; set; }
        public string PernyataanRs { get; set; }
        public string Nama { get; set; }
        public string NRM { get; set; }
        public string tglLahir { get; set; }

        public string JenisKelain { get; set; }

        public int MODEVIEW { get; set; }



    }
}