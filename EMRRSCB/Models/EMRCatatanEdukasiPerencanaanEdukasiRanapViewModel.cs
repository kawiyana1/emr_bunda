﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanEdukasiPerencanaanEdukasiRanapViewModel
    {
        public ListDetail<EMRCatatanEdukasiRawatJalanViewModel> Edukasi_List { get; set; }

        public string NoBukti { get; set; }
        public string DokterSpesialis_F { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DokterSpesialis_Tanggal { get; set; }
        public string DokterSpesialis_Metode { get; set; }
        public string DokterSpesialis_Edukator { get; set; }
        public string DokterSpesialis_EdukatorNama { get; set; }
        public string DokterSpesialis_Keluarga { get; set; }
        public bool DokterSpesialis_TidakRespon { get; set; }
        public bool DokterSpesialis_TidakPaham { get; set; }
        public bool DokterSpesialis_Paham { get; set; }
        public bool DokterSpesialis_MenjelaskanTanpaDiBantu { get; set; }
        public bool DokterSpesialis_MenjelaskanDiBantu { get; set; }
        public bool DokterSpesialis_MampuMendemontrasikan { get; set; }
        public string DokterSpesialis_MampuMendemontrasikanKet { get; set; }
        public string Nutrisi_C { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Nutrisi_Tanggal { get; set; }
        public string Nutrisi_Metode { get; set; }
        public string Nutrisi_Edukator { get; set; }
        public string Nutrisi_EdukatorNama { get; set; }
        public string Nutrisi_Keluarga { get; set; }
        public bool Nutrisi_TidakRespon { get; set; }
        public bool Nutrisi_TidakPaham { get; set; }
        public bool Nutrisi_DapatMenjelaskan { get; set; }
        public bool Nutrisi_MampuMendemontrasikan { get; set; }
        public string Nutrisi_MampuMendemontrasikanKet { get; set; }
        public string DrSpesialis { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DrSpesialis_Tanggal { get; set; }
        public string DrSpesialis_Metode { get; set; }
        public string DrSpesialis_Edukator { get; set; }
        public string DrSpesialis_EdukatorNama { get; set; }
        public string DrSpesialis_Keluarga { get; set; }
        public bool DrSpesialis_TidakRespon { get; set; }
        public bool DrSpesialis_TidakPaham { get; set; }
        public bool DrSpesialis_Paham { get; set; }
        public bool DrSpesialis_MenjelaskanTanpaDiBantu { get; set; }
        public bool DrSpesialis_MenjelaskanDiBantu { get; set; }
        public bool DrSpesialis_MampuMendemontrasikan { get; set; }
        public string DrSpesialis_MampuMendemontrasikanKet { get; set; }
        public string ManajemenNyeri_C { get; set; }
        public string ManajemenNyeri_D { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ManajemenNyeri_Tanggal { get; set; }
        public string ManajemenNyeri_Metode { get; set; }
        public string ManajemenNyeri_Edukator { get; set; }
        public string ManajemenNyeri_EdukatorNama { get; set; }
        public string ManajemenNyeri_Keluarga { get; set; }
        public bool ManajemenNyeri_TidakRespon { get; set; }
        public bool ManajemenNyeri_Paham { get; set; }
        public bool ManajemenNyeri_DapatMenjelaskan { get; set; }
        public bool ManajemenNyeri_MampuMendemontrasikan { get; set; }
        public string ManajemenNyeri_MampuMendemontrasikanKet { get; set; }
        public string Rohaniawan_C { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Rohaniawan_Tanggal { get; set; }
        public string Rohaniawan_Metode { get; set; }
        public string Rohaniawan_Edukator { get; set; }
        public string Rohaniawan_EdukatorNama { get; set; }
        public string Rohaniawan_Keluarga { get; set; }
        public bool Rohaniawan_MampuMenjalankanBimbingan { get; set; }
        public string Farmasi_G { get; set; }
        public string Farmasi_H { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Farmasi_Tanggal { get; set; }
        public string Farmasi_Metode { get; set; }
        public string Farmasi_Edukator { get; set; }
        public string Farmasi_EdukatorNama { get; set; }
        public string Farmasi_Keluarga { get; set; }
        public bool Farmasi_TidakRespon { get; set; }
        public bool Farmasi_TidakPaham { get; set; }
        public bool Farmasi_Paham { get; set; }
        public bool Farmasi_MenjelaskanTanpaDiBantu { get; set; }
        public bool Farmasi_MenjelaskanDiBantu { get; set; }
        public bool Farmasi_MampuMendemontrasikan { get; set; }
        public string Farmasi_MampuMendemontrasikanKet { get; set; }
        public string Perawat_Pendidikan { get; set; }
        public string Perawat_Pendidikan2 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Perawat_Tanggal { get; set; }
        public string Perawat_Metode { get; set; }
        public string Perawat_Edukator { get; set; }
        public string Perawat_EdukatorNama { get; set; }
        public string Perawat_Keluarga { get; set; }
        public bool Perawat_TidakRespon { get; set; }
        public bool Perawat_TidakPaham { get; set; }
        public bool Perawat_Paham { get; set; }
        public bool Perawat_MenjelaskanTanpaDiBantu { get; set; }
        public bool Perawat_MenjelaskanDiBantu { get; set; }
        public bool Perawat_MampuMendemontrasikan { get; set; }
        public string Perawat_MampuMendemontrasikanKet { get; set; }
        public string Rehabilitasi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Rehabilitasi_Tanggal { get; set; }
        public string Rehabilitasi_Metode { get; set; }
        public string Rehabilitasi_Edukator { get; set; }
        public string Rehabilitasi_EdukatorNama { get; set; }
        public string Rehabilitasi_Keluarga { get; set; }
        public bool Rehabilitasi_TidakRespon { get; set; }
        public bool Rehabilitasi_TidakPaham { get; set; }
        public bool Rehabilitasi_Paham { get; set; }
        public bool Rehabilitasi_MenjelaskanTanpaDiBantu { get; set; }
        public bool Rehabilitasi_MenjelaskanDiBantu { get; set; }
        public bool Rehabilitasi_MampuMendemontrasikan { get; set; }
        public string Rehabilitasi_MampuMendemontrasikanKet { get; set; }
        public string Username { get; set; }

        public bool DokterSpesialis_PenjelasanPenyakit { get; set; }
        public bool DokterSpesialis_HasilPemeriksaan { get; set; }
        public bool DokterSpesialis_TindakanMedis { get; set; }
        public bool DokterSpesialis_PerkiraanHari { get; set; }
        public bool DokterSpesialis_PenjelasanKomplikasi { get; set; }
        public bool DokterSpesialis_Flainnya { get; set; }
        public bool Nutrisi_Diet { get; set; }
        public bool Nutrisi_Penyuluhan { get; set; }
        public bool Nutrisi_C_Lainnya { get; set; }
        public bool Farmasi_NamaObat { get; set; }
        public bool Farmasi_AturanPemakaian { get; set; }
        public bool Farmasi_JumlahObat { get; set; }
        public bool Farmasi_CaraPenyempianan { get; set; }
        public bool Farmasi_EfekSamping { get; set; }
        public bool Farmasi_Kontraindikasi { get; set; }

        public string TTD_Edukator_1 { get; set; }
        public string TTD_Pasien_1 { get; set; }
        public string TTD_Edukator_2 { get; set; }
        public string TTD_Pasien_2 { get; set; }
        public string TTD_Edukator_3 { get; set; }
        public string TTD_Pasien_3 { get; set; }
        public string TTD_Edukator_4 { get; set; }
        public string TTD_Pasien_4 { get; set; }
        public string TTD_Edukator_5 { get; set; }
        public string TTD_Pasien_5 { get; set; }
        public string TTD_Edukator_6 { get; set; }
        public string TTD_Pasien_6 { get; set; }
        public string TTD_Edukator_7 { get; set; }
        public string TTD_Pasien_7 { get; set; }
        public string TTD_Edukator_8 { get; set; }
        public string TTD_Pasien_8 { get; set; }
        public string DokterSpesialis_Metode1 { get; set; }
        public string Nutrisi_Metode1 { get; set; }
        public string DrSpesialis_Metode1 { get; set; }
        public string ManajemenNyeri_Metode1 { get; set; }
        public string Rohaniawan_Metode1 { get; set; }
        public string Farmasi_Metode1 { get; set; }
        public string Perawat_Metode1 { get; set; }
        public string Rehabilitasi_Metode1 { get; set; }
        public bool Verifikasi_Edukator_1 { get; set; } 
        public bool Verifikasi_Edukator_2 { get; set; }
        public bool Verifikasi_Edukator_3 { get; set; }
        public bool Verifikasi_Edukator_4 { get; set; }
        public bool Verifikasi_Edukator_5 { get; set; }
        public bool Verifikasi_Edukator_6 { get; set; }
        public bool Verifikasi_Edukator_7 { get; set; }
        public bool Verifikasi_Edukator_8 { get; set; }
        public bool DokterSpesialis_Glainnya { get; set; }
        public string DokterSpesialis_G { get; set; }
        public bool Nutrisi_D_Lainnya { get; set; }
        public string Nutrisi_D { get; set; }
        public bool Farmasi_Potensi { get; set; }
        public bool Farmasi_Penggunaan { get; set; }
        public bool Perawat_a { get; set; }
        public bool Perawat_b { get; set; }
        public bool Perawat_c { get; set; }
        public bool Perawat_d { get; set; }
        public string Perawat_b_Ket { get; set; }

        // disable all form
        public string NamaPasien { get; set; }
        public int MODEVIEW { get; set; }
    }
}