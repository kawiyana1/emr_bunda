﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRHeaderViewDetail
    {
        public string NoBukti { get; set; }
        public string Diagnosa { get; set; }
        public string Tindakan { get; set; }
        public string Alergi { get; set; }
        public string BB { get; set; }
        public string TB { get; set; }
        public int Report { get; set; }
        public int MODEVIEW { get; set; }
        public string DietAwal { get; set; }
        public string E { get; set; }
        public string P { get; set; }
        public string AsupanMakan { get; set; }
        public string Keterangan { get; set; }
        public string Paraf { get; set; }
        public string Tanggal { get; set; }
        public string TglLahir { get; set; }
        public string NamaPasien { get; set; }
        public string Umur { get; set; }
        public string NRM { get; set; }
        public string NoIdentitas { get; set; }
        public int No { get; set; }

        public ListDetail<EMRTriageIGDViewModel> EMRTriage_List { get; set; }
        public ListDetail<EMRCatatanEdukasiRawatJalanViewModel> EMREdukasi_List { get; set; }
        public ListDetail<EMRKunjunganPoliklinikViewModel> EMRKunjunganPoliklinik_List { get; set; }
        public ListDetail<EMRObatDanAlatKesehatanHabisPakaiViewModel> EMRObatAlkes_List { get; set; }
        public ListDetail<EMRCatatanPengobatanViewModel> EMRCatatanPengobatan_List { get; set; }
        public ListDetail<EMRCatatanPengobatanAntibiotikViewModel> EMRCatatanPengobatanAntibiotik_List { get; set; }
        public ListDetail<EMRRekamanAsuhanKeperawatanViewModel> EMRRekamanAsuhan_List { get; set; }
        public ListDetail<EMRPemberianCairanParenteralViewModel> EMRPemberianCairan_List { get; set; }
        public ListDetail<EMRFormDaftarDPJPViewModel> EMRDaftarDPJP_List { get; set; }
        public ListDetail<EMRFormRekonsiliasiObat_DetailDetailModel> EMRFormRekonsiliasiObat_List { get; set; }
        public ListDetail<EMRFormRekonsiliasiObatDipakaiDirumah_DetailDetailModel> EMRRekonsiliasiBHP_List { get; set; }
        public ListDetail<EMRRekamanAsuhanKeperawatanDetailModel> EMRRekamKeperawatan_List { get; set; }
        public ListDetail<EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteDetailModel> EMRCatatanIntergrated_List { get; set; }
        public ListDetail<EMRGrafikTandaKardinalViewModel> EMRGrafikKardial_List { get; set; }
        public ListDetail<EMRCatatanPerkembanganPasienSarafDetailModel> EMRPekembanganSaraf_List { get; set; }
        public ListDetail<EMREWS_DetailViewModel> EMREWS_List { get; set; }
        public ListDetail<EMRCatatanObservasi_DetailViewModel> EMRObservasi_List { get; set; }
        public ListDetail<EMRIntervensiMonitoringEvaluasi_DetailViewModel> EMRIntervensi_List { get; set; }
        public ListDetail<EMRAssesmenRisikoJatuhPadaPedriatriViewModel> EMRpedriatri_List { get; set; }
        public ListDetail<EMRFormAEvaluasiAwalViewModel> EMRFormA_List { get; set; }
        public ListDetail<EMRFormBCatatanImplementasiViewModel> EMRFormB_List { get; set; }
        public ListDetail<EMRAssesmenRisikoJatuhMorseViewModel> EMRAssMorse_List { get; set; }
        public ListDetail<EMRFormulirKunjunganRohaniawanViewModel> EMRFormKunjunganKerohanian_List { get; set; }
        public ListDetail<EMRDaftarKeluahanKomplinPasienViewModel> EMRDftrKeluhan_List { get; set; }
        public ListDetail<EMRMonitoringKontrolPenungguViewModel> EMRMntrKntrlPenunggu_List { get; set; }
        public ListDetail<EMRMonitoringPengunjungDiluarJamViewModel> EMRMntrDluarJam_List { get; set; }
        public ListDetail<EMRDaftarPengunjungDiluarJamViewModel> EMRDftrPngjungLuarjam_List { get; set; }

        public ListDetail<EMRMonitoringBalanceCarianViewModel> EMRMoitoringBalanceCairan_List { get; set; }
        public ListDetail<EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel> EMRMonitor_List { get; set; }
        public ListDetail<EMRLembarObservasiKebidanananDetailModel> EMRLmbrObservsKeb_List { get; set; }
        public ListDetail<EMRMEWSViewModel> EMRMEWS_List { get; set; }
        public ListDetail<EMRMEOWSViewModel> EMRMEOWS_List { get; set; }
        public List<SelectItemListObatAlkes> ListTemplateObatAlkes { get; set; }
        public ListDetail<EMRIntervensiDanPengkajianUlangNyeriModalDetail> InterPengkajianNyeri_List { get; set; }
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
    }

    public class SelectItemListObatAlkes
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}