﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPemberianInformasiPemasanganKateterViewModel
    {
        public string NoBukti { get; set; }
        public string DokterPelaksanaTIndakan { get; set; }
        public string DokterPelaksanaTIndakanNama { get; set; }
        public string PemberiInformasi { get; set; }
        public string PenerimaInformasi { get; set; }
        public string DiagnosaKet { get; set; }
        public bool Diagnosa { get; set; }
        public string DasarDiagnosaKet { get; set; }
        public bool DasarDiagnosa { get; set; }
        public string TindakanKedokteranKet { get; set; }
        public bool TindakanKedokteran { get; set; }
        public string IndikasiTindakanKet { get; set; }
        public bool IndikasiTindakan { get; set; }
        public string TataCaraKet { get; set; }
        public bool TataCara { get; set; }
        public string TujuanKet { get; set; }
        public bool Tujuan { get; set; }
        public string RisikoKet { get; set; }
        public bool Risiko { get; set; }
        public string KomplikasiKet { get; set; }
        public bool Komplikasi { get; set; }
        public string PrognosisKet { get; set; }
        public bool Prognosis { get; set; }
        public string AlternatifDanRisikoKet { get; set; }
        public bool AlternatifDanRisiko { get; set; }
        public string LainLainKet { get; set; }
        public bool LainLain { get; set; }
        public string TelahMenerangkan { get; set; }
        public string TelahMenerangkanNama { get; set; }
        public string TelahMenerima { get; set; }
        public bool VerifikasiPetugas { get; set; }
        public string TTDPasien { get; set; }
        public int MODEVIEW { get; set; }

    }
}