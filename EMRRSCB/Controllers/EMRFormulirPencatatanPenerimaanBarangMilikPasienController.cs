﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormulirPencatatanPenerimaanBarangMilikPasienController : Controller
    {
        // GET: EMRFormulirPencatatanPenerimaanBarangMilikPasien
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRFormulirPencatatanPenerimaanBarangMilikPasienViewModel();
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var sectionname = Request.Cookies["EMRSectionNamaPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.FormulirPencatatanPenerimaanBarangMilikPasien.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormulirPencatatanPenerimaanBarangMilikPasienViewModel>(dokumen);

                        model.NoBukti = id;

                        var petugas = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (petugas != null) model.PerawatNama = petugas.NamaDOkter;

                        #region === Detail 
                        model.Detail_List = new ListDetail<EMRFormulirPencatatanPenerimaanBarangMilikPasienDetailModel>();
                        var detail_1 = s.Keperawatan_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRFormulirPencatatanPenerimaanBarangMilikPasienDetailModel>(x);
                            model.Detail_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.TglLahirYear = identitas.TglLahir.Value.ToString("yyyy");
                        model.TglLahirMounth = identitas.TglLahir.Value.ToString("MM");
                        model.TglLahirDay = identitas.TglLahir.Value.ToString("dd");
                        int a = Convert.ToInt32(Convert.ToDouble(model.TglLahirYear));
                        int b = Convert.ToInt32(Convert.ToDouble(model.TglLahirMounth));
                        int c = Convert.ToInt32(Convert.ToDouble(model.TglLahirDay));
                        DateTime Birth = new DateTime(a, b, c);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;
                        model.Nama = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.Alamat = (identitas.Alamat == null ? "" : identitas.Alamat);
                        model.Umur = Years.ToString();
                        model.NRM = nrm + " / / " + sectionname;
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));

                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRFormulirPencatatanPenerimaanBarangMilikPasienViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.FormulirPencatatanPenerimaanBarangMilikPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormulirPencatatanPenerimaanBarangMilikPasien>(item);
                        s.FormulirPencatatanPenerimaanBarangMilikPasien.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Formulir Serah Terima Barang";
                    }
                    else
                    {
                        model = IConverter.Cast<FormulirPencatatanPenerimaanBarangMilikPasien>(item);
                        s.FormulirPencatatanPenerimaanBarangMilikPasien.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Formulir Serah Terima Barang";
                    }

                    #region === Detail Keperawatan
                    if (item.Detail_List == null) item.Detail_List = new ListDetail<EMRFormulirPencatatanPenerimaanBarangMilikPasienDetailModel>();
                    item.Detail_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Detail_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Detail_List;
                    var real_list = s.Keperawatan_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.Keperawatan_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.Keperawatan_Detail.Add(IConverter.Cast<Keperawatan_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Pengkajian = x.Model.Barang;
                            _m.Diagnosa = x.Model.Jumlah;
                            _m.RencanaTindakan = x.Model.Kondisi;
                        }
                    }
                    #endregion


                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}