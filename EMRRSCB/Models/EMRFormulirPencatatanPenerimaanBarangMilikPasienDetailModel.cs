﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormulirPencatatanPenerimaanBarangMilikPasienDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Barang { get; set; }
        public string Jumlah { get; set; }
        public string Kondisi { get; set; }
    }
}