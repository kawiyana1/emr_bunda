﻿//using EMRRSCB.Entities.EMR;
//using EMRRSCB.Entities.SIM;
//using EMRRSCB.Models;
//using iHos.MVC.Converter;
//using iHos.MVC.Property;
//using Microsoft.AspNet.Identity;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Migrations;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace EMRRSCB.Controllers
//{
//    public class EMRSIMtrSOAP_TBKController : Controller
//    {
//        [HttpGet]
//        [ActionName("Create")]
//        public ActionResult CreateGet(string id, int view = 0, int cppt = 0)
//        {
//            var model = new CPPTViewModel();
//            using (var sim = new SIM_Entities())
//            { 
//                using (var s = new EMREntities())
//            {
//                if (cppt == 0) HttpNotFound();
//                var dokumen = s.CPPT.FirstOrDefault(x => x.NoBukti == id);
//                var data = s.CPPT.FirstOrDefault(x => x.NoBukti == id);
//                if (dokumen != null)
//                {
//                    model = IConverter.Cast<CPPTViewModel>(dokumen);
//                    var petugasT = sim.mDokter.FirstOrDefault(e => e.DokterID == model.T_Petugas);
//                    if (petugasT != null) model.T_Petugas_Nama = petugasT.NamaDOkter;
//                    var petugasB = sim.mDokter.FirstOrDefault(e => e.DokterID == model.B_Petugas);
//                    if (petugasB != null) model.B_Petugas_Nama = petugasB.NamaDOkter;
//                    var petugasK = sim.mDokter.FirstOrDefault(e => e.DokterID == model.K_Petugas);
//                    if (petugasK != null) model.K_Petugas_Nama = petugasK.NamaDOkter;
//                    model.MODEVIEW = 1;
//                }
//                else
//                {
//                    if (data != null)
//                    {
//                        model.NoBukti = id;
//                    }
//                    model.NoBukti = id;
//                    model.MODEVIEW = view;
//                }

//            }
//            }
//            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
//            if (Request.IsAjaxRequest())
//                return PartialView(model);
//            else
//                return View(model);
//        }

//        [HttpPost]
//        [ActionName("Create")]
//        [ValidateAntiForgeryToken]
//        public string CreatePost()
//        {
//            try
//            {
//                var item = new CPPTViewModel();
//                TryUpdateModel(item);
//                ResultSS result;
//                using (var s = new EMREntities())
//                {
//                    var model = s.CPPT.FirstOrDefault(x => x.NoBukti == item.NoBukti);
//                    var activity = "";
//                    if (model == null)
//                    {
//                        var o = IConverter.Cast<CPPT>(item);
//                        s.CPPT.Add(o);
//                        activity = "Create Form TBK CPPT";
//                    }
//                    else
//                    {
//                        model = IConverter.Cast<CPPT>(item);
//                        model.TanggalInput = item.TanggalInput;
//                        model.T_Jam = item.T_Jam;
//                        model.T_Tanggal = item.T_Tanggal;
//                        model.T_Petugas = item.T_Petugas;
//                        model. = item.B_TTD;
//                        s.CPPT.AddOrUpdate(model);
//                        activity = "Update Form TBK CPPT";
//                    }

//                    s.SaveChanges();

//                    result = new ResultSS(1);

//                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
//                    {
//                        Activity = $" {activity} {item.NoBukti} {"TBK CPPT :" + item.NoBukti}"
//                    };
//                    UserActivity.InsertUserActivity(userActivity);
//                }
//                return JsonHelper.JsonMsgCreate(result);
//            }
//            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
//            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
//        }
//    }
//}