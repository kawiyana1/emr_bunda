﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSuratPermohonanRawatInapController : Controller
    {
        // GET: EMRSuratPermohonanRawatInap
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string noreg, int view = 0)
        {
            var model = new EMRSuratPermohonanRawatInapViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.SuratPermohonanRawatInap.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRSuratPermohonanRawatInapViewModel>(dokumen);

                        model.NoBukti = id;
                        if (identitas != null)
                        {
                            model.NamaPasien = identitas.NamaPasien;
                            model.Umur = identitas.TglLahir.Value.ToString("dd-MM-yyyy") + " / " +identitas.UmurThn.ToString() + " Thn";
                            model.Alamat = identitas.Alamat;
                            model.JenisKelamin = identitas.JenisKelamin == "M" ? "Laki" : "Perempuan";
                            model.NRM = identitas.NRM;
                        }
                        var dokterpelaksana = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        var irj = sim.SIMmSection.FirstOrDefault(e => e.SectionID == model.IRD);
                        var dirawat = sim.SIMmSection.FirstOrDefault(e => e.SectionID == model.Dirawat);
                        if (dokterpelaksana != null) model.Dokter_Nama = dokterpelaksana.NamaDOkter;
                        model.Dirawat_Nama = dirawat == null ? "" : dirawat.SectionName;
                        model.IRD_Nama = irj == null ? "" : irj.SectionName;
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        if (identitas != null)
                        {  
                            model.NamaPasien = identitas.NamaPasien;
                            model.Umur = identitas.TglLahir.Value.ToString("dd-MM-yyyy") + " / " + identitas.UmurThn.ToString() + " Thn";
                            model.Alamat = identitas.Alamat;
                            model.JenisKelamin = identitas.JenisKelamin == "M" ? "Laki" : "Perempuan";
                            model.NRM = identitas.NRM;
                        }
                        model.Diagnosa = soap == null ? "" : soap.CPPT_A;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRSuratPermohonanRawatInapViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.SuratPermohonanRawatInap.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<SuratPermohonanRawatInap>(item);
                        s.SuratPermohonanRawatInap.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Surat Permohonan MRS";
                    }
                    else
                    {
                        model = IConverter.Cast<SuratPermohonanRawatInap>(item);
                        s.SuratPermohonanRawatInap.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Surat Permohonan MRS";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string SelectIRD(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.TipePelayanan == "RJ" && x.StatusAktif == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmSection>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string SelectRawat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.TipePelayanan == "RI" && x.StatusAktif == true && x.RIOnly == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmSection>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}