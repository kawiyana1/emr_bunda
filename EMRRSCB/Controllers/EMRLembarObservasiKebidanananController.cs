﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRLembarObservasiKebidanananController : Controller
    {
        // GET: EMRLembarObservasiKebidananan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.LembarObservasiKebidananan.Where(x => x.NoBukti == id).ToList();
                    if (dokumen != null)
                    {
                        model.EMRLmbrObservsKeb_List = new ListDetail<EMRLembarObservasiKebidanananDetailModel>();
                        var detail_1 = s.LembarObservasiKebidananan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRLembarObservasiKebidanananDetailModel>(x);

                            model.EMRLmbrObservsKeb_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        model.Report = 1;
                    }
                    model.NoBukti = id;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.LembarObservasiKebidananan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create EWS ";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update EWS ";
                    }

                    if (item.EMRLmbrObservsKeb_List == null) item.EMRLmbrObservsKeb_List = new ListDetail<EMRLembarObservasiKebidanananDetailModel>();
                    item.EMRLmbrObservsKeb_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.EMRLmbrObservsKeb_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Tanggal = DateTime.Now;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.EMRLmbrObservsKeb_List;
                    var real_list = s.LembarObservasiKebidananan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.LembarObservasiKebidananan.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.LembarObservasiKebidananan.Add(IConverter.Cast<LembarObservasiKebidananan>(x.Model));
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.TanggalManual = x.Model.TanggalManual;
                            _m.JamManual = x.Model.JamManual;
                            _m.DJJ = x.Model.DJJ;
                            _m.HIS = x.Model.HIS;
                            _m.Keterangan = x.Model.Keterangan;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}