//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_DokumenAssesmenPasienCPPT
    {
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> TanggalCreate { get; set; }
        public Nullable<System.DateTime> TanggalUpdate { get; set; }
        public Nullable<System.DateTime> TanggalCanceled { get; set; }
        public string CretaedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string UserIdWeb_Created { get; set; }
        public string UserIdWeb_Updated { get; set; }
        public bool Simpan { get; set; }
        public bool Batal { get; set; }
        public string DokumenID { get; set; }
        public string NamaDokumen { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string CPPT_S { get; set; }
        public string CPPT_O { get; set; }
        public string CPPT_A { get; set; }
        public string CPPT_P { get; set; }
        public string CPPT_I { get; set; }
        public string Profesi { get; set; }
        public string NamaDOkter { get; set; }
        public byte[] CPPT_OGambar { get; set; }
    }
}
