﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratKeteranganKematianViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKontrol { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NomorSurat { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Alamat { get; set; }
        public string TanggalKematian { get; set; }
        public string JamKematian { get; set; }
        public string NamaKeluarga { get; set; }
        public string TandaTanganpasien { get; set; }

        public int MODEVIEW { get; set; }
    }
}