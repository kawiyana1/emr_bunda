﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRMonitorPelaksaanRisikoJatuhAndGUGTController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRMonitorPelaksaanPencegahanRisikoJatuhViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var dokumen = emr.MonitorPelaksaanRisikoJatuhAndGUGT.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRMonitorPelaksaanPencegahanRisikoJatuhViewModel>(dokumen);
                        model.NoBukti = id;

                        // Monitor Pelaksaan Detail
                        model.EMRMonitor_List = new ListDetail<EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel>();
                        var detail_2 = emr.MonitorPelaksaanRisikoJatuhAndGUGT_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel>(x);

                            model.EMRMonitor_List.Add(false, y);

                        }
                        model.MODEVIEW = view;



                    }
                    else
                    {
                        model.NoBukti = id;

                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);

            }
            else
            {
                return View(model);

            }
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]

        public string CreatePost()
        {
            try
            {
                var itemModel = new EMRMonitorPelaksaanPencegahanRisikoJatuhViewModel();
                TryUpdateModel(itemModel);
                ResultSS result;
                using (var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = emr.MonitorPelaksaanRisikoJatuhAndGUGT.FirstOrDefault(x => x.NoBukti == itemModel.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var obj = IConverter.Cast<MonitorPelaksaanRisikoJatuhAndGUGT>(itemModel);
                        emr.MonitorPelaksaanRisikoJatuhAndGUGT.Add(obj);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == itemModel.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create MonitorPelaksaanRisikoJatuhAndGetUpGoTest";
                    }
                    else
                    {
                        model = IConverter.Cast<MonitorPelaksaanRisikoJatuhAndGUGT>(itemModel);
                        emr.MonitorPelaksaanRisikoJatuhAndGUGT.AddOrUpdate(model);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == itemModel.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update  Monitor Pelaksaan Risiko Jatuh And GetUpGoTest";
                    }

                    // Detail Monitor Pelaksanaan
                    if (itemModel.EMRMonitor_List == null) itemModel.EMRMonitor_List = new ListDetail<EMRMonitorPelaksaanPencegahanRisikoJatuh_DetailViewModel>();
                    itemModel.EMRMonitor_List.RemoveAll(x => x.Remove);
                    foreach (var x in itemModel.EMRMonitor_List)
                    {
                        x.Model.NoBukti = itemModel.NoBukti;

                    }
                    var new_list1 = itemModel.EMRMonitor_List;
                    var real_list1 = emr.MonitorPelaksaanRisikoJatuhAndGUGT_Detail.Where(x => x.NoBukti == itemModel.NoBukti);
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null)
                        {
                            emr.MonitorPelaksaanRisikoJatuhAndGUGT_Detail.Remove(x);
                        }
                    }
                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            emr.MonitorPelaksaanRisikoJatuhAndGUGT_Detail.Add(IConverter.Cast<MonitorPelaksaanRisikoJatuhAndGUGT_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No  = x.Model.No;
                            _m.No1 = x.Model.No1;
                            _m.No2 = x.Model.No2;
                            _m.No3 = x.Model.No3;
                            _m.No4 = x.Model.No4;
                            _m.No5 = x.Model.No5;
                            _m.No6 = x.Model.No6;

                        }
                    }

                    emr.SaveChanges();

                    result = new ResultSS(1);


                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {itemModel.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);

                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}