//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSCB.Entities.EMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class Monitoring24Jam
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Dokter1 { get; set; }
        public string Dokter2 { get; set; }
        public string Dokter3 { get; set; }
        public string Dokter4 { get; set; }
        public string Dokter5 { get; set; }
        public string Diagnosa { get; set; }
        public string Enternal { get; set; }
        public string Parenteral { get; set; }
        public string PolaVentilasi { get; set; }
        public string Enternal_Volume { get; set; }
        public string Enternal_Kalori { get; set; }
        public string Enternal_Protein { get; set; }
        public string Enternal_Lemak { get; set; }
        public string Parenteral_Volume { get; set; }
        public string Parenteral_Kalori { get; set; }
        public string Parenteral_Protein { get; set; }
        public string Parenteral_Lemak { get; set; }
        public string PolaVentilasi_Ventilator { get; set; }
        public string PolaVentilasi_BB { get; set; }
        public string PolaVentilasi_TB { get; set; }
        public string PolaVentilasi_Alergi { get; set; }
        public string CatatanLain { get; set; }
        public string IVLine_Ukuran { get; set; }
        public string IVLine_Lokasi { get; set; }
        public Nullable<System.DateTime> IVLine_Tanggal { get; set; }
        public string CVC_Ukuran { get; set; }
        public string CVC_Lokasi { get; set; }
        public Nullable<System.DateTime> CVC_Tanggal { get; set; }
        public string ArteriLine_Ukuran { get; set; }
        public string ArteriLine_Lokasi { get; set; }
        public Nullable<System.DateTime> ArteriLine_Tanggal { get; set; }
        public string SwanzGanz_Ukuran { get; set; }
        public string SwanzGanz_Lokasi { get; set; }
        public Nullable<System.DateTime> SwanzGanz_Tanggal { get; set; }
        public string OTT_Ukuran { get; set; }
        public string OTT_Lokasi { get; set; }
        public Nullable<System.DateTime> OTT_Tanggal { get; set; }
        public string NGT_Ukuran { get; set; }
        public string NGT_Lokasi { get; set; }
        public Nullable<System.DateTime> NGT_Tanggal { get; set; }
        public string WSD_Ukuran { get; set; }
        public string WSD_Lokasi { get; set; }
        public Nullable<System.DateTime> WSD_Tanggal { get; set; }
        public string Drain_Ukuran { get; set; }
        public string Drain_Lokasi { get; set; }
        public Nullable<System.DateTime> Drain_Tanggal { get; set; }
        public string UrinKateter_Ukuran { get; set; }
        public string UrinKateter_Lokasi { get; set; }
        public Nullable<System.DateTime> UrinKateter_Tanggal { get; set; }
        public string Luka_Ukuran { get; set; }
        public string Luka_Lokasi { get; set; }
        public Nullable<System.DateTime> Luka_Tanggal { get; set; }
        public string BalanceCairanMasuk { get; set; }
        public string BalanceCairanKeluar { get; set; }
        public string BalanceCairanIWL { get; set; }
        public string BalanceCairanBC24Jam { get; set; }
        public string BalanceCairanMasukBCSebelumnya { get; set; }
        public string HariRawatKe { get; set; }
        public string CaraBayar { get; set; }
        public string AsalRuangan { get; set; }
        public string Ruang { get; set; }
        public string Instalasi { get; set; }
        public string KreteriaResiko { get; set; }
        public string Kreteria_TotalSkor { get; set; }
        public string GambaranEKG { get; set; }
        public string NyeriKenyamanan { get; set; }
        public string NyeriKenyamanan_Intensitas { get; set; }
        public string BesarPupil { get; set; }
    }
}
