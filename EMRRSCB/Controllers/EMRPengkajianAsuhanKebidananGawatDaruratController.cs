﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRPengkajianAsuhanKebidananGawatDaruratController : Controller
    {
        // GET: PengkajianAsuhanKebidananGawatDarurat
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRPengkajianAsuhanKebidananGawatDaruratViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;

                    var dokumen = s.PengkajianAsuhanKebidananGawatDarurat.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDaruratViewModel>(dokumen);

                        model.NoBukti = id;

                        var bidan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (bidan != null) model.BidanNama = bidan.NamaDOkter;

                        model.Observasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail>();
                        var detail_1 = s.PengkajianAsuhanKebidananGawatDarurat_Observasi.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail>(x);
                            model.Observasi_List.Add(false, y);
                        }

                        model.Evaluasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasienModelDetail>();
                        var detail_2 = s.PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasienModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Evaluasi_List.Add(false, y);
                        }

                        model.Tindakan_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_TindakanKebidananModelDetail>();
                        var detail_3 = s.PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_TindakanKebidananModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Tindakan_List.Add(false, y);
                        }

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_4 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.BioPsikososialIstirahatTidurJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.PemindahanRuanganMeninggalJam = DateTime.Now;
                        model.PemindahanRuanganMinggatKet = DateTime.Now;
                    }


                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemAsuhanGawatDarurat>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemAsuhanGawatDarurat()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy)
        {
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            var model = new EMRPengkajianAsuhanKebidananGawatDaruratViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.PengkajianAsuhanKebidananGawatDarurat.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDaruratViewModel>(dokumen);

                        model.NoBukti = id;

                        var bidan = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Bidan);
                        if (bidan != null) model.BidanNama = bidan.NamaDOkter;

                        model.Observasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail>();
                        var detail_1 = s.PengkajianAsuhanKebidananGawatDarurat_Observasi.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail>(x);
                            model.Observasi_List.Add(false, y);
                        }

                        model.Evaluasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasienModelDetail>();
                        var detail_2 = s.PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasienModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Evaluasi_List.Add(false, y);
                        }

                        model.Tindakan_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_TindakanKebidananModelDetail>();
                        var detail_3 = s.PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_3)
                        {
                            var y = IConverter.Cast<EMRPengkajianAsuhanKebidananGawatDarurat_TindakanKebidananModelDetail>(x);
                            var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            if (dokterdetail != null)
                            {
                                y.DokterNama = dokterdetail.NamaDOkter;
                            }
                            model.Tindakan_List.Add(false, y);
                        }

                        model.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                        var detail_4 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_4)
                        {
                            var y = IConverter.Cast<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>(x);
                            model.RiwayatKehamilan_List.Add(false, y);
                        }
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.BioPsikososialIstirahatTidurJam = DateTime.Now;
                        model.AnogenitalInspekuioVaginaToucherOlehTanggal = DateTime.Today;
                        model.PemindahanRuanganMeninggalJam = DateTime.Now;
                        model.PemindahanRuanganMinggatKet = DateTime.Now;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemAsuhanGawatDarurat>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemAsuhanGawatDarurat()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRPengkajianAsuhanKebidananGawatDaruratViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var model = s.PengkajianAsuhanKebidananGawatDarurat.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianAsuhanKebidananGawatDarurat>(item);
                        s.PengkajianAsuhanKebidananGawatDarurat.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Create Assesmen Pengkajian Asuhan Kebidanan";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianAsuhanKebidananGawatDarurat>(item);
                        s.PengkajianAsuhanKebidananGawatDarurat.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;
                        activity = "Update Assesmen Pengkajian Asuhan Kebidanan";
                    }
                    #region === Detail Observasi
                    if (item.Observasi_List == null) item.Observasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_ObservasiModelDetail>();
                    item.Observasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Observasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Observasi_List;
                    var real_list = s.PengkajianAsuhanKebidananGawatDarurat_Observasi.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianAsuhanKebidananGawatDarurat_Observasi.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianAsuhanKebidananGawatDarurat_Observasi.Add(IConverter.Cast<PengkajianAsuhanKebidananGawatDarurat_Observasi>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.BAB = x.Model.BAB;
                            _m.Drain = x.Model.Drain;
                            _m.IV = x.Model.IV;
                            _m.JenisCairan = x.Model.JenisCairan;
                            _m.NGT = x.Model.NGT;
                            _m.NoBotol = x.Model.NoBotol;
                            _m.Oral = x.Model.Oral;
                            _m.Urin = x.Model.Urin;
                        }
                    }
                    #endregion

                    #region === Detail Evaluasi
                    if (item.Evaluasi_List == null) item.Evaluasi_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasienModelDetail>();
                    item.Evaluasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Evaluasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Evaluasi_List;
                    var real_list1 = s.PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien.Add(IConverter.Cast<PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.SOAP = x.Model.SOAP;
                        }
                    }
                    #endregion

                    #region === Detail Tindakan
                    if (item.Tindakan_List == null) item.Tindakan_List = new ListDetail<EMRPengkajianAsuhanKebidananGawatDarurat_TindakanKebidananModelDetail>();
                    item.Tindakan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Tindakan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list2 = item.Tindakan_List;
                    var real_list2 = s.PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list2)
                    {
                        var m = new_list2.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan.Remove(x);
                    }

                    foreach (var x in new_list2)
                    {
                        var _m = real_list2.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan.Add(IConverter.Cast<PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Dokter = x.Model.Dokter;
                            _m.Evaluasi = x.Model.Evaluasi;
                            _m.TindakanKebidanan = x.Model.TindakanKebidanan;
                        }
                    }
                    #endregion

                    #region === Detail Riwayat Kehamilan
                    if (item.RiwayatKehamilan_List == null) item.RiwayatKehamilan_List = new ListDetail<EMRPengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilanModelDetail>();
                    item.RiwayatKehamilan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RiwayatKehamilan_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list3 = item.RiwayatKehamilan_List;
                    var real_list3 = s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list3)
                    {
                        var m = new_list3.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Remove(x);
                    }

                    foreach (var x in new_list3)
                    {
                        var _m = real_list3.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan.Add(IConverter.Cast<PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Abortus = x.Model.Abortus;
                            _m.Aterm = x.Model.Aterm;
                            _m.BBL = x.Model.BBL;
                            _m.Cacat = x.Model.Cacat;
                            _m.JenisKelaminLaki = x.Model.JenisKelaminLaki;
                            _m.JenisKelaminPerempuan = x.Model.JenisKelaminPerempuan;
                            _m.JenisPartus = x.Model.JenisPartus;
                            _m.Komplikasi = x.Model.Komplikasi;
                            _m.Meninggal = x.Model.Meninggal;
                            _m.Nakes = x.Model.Nakes;
                            _m.Normal = x.Model.Normal;
                            _m.Non = x.Model.Non;
                            _m.Prematur = x.Model.Prematur;
                        }
                    }
                    #endregion

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trDokumenTemplate();
                        temp.NoBukti = item.NoBukti;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.Bidan;
                        temp.DokumenID = dokumenid;
                        temp.SectionID = sectionid;
                        s.trDokumenTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}