﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisIGD_ObservasiDetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Observasi { get; set; }
        public string Instruksi { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string username { get; set; }
    }
}