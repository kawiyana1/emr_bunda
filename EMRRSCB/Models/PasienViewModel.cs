﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class PasienViewModel
    {
        public string NRM { get; set; }
        [Required]
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        [Required]
        public string JenisKelamin { get; set; }
        [Required]
        public string TempatLahir { get; set; }
        public string TglLahir_View { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string Pekerjaan { get; set; }
        [Required]
        public string Alamat { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public int JenisKerjasamaID { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public string CustomerKerjasamaIDNama { get; set; }
        public string NoKartu { get; set; }
        public bool PasienKTP { get; set; }
        public Nullable<int> AgamaID { get; set; }
        public bool PenanggungIsPasien { get; set; }
        public string PenanggungNRM { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungAlamat { get; set; }
        public string PenanggungPhone { get; set; }
        public string PenanggungHubungan { get; set; }
        public bool Aktif { get; set; }
    }
}