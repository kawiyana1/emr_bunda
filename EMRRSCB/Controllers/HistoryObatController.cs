﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class HistoryObatController : Controller
    {
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_OBAT> proses = s.EMR_VIEW_OBAT;
                    proses = proses.Where($"{nameof(EMR_VIEW_OBAT.NoReg)}=@0", filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HistoryObatViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public ActionResult Detail(string id, string noreg)
        {
            HistoryObatViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_OBAT.Where(x => x.NoBukti == id && x.NoReg == noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_OBAT_DETAIL.Where(x => x.NoBukti == id && x.NoReg == noreg).ToList();
                    result = IConverter.Cast<HistoryObatViewModel>(m);
                    result.Detail_List = new List<HistoryObatDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<HistoryObatDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        public ActionResult ExportPDFObat(string nobukti)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Reports/HistoryReport"), $"Informasi_Obat.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand($"Informasi_Obat", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }
    }
}