﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPemberianCairanParenteralViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
     
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Section { get; set; }
        public string Nadi { get; set; }
        public string Suhu { get; set; }
        public string Tensi { get; set; }
        public string Cairan { get; set; }
        public string CairanNama { get; set; }
        public string Tetes { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string SectionID { get; set; }
        public string Username { get; set; }
        public bool Verifikasi { get; set; }

        public int MODEVIEW { get; set; }
    }
}