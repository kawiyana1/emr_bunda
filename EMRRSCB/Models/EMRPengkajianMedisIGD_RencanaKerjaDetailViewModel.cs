﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPengkajianMedisIGD_RencanaKerjaDetailViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }
        public string DaftarMasalah { get; set; }
        public string RencanaIntervensi { get; set; }
        public string Target { get; set; }
        public string Konsultasi { get; set; }
    }
}