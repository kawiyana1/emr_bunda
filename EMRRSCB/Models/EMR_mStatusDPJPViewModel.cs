﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMR_mStatusDPJPViewModel
    {
        public int Id { get; set; }
        public string StatusDPJP { get; set; }
    }
}