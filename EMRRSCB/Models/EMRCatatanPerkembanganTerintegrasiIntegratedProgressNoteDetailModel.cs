﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRCatatanPerkembanganTerintegrasiIntegratedProgressNoteDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        public string PPA { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SOAP { get; set; }
        public string Intruksi { get; set; }
        public string Verifikasi { get; set; }
        public string VerifikasiNama { get; set; }
    }
}