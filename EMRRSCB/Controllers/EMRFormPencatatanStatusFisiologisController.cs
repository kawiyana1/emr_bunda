﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRFormPencatatanStatusFisiologisController : Controller
    {
        // GET: EMRFormPencatatanStatusFisiologis
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRFormPencatatanStatusFisiologisViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.FormPencatatanStatusFisiologis.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormPencatatanStatusFisiologisViewModel>(dokumen);

                        model.NoBukti = id;

                        var dokteroperator = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterOperator);
                        if (dokteroperator != null) model.DokterOperatorNama = dokteroperator.NamaDOkter;

                        var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                        #region === Detail Rencana Sedasi
                        model.StatutsFisio_List = new ListDetail<EMRFormPencatatanStatusFisiologis_DetailDetailModel>();
                        var detail_1 = s.FormPencatatanStatusFisiologis_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRFormPencatatanStatusFisiologis_DetailDetailModel>(x);
                            model.StatutsFisio_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRFormPencatatanStatusFisiologisViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.FormPencatatanStatusFisiologis.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormPencatatanStatusFisiologis>(item);
                        s.FormPencatatanStatusFisiologis.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Form Pencatatan Status Fisiologis";
                    }
                    else
                    {
                        model = IConverter.Cast<FormPencatatanStatusFisiologis>(item);
                        s.FormPencatatanStatusFisiologis.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Form Pencatatan Status Fisiologis";
                    }
                    #region === Detail 
                    if (item.StatutsFisio_List == null) item.StatutsFisio_List = new ListDetail<EMRFormPencatatanStatusFisiologis_DetailDetailModel>();
                    item.StatutsFisio_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.StatutsFisio_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.StatutsFisio_List;
                    var real_list = s.FormPencatatanStatusFisiologis_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.FormPencatatanStatusFisiologis_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.FormPencatatanStatusFisiologis_Detail.Add(IConverter.Cast<FormPencatatanStatusFisiologis_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Jam = x.Model.Jam;
                            _m.TensiSystole = x.Model.TensiSystole;
                            _m.TensiDiastole = x.Model.TensiDiastole;
                            _m.Nadi = x.Model.Nadi;
                            _m.Respirasi = x.Model.Respirasi;
                            _m.CatatanObat = x.Model.CatatanObat;
                            _m.Username = x.Model.Username;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}