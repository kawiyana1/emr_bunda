﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPartografViewModel
    {
        public ListDetail<EMRPartograf_DetailDetailModel> Detail_List { get; set; }

        public string NoBukti { get; set; }
        public string NoRegistrasi { get; set; }
        public string NamaIbuBapak { get; set; }
        public string Umur { get; set; }
        public string G { get; set; }
        public string P { get; set; }
        public string A { get; set; }
        public string Hamil { get; set; }
        public string RSPuskesmasRB { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> MasukTanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pukul { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> KetubanPecahPukul { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MulesSejakPukul { get; set; }
        public string Alamat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MakananTerakhir_Pukul { get; set; }
        public string MakananTerakhir_Jenis { get; set; }
        public string MakananTerakhir_Porsi { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MinumanTerakhir_Pukul { get; set; }
        public string MinumanTerakhir_Jenis { get; set; }
        public string MinumanTerakhir_Porsi { get; set; }
        public string Penolong { get; set; }
        public string PenolongNama { get; set; }
        public string TTDPenolong { get; set; }

        public int MODEVIEW { get; set; }
    }
}