﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRMonitoringIntraAnestesi_ObatDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Username { get; set; }
        public string VitalSign { get; set; }
        public string TVS { get; set; }
        public string RR { get; set; }
        public string N { get; set; }
        public string TD { get; set; }
        public string Waktu { get; set; }
        public string SPO2 { get; set; }
        public string PECo2 { get; set; }
        public string FiO2 { get; set; }
        public string TekananNafas { get; set; }
        public string CairanInfus { get; set; }
        public string Darah { get; set; }
        public string Urin { get; set; }
        public string Pendarahan { get; set; }
    }
}