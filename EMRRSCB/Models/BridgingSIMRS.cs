﻿using EMRRSCB.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSCB.Controllers
{
    public class BridgingSIMRS : BridgingServices
    {
        public PasienResponseListModel GetPasien()
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"pasien", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<PasienResponseListModel>(request);
            return response.Data;
        }

        public PasienResponseModel GetPasienByNIK(string nik)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"pasien/nik/{nik}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<PasienResponseModel>(request);
            return response.Data;
        }

        public PasienResponseModel GetPasienByNoRM(string norm)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"pasien/norm/{norm}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<PasienResponseModel>(request);
            return response.Data;
        }

        public RegisterResponseListModel GetRegistrasi()
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"register", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<RegisterResponseListModel>(request);
            return response.Data;
        }

        public RegisterResponseModel GetRegistrasiByNoReg(string noreg)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"register/noreg/{noreg}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<RegisterResponseModel>(request);
            return response.Data;
        }

        public RegisterResponseModel GetRegistrasiByNIK(string nik)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"register/nik/{nik}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<RegisterResponseModel>(request);
            return response.Data;
        }

        public ObatResponseListModel GetObat()
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"obat", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<ObatResponseListModel>(request);
            return response.Data;
        }

        public ObatResponseModel GetObatByKode(string kode)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"obat/kode/{kode}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<ObatResponseModel>(request);
            return response.Data;
        }

        public ObatResponseModel GetObatByNama(string nama)
        {
            var token = GetToken();
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"obat/nama/{nama}", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.response.token);
            var response = client.Execute<ObatResponseModel>(request);
            return response.Data;
        }
    }
}