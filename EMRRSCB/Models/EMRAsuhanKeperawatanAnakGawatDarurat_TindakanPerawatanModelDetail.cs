﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRAsuhanKeperawatanAnakGawatDarurat_TindakanPerawatanModelDetail
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TindakanPerawatan { get; set; }
        public string Evaluasi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Username { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }

    }
}