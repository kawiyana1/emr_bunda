﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMROrientasiPasienBaruViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalOrientasi { get; set; }
        public string PeraturanRS { get; set; }
        public string HakdanKewajiban { get; set; }
        public string PerawatdanDokter { get; set; }
        public string JamVisite { get; set; }
        public string WaktuBerkunjung { get; set; }
        public string LayananObat { get; set; }
        public string JadwalPemberian { get; set; }
        public string Administrasi { get; set; }
        public string PenjelasanPenggunaan { get; set; }
        public string Fasilitas { get; set; }
        public string KamarMandi { get; set; }
        public string KipasAngin { get; set; }
        public string Wastafel { get; set; }
        public string LemariPasien { get; set; }
        public string AC { get; set; }
        public string Televisi { get; set; }
        public string Telepon { get; set; }
        public string TempatSampah { get; set; }
        public string KotakSaran { get; set; }
        public string TataTertib { get; set; }
        public string LayananPengaduan { get; set; }
        public string KeadaanDarurat { get; set; }
        public string LaranganMerokok { get; set; }
        public string PemberiOrientasi { get; set; }
        public string PemberiOrientasiNama { get; set; }
        public string TTDPemberiOrientasi { get; set; }
        public bool VerifikasiPemberiOrientasi { get; set; }
        public string PenerimaOrientasi { get; set; }
        public string TTDPenerimaOrientasi { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}