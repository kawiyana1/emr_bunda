﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRekamanAsuhanKeperawatanViewModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DiagnosaKeperawatan { get; set; }
        public string TindakanKeperawatan { get; set; }
        public string Evaluasi { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }

        public int MODEVIEW { get; set; }
    }
}