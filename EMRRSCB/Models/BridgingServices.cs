﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class BridgingServices
    {
        protected string BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        protected string UsernameToken = ConfigurationManager.AppSettings["UsernameToken"];
        protected string PasswordToken = ConfigurationManager.AppSettings["PasswordToken"];

        public TokenResponseModel GetToken()
        {
            var client = new RestClient(BaseUrl); 
            var request = new RestRequest($"getToken", Method.GET);
            request.AddHeader("x-username", UsernameToken);
            request.AddHeader("x-password", PasswordToken);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute<TokenResponseModel>(request);
            return response.Data;
        }

        #region META
        public class MetaData
        {
            public string code { get; set; }
            public string message { get; set; }
        }
        #endregion

        #region T O K E N
        public class TokenResponseModel
        {
            public TokenResponse response { get; set; }
            public MetaData metadata { get; set; }
        }

        public class TokenResponse
        {
            public string token { get; set; }
        }
        #endregion

        #region P A S I E N
        public class PasienResponseListModel
        {
            public List<PasienResponse> response { get; set; }
            public MetaData metadata { get; set; }
        }
        public class PasienResponseModel
        {
            public PasienResponse response { get; set; }
            public MetaData metadata { get; set; }
        }

        public class PasienResponse
        {
            public string NRM { get; set; }
            public string NamaPasien { get; set; }
            public string NoIdentitas { get; set; }
            public string JenisKelamin { get; set; }
            public string TempatLahir { get; set; }
            public DateTime TglLahir { get; set; }
            public string Pekerjaan { get; set; }
            public string Alamat { get; set; }
            public string Phone { get; set; }
            public string JenisKerjasama { get; set; }
            public string CustomerKerjasama { get; set; }
            public string NoKartu { get; set; }
            public string PasienKTP { get; set; }
            public string Agama { get; set; }
            public bool PenanggungIsPasien { get; set; }
            public string PenanggungNRM { get; set; }
            public string PenanggungNama { get; set; }
            public string PenanggungAlamat { get; set; }
            public string PenanggungPhone { get; set; }
            public string PenanggungHubungan { get; set; }
            public string Aktif { get; set; }
        }

        #endregion

        #region R E G I S T E R
        public class RegisterResponseListModel
        {
            public List<RegisterResponse> response { get; set; }
            public MetaData metadata { get; set; }
        }

        public class RegisterResponseModel
        {
            public RegisterResponse response { get; set; }
            public MetaData metadata { get; set; }
        }

        public class RegisterResponse
        {
            public string NoReg { get; set; }
            public string PasienBaru { get; set; }
            public string JenisKerjasama { get; set; }
            public string CustomerKerjasama { get; set; }
            public string NoKartu { get; set; }
            public string NRM { get; set; }
            public string TglReg { get; set; }
            public string JamReg { get; set; }
            public Section Section { get; set; }
            public Dokter Dokter { get; set; }
            public string PasienKTP { get; set; }
            public string PenanggungIsPasien { get; set; }
            public string PenanggungNRM { get; set; }
            public string PenanggungNama { get; set; }
            public string PenanggungAlamat { get; set; }
            public string PenanggungHubungan { get; set; }
            public string PenanggungTelp { get; set; }
            public string UmurThn { get; set; }
            public string UmurBln { get; set; }
            public string UmurHr { get; set; }
            public string Batal { get; set; }
        }

        public class Section
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class Dokter
        {
            public string dpjp { get; set; }
            public string nama { get; set; }
        }
        #endregion

        #region O B A T
        public class ObatResponseListModel
        {
            public List<ObatResponse> response { get; set; }
            public MetaData metadata { get; set; }
        }

        public class ObatResponse
        {
            public string KodeBarang { get; set; }
            public string NamaBarang { get; set; }
            public string Satuan { get; set; }
        }

        public class ObatResponseModel
        {
            public ObatResponse response { get; set; }
            public MetaData metadata { get; set; }
        }

        #endregion
    }
}