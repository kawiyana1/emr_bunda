﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPemeriksaanLakmusViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Lakmus { get; set; }
        public string Bidan { get; set; }
        public string BidanNama { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string TTDDimintaOleh { get; set; }
        public string TTDLakmus { get; set; }
        public string TTDBidan { get; set; }
        public string TanggalManual { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}