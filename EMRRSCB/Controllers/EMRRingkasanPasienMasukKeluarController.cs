﻿using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRRingkasanPasienMasukKeluarController : Controller
    {
        // GET: EMRRingkasanPasienMasukKeluar
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, int view = 0)
        {
            var model = new EMRRingkasanPasienMasukKeluarViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var dokumen = s.RingkasanPasienMasukKeluar.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRRingkasanPasienMasukKeluarViewModel>(dokumen);

                        model.NoBukti = id;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterDPJP);
                        if (dpjp != null) model.DokterDPJPNama = dpjp.NamaDOkter;

                        #region === Detail Operasi Nama
                        model.Operasi_List = new ListDetail<EMRNamaOperasi_DetailViewModel>();
                        var detail_1 = s.NamaOperasi_Detail.Where(x => x.NoBukti == id).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRNamaOperasi_DetailViewModel>(x);
                            model.Operasi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.TanggalKeluar = DateTime.Today;
                        model.TanggalKeluarJam = DateTime.Now;
                        model.TanggalMasuk = DateTime.Today;
                        model.TanggalMasukJam = DateTime.Now;
                        model.MeninggalTanggal = DateTime.Today;
                        model.MeninggalTanggalJam = DateTime.Now;
                        model.DipindahKeRuanganTanggal = DateTime.Today;
                        model.PindahanDariRuanganTanggal = DateTime.Today;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRRingkasanPasienMasukKeluarViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.RingkasanPasienMasukKeluar.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<RingkasanPasienMasukKeluar>(item);
                        s.RingkasanPasienMasukKeluar.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Ringkasan Pasien Masuk Keluar";
                    }
                    else
                    {
                        model = IConverter.Cast<RingkasanPasienMasukKeluar>(item);
                        s.RingkasanPasienMasukKeluar.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Ringkasan Pasien Masuk Keluar";
                    }
                    #region === Detail Operasi Nama
                    if (item.Operasi_List == null) item.Operasi_List = new ListDetail<EMRNamaOperasi_DetailViewModel>();
                    item.Operasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Operasi_List)
                    {
                        x.Model.NoBukti = item.NoBukti;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Operasi_List;
                    var real_list = s.NamaOperasi_Detail.Where(x => x.NoBukti == item.NoBukti).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.NamaOperasi_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.NamaOperasi_Detail.Add(IConverter.Cast<NamaOperasi_Detail>(x.Model));
                        }
                        else
                        {
                            _m.NoBukti = x.Model.NoBukti;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.NamaOperasi = x.Model.NamaOperasi;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}