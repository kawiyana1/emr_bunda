﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRBeritaAcaraSerahTerimaViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Daerah { get; set; }
        public string Resort { get; set; }
        public string Nomor { get; set; }
        public string HariIni { get; set; }
        public string Tanggalini { get; set; }
        public string JamIni { get; set; }
        public string Berupa { get; set; }
        public string YangMenyerahkan { get; set; }
        public string YangMenerima { get; set; }
        public string YangMenerimaNama { get; set; }
        public string Kepolisian { get; set; }
        public string PihakKeluarga { get; set; }
        public string RSUBUNDA { get; set; }
        public string RSUBUNDANama { get; set; }
        public string TTDYangMenyerahkan { get; set; }
        public string TTDYangMenerima { get; set; }
        public string TTDKepolisian { get; set; }
        public string TTDPihakKeluarga { get; set; }
        public string TTDRSUBUNDA { get; set; }

        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }

        public int MODEVIEW { get; set; }
    }
}