﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRSuratPernyataanPendapatLainViewModel
    {
        public string NoBukti { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string NamaBarang { get; set; }
        public string KondisiBarang { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string YangMenerima { get; set; }
        public string YangMenerimaNama { get; set; }
        public string Satpam { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Saksi { get; set; }
        public string TTDYangMenerima { get; set; }
        public string TTDSatpam { get; set; }
        public string TTDPerawat { get; set; }
        public string TTDSaksi { get; set; }
        public string Atas { get; set; }

        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }

        public int MODEVIEW { get; set; }
    }
}