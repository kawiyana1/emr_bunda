﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRFormRekonsiliasiObat_SaatKeluarRumahSakitDetailModel
    {
        public string NoBukti { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NamaObat { get; set; }
        public string AturanPakaiDanRute { get; set; }
        public string TindakLanjut { get; set; }
        public string PerubahanAturanPakai { get; set; }
        public string Username { get; set; }
    }
}