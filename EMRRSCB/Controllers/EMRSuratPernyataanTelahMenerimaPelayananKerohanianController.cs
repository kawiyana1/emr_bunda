﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSuratPernyataanTelahMenerimaPelayananKerohanianController : Controller
    {
        // GET: SuratPernyataanTelahMenerimaPelayananKerohanian
        
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {

            var model = new EMRSuratPernyataanTelahMenerimaPelayananKerohanianViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;

                    var identitas = sim.VW_DataPasienReg.FirstOrDefault(x => x.NRM == nrm);
                    var soap = emr.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = emr.SuratPernyataanTelahMenerimaPelayananKerohanian.FirstOrDefault(x => x.NoBukti == id);

                    if(dokumen != null)
                    {
                        model = IConverter.Cast<EMRSuratPernyataanTelahMenerimaPelayananKerohanianViewModel>(dokumen);
                        model.NoBukti = id;
                        model.NRM = identitas.NRM;
                        model.NamaParagraf = identitas.NamaPasien;
                        model.MODEVIEW = view;

                    }
                    else
                    {
                        model.NoBukti = id;
                        model.NRM = identitas.NRM;

                        model.NamaParagraf = identitas.NamaPasien;

                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if(Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]

        public string CreatePost()
        {
            try
            {
                var item = new EMRSuratPernyataanTelahMenerimaPelayananKerohanianViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var emr = new EMREntities())
                {
                    var model = emr.SuratPernyataanTelahMenerimaPelayananKerohanian.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if(model == null)
                    {
                        var o = IConverter.Cast<SuratPernyataanTelahMenerimaPelayananKerohanian>(item);
                        emr.SuratPernyataanTelahMenerimaPelayananKerohanian.Add(o);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Surat GetUpGoTestnya";
                    }
                    else
                    {
                        model = IConverter.Cast<SuratPernyataanTelahMenerimaPelayananKerohanian>(item);
                        emr.SuratPernyataanTelahMenerimaPelayananKerohanian.AddOrUpdate(model);

                        var header = emr.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Update Surat GetUpGoTestnya";
                    }
                    emr.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}