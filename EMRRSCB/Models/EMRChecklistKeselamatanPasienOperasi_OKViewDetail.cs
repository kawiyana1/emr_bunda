﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRChecklistKeselamatanPasienOperasi_OKViewDetail
    {
        public string NoBukti { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignIn { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignIn { get; set; }
        public string IdentitasPasien { get; set; }
        public string RencananTindakan { get; set; }
        public string PersetujuanTindakan { get; set; }
        public string AreaYangDioperasi { get; set; }
        public string MesinAnestesi { get; set; }
        public string PasienMemakaiPulse { get; set; }
        public string RiwayatAlergi { get; set; }
        public string GangguanPernafasan { get; set; }
        public string RisikoPerdarahan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_TimeOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_TimeOut { get; set; }
        public string PastikanTimOperasi { get; set; }
        public string BacaUlang { get; set; }
        public string ApakahProfilaksis { get; set; }
        public string ApakahAdaRisiko { get; set; }
        public string BerapaLamaPerkiraan { get; set; }
        public string SudahAntisipasi { get; set; }
        public string HalKhusus { get; set; }
        public string KesterilanPeralatan { get; set; }
        public string MasalahPeralatan { get; set; }
        public string HasilRadiologi { get; set; }
        public string PlatSudahTerpasang { get; set; }
        public string SelangSuctionBerfungsi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignOut { get; set; }
        public string NamaTindakan { get; set; }
        public string KelengkapanAlat { get; set; }
        public string JumlahKassa { get; set; }
        public string JumlahJarum { get; set; }
        public string PelabelanSpesimen { get; set; }
        public string PelabelanPlasenta { get; set; }
        public string AdaMasalahAlat { get; set; }
        public string CatatanKhusus { get; set; }
        public string DokterAnestesi { get; set; }
        public string DokterAnestesiNama { get; set; }
        public string PerawatAnestesi { get; set; }
        public string PerawatAnestesiNama { get; set; }
        public string PerawatSirkuler { get; set; }
        public string PerawatSirkulerNama { get; set; }
        public string OnLoop { get; set; }
        public string Operator { get; set; }
        public string OperatorNama { get; set; }
        public string Asisten { get; set; }
        public string AsistenNama { get; set; }
        public string Instrumen { get; set; }
        public string InstrumenNama { get; set; }
        public string DokterAnak { get; set; }
        public string DokterAnakNama { get; set; }
        public string Username { get; set; }
        public bool Verifikasi_DokterAnestesi { get; set; }
        public bool Verifikasi_PerawatAnestesi { get; set; }
        public bool Verifikasi_PerawatSirkuler { get; set; }  
        public bool Verifikasi_OnLoop { get; set; }
        public bool Verifikasi_Operator { get; set; }
        public bool Verifikasi_Asisten { get; set; }
        public bool Verifikasi_Instrumen { get; set; }
        public bool Verifikasi_DokterAnak { get; set; }
        public bool Verifikasi_DokterAnestesi_SignIn { get; set; }
        public bool Verifikasi_PerawatSirkuler_SignIn { get; set; }
        public bool Verifikasi_PerawatAnestesi_SignIn { get; set; }
        public bool Verifikasi_DokterAnestesi_TimeOut { get; set; }
        public bool Verifikasi_PerawatAnestesi_TimeOut { get; set; }
        public string PerawatAnestesi_SignIn { get; set; }
        public string PerawatAnestesi_SignInNama { get; set; }
        public string PerawatSirkuler_SignIn { get; set; }
        public string PerawatSirkuler_SignInNama { get; set; }
        public string DokterAnestesi_SignIn { get; set; }
        public string DokterAnestesi_SignInNama { get; set; }
        public string DokterAnestesi_TimeOut { get; set; }
        public string DokterAnestesi_TimeOutNama { get; set; }
        public string PerawatAnestesi_TimeOut { get; set; }
        public string PerawatAnestesi_TimeOutNama { get; set; }


        public string NamaPasien { get; set; }
        public string TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListOK_ChecklistPasienOperasi> ListTemplate { get; set; }
    }

    public class SelectItemListOK_ChecklistPasienOperasi
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}