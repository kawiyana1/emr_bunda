﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace EMRRSCB.Controllers
{
    public class EMRChecklistKeselamatanPasienOperasi_OKController : Controller
    {
        // GET: EMRChecklistKeselamatanPasienOperasi_OK
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRChecklistKeselamatanPasienOperasi_OKViewDetail();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRChecklistKeselamatanPasienOperasi_OKViewDetail>(dokumen);

                        model.NoBukti = id;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.NRM = (identitas.NRM == null ? "" : identitas.NRM);
                        model.JenisKelamin = (identitas.JenisKelamin == "L" ? "Laki-Laki" : "Perempuan");
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                        model.Username = User.Identity.GetUserName();

                        var dokteranestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi);
                        if (dokteranestesi != null) model.DokterAnestesiNama = dokteranestesi.NamaDOkter;

                        var dokteranak = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnak);
                        if (dokteranak != null) model.DokterAnakNama = dokteranak.NamaDOkter;

                        var perawatanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PerawatAnestesi);
                        if (perawatanestesi != null) model.PerawatAnestesiNama = perawatanestesi.NamaDOkter;

                        var perawatsirkulasi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PerawatSirkuler);
                        if (perawatsirkulasi != null) model.PerawatSirkulerNama = perawatsirkulasi.NamaDOkter;

                        var oprtor = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Operator);
                        if (oprtor != null) model.OperatorNama = oprtor.NamaDOkter;

                        var instrumen = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Instrumen);
                        if (instrumen != null) model.InstrumenNama = instrumen.NamaDOkter;

                        var asisten = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Asisten);
                        if (asisten != null) model.AsistenNama = asisten.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.NRM = (nrm == null ? "" : nrm);
                        model.JenisKelamin = (identitas.JenisKelamin == "L" ? "Laki-Laki" : "Perempuan");
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                        model.Username = User.Identity.GetUserName();
                    }

                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == model.NoBukti);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListOK_ChecklistPasienOperasi>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListOK_ChecklistPasienOperasi()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string id, string copy, string nrm)
        {
            var model = new EMRChecklistKeselamatanPasienOperasi_OKViewDetail();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var dokumen = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoBukti == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRChecklistKeselamatanPasienOperasi_OKViewDetail>(dokumen);

                        model.Tanggal = DateTime.Today;
                        model.NoBukti = id;
                        model.Jam = DateTime.Now;
                        model.NamaPasien = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.NRM = (identitas.NRM == null ? "" : identitas.NRM);
                        model.JenisKelamin = (identitas.JenisKelamin == "L" ? "Laki-Laki" : "Perempuan");
                        model.TglLahir = (identitas.TglLahir.Value.ToString("yyyy/MM/dd") == null ? "" : identitas.TglLahir.Value.ToString("yyyy/MM/dd"));
                        model.Username = User.Identity.GetUserName();

                        var dokteranestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnestesi);
                        if (dokteranestesi != null) model.DokterAnestesiNama = dokteranestesi.NamaDOkter;

                        var dokteranak = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterAnak);
                        if (dokteranak != null) model.DokterAnakNama = dokteranak.NamaDOkter;

                        var perawatanestesi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PerawatAnestesi);
                        if (perawatanestesi != null) model.PerawatAnestesiNama = perawatanestesi.NamaDOkter;

                        var perawatsirkulasi = sim.mDokter.FirstOrDefault(e => e.DokterID == model.PerawatSirkuler);
                        if (perawatsirkulasi != null) model.PerawatSirkulerNama = perawatsirkulasi.NamaDOkter;

                        var oprtor = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Operator);
                        if (oprtor != null) model.OperatorNama = oprtor.NamaDOkter;

                        var instrumen = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Instrumen);
                        if (instrumen != null) model.InstrumenNama = instrumen.NamaDOkter;

                        var asisten = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Asisten);
                        if (asisten != null) model.AsistenNama = asisten.NamaDOkter;

                    }
                    else
                    {
                        model.NoBukti = id;
                    }
                    model.MODEVIEW = 0;
                    model.Verifikasi_Asisten = false;
                    model.Verifikasi_DokterAnak = false;
                    model.Verifikasi_DokterAnestesi = false;
                    model.Verifikasi_Instrumen = false;
                    model.Verifikasi_OnLoop = false;
                    model.Verifikasi_Operator = false;
                    model.Verifikasi_PerawatAnestesi = false;
                    model.Verifikasi_PerawatSirkuler = false;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoBukti == id);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trDokumenTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListOK_ChecklistPasienOperasi>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListOK_ChecklistPasienOperasi()
                        {
                            Value = list.NoBukti,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRChecklistKeselamatanPasienOperasi_OKViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<ChecklistKeselamatanPasienOperasi_OK>(item);
                        s.ChecklistKeselamatanPasienOperasi_OK.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Woi Create Checklist Keselamatan Pasien Operasi";
                    }
                    else
                    {
                        model = IConverter.Cast<ChecklistKeselamatanPasienOperasi_OK>(item);
                        s.ChecklistKeselamatanPasienOperasi_OK.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Woi Update Checklist Keselamatan Pasien Operasi";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}