﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRLampiranDaftarPerawatanHeaderViewModel
    {
        public ListDetail<EMRLampiranDaftarPerawatanDetailModel> Lampiran_List { get; set; }
        public string NoBukti { get; set; }
        public string Lampiran { get; set; }
        public string PenanggungJawab { get; set; }
        public string AlamatTempatIbadah { get; set; }
        public string PemimpinAcara { get; set; }
        public string PelayananDoa { get; set; }


        // disable all form
        public int MODEVIEW { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemLisLampiranKerohania> ListTemplate { get; set; } 

        public string PilihICD { get; set; }
        public string PilihICDNama { get; set; }
    }

    public class SelectItemLisLampiranKerohania
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}