﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRPersetujuanPindahKelasViewModel
    {
        public string NoBukti { get; set; }
        public string PenanggungJawab { get; set; }
        public string PenanggungJawab_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglMRS { get; set; }
        public string Kelas { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglMulai { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamMulai { get; set; }
        public string KelasPindah { get; set; }
        public string Menyatakan { get; set; }
        public string Saksi { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }



        public string TglLahirYear { get; set; }
        public string TglLahirMounth { get; set; }
        public string TglLahirDay { get; set; }
        public string Saya { get; set; }
        public string SayaUmur { get; set; }
        public string SayaJK { get; set; }
        public string SayaAlamat { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Noreg { get; set; }
        public string NRM { get; set; }
        public string TglLahir { get; set; }
        public int MODEVIEW { get; set; }
      
    }
}