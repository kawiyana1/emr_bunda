﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSCB.Models
{
    public class EMRRingkasanPasienMasukKeluarViewModel
    {
        public ListDetail<EMRNamaOperasi_DetailViewModel> Operasi_List { get; set; }
        public string NoBukti { get; set; }
        public string NamaLengkap { get; set; }
        public string TanggalLahir { get; set; }
        public string Umur { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public string StatusPerkawinan { get; set; }
        public string Agama { get; set; }
        public string Kebangsaan { get; set; }
        public string NoTelp { get; set; }
        public string KartuIdentitas { get; set; }
        public string NoIdentitas { get; set; }
        public string Alamat { get; set; }
        public string NamaPenanggungJawab { get; set; }
        public string HubunganKeluarga { get; set; }
        public string PekerjaanPenanggung { get; set; }
        public string AlamatPenanggung { get; set; }
        public string SebabDirawat { get; set; }
        public string DirawatDiRuangan { get; set; }
        public string Bagian { get; set; }
        public string KelasPerawatan { get; set; }
        public string DirawatYangKe { get; set; }
        public string DikirimOleh { get; set; }
        public string DikirimOlehKet { get; set; }
        public string ProsedurMasukRSMelalui { get; set; }
        public string PesertaBPJS { get; set; }
        public string NoJKNPBI { get; set; }
        public string NoJKNNonPBI { get; set; }
        public string KasusPolisi { get; set; }
        public bool BCG { get; set; }
        public bool DPT { get; set; }
        public bool TPT { get; set; }
        public bool DT { get; set; }
        public bool Polio { get; set; }
        public bool Campak { get; set; }
        public string KBMasuk { get; set; }
        public string KBKeluar { get; set; }
        public string DipindahKeRuangan { get; set; }
        public string DipindahKeRuanganKelas { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DipindahKeRuanganTanggal { get; set; }
        public string PindahanDariRuangan { get; set; }
        public string PindahanDariRuanganKelas { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> PindahanDariRuanganTanggal { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMasuk { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> TanggalMasukJam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKeluar { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> TanggalKeluarJam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> MeninggalTanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MeninggalTanggalJam { get; set; }
        public string LamaDirawat { get; set; }
        public string AlergiTerhadap { get; set; }
        public string CacatBawaan { get; set; }
        public string ResikoJatuh { get; set; }
        public string DiagnosaAkhir { get; set; }
        public string DiagnosaUtama { get; set; }
        public string Sekunder { get; set; }
        public string Komplikasi { get; set; }
        public string SebabKematian { get; set; }
        public string InfeksiNosokomial { get; set; }
        public string KodeICD10 { get; set; }
        public string KeadaanKeluar { get; set; }
        public string KodeICD9 { get; set; }
        public string CaraKeluar { get; set; }
        public string CaraKeluarKet { get; set; }
        public string DokterDPJP { get; set; }
        public string DokterDPJPNama { get; set; }
        public string TTDDokterDPJP { get; set; }
        public bool Verifikasi { get; set; }

        // disable all form
        public int MODEVIEW { get; set; }
    }
}