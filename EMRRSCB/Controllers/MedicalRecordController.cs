﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSCB.Controllers
{
    public class MedicalRecordController : Controller
    {
        [Authorize]
        public ActionResult Index(string noreg, int nomor)
        {
            var model = new RegistrasiViewModel();
            if (noreg == null) return Redirect(Url.Action("Index", "Registrasi"));
            var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var emr = new EMREntities())
            {
                using (var s = new SIM_Entities())
                {
                    var pasien = s.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
                    var cekalergi = emr.EMR_FN_CekRiwayatAlergi().Where(c => c.NRM == pasien.NRM).FirstOrDefault();
                    if (pasien == null) return Redirect(Url.Action("Index", "Registrasi"));
                    model = IConverter.Cast<RegistrasiViewModel>(pasien);
                    var cekstatusbayar = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var dokterdefault = s.mDokter.Where(xx => xx.DokterID == cekstatusbayar.DokterRawatID).FirstOrDefault();
                    if (dokterdefault != null)
                    {
                        model.DokterID = dokterdefault.DokterID;
                        model.Nama_Supplier = dokterdefault.NamaDOkter;
                    }
                    model.StatusBayar = cekstatusbayar.StatusBayar;
                    model.TglLahir = pasien.TglLahir.Value.ToString("dd-MM-yyyy");
                    model.SectionID = sectionid;
                    model.Nomor = nomor;
                    model.PenanggungNama = cekstatusbayar.PenanggungNama;
                    model.Alergi = (cekalergi == null ? "" : cekalergi.Deskripsi);
                }
            }

            // B R I D G I N G
            //var s = new BridgingSIMRS();
            //var pasien = s.GetRegistrasiByNoReg(noreg);
            //if (pasien.metadata.code != "200") return Redirect(Url.Action("Index", "Registrasi"));
            //var p = s.GetPasienByNIK(pasien.response.NRM);
            //model = IConverter.Cast<RegistrasiViewModel>(pasien.response);
            //if (p.response != null)
            //{
            //    model.NamaPasien = p.response.NamaPasien;
            //    model.Alamat = p.response.Alamat;

            //    using (var sEMR = new EMREntities())
            //    {
            //        var ps = sEMR.mPasienBridging.FirstOrDefault(x => x.NRM == p.response.NRM);
            //        if (ps == null)
            //        {
            //            sEMR.mPasienBridging.Add(new mPasienBridging()
            //            {
            //                Agama = p.response.Agama,
            //                Aktif = p.response.Aktif,
            //                Alamat = p.response.Alamat,
            //                PenanggungAlamat = p.response.PenanggungAlamat,
            //                CustomerKerjasama = p.response.CustomerKerjasama,
            //                JenisKelamin = p.response.JenisKelamin,
            //                JenisKerjasama = p.response.JenisKerjasama,
            //                NamaPasien = p.response.NamaPasien,
            //                NoIdentitas = p.response.NoIdentitas,
            //                NoKartu = p.response.NoKartu,
            //                NRM = p.response.NRM,
            //                PasienKTP = p.response.PasienKTP,
            //                Pekerjaan = p.response.Pekerjaan,
            //                PenanggungHubungan = p.response.PenanggungHubungan,
            //                PenanggungIsPasien = p.response.PenanggungIsPasien,
            //                PenanggungNama = p.response.PenanggungNama,
            //                PenanggungNRM = p.response.PenanggungNRM,
            //                PenanggungPhone = p.response.PenanggungPhone,
            //                Phone = p.response.Phone,
            //                TempatLahir = p.response.TempatLahir,
            //                TglLahir = p.response.TglLahir
            //            });
            //            sEMR.SaveChanges();
            //        }
            //    }
            //}
            return View(model);
        }

        public ActionResult ChangePasien(string noreg)
        {
            return Redirect(Url.Action("Index", "MedicalRecord") + "?noreg=" + noreg);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post(string noreg, string nrm, string dokumen, string nomor)
        {
            try
            {
                ResultSS result;
                object dt;
                using (var s = new EMREntities())
                {
                    var sectionId = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    if (sectionId == "SEC004")
                    {
                        int nomorint = int.Parse(nomor);
                        var checkOK = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionId && x.DokumenID == dokumen && x.Batal == false && x.PersetujuanTindakan == 0 && x.Nomor == nomorint);
                        if (checkOK != null) return JsonHelper.JsonMsgError("Formulir sudah dibuat.");
                    }
                    else
                    {
                        var check = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionId && x.DokumenID == dokumen && x.Batal == false && x.PersetujuanTindakan == 0);
                        if (check != null) return JsonHelper.JsonMsgError("Formulir sudah dibuat.");
                    }

                    var nobukti = s.AutoNumber_EMR().FirstOrDefault();
                    var m = new trDokumenAssesmenPasien();
                    m.NoBukti = nobukti;
                    m.NoReg = noreg;
                    m.NRM = nrm;
                    m.SectionID = sectionId;
                    m.TanggalCreate = DateTime.Now;
                    m.TanggalUpdate = DateTime.Now;
                    m.TanggalCanceled = DateTime.Now;
                    m.DokumenID = dokumen;
                    if (dokumen == "PemberianInformasi")
                    {
                        m.PersetujuanTindakan = 1;
                    }
                    else
                    {
                        m.PersetujuanTindakan = 0;
                    }
                    m.CretaedBy = User.Identity.GetUserName();
                    m.UpdatedBy = User.Identity.GetUserName();
                    m.UserIdWeb_Created = User.Identity.GetUserId();
                    m.UserIdWeb_Updated = User.Identity.GetUserId();
                    m.Simpan = false;
                    m.Batal = false;
                    // B R I D G I N G
                    //var sRS = new BridgingSIMRS();
                    //var reg = sRS.GetRegistrasiByNoReg(noreg);
                    //if (reg.response != null)
                    //{
                    //    m.Umur = reg.response.UmurThn;
                    //}
                    var namaDoktumen = "";
                    var dok = s.mDokumen.FirstOrDefault(x => x.DokumenID == dokumen);
                    if (dok != null)
                    {
                        namaDoktumen = dok.NamaDokumen;
                    }
                    dt = new
                    {
                        NoBukti = nobukti,
                        DokumenID = dokumen,
                        NamaDokumen = namaDoktumen,
                        NRM = nrm,
                        NoReg = noreg
                    };

                    s.trDokumenAssesmenPasien.Add(m);
                    result = new ResultSS(s.SaveChanges(), m);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" Pembuatan dokumen baru {dokumen}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return new JavaScriptSerializer().Serialize(new ResultStatus()
                {
                    Data = dt,
                    IsSuccess = result.IsSuccess,
                    Message = "Formulir berhasil di buat.",
                    Count = 0,
                    Status = "success"
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListAssesmen(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMREntities())
                {
                    pageSize = 10;
                    var noreg = filter[0];
                    int nomor = int.Parse(filter[6]);
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var sectioniddokumen = Request.Cookies["EMRTipeNamaPelayanan"].Value;
                    //var dokumenid = s.mDokumen.Where(x => x.DokumenID == doc).FirstOrDefault();
                    IQueryable<vw_DokumenAssesmenPasien> p = s.vw_DokumenAssesmenPasien.Where(x => x.NoReg == noreg && x.SectionID == sectionid && x.DokumenID != "CPPT" && x.Batal == false);
                    if (sectionid == "SEC004")
                    {
                        p.Where(z => z.Nomor == nomor);
                    }
                    p = p.Where($"NamaDokumen.Contains(@0)", filter[7]);
                    var totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<DokumenAssesmenPasienViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                        m.TanggalCreate_View = x.TanggalCreate.Value.ToString("dd-MM-yyyy HH:mm");
                        m.TanggalUpdate_View = x.TanggalUpdate.Value.ToString("dd-MM-yyyy HH:mm");
                        datas.Add(m);
                    }
                    result.Data = datas;

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListAssesmenCPPT(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var e = new SIM_Entities())
                {
                    using (var s = new EMREntities())
                    {
                        var noreg = filter[0];
                        var nrm = filter[1];
                        var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        IQueryable<vw_DokumenAssesmenPasienCPPT> p = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NRM == nrm && x.Batal == false);

                        var totalcount = p.Count();
                        var models = p.OrderByDescending(wkt => wkt.Jam).OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<DokumenAssesmenPasienViewModel>();
                        var userId = User.Identity.GetUserId();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                            if (x.NamaDOkter == null)
                            {
                                m.DokterID = "";
                                m.NamaDOkter = "";
                            }
                            else
                            {
                                var kodedokter = e.mDokter.FirstOrDefault(kd => kd.NamaDOkter == x.NamaDOkter);
                                m.DokterID = kodedokter.DokterID;
                                m.NamaDOkter = kodedokter.NamaDOkter;

                                var dpjp = s.CPPT.FirstOrDefault(z => z.NoBukti == x.NoBukti).DPJP;
                                if (dpjp == null || dpjp == "null")
                                {
                                    m.DPJP = "";
                                    m.DPJPNama = "";
                                }
                                else
                                {
                                    var getdpjp = e.mDokter.FirstOrDefault(d => d.DokterID == dpjp);
                                    m.DPJP = getdpjp.DokterID;
                                    m.DPJPNama = getdpjp.NamaDOkter;
                                }

                                var konformasi = s.CPPT.FirstOrDefault(z => z.NoBukti == x.NoBukti).PetugasKonfirmasi;
                                if (konformasi == "null" || konformasi == null)
                                {
                                    m.Menyerahkan = "";
                                    m.MenyerahkanNama = "";
                                }
                                else
                                {
                                    var getkonformasi = e.mDokter.FirstOrDefault(d => d.DokterID == konformasi);
                                    m.PetugasKonfirmasi = getkonformasi.DokterID;
                                    m.PetugasKonfirmasiNama = getkonformasi.NamaDOkter;
                                }
                                m.VerifikasiPetugasCPPT = s.CPPT.FirstOrDefault(c => c.NoBukti == x.NoBukti).VerifikasiPetugasCPPT ?? false; 
                                m.VerifikasiPetugasKonfirmasiCPPT = s.CPPT.FirstOrDefault(c => c.NoBukti == x.NoBukti).VerifikasiPetugasKonfirmasiCPPT ?? false; 
                            }
                            m.Jam_View = x.TanggalUpdate.Value.ToString("dd-MM-yyyy") + " " + x.Jam.Value.ToString("HH:mm");
                            m.CPPT_OGambar = x.CPPT_OGambar == null ? "/Theme/images/orto4.jpg" : "data:image/jpeg;base64," + Convert.ToBase64String(x.CPPT_OGambar);
                            m.Edit = "0";
                            if (x.UserIdWeb_Created == userId && x.NoReg == noreg && x.SectionID == sectionid)
                            {
                                m.Edit = "1";
                            }

                            datas.Add(m);
                        }


                        result.Data = datas;

                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListAssesmenTopHistoryKunjungan(string nrm, string filter)
        {
            try
            {

                var header = new List<Object>();
                using (var s = new EMREntities())
                {
                    var userId = User.Identity.GetUserId();
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    IQueryable<EMR_FN_HistoryKunjungan_Result> proses = s.EMR_FN_HistoryKunjungan().Where(x => x.NRM == nrm); 
                    var models_header = proses.ToList();
                    var groupbydokter = models_header.GroupBy(x => x.SectionID).Select(y => new EMR_FN_HistoryKunjungan_Result
                    {
                        NamaDokter = y.Max(x => x.NamaDokter),
                        SectionName = y.Max(x => x.SectionName),
                        Tanggal = y.Max(x => x.Tanggal),
                        NRM = y.Max(x => x.NRM)
                    });

                    var Fullname = (System.Security.Claims.ClaimsIdentity)User.Identity;
                    var NamaUser = Fullname.FindFirstValue("FullName");

                    foreach (var e in groupbydokter)
                    {
                        IQueryable<EMR_FN_HistoryKunjungan_Result> p = s.EMR_FN_HistoryKunjungan().Where(z => z.NamaDokter == e.NamaDokter && z.NRM == e.NRM && z.SectionName == e.SectionName);
                        var models_detail = p.OrderBy("Tanggal DESC").ToList();
                        var details = new List<Object>();

                        var cppt_count = 0;
                        foreach (var x in models_detail)
                        {
                            if (x.DokumenID == "CPPT")
                            {
                                if (cppt_count == 0)
                                {
                                    var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                                    m.DateDisplay = x.Tanggal.Value.ToString("dd");
                                    m.TanggalCreate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                    m.TanggalUpdate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                    details.Add(m);
                                }
                                cppt_count += 1;
                            }
                            else
                            {
                                var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                                m.DateDisplay = x.Tanggal.Value.ToString("dd");
                                m.TanggalCreate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                m.TanggalUpdate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                details.Add(m);
                            }

                        }

                        var h = new
                        {
                            Tanggal = e.Tanggal.Value.ToString("dd-MM-yyyy HH:mm"),
                            NamaDokter = e.NamaDokter,
                            SectionName = e.SectionName,
                            detail = details
                        };
                        header.Add(h);

                    }
                }
                return new JavaScriptSerializer().Serialize(new ResultStatus()
                {
                    Data = header,
                    IsSuccess = true
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListAssesmenTopHistoryFormulir(string nrm, string filter)
        {
            try
            {

                var header = new List<Object>();
                using (var s = new EMREntities())
                {
                    var userId = User.Identity.GetUserId();
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    IQueryable<EMR_FN_HistoryKunjungan_Result> proses = s.EMR_FN_HistoryKunjungan().Where(x => x.NRM == nrm);
                    var models_header = proses.ToList();
                    var groupbydokter = models_header.GroupBy(x => new { x.SectionID, x.NamaDokter, x.JenisDokumen }).Select(y => new EMR_FN_HistoryKunjungan_Result
                    {
                        NamaDokter = y.Max(x => x.NamaDokter),
                        SectionName = y.Max(x => x.SectionName),
                        Tanggal = y.Max(x => x.Tanggal),
                        JenisDokumen = y.Max(x => x.JenisDokumen),
                        NRM = y.Max(x => x.NRM)
                    });

                    var Fullname = (System.Security.Claims.ClaimsIdentity)User.Identity;
                    var NamaUser = Fullname.FindFirstValue("FullName");

                    foreach (var e in groupbydokter)
                    {
                        IQueryable<EMR_FN_HistoryKunjungan_Result> p = s.EMR_FN_HistoryKunjungan().Where(z => z.NamaDokter == e.NamaDokter && z.NRM == e.NRM && z.SectionName == e.SectionName && z.JenisDokumen == e.JenisDokumen);
                        var models_detail = p.OrderBy("Tanggal DESC").ToList();
                        var details = new List<Object>();

                        var cppt_count = 0;
                        foreach (var x in models_detail)
                        {
                            if (x.DokumenID == "CPPT")
                            {
                                if (cppt_count == 0)
                                {
                                    var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                                    m.DateDisplay = x.Tanggal.Value.ToString("dd");
                                    m.TanggalCreate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                    m.TanggalUpdate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                    details.Add(m);
                                }
                                cppt_count += 1;
                            }
                            else
                            {
                                var m = IConverter.Cast<DokumenAssesmenPasienViewModel>(x);
                                m.DateDisplay = x.Tanggal.Value.ToString("dd");
                                m.TanggalCreate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                m.TanggalUpdate_View = x.Tanggal.Value.ToString("dd-MM-yyyy HH:mm");
                                details.Add(m);
                            }

                        }

                        var h = new
                        {
                            Tanggal = e.Tanggal.Value.ToString("dd-MM-yyyy HH:mm"),
                            NamaDokter = e.NamaDokter,
                            SectionName = e.SectionName,
                            JenisDokumen = e.JenisDokumen,
                            detail = details
                        };
                        header.Add(h);

                    }
                }
                return new JavaScriptSerializer().Serialize(new ResultStatus()
                {
                    Data = header,
                    IsSuccess = true
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var m = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(s.SaveChanges());
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" Batal {m.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string target_form, string target_id)
        {
            try
            {
                if (target_form == "PengkajianMedisIGD")
                {
                    target_form = "PengkajianMedisGawatDarurat";
                }
                else if (target_form == "FormRekonsiliasiObatDipakaiDirumah_Detail")
                {
                    target_form = "FormRekonsiliasiHeader";
                }

                string ServerPath = "~/Reports/EMR/";
                string reportname = $"Rpt_{target_form}";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname + ".rpt"));

                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"{reportname};1"].SetDataSource(ds[i].Tables[0]);

                    if (target_form == "PengkajianMedisGawatDarurat")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_RencanaKerja", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_RencanaKerja;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_Observasi", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_Observasi;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "AsuhanKeperawatanDewasaIGD")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_TindakanPerawatan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_ObservasiCairan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_ObservasiCairan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_EvaluasiPerkembangan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_EvaluasiPerkembangan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianMedisKebidananDanKandunganGawatDarurat")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_RiwayatKehamilan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_RiwayatKehamilan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_RencanaKerjaMedis", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_RencanaKerjaMedis;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_Observasi", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_Observasi;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "AsuhanKeperawatanAnakGawatDarurat")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_TindakanPerawatan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_ObservasiCairan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_ObservasiCairan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_EvaluasiPerkembangan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_EvaluasiPerkembangan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "AsuhanKeperawatanDewasaIGD")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_AsuhanKeperawatanDewasaIGD_EvaluasiPerkembangan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_AsuhanKeperawatanDewasaIGD_ObservasiCairan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_AsuhanKeperawatanDewasaIGD_ObservasiCairan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PoliklinikSaraf")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        //i++;
                        //cmd.Add(new SqlCommand($"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan", conn));
                        //cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        //cmd[i].CommandType = CommandType.StoredProcedure;
                        //da.Add(new SqlDataAdapter(cmd[i]));
                        //ds.Add(new DataSet());
                        //da[i].Fill(ds[i]);
                        //rd.Database.Tables[$"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PoliklinikUmum")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        //i++;
                        //cmd.Add(new SqlCommand($"AsuhanKeperawatanDewasaIGD_TindakanPerawatan", conn));
                        //cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        //cmd[i].CommandType = CommandType.StoredProcedure;
                        //da.Add(new SqlDataAdapter(cmd[i]));
                        //ds.Add(new DataSet());
                        //da[i].Fill(ds[i]);
                        //rd.Database.Tables[$"AsuhanKeperawatanDewasaIGD_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PoliklinikPenyakitKulitDanKelamin")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        //i++;
                        //cmd.Add(new SqlCommand($"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan", conn));
                        //cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        //cmd[i].CommandType = CommandType.StoredProcedure;
                        //da.Add(new SqlDataAdapter(cmd[i]));
                        //ds.Add(new DataSet());
                        //da[i].Fill(ds[i]);
                        //rd.Database.Tables[$"{reportname}_AsuhanKeperawatanDewasaIGD_TindakanPerawatan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PoliklinikKandunganDanKebidanan")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RiwayatKehamilan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RiwayatKehamilan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeparawatanIlmuPenyakitDalam")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanTHT")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanIlmuKesehatanAnak")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    }
                    else
                        if (target_form == "PengkajianAsuhanKebidananGawatDarurat")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_PengkajianMedisKebidananDanKandunganGawatDarurat_RiwayatKehamilan;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_PengkajianAsuhanKebidananGawatDarurat_EvaluasiPerkembanganPasien;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_PengkajianAsuhanKebidananGawatDarurat_Observasi", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_PengkajianAsuhanKebidananGawatDarurat_Observasi;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_PengkajianAsuhanKebidananGawatDarurat_TindakanKebidanan;1"].SetDataSource(ds[i].Tables[0]);

                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanBedahTrauma")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanBedahNonTrauma")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanMata")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianAwalMedisKeperawatanGigi")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "CatatanPemindahanPasienAntarRuangan")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_CatatanPemindahanPasienAntarRuangan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_CatatanPemindahanPasienAntarRuangan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        //i++;
                        //cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        //cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        //cmd[i].CommandType = CommandType.StoredProcedure;
                        //da.Add(new SqlDataAdapter(cmd[i]));
                        //ds.Add(new DataSet());
                        //da[i].Fill(ds[i]);
                        //rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "RI_PengkajianAwalMedisIlmuPenyakitSaraf")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RI_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RI_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "RI_PengkajianAwalMedisKeparawatanIlmuPenyakitDalam")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RI_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RI_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "RI_PengkajianAwalMedisKeperawatanBedahNonTrauma")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RI_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RI_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "RI_PengkajianAwalMedisKeperawatanBedahTrauma")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RI_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti(RencanaKerja)", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RI_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "FormulirPencatatanPenerimaanBarangMilikPasien")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormulirPencatatanPenerimaanBarangMilikPasienDetail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormulirPencatatanPenerimaanBarangMilikPasienDetail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "FormulirPencatatanPenerimaanBarangMilikPasien")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_ClinicalPathwaysParu", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_ClinicalPathwaysParu;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "CatatanEdukasiPerencanaanEdukasiRanap")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_CatatanEdukasiRawatJalan", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_CatatanEdukasiRawatJalan;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "FormRekonsiliasiHeader")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormRekonsiliasiObat_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormRekonsiliasiObat_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormRekonsiliasiObat_SaatTransfer", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormRekonsiliasiObat_SaatTransfer;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormRekonsiliasiObat_SaatKeluarRumahSakit", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormRekonsiliasiObat_SaatKeluarRumahSakit;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormRekonsiliasiObatDipakaiDirumah_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormRekonsiliasiObatDipakaiDirumah_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "IlmuPenyakitKesehatanAnakRI")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RI_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RI_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PoliklinikSkinCare")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_PengkajianAwalSkinCare", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_PengkajianAwalSkinCare;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "CatatanAnestesi")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaSedasi_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaSedasi_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_AsesmentPraIndukasi_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_AsesmentPraIndukasi_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_MonitoringIntraAnestesi_Obat", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_MonitoringIntraAnestesi_Obat;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_MonitoringIntraAnestesi_VitalSign", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_MonitoringIntraAnestesi_VitalSign;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_MonitoringPascaAnestesi_VitalSign", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_MonitoringPascaAnestesi_VitalSign;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "PengkajianMedisNeonatus")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    }
                    else
                        if (target_form == "PenilaianAwalMedisKeperawatanGeriatri")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_RencanaKerja_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_RencanaKerja_Detail;1"].SetDataSource(ds[i].Tables[0]);

                        i++;
                        cmd.Add(new SqlCommand($"Rpt_Keperawatan_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_Keperawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                    else
                        if (target_form == "FormPencatatanStatusFisiologis")
                    {
                        i++;
                        cmd.Add(new SqlCommand($"Rpt_FormPencatatanStatusFisiologis_Detail", conn));
                        cmd[i].Parameters.Add(new SqlParameter("@NoBukti", target_id));
                        cmd[i].CommandType = CommandType.StoredProcedure;
                        da.Add(new SqlDataAdapter(cmd[i]));
                        ds.Add(new DataSet());
                        da[i].Fill(ds[i]);
                        rd.Database.Tables[$"Rpt_FormPencatatanStatusFisiologis_Detail;1"].SetDataSource(ds[i].Tables[0]);
                    }
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        [HttpGet]
        public ActionResult showRiwayat(string noreg)
        {
            var model = new RegistrasiViewModel();
            ViewBag.NoReg = noreg;
            model.NoReg = noreg;
            //model.DariTanggal = DateTime.Today.ToString("MM/dd/yyyy");
            //model.SampaiTanggal = DateTime.Today.ToString("MM/dd/yyyy");
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

    }
}