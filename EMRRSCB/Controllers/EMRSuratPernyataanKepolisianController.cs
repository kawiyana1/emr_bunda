﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSCB.Entities.EMR;
using EMRRSCB.Entities.SIM;
using EMRRSCB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class EMRSuratPernyataanKepolisianController : Controller
    {
        // GET: EMRSuratPernyataanKepolisian
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, string noreg, int view = 0)
        {
            var model = new EMRSuratPernyataanKepolisianViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMREntities())
                {
                    var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
                    var identitas = sim.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                    var soap = s.vw_DokumenAssesmenPasienCPPT.Where(x => x.NoReg == noreg && x.SectionID == sectionid).OrderByDescending(x => x.TanggalUpdate).FirstOrDefault();
                    var dokumen = s.SuratPernyataanKepolisian.FirstOrDefault(x => x.NoBukti == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRSuratPernyataanKepolisianViewModel>(dokumen);

                        model.NoBukti = id;

                        //var perawat = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Perawat);
                        //if (perawat != null) model.PerawatNama = perawat.NamaDOkter;

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoBukti = id;
                        model.Tanggal = DateTime.Today;
                        model.TglLahirYear = identitas.TglLahir.Value.ToString("yyyy");
                        model.TglLahirMounth = identitas.TglLahir.Value.ToString("MM");
                        model.TglLahirDay = identitas.TglLahir.Value.ToString("dd");
                        int a = Convert.ToInt32(Convert.ToDouble(model.TglLahirYear));
                        int b = Convert.ToInt32(Convert.ToDouble(model.TglLahirMounth));
                        int c = Convert.ToInt32(Convert.ToDouble(model.TglLahirDay));
                        DateTime Birth = new DateTime(a, b, c);
                        DateTime Today = DateTime.Now;
                        TimeSpan Span = Today - Birth;
                        DateTime Age = DateTime.MinValue + Span;
                        int Years = Age.Year - 1;
                        model.Nama = (identitas.NamaPasien == null ? "" : identitas.NamaPasien);
                        model.Alamat = identitas.Alamat;
                        model.Pekerjaan = identitas.Pekerjaan;
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRSuratPernyataanKepolisianViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMREntities())
                {
                    var model = s.SuratPernyataanKepolisian.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<SuratPernyataanKepolisian>(item);
                        s.SuratPernyataanKepolisian.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();

                        activity = "Create Surat Pernyataan Kepolisian";
                    }
                    else
                    {
                        model = IConverter.Cast<SuratPernyataanKepolisian>(item);
                        s.SuratPernyataanKepolisian.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();

                        activity = "Update Surat Pernyataan Kepolisian";
                    }
                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}